Klickrr::Application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  #devise_for :users
  devise_for :users, :controllers => { registrations: 'registrations' }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'spa#index'

  get "tracking/:token" => 'spa#tracking', :as => 'tracking_pixel'

  # CHANGE THIS TO POST!!!!!
  get "json/service_conf" => "spa#service_conf"
  post "save_service_conf" => "spa#save_service_conf"
  post "delete_service_conf" => "spa#delete_service_conf"

  # Services.
  get "services/list/active" => "spa#active_services"
  get "list/services/:list_id" => "spa#active_list_services"
  post "list/services/update" => "spa#update_list_services"

  # Lists.
  post "list/upload" => "spa#upload_list"
  post "list/create" => "spa#create_list"
  post "list/update" => "spa#update_list"
  post "list/destroy" => "spa#destroy_list"
  post "list/domain/save" => "spa#save_list_domain"
  post "list/sender_email/save" => "spa#save_list_sender_email"
  post "list/sender_name/save" => "spa#save_list_sender_name"
  get "lists/list" => "spa#lists_list"
  get "lists/total_pages" => "spa#lists_total_pages"
  get "list" => "spa#get_list"
  get "lists/collection" => "spa#lists_collection"
  get "list/:list_id/broadcasts" => "spa#broadcasts_collection"

  # Broadcasts.
  get "broadcast/list" => "spa#broadcast_list"
  get "broadcast/total_pages" => "spa#broadcast_total_pages"
  post "broadcast/create" => "spa#create_broadcast"
  post "broadcast/clone" => "spa#clone_broadcast"
  post "broadcast/update" => "spa#update_broadcast"
  post "broadcast/update_and_send" => "spa#update_and_send_broadcast"
  post "broadcast/segments" => "spa#segment_broadcast"
  delete "broadcast/delete/:id" => "spa#delete_broadcast"
  delete "broadcast/segments/delete" => "spa#delete_broadcast_segments"
  post "/broadcast/save_test_message" => "spa#save_test_message"
  get "list/:list_id/check_test_broadcast" => "spa#check_test_broadcast"
  post "/broadcast/send_test_message" => "spa#send_test_broadcast"
  post "/broadcast/send_test_message_from_broadcast" => "spa#send_test_message_from_broadcast"
  get '/broadcasts/status/:id' => 'spa#broadcast_status'
  get '/broadcasts/:id/unschedule' => 'spa#broadcast_unschedule'
  get 'broadcast' => 'spa#get_broadcast'
  post 'broadcast/services/update' => 'spa#update_broadcast_services'

  # Followups.
  get "followup/list" => "spa#followup_list"
  get "followup/load/:id", to: "spa#followup_load"
  get "followup/total_pages" => "spa#followup_total_pages"
  post "followup/save" => "spa#save_followup"
  post "followup/clone" => "spa#clone_followup"
  post "followup/update" => "spa#update_followup"
  delete "followup/delete/:id" => "spa#delete_followup"
  # post "followup/segments" => "spa#segment_followup"
  # delete "followup/segments/remove" => "spa#remove_segment_followup"
  get "followup/triggers_by_scope" => "spa#followup_triggers_by_scope"
  post "followup/state/toggle", to: "spa#followup_state_toggle"
  get 'followup' => 'spa#get_followup'
  post 'followup/services/update' => 'spa#update_followup_services'
  get 'followups', to: 'spa#get_followups'

  # Analytics
  get "analytics/data" => "spa#generate_analytics"
  post "analytics/data" => "spa#generate_analytics"

  post "analytics/get_pixel" => "spa#get_pixel"

  get "payload" => "spa#aggregate_payload"
  post "payload" => "spa#aggregate_payload"

  #########################################
  ## Subscripitions
  #########################################
  get "subscribe/:list_group_id" => "outside#subscribe"
  get "unsubscribe/me/:tracking_code" => "outside#unsubscribe_me"
  get "list/:list_id/subscribers" => "spa#subscriber_preview"
  get "list/:list_id/subscribers/columns" => "spa#subscriber_columns"
  post "list/search" => "spa#subscriber_search"
  get "list/subscribers/:list_id/export" => "spa#subscriber_csv_export"
  post "list/subscribers/count" => "spa#list_subscriber_count"
  post "list/delete/contact" => "spa#remove_subscriber_from_list"
  get "list/combos", to: "spa#list_combos"
  get "list/segments/:list_id", to: "spa#list_segments"
  post "list/segments/save", to: "spa#save_list_segments"
  post "list/fields", to: "spa#fields_list"
  get "list/:list_id/add_new_subscriber", to: "spa#add_new_subscriber"
  post "list/:list_id/save_subscriber", to: "spa#save_subscriber"
  post "list/:list_id/subscriber_email_validate", to: "spa#subscriber_email_validate"
  post "list/update_subscriber/:id", to: "spa#update_subscriber"
  post "dashboard/list/analytics", to: "analytics#list_analytics"
  get "dashboard/list/analytics", to: "analytics#list_analytics"
  post "analytics/broadcast/count", to: "analytics#broadcast_send_count"
  get "analytics/broadcast/:broadcast_id/count", to: "analytics#broadcast_send_count"

  get "lnks/:tracking_code" => "mail#click_tracking"

  post "partners/zaxaa" => "accounts#zaxaa_zpn"
  match "user/create" => "accounts#spawn_user", via: [:options]
  post "user/create" => "accounts#spawn_user"

  #########################################
  # Testing for mailer.
  #########################################
  post "smtp/send" => "mail#send_smtp"
  post "api/send" => "mail#send_api"
  post "api/test" => "mail#test_service"
  post "broadcast/send" => "spa#create_and_send_broadcast"
  post "followup/test/send" => "mail#send_message"
  get "followup/send/:id" => "mail#send_followup"
  post "mail/raw" => "mail#raw_mail"
  get "mail/progress/:message_id/:message_type" => "mail#mailing_progress"

  # Feedback.
  post "feedback/save" => "spa#save_feedback"

  #########################################
  ## Webforms
  #########################################
  # Parse a user-submitted form.
  post "webform/process/:form_code" => "spa#process_submitted_webform"

  post "webform/save" => "spa#save_webform"
  post "webform/delete" => "spa#destroy_webform"
  post "webform/clone" => "spa#clone_webform"
  get "webform/:list_field_group_id/load" => "spa#load_webform"
  get "webform/list" => "spa#webform_list"
  get "webform/total_pages" => "spa#webform_total_pages"
  get "list/webforms" => "spa#webforms_for_list"

  # Update a form.
  post "webform/:list_field_group_id/save" => "spa#save_webform"

  get "webform/template/:template_name" => "spa#load_form_template"
  get "list/:list_id/collections" => "spa#list_collections"
  #########################################
  ## End Webforms
  #########################################

  #########################################
  ## Support URLs
  #########################################
  get "tutorials", to: 'support#tutorials'
  #########################################
  ## End support URLs
  #########################################

  # Tracking pixel.
  get "open" => "mail#open_pixel"

  post "webhooks/incoming" => "webhooks#incoming"

  resources :services_users, only: :update
  resources :user_services_settings, only: [] do
    put :update, on: :collection
  end
end
