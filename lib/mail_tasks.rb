class MailTasks
  include SendingApi
  def initialize
  end

  def message_sender(id_val, send_type_val, args)
    MailSender.old_calling(id_val, send_type_val, args)
    # OldMailSender.message_sender(id_val, send_type_val, args) # old api
  rescue
    puts $!.backtrace.join("\n")
    raise $!
  end
end
