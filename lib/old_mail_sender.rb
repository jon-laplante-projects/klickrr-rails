require 'net/http'
require 'action_view'
require 'SmtpApiHeader.rb'
require "resolv-replace.rb"
require 'valid_email'

module OldMailSender extend self
  include SendingApi
  class MailTasks::NoAvailableServices < Exception; end

  class MailTasks::ServicesUsers

    def initialize(list)
      @user = list.user
      @scope = ServicesUser.includes(:service).where(user_id: @user.id, used: true)
      @scope = @scope.where(service_id: list.services.split(',')) if list.services.present?
      @scope = @scope.order(:id).to_a
      @index = 0
      @index -= 1 if @user.services_settings.evenly?
    end

    def current
      if contact_changed?
        if @user.services_settings.evenly?
          while true
            go_next
            break if current_is_available?
          end
        elsif @user.services_settings.series?
          while !current_is_available?
            go_next
          end
        end
        @old_contact_id = @contact_id
      end
      _current
    end

    def contact=(contact)
      raise MailTasks::NoAvailableServices.new if @scope.empty?
      @old_contact_id = @contact_id
      @contact_id = contact.object_id
    end

    private
    def go_next
      @index >= @scope.count - 1 ? @index = 0 : @index += 1
    end

    def _current
      @scope[@index]
    end

    def current_is_available?
      service = _current.service
      mails_count = service.sent_mails_from(@user).this_month.count
      current_is_available = _current.limit && mails_count < _current.limit || !_current.limit
      if @user.services_settings.autodeactivate? && _current.limit && mails_count + 1 >= _current.limit
        _current.update(used: false)
      end
      @scope.delete_at(@index) unless current_is_available
      current_is_available
    end

    def contact_changed?
      @old_contact_id != @contact_id
    end

  end

  def message_sender(id_val, send_type_val, args)
    #################################################
  ## TODO: Move this, send_smtp and send_api into
  ## their own class and simply call them from
  ## MailController.
  #################################################
    # Thread.new do
      config = {}
      # Determine what time of mailing this is, and pull data as is appropriate.
      case send_type_val
      when 'broadcast'
          bc = Broadcast.find(id_val)
          bc.update(state: :processing)
          config['subject'] = bc.subject.clone
          master_subject = bc.subject.clone
          master_content = bc.content

          list = bc.list

          segments = []
          bc.segments.each { |segment| segments = [segments, segment.get_emails].reject(&:empty?).reduce(:&)}

        when "followup"
          fu = Followup.find(id_val)
          config["subject"] = fu.subject
          master_subject = fu.subject
          master_content = fu.content
          list = fu.broadcasts.list
          segments = nil
          # segments = get_segments(fu.list.user_id, fu.followup_segments, list)

        when "conn_test"
          config["subject"] = "Klickrr :: Testing #{ config["service"] } Connection"
          master_subject = "Klickrr :: Testing #{ config["service"] } Connection"
          master_content = "This is a test of the #{ config["service"] } service in Klickrr."
            service = args["service_id"]
          list = {"email" => args["email"]}.to_json

        when "email_preview"
          # Put together the email preview var inits.
          config["subject"] = "#{args["subject"]}"
          master_subject = "#{args["subject"]}"
          master_content = "#{args["content"]}"
          list = {"email" => args["email"]}.to_json

        when "test_message_from_broadcast"
          config["subject"] = "#{args["subject"]}"
          master_subject = "#{args["subject"]}"
          config["email"] = "#{args['email']}"
          master_content = "#{args["content"]}"
          # fake list
          list = List.new(id: args["list_id"], user_id: User.first.id)
          fake_contact = Struct.new(:id, :email, :set_id, :attributes, :first_name)
          contact = fake_contact.new(0, args['email'], 0, {}, 'test_name')
          fake_contacts = [contact]
      end

      #unless args["user_id"].blank?
      config['user_id'] = args[:user_id]
      #end
      # Set our content variable - we won't touch master_content.
      content = master_content
      services_users = MailTasks::ServicesUsers.new(list)
      # Because this is using the old 'data bag' model, we need to
      # get the column name/label order from list_values and then
      # match them up the the labels in list_fields.
      list_groups = ListField.find_by_sql(["SELECT DISTINCT list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_field_group_id ASC;", list.id])
      list_groups.each do |group|
        columns = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id", group.list_field_group_id])
        #columns = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id", list.id])
        cols = columns.map(&:label).uniq

        col_string = 'set_id int'
        cols.each {|col| col_string.concat(", #{col} text") }

        #################################################
        ## TODO: Figure out a way to get all columns, while
        ## honoring the correct order, and still
        ## identifying email addresses.
        #################################################

        # #contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id') AS ct(set_id int, first_name text, last_name text, email text, phone text)", list.id])
        # contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string})", group.list_field_group_id])
        # #puts "#{contacts.to_json}"
        # #contacts = ListValue.find_by_sql("SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = #{params[:list_id].to_i} AND set_id IN (#{record_sets.join(",")}) ORDER BY list_values.id') AS ct(#{col_string})")

        api_key = 'api_key'
        api_name = 'api_name'

        json_arr = Array.new

        columns = ListField.find_by_sql(["SELECT '{{' || label || '}}' AS token, label, id, list_id, list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_fields.id;", list.id])

        # We're going to pull batches of 1,000 rows to process.
        offset = 0
        limit = 1000
        contacts = fake_contacts if fake_contacts.present?
        contacts ||= ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string}) LIMIT 1000", group.list_field_group_id])
        # As long as we've got contacts, keep batching in 1,000s.
        while contacts.present?
          # Unless there are segments *AND* the current email is not in the segmented list.while contacts.present?
          offset += limit
          contacts.each do |contact|
            # Unless there are segments *AND* the current email is not in the segmented list.
            if segments.blank? || segments.include?(contact.email)
              # Let's check and see if there are unsubscribers in this list.
              unsubscribed = Unsubscribe.where(set_id: contact.set_id).size
              unless unsubscribed > 0
                config.merge!(contact.attributes)
                # Check and make sure that this email address hasn't already received  this message.
                sent_mail = SentMail.where(["message_id = ? AND message_type = ? AND message_type != 'service_test' AND email = ?", id_val, send_type_val, contact.email]).count
                if sent_mail.zero?
                  # List does not track domain or sender, so pull it from the usage JSON.
                  services_users.contact = contact
                  usage = JSON.parse(services_users.current.usage)
                  # Get the user's service preferences (i.e. which is active, logins, etc.)
                  config.merge!(usage)
                  config["domain"] = usage["domain"] if usage["domain"].present?
                  config["sender"] = usage["sender"] if usage["sender"].present?
                  config["sendmail"] = true

                  # Update the message content - replace {{placeholders}}, add footer, and tracking pixel.
                  #tracking_code = SecureRandom.hex(9)
                  ###################################
                  ## TODO: Ensure the tracking_code
                  ## is unique.
                  ###################################
                  # Save this 'sent' to the sent_mail table.
                  sent_mail = SentMail.new({
                    user_id: config['user_id'],
                    service_id: services_users.current.service_id,
                    message_id: id_val,
                    contact_id: contact.id,
                    message_type: send_type_val,
                    email: contact.email,
                    sent: false
                  })
                  sent_mail.generate_token('tracking_code', 12)
                  sent_mail.save

                  if args[:test_message_b]
                    if Random.rand(2) == 1
                      sent_mail.update_attribute(:message_id, args[:test_message_b].id)
                      config["subject"] = args[:test_message_b].subject
                      content = args[:test_message_b].content
                    end
                  end
                  config["sent_mail_id"] = sent_mail.id
                  config["to_email"] = config.delete("email")


                  columns.each do |column|
                    unless contact[column[:label].parameterize.underscore.to_sym].blank?
                      config["subject"] = config["subject"].to_s.gsub(column[:token], contact[column[:label].parameterize.underscore.to_sym])
                      content = content.to_s.gsub(column[:token],contact[column[:label].parameterize.underscore.to_sym])
                    else
                      config["subject"] = config["subject"].gsub(column[:token], "")
                      content = content.to_s.gsub(column[:token],"")
                    end
                  end

                  # Let's get our message built!
                  service_desc = services_users.current.service

                  #puts "Service: #{services_desc.name}"

                  # if service_desc.is_smtp? || service_desc.name == "ses" || service_desc.name == "postage" || service_desc.name == "sendgrid"
                  footer = "<hr style=\"margin: 15px 0 15px 0;\"/><div style=\"text-align: center; font-size: 0.75em\"><a href=\"http://klickrr.net/unsubscribe/me/#{sent_mail.tracking_code}\" target=\"_BLANK\">Please remove me from this list.</a>&nbsp;&bull;&nbsp;Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
                  content = replace_links_in_message(content, sent_mail.tracking_code)

                  # else
                  #   footer = "<div style=\"text-align: center; font-size: 0.75em\">Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
                  # end

                  #######################################################
                  ## TODO: Unsubscribe endpoint can stay the same, but the
                  ## link will eventually be conditionally and only for
                  ## services that don't provide there own.
                  ##
                  ##
                  ## TODO: Add conditionals for services with webhooks.
                  ##
                  ## Don't forget to handle the tracking pixel
                  ## appropriately, too.
                  #######################################################
                  #content = replace_links_in_message(content)

                  # Prep our email for delivery.

                  #message = message.concat('<hr style="margin: 15px 0 15px 0;"/><div style="text-align: center;">Provided by <a href="klickrr.net">Klickrr.net</a></div>')
                  #message = message.concat("</body></html>")

                  # Check to see if we are sending something other than email.
                  service_type = service_desc.service_type

                  if ["email", "smtp_email"].include? service_type.name
                    config["content"]  = <<-CONTENT
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    </head>
                    <body>
                    <img src="http://www.klickrr.net/open.png?tc=#{ sent_mail.tracking_code }" #{ "mc:disable-tracking" if services_users.current.service.name == "mandrill" } />
                    #{content}
                    #{footer}
                    </body>
                    </html>
                    CONTENT
                  else
                    config["content"] = content
                  end

                  config['service'] = service_desc.name
                  result = service_desc.is_smtp? || Rails.env.development? || Rails.env.test? ? send_smtp(config) : send_api(config)

                  if result
                    @sent_count ||= 0
                    @sent_count += 1
                    bc.update(sent_count: @sent_count) if bc
                  end

                  # Reset content so it's not sending the same message again and again.
                  content = master_content
                  config["subject"] = master_subject

                  # Don't forget to move SMTP stuff to the API OR call it seperately.
                else
                  sent_mail = SentMail.new
                    sent_mail.user_id = config["user_id"]
                    sent_mail.service_id = services_users.current.service.id
                    sent_mail.message_id = id_val
                    sent_mail.contact_id = contact.id
                    sent_mail.message_type = 'broadcast'
                    sent_mail.email = contact.email
                    sent_mail.sent = false
                    sent_mail.response_message = 'This contact has already been sent this message.'
                  sent_mail.save
                end # Closing UNLESS this message has been sent to this contact...
              end
            end
          end
          # Get the next batch of 1,000 contacts.
          contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string}) OFFSET #{offset} LIMIT #{limit}", group.list_field_group_id])
        end
      end

      bc.update(sent: Time.now, state: :completed) if bc.present?
      args[:test_message_b].update(sent: Time.now, state: :completed) if args[:test_message_b].present?
      fu.update_attribute(:sent, Time.now) if fu.present?
      # end # Close the thread
  rescue
    puts $!.backtrace.join("\n")
    raise $!
  end

  def subdomain(address, domain)
    address.include?(domain) && !address.include?('@') ? address : domain
  end

  def send_smtp(config)
    unless config.size < 1
      sent_mail = SentMail.find(config["sent_mail_id"])
      begin
        address = if Rails.env.development? || Rails.env.test?
          smtp_settings = {address: 'localhost', port: 1025 }
          config.merge!(smtp_settings.stringify_keys)
          config['address']
        else
          subdomain(config['address'], config['domain'])
        end
        sender = if ValidateEmail.valid? config['user_name']
          config['user_name']
        elsif ValidateEmail.valid? config['address']
          config['address']
        elsif ValidateEmail.valid? config['sender']
          config['sender']
        else
          'noreply@klickrr.net'
        end
        begin
          mail = Mail.new do
            from     sender
            to       config["to_email"]
            subject  config["subject"]

            text_content = ActionController::Base.helpers.sanitize(config["content"])
            text_part do
              body text_content
            end

            html_part do
              content_type 'text/html; charset=UTF-8'
              body config["content"]
            end
          end
          # if config["sendmail"] == true
          #   mail.delivery_method :sendmail, address: address, port: config["port"], domain: config["domain"], user_name: config["user_name"], password: config["password"], authentication: config["authentication"], enable_starttls_auto: true, tls: true
          # else
          smtp_settings ||= {
            address: address,
            port: config['port'],
            domain: config['domain'],
            user_name: config['user_name'],
            password: config['password'],
            authentication: config['authentication'],
            enable_starttls_auto: true,
            tls: true
          }
          mail.delivery_method(:smtp, smtp_settings)
          # end
          mail.deliver!


        # SmtpMailer.smtp_test(recipient, sender).deliver_now

          sent_mail.sent = true
          sent_mail.response_message = "#{Net::SMTP::Response} - You have successfully sent a message via SMTP."
          sent_mail.save
          return true

        rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e

          sent_mail.sent = false
          sent_mail.response_message = "#{e.class} - #{e.message}"
          sent_mail.save

          return e
        end
      rescue => e
        sent_mail.sent = false
        sent_mail.response_message = "#{e.class} - #{e.message}"
        sent_mail.save

        return e
      end
    end
  end

  def send_api(config)
    unless config.size < 1
      sent_mail = SentMail.find(config["sent_mail_id"])

      api_key = config["api_key"]
      api_name = config["api_name"]

      #==================================
      # From 'Broadcasts' and 'Followups'
      #==================================
      subject = config["subject"]
      content = config["content"]

      #==================================
      # Attached to 'List'
      #==================================
      recipient = config["to_email"]
      recipient_name = config["to_email"]
      sender = config["from_email"]
      sender_name = config["from_email"]
      service = config["service"]
      domain = config["domain"]

      case service
        when "sendgrid"
          begin
            # Set the external id for this sent_mail record.
            # sent_mail.generate_token("ext_id", 12)

            # Set the SMTPAPI header.
            hdr = SmtpApiHeader.new

            receiver = ["#{recipient}"]

            hdr.addTo(receiver)
            hdr.setUniqueArgs({"ext_id" => sent_mail.ext_id})

            # Create and populate the mail obj.
            mail = Mail.new do
              header['X-SMTPAPI'] =  hdr.asJSON()
              from     "#{sender}"
              to       "#{recipient}"
              subject  "#{subject}"

              text_content = "#{ActionController::Base.helpers.sanitize(content)}"
              text_part do
                body "#{text_content}"
              end

              html_part do
                content_type 'text/html; charset=UTF-8'
                body "#{content}"
              end
            end

            # Moving to API from SMTP.
            sendgrid = SendGrid::Client.new do |c|
              c.api_key = "#{api_key}"
            end

            email = SendGrid::Mail.new do |m|
              m.to      = "#{recipient}"
              m.from    = "#{sender}"
              m.subject = "#{subject}"
              m.html    = "#{content}"
            end

            # Set the SMTP settings.
            #mail.delivery_method :smtp, address: "smtp.sendgrid.net", port: 587, domain: "#{domain}", user_name: "#{api_name}", password: "#{api_key}", authentication: "plain", enable_starttls_auto: true

            # ...and send that mail!
            sendgrid.send(email)
            #mail.deliver!

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via Sendgrid"
          rescue => e
            sent_mail.sent = false
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "mandrill"
          begin
              mandrill = Mandrill::API.new "#{api_key}"
              template_content = [{}]
              message = {"from_name"=>"#{sender_name}",
              "metadata"=>{"sent_mail_id"=>"#{sent_mail.id}"},
              "track_clicks"=>true,
              "subject"=>"#{subject}",
              "html"=>"#{content}",
               "to"=>
                  [{"email"=>"#{recipient}",
                      "name"=>"#{recipient_name}",
                      "type"=>"to"}],
               "track_opens" => true,
               "headers"=>{"Reply-To"=>"#{sender}"},
               "from_email"=>"#{sender}"}
              async = true
              ip_pool = "Main Pool"
              send_at = {}
              result = mandrill.messages.send message, async, ip_pool, send_at
                  unless result[0]["_id"].blank?
                    sent_mail.ext_id = result[0]["_id"]
                  end
                  unless result[0]["status"].blank?
                    sent_mail.status = result[0]["status"]
                  end
              sent_mail.sent = true
              sent_mail.response_message = "Message sent via Mandrill"

          rescue => e
              # Mandrill errors are thrown as exceptions
              sent_mail.sent = false
              sent_mail.response_message = "A mandrill error occurred: #{e.class} - #{e.message}"
              sent_mail.save
              return false
          end

        when "mailjet"
          begin
            #HTTParty.post("https://#{api_key}:#{api_name}@https://api.mailjet.com/v3/send/message")
            Mailjet.configure do |config|
              config.api_key = "#{api_key}"
              config.secret_key = "#{api_name}"
              config.default_from = "#{sender}"
            end

            result = Mailjet::MessageDelivery.create(from: "#{sender}", to: "#{recipient}", subject: "#{subject}", html: "#{content}")


            puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            puts "~~ #{result.inspect}"
            puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

            unless result.attributes["id"].blank?
              sent_mail.ext_id = result.attributes["id"]
              sent_mail.status = "sent"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via Mailjet"
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "mailgun"
          begin
            # First, instantiate the Mailgun Client with your API key
            mg_client = Mailgun::Client.new "#{api_key}"

            # Define your message parameters
            message_params = {:from    => "#{sender}",
                              :to      => "#{recipient}",
                              :subject => "#{subject}",
                              :html    => "#{content}"}

            # Send your message through the client
            result = mg_client.send_message("#{domain}", message_params).to_h!

            unless result["id"].blank?
              sent_mail.ext_id = result["id"]
            end
            unless result["message"].blank?
              sent_mail.response_message = result["message"]
            end

            sent_mail.sent = true
            sent_mail.status = "sent"

          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "ses"
          begin
            ses = AWS::SES::Base.new(
              :access_key_id     => "#{api_name}",
              :secret_access_key => "#{api_key}"
            )

            result = ses.send_email(
              :to        => ["#{recipient}"],
              :source    => "\"#{sender_name}\" <#{sender}>",
              :subject   => "#{subject}",
              :html_body => "#{content}"
            )

            result_hash = Hash.from_xml(result)

            unless result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"].blank?
              sent_mail.ext_id = "#{result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"]}"
              sent_mail.status = "sent"
            end

            sent_mail.sent = true
              sent_mail.response_message = "Message sent via Amazon SES"
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "postage"
          begin
            result_args = Hash.new
            arguments = Hash.new
            headers = Hash.new
            content_hash = Hash.new

            content_hash["text/html"] = "#{content}"

            headers["subject"] =  "#{subject}"
            headers["from"] =  "#{sender}"

            arguments["recipients"] = ["#{recipient}"]
            arguments["headers"] = headers
            arguments["content"] = content_hash

            result_args["api_key"] = "#{api_key}"
            result_args["arguments"] = arguments

            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            # puts "JSON: #{result_args.to_json}"
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            # puts "Content: #{content}"
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

            result = HTTParty.post("https://api.postageapp.com/v.1.0/send_message.json",
              :body => result_args.to_json,
              :headers => { 'Content-Type' => 'application/json' } )

            if result["response"]["status"] == "ok"
              sent_mail.sent = true
              sent_mail.status = "sent"
              sent_mail.response_message = "Message sent via Postage"

              unless result["data"]["message"]["id"].blank?
                sent_mail.ext_id = result["data"]["message"]["id"]
              end
            else
              sent_mail.sent = false
              sent_mail.status = "error"
              sent_mail.response_message = "Postage API failure - #{result["response"]["message"]}"
            end

          rescue => e
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            # puts "~~ Postage send failure: #{e.class} - #{e.message}"
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "Postage send failure: #{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "twilio"
          begin
            # result_args = Hash.new
            # arguments = Hash.new
            # headers = Hash.new
            # content_hash = Hash.new

            # content_hash["text/html"] = "#{content}"

            # headers["subject"] =  "#{subject}"
            # headers["from"] =  "#{sender}"

            # arguments["recipients"] = ["#{recipient}"]
            # arguments["headers"] = headers
            # arguments["content"] = content_hash

            # result_args["account_sid"] = "#{api_key}"
            # result_args["auth_token"] = "#{api_name}"
            # result_args["twilio_phone_num"] = "#{twilio_phone_num}"
            # result_args["arguments"] = arguments

            sanitize = ActionView::Base.new
            content = sanitize.strip_tags("#{content}")

            # put your own credentials here
            account_sid = "#{config["account_sid"]}"
            auth_token = "#{config["auth_token"]}"
            twilio_phone_num = "#{config["twilio_phone_num"]}"

            # set up a client to talk to the Twilio REST API
            @client = Twilio::REST::Client.new account_sid, auth_token

            # and then you can create a new client without parameters
            # @client = Twilio::REST::Client.new

            @client.messages.create(
              from: twilio_phone_num,
              to: config["phone"],
              body: content
            )
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "Postage send failure: #{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "socketlabs"
          ###############################################################
          ## From http://www.socketlabs.com/api-reference/injection-api/
          ###############################################################
          begin
            #Socketlabs.api_user='#{api_name}'
            #Socketlabs.api_password='#{api_key}'

            #SocketLabs.url="https://api.socketlabs.com/v1/email/send"
            ##SocketLabs.user="#{api_name}"
            ##SocketLabs.pw="#{api_key}"

            ## You can instantiate the EmailMessage object and work with its properties and methods
            #msg = SocketLabs::EmailMessage.new
            #msg.from = "#{sender_email}"
            #msg.to = "#{recipient}"
            #msg.subject = "#{subject}"

            ## You can specify a text and/or HTML body.
            #msg.htmlBody = "#{content}"

            ## You can specify custom headers
            #msg.headers = {:X-MyHeader=>'Value', :List-Unsubscribe=>'Value'}

            #msg.send

            result_args = Hash.new
            messages = Hash.new
            message_to = Hash.new
            message_from = Hash.new
            reply_to = Hash.new

            message_from["EmailAddress"] = sender
            #message_from["FriendlyName"] = sender

            message_to["EmailAddress"] = sender
            #message_to["FriendlyName"] = nil

            reply_to["EmailAddress"] = "#{sender}"
            reply_to["FriendlyName"] = "#{sender}"

            messages["To"] = [message_to]
            messages["From"] = message_from
            messages["Subject"] = subject
            messages["HtmlBody"] = content
            #messages["HtmlBody"] = body
            messages["MailingId"] = nil
            messages["MessageId"] = sent_mail.id
            messages["MergeData"] = nil
            messages["CustomHeaders"] = nil
            messages["Cc"] =  nil
            messages["Bcc"] =  nil
            messages["ReplyTo"] = nil
            messages["Charset"] = nil
            messages["Attachments"] = nil

            result_args["ServerID"] = api_name
            result_args["ApiKey"] = api_key
            result_args["messages"] = [messages]
            result_args["Bcc"] =  nil
            result_args["ReplyTo"] = nil
            result_args["Charset"] = nil
            result_args["Attachments"] = nil

            result = HTTParty.post("https://inject.socketlabs.com/api/v1/email",
              :body => result_args.to_json,
              :headers => { 'Content-Type' => 'application/json' } )
            if result["ErrorCode"] == "Success"
              sent_mail.sent = true
              sent_mail.ext_id = sent_mail.id
              sent_mail.response_message = "Message sent via Socket Labs"
            else
              sent_mail.sent = false
              sent_mail.response_message = "Postage API failure - #{result["response"]["message"]}"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via SocketLabs."
          rescue => e
            sent_mail.sent = false
            sent_mail.response_message = "Message could not be sent. #{e.message}"
            sent_mail.save
            return false
          end

        when "elastic_email"
          begin
            result = HTTParty.post("https://api.elasticemail.com/mailer/send",
            #result = HTTParty.post("https://#{api_key}:#{api_name}@api.elasticemail.com/mailer/send",
                :body => { "username" => "#{api_name}",
                "api_key" => "#{api_key}",
                "from" => "#{sender}",
                "from_name" => "#{sender_name}",
                "to" => "#{recipient}",
                "subject" => "#{subject}",
                "body_html" => "#{content}",
                "reply_to" => "#{sender}",
                "reply_to_name" => "#{sender_name}"
                })

            unless result == /#^Error:/
              sent_mail.sent = true
                sent_mail.response_message = "Message sent via Elastic Email"
              sent_mail.ext_id = result
            else
              sent_mail.sent = false
                sent_mail.response_message = "#{result["response"]["message"]}"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via Elastic Email."
            sent_mail.ext_id = result
          rescue => e
            sent_mail.sent = false
            sent_mail.response_message = "Message could not be sent. #{e.message}"
            sent_mail.save

            return false
          end

        when "sparkpost"
          begin
            body = {
              recipients: [
                {
                  address: config["to_email"]
                }
              ],
              content: {
                from: {
                  email: config["from_email"],
                },
                subject: config["subject"],
                html: config["content"]
              }
            }

            result = HTTParty.post("https://api.sparkpost.com/api/v1/transmissions?num_rcpt_errors=3", body: body.to_json, headers: { "Authorization" => config['api_key'], 'Content-Type' => 'application/json'  })

            if result["id"].present?
              sent_mail.ext_id = result["id"]
              sent_mail.status = "sent"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via SparkPost"
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        #when "postmark"
      end

      sent_mail.save
    else
      send_results["response_message"] = "No config parameters provided. Unable to proceed."
      return false
    end
    return true
  end

  def replace_links_in_message(msg, tracking_code)
    message = Nokogiri::HTML.fragment(msg)
    if tracking_code.include? "=3D"
      tracking_code == tracking_code.unpack('M')[0]
    end

    message.css('a[href]').each do |link|
    #################################################
    ## TODO: Change to SSL when implemented.
    #################################################
      link["href"] = "http://www.klickrr.net/lnks/#{tracking_code}?url=#{link['href']}"
    end

    return message.to_html
  end
end
