require 'net/http'
require 'action_view'
require 'SmtpApiHeader.rb'
require "resolv-replace.rb"
require 'valid_email'

module SendingApi extend self
  def subdomain(address, domain)
    address.include?(domain) && !address.include?('@') ? address : domain
  end

  def send_smtp(config)
    unless config.size < 1
      sent_mail = SentMail.find(config["sent_mail_id"])
      begin
        address = if Rails.env.development? || Rails.env.test?
          smtp_settings = {address: 'localhost', port: 1025 }
          config.merge!(smtp_settings.stringify_keys)
          config['address']
        else
          subdomain(config['address'], config['domain'])
        end
        sender = if ValidateEmail.valid? config['user_name']
          config['user_name']
        elsif ValidateEmail.valid? config['address']
          config['address']
        elsif ValidateEmail.valid? config['sender']
          config['sender']
        else
          'noreply@klickrr.net'
        end
        begin
          mail = Mail.new do
            from     sender
            to       config["to_email"]
            subject  config["subject"]

            text_content = ActionController::Base.helpers.sanitize(config["content"])
            text_part do
              body text_content
            end

            html_part do
              content_type 'text/html; charset=UTF-8'
              body config["content"]
            end
          end

          smtp_settings ||= {
            address: address,
            port: config['port'],
            domain: config['domain'],
            user_name: config['user_name'],
            password: config['password'],
            authentication: config['authentication'],
            enable_starttls_auto: true,
            tls: true
          }
          mail.delivery_method(:smtp, smtp_settings)
          mail.deliver!


        # SmtpMailer.smtp_test(recipient, sender).deliver_now

          sent_mail.sent = true
          sent_mail.response_message = "#{Net::SMTP::Response} - You have successfully sent a message via SMTP."
          sent_mail.save
          return true

        rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e

          sent_mail.sent = false
          sent_mail.response_message = "#{e.class} - #{e.message}"
          sent_mail.save

          return e
        end
      rescue => e
        sent_mail.sent = false
        sent_mail.response_message = "#{e.class} - #{e.message}"
        sent_mail.save

        return e
      end
    end
  end

  def send_api(config)
    unless config.size < 1
      sent_mail = SentMail.find(config["sent_mail_id"])

      api_key = config["api_key"]
      api_name = config["api_name"]

      #==================================
      # From 'Broadcasts' and 'Followups'
      #==================================
      subject = config["subject"]
      content = config["content"]

      #==================================
      # Attached to 'List'
      #==================================
      recipient = config["to_email"]
      recipient_name = config["to_email"]
      sender = config["from_email"]
      sender_name = config["from_email"]
      service = config["service"]
      domain = config["domain"]

      case service
        when "sendgrid"
          begin
            # Set the external id for this sent_mail record.
            # sent_mail.generate_token("ext_id", 12)

            # Set the SMTPAPI header.
            hdr = SmtpApiHeader.new

            receiver = ["#{recipient}"]

            hdr.addTo(receiver)
            hdr.setUniqueArgs({"ext_id" => sent_mail.ext_id})

            # Create and populate the mail obj.
            mail = Mail.new do
              header['X-SMTPAPI'] =  hdr.asJSON()
              from     "#{sender}"
              to       "#{recipient}"
              subject  "#{subject}"

              text_content = "#{ActionController::Base.helpers.sanitize(content)}"
              text_part do
                body "#{text_content}"
              end

              html_part do
                content_type 'text/html; charset=UTF-8'
                body "#{content}"
              end
            end

            # Moving to API from SMTP.
            sendgrid = SendGrid::Client.new do |c|
              c.api_key = "#{api_key}"
            end

            email = SendGrid::Mail.new do |m|
              m.to      = "#{recipient}"
              m.from    = "#{sender}"
              m.subject = "#{subject}"
              m.html    = "#{content}"
            end

            # Set the SMTP settings.
            #mail.delivery_method :smtp, address: "smtp.sendgrid.net", port: 587, domain: "#{domain}", user_name: "#{api_name}", password: "#{api_key}", authentication: "plain", enable_starttls_auto: true

            # ...and send that mail!
            sendgrid.send(email)
            #mail.deliver!

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via Sendgrid"
          rescue => e
            sent_mail.sent = false
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "mandrill"
          begin
              mandrill = Mandrill::API.new "#{api_key}"
              template_content = [{}]
              message = {"from_name"=>"#{sender_name}",
              "metadata"=>{"sent_mail_id"=>"#{sent_mail.id}"},
              "track_clicks"=>true,
              "subject"=>"#{subject}",
              "html"=>"#{content}",
               "to"=>
                  [{"email"=>"#{recipient}",
                      "name"=>"#{recipient_name}",
                      "type"=>"to"}],
               "track_opens" => true,
               "headers"=>{"Reply-To"=>"#{sender}"},
               "from_email"=>"#{sender}"}
              async = true
              ip_pool = "Main Pool"
              send_at = {}
              result = mandrill.messages.send message, async, ip_pool, send_at
                  unless result[0]["_id"].blank?
                    sent_mail.ext_id = result[0]["_id"]
                  end
                  unless result[0]["status"].blank?
                    sent_mail.status = result[0]["status"]
                  end
              sent_mail.sent = true
              sent_mail.response_message = "Message sent via Mandrill"

          rescue => e
              # Mandrill errors are thrown as exceptions
              sent_mail.sent = false
              sent_mail.response_message = "A mandrill error occurred: #{e.class} - #{e.message}"
              sent_mail.save
              return false
          end

        when "mailjet"
          begin
            #HTTParty.post("https://#{api_key}:#{api_name}@https://api.mailjet.com/v3/send/message")
            Mailjet.configure do |config|
              config.api_key = "#{api_key}"
              config.secret_key = "#{api_name}"
              config.default_from = "#{sender}"
            end

            result = Mailjet::MessageDelivery.create(from: "#{sender}", to: "#{recipient}", subject: "#{subject}", html: "#{content}")


            puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            puts "~~ #{result.inspect}"
            puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

            unless result.attributes["id"].blank?
              sent_mail.ext_id = result.attributes["id"]
              sent_mail.status = "sent"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via Mailjet"
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "mailgun"
          begin
            # First, instantiate the Mailgun Client with your API key
            mg_client = Mailgun::Client.new "#{api_key}"

            # Define your message parameters
            message_params = {:from    => "#{sender}",
                              :to      => "#{recipient}",
                              :subject => "#{subject}",
                              :html    => "#{content}"}

            # Send your message through the client
            result = mg_client.send_message("#{domain}", message_params).to_h!

            unless result["id"].blank?
              sent_mail.ext_id = result["id"]
            end
            unless result["message"].blank?
              sent_mail.response_message = result["message"]
            end

            sent_mail.sent = true
            sent_mail.status = "sent"

          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "ses"
          begin
            ses = AWS::SES::Base.new(
              :access_key_id     => "#{api_name}",
              :secret_access_key => "#{api_key}"
            )

            result = ses.send_email(
              :to        => ["#{recipient}"],
              :source    => "\"#{sender_name}\" <#{sender}>",
              :subject   => "#{subject}",
              :html_body => "#{content}"
            )

            result_hash = Hash.from_xml(result)

            unless result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"].blank?
              sent_mail.ext_id = "#{result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"]}"
              sent_mail.status = "sent"
            end

            sent_mail.sent = true
              sent_mail.response_message = "Message sent via Amazon SES"
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "postage"
          begin
            result_args = Hash.new
            arguments = Hash.new
            headers = Hash.new
            content_hash = Hash.new

            content_hash["text/html"] = "#{content}"

            headers["subject"] =  "#{subject}"
            headers["from"] =  "#{sender}"

            arguments["recipients"] = ["#{recipient}"]
            arguments["headers"] = headers
            arguments["content"] = content_hash

            result_args["api_key"] = "#{api_key}"
            result_args["arguments"] = arguments

            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            # puts "JSON: #{result_args.to_json}"
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            # puts "Content: #{content}"
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

            result = HTTParty.post("https://api.postageapp.com/v.1.0/send_message.json",
              :body => result_args.to_json,
              :headers => { 'Content-Type' => 'application/json' } )

            if result["response"]["status"] == "ok"
              sent_mail.sent = true
              sent_mail.status = "sent"
              sent_mail.response_message = "Message sent via Postage"

              unless result["data"]["message"]["id"].blank?
                sent_mail.ext_id = result["data"]["message"]["id"]
              end
            else
              sent_mail.sent = false
              sent_mail.status = "error"
              sent_mail.response_message = "Postage API failure - #{result["response"]["message"]}"
            end

          rescue => e
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            # puts "~~ Postage send failure: #{e.class} - #{e.message}"
            # puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "Postage send failure: #{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "twilio"
          begin
            # result_args = Hash.new
            # arguments = Hash.new
            # headers = Hash.new
            # content_hash = Hash.new

            # content_hash["text/html"] = "#{content}"

            # headers["subject"] =  "#{subject}"
            # headers["from"] =  "#{sender}"

            # arguments["recipients"] = ["#{recipient}"]
            # arguments["headers"] = headers
            # arguments["content"] = content_hash

            # result_args["account_sid"] = "#{api_key}"
            # result_args["auth_token"] = "#{api_name}"
            # result_args["twilio_phone_num"] = "#{twilio_phone_num}"
            # result_args["arguments"] = arguments

            sanitize = ActionView::Base.new
            content = sanitize.strip_tags("#{content}")

            # put your own credentials here
            account_sid = "#{config["account_sid"]}"
            auth_token = "#{config["auth_token"]}"
            twilio_phone_num = "#{config["twilio_phone_num"]}"

            # set up a client to talk to the Twilio REST API
            @client = Twilio::REST::Client.new account_sid, auth_token

            # and then you can create a new client without parameters
            # @client = Twilio::REST::Client.new

            @client.messages.create(
              from: twilio_phone_num,
              to: config["phone"],
              body: content
            )
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "Postage send failure: #{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        when "socketlabs"
          ###############################################################
          ## From http://www.socketlabs.com/api-reference/injection-api/
          ###############################################################
          begin
            #Socketlabs.api_user='#{api_name}'
            #Socketlabs.api_password='#{api_key}'

            #SocketLabs.url="https://api.socketlabs.com/v1/email/send"
            ##SocketLabs.user="#{api_name}"
            ##SocketLabs.pw="#{api_key}"

            ## You can instantiate the EmailMessage object and work with its properties and methods
            #msg = SocketLabs::EmailMessage.new
            #msg.from = "#{sender_email}"
            #msg.to = "#{recipient}"
            #msg.subject = "#{subject}"

            ## You can specify a text and/or HTML body.
            #msg.htmlBody = "#{content}"

            ## You can specify custom headers
            #msg.headers = {:X-MyHeader=>'Value', :List-Unsubscribe=>'Value'}

            #msg.send

            result_args = Hash.new
            messages = Hash.new
            message_to = Hash.new
            message_from = Hash.new
            reply_to = Hash.new

            message_from["EmailAddress"] = sender
            #message_from["FriendlyName"] = sender

            message_to["EmailAddress"] = sender
            #message_to["FriendlyName"] = nil

            reply_to["EmailAddress"] = "#{sender}"
            reply_to["FriendlyName"] = "#{sender}"

            messages["To"] = [message_to]
            messages["From"] = message_from
            messages["Subject"] = subject
            messages["HtmlBody"] = content
            #messages["HtmlBody"] = body
            messages["MailingId"] = nil
            messages["MessageId"] = sent_mail.id
            messages["MergeData"] = nil
            messages["CustomHeaders"] = nil
            messages["Cc"] =  nil
            messages["Bcc"] =  nil
            messages["ReplyTo"] = nil
            messages["Charset"] = nil
            messages["Attachments"] = nil

            result_args["ServerID"] = api_name
            result_args["ApiKey"] = api_key
            result_args["messages"] = [messages]
            result_args["Bcc"] =  nil
            result_args["ReplyTo"] = nil
            result_args["Charset"] = nil
            result_args["Attachments"] = nil

            result = HTTParty.post("https://inject.socketlabs.com/api/v1/email",
              :body => result_args.to_json,
              :headers => { 'Content-Type' => 'application/json' } )
            if result["ErrorCode"] == "Success"
              sent_mail.sent = true
              sent_mail.ext_id = sent_mail.id
              sent_mail.response_message = "Message sent via Socket Labs"
            else
              sent_mail.sent = false
              sent_mail.response_message = "Postage API failure - #{result["response"]["message"]}"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via SocketLabs."
          rescue => e
            sent_mail.sent = false
            sent_mail.response_message = "Message could not be sent. #{e.message}"
            sent_mail.save
            return false
          end

        when "elastic_email"
          begin
            result = HTTParty.post("https://api.elasticemail.com/mailer/send",
            #result = HTTParty.post("https://#{api_key}:#{api_name}@api.elasticemail.com/mailer/send",
                :body => { "username" => "#{api_name}",
                "api_key" => "#{api_key}",
                "from" => "#{sender}",
                "from_name" => "#{sender_name}",
                "to" => "#{recipient}",
                "subject" => "#{subject}",
                "body_html" => "#{content}",
                "reply_to" => "#{sender}",
                "reply_to_name" => "#{sender_name}"
                })

            unless result == /#^Error:/
              sent_mail.sent = true
                sent_mail.response_message = "Message sent via Elastic Email"
              sent_mail.ext_id = result
            else
              sent_mail.sent = false
                sent_mail.response_message = "#{result["response"]["message"]}"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via Elastic Email."
            sent_mail.ext_id = result
          rescue => e
            sent_mail.sent = false
            sent_mail.response_message = "Message could not be sent. #{e.message}"
            sent_mail.save

            return false
          end

        when "sparkpost"
          begin
            body = {
              recipients: [
                {
                  address: config["to_email"]
                }
              ],
              content: {
                from: {
                  email: config["from_email"],
                },
                subject: config["subject"],
                html: config["content"]
              }
            }

            result = HTTParty.post("https://api.sparkpost.com/api/v1/transmissions?num_rcpt_errors=3", body: body.to_json, headers: { "Authorization" => config['api_key'], 'Content-Type' => 'application/json'  })

            if result["id"].present?
              sent_mail.ext_id = result["id"]
              sent_mail.status = "sent"
            end

            sent_mail.sent = true
            sent_mail.response_message = "Message sent via SparkPost"
          rescue => e
            sent_mail.sent = false
            sent_mail.status = "error"
            sent_mail.response_message = "#{e.class} - #{e.message}"
            sent_mail.save
            return false
          end

        #when "postmark"
      end

      sent_mail.save
    else
      send_results["response_message"] = "No config parameters provided. Unable to proceed."
      return false
    end
    return true
  end

  def replace_links_in_message(msg, tracking_code)
    message = Nokogiri::HTML.fragment(msg)
    if tracking_code.include? "=3D"
      tracking_code == tracking_code.unpack('M')[0]
    end

    message.css('a[href]').each do |link|
    #################################################
    ## TODO: Change to SSL when implemented.
    #################################################
      link["href"] = "http://www.klickrr.net/lnks/#{tracking_code}?url=#{link['href']}"
    end

    return message.to_html
  end
end
