class MailSender::FollowupMailSender < MailSender::Base
  def initialize(args)
    @message_id = args[:message_id]

    @followup = Followup.find @message_id
    @subject  = @followup.subject
    @content  = @followup.content
    @list     = @followup.broadcasts.list

    @segments = nil
    @message_type = 'followup'
  end

  def perform
    super
    @followup.update(sent: Time.now, state: :completed)
  end
end
