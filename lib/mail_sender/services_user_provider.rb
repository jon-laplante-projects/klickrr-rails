class MailSender::ServicesUserProvider

  def initialize(list)
    @user = list.user
    @settings = @user.services_settings
    @index = @user.services_settings.evenly? ? -1 : 0
  end

  def set_scope(obj)
    @scope = ServicesUser.includes(:service).where(user_id: @user.id, used: true, service_id: obj.services.split(',')).order(:id).to_a
  end 

  def get_interface
    set_new_interface if contact_changed?
    @interface
  end

  def contact=(contact)
    raise MailTasks::NoAvailableServices.new if @scope.empty?
    @old_contact_id = @contact_id
    @contact_id = contact.object_id
  end

  private
  def set_new_interface
    if @settings.evenly?
      loop do
        go_next
        break if current_is_available?
      end
    elsif @settings.series?
      while !current_is_available?
        go_next
      end
    end
    @old_contact_id = @contact_id
    @interface = MailSender::LegacyMailInterface.new _current
  end

  def go_next
    @index >= @scope.count - 1 ? @index = 0 : @index += 1
  end

  def _current
    @scope[@index]
  end

  def current_is_available?
    service = _current.service
    mails_count = service.sent_mails_from(@user).this_month.count
    limit = _current.limit
    current_is_available = limit && mails_count < limit || !limit
    if @settings.autodeactivate? && limit && mails_count + 1 >= limit
      _current.update(used: false)
    end
    @scope.delete_at(@index) unless current_is_available
    current_is_available
  end

  def contact_changed?
    @old_contact_id != @contact_id
  end

end
