class MailSender::ListMailSender < MailSender::Base
  # args:
  #   message_id
  #   user_id
  def initialize(args)
    @user_id = args[:user_id]
    @test_message_b = args[:test_message_b]

    @sent_mail_helper =  MailSender::SentMailHelper.new(message_id:   @message_id,
                                                        message_type: @message_type,
                                                        user_id:      @user_id)

    @segments = get_segments
    @contact_loader ||=  MailSender::ContactLoader.new(list:             @list,
                                                       message_id:       @message_id,
                                                       message_type:     @message_type,
                                                       sent_mail_helper: @sent_mail_helper,
                                                       segments:         @segments)

    @services_user_provider =  MailSender::ServicesUserProvider.new(@list)

    @mail_builder =  MailSender::AbMailBuilder.new args.merge(sent_mail_helper: @sent_mail_helper,
                                                              list:             @list,
                                                              subject:          @subject,
                                                              content:          @content)
  end

  def perform
    loop do
      contacts = @contact_loader.get_batch
      break if contacts.blank?

      contacts.each do |contact|
        @services_user_provider.contact = contact
        interface = @services_user_provider.get_interface
        mail = @mail_builder.build_mail contact, interface

        after_sent if interface.send(mail)
      end
    end

    @test_message_b.update(sent: Time.now, state: :completed) if @test_message_b.present?
  end

  protected

  def after_sent
  end
end
