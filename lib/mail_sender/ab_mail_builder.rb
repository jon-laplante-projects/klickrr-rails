class MailSender::AbMailBuilder < MailSender::MailBuilder
  def initialize(args)
    super(args)
    @test_message_b = args[:test_message_b]
  end

  def build_mail(contact, service_interface)
    select_variant
    super(contact, service_interface)
  end

  protected

  def create_initial_sent_mail(contact, service_interface)
    if @test_message_b
      @sent_mail_helper.create_initial_sent_mail(
        message_id: @test_message_b.id,
        service_id: service_interface.services_user.id,
        contact_id: contact.id,
        email:      contact.email
      )
    else
      super(contact, service_interface)
    end
  end

  def get_content_for(contact, content_template = nil)
    super(contact, @selected_content)
  end

  def get_subject_for(contact, content_template = nil)
    super(contact, @selected_subject)
  end

  def select_variant
    if @test_message_b.blank? || Random.rand(2) == 1
      @selected_content    = @content
      @selected_subject    = @subject
    else
      @selected_content    = @test_message_b.content
      @selected_subject    = @test_message_b.subject
    end
  end
end
