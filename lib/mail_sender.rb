class MailSender
  def initialize(type, args={})
    sender_class = get_sender_by type
    if sender_class
      @sender = sender_class.new args
    else
      raise NotImplementedError, "Type '#{type}' is not implemented"
    end
  end

   def self.old_calling(id_val, send_type_val, args)
     args[:message_id] = id_val
     sender = MailSender.new send_type_val, args
     sender.send
   end

  def send
    @sender.perform
  end

  private

  SENDERS = {
    broadcast:                   BroadcastMailSender,
    followup:                    FollowupMailSender,
    test_message_from_broadcast: BroadcastTestMailSender
  }

  def get_sender_by(type)
    prepared_type = type.to_s.underscore.to_sym
    SENDERS[prepared_type]
  end
end
