desc "This task fixs message_type of SentMail instance"
task fix_message_type: :environment do
  SentMail.where(message_type: 'broadcast').find_each do |sent_mail|
    sent_mail.update_attribute(:message_type, 'Broadcast')
  end 
end