desc "This task checks perfoming of followups triggers"
task check_followups_triggers: :environment do
  [1, 3, 4, 8, 9].each { |id|  eval(FollowupTrigger.find(id).code) }
end