desc "This task checks new messages for delivary"
task delivery_of_messages: :environment do
  QueuedMessage.where(delivery_on: nil).find_each do |queued_message|
    UserMailer.followup_delivery(queued_message)
    queued_message.delivery_on = Time.now
    queued_message.save
  end
end