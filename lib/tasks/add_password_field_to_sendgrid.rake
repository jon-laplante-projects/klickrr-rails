desc "This task adds field Password for Sendgrid"
task add_password_field_to_sendgrid: :environment do
  service = Service.where(name: 'sendgrid').first
  ServiceField.create(service_id: service.id, label: 'Password', field_type: "password", name: 'password', required: true, field_options: "{\"data-toggle\": \"popover\", \"data-placement\": \"right\", \"title\": \"API Key\", \"data-content\": \"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"}")
end