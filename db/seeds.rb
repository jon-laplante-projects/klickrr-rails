# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if SegmentOperation.count == 0

  SegmentOperation.create(
    label: "is like",
    expression: "field_value LIKE '%{{value}}%'",
  )

  SegmentOperation.create(
    label: "is not like",
    expression: "field_value NOT LIKE '%{{value}}%'",
  )

  SegmentOperation.create(
    label: "equals",
    expression: "field_value = '{{value}}'",
  )

  SegmentOperation.create(
    label: "does not equal",
    expression: "field_value != '{{value}}'",
  )

  SegmentOperation.create(
    label: "is before",
    expression: "field_value < '{{value}}'",
  )

  SegmentOperation.create(
    label: "is after",
    expression: "field_value > '{{value}}'",
  )

  SegmentOperation.create(
    label: "is on",
    expression: "field_value = '{{value}}'",
  )

  SegmentOperation.create(
    label: "is bigger than",
    expression: "field_value < '{{value}}'",
  )

  SegmentOperation.create(
    label: "is smaller than",
    expression: "field_value > '{{value}}'",
  )

end

unless Service.where(label: 'SparkPost').exists?
  sparkpost = Service.create(label: 'SparkPost', url: 'https://www.sparkpost.com/', description: 'SparkPost is the email delivery service based on the Momentum platform from Message Systems. Get the same deliverability, scalability and speed as we provide to the biggest senders in the world, in an easily integrated cloud service, while at the same time ensuring email best practices compliance.', name: 'sparkpost', is_smtp: false, service_type_id: 1)
  ServiceField.create(service_id: sparkpost.id, label: "From Email", field_type: "text", required: true, field_options: "{\"data-toggle\": \"popover\", \"data-placement\": \"right\", \"title\": \"From Name\", \"data-content\": \"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"}", name: "from_email")
  ServiceField.create(service_id: sparkpost.id, label: "API Key", field_type: "text", required: true, field_options: "{\"data-toggle\": \"popover\", \"data-placement\": \"right\", \"title\": \"From Name\", \"data-content\": \"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"}", name: "api_key")
end