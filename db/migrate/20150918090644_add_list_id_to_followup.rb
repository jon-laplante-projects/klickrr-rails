class AddListIdToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :list_id, :integer
  end
end
