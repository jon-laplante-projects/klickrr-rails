class AddTriggerIdToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :trigger_id, :integer
  end
end
