class RemoveListFieldGroupIdFromListField < ActiveRecord::Migration
  def change
  	remove_column :list_fields, :list_field_group_id
  end
end
