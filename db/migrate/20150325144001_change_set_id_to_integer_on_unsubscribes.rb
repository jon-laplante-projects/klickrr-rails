class ChangeSetIdToIntegerOnUnsubscribes < ActiveRecord::Migration
  def change
  	remove_column :unsubscribes, :set_id
  	add_column  :unsubscribes, :set_id, :integer
  end
end
