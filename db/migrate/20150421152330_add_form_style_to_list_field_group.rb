class AddFormStyleToListFieldGroup < ActiveRecord::Migration
  def change
    add_column :list_field_groups, :form_style, :text
  end
end
