class CreateFollowupMessagePart < ActiveRecord::Migration
  def change
    create_table :followup_message_parts do |t|
      t.integer :followup_id
      t.integer :delay
      t.string :subject
      t.text :message
    end
  end
end
