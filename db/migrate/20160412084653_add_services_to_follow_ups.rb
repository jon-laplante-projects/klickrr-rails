class AddServicesToFollowUps < ActiveRecord::Migration
  def change
    change_table :followups do |t|
      t.string :services
    end 
  end
end
