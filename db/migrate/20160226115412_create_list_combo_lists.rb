class CreateListComboLists < ActiveRecord::Migration
  def change
    create_table :list_combo_lists do |t|
      t.integer :list_id
      t.integer :list_combo_id

      t.timestamps null: false
    end
  end
end
