class DropSegments < ActiveRecord::Migration
  def change
    execute "DROP TABLE #{:segments} CASCADE"
  end
end
