class AddScopeIdToFollowupTrigger < ActiveRecord::Migration
  def change
    add_column :followup_triggers, :scope_id, :integer
  end
end
