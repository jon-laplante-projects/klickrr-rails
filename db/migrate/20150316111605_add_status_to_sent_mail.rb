class AddStatusToSentMail < ActiveRecord::Migration
  def change
    add_column :sent_mails, :status, :string
  end
end
