class AddSentCountAndStateToBroadcast < ActiveRecord::Migration
  def change
    add_column :broadcasts, :sent_count, :integer, default: 0
    add_column :broadcasts, :state, :integer, default: 0
  end
end
