class CreateSentEmailFollowups < ActiveRecord::Migration
  def change
    create_table :sent_email_followups do |t|
      t.integer :sent_mail_id
      t.integer :followup_id
      t.datetime :sent_at

      t.timestamps null: false
    end
  end
end
