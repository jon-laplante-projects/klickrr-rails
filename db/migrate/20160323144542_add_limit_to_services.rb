class AddLimitToServices < ActiveRecord::Migration
  def change
    add_column :services, :limit, :integer, default: 1000, null: false
    add_column :services_users, :limit, :integer, default: nil
  end
end
