class CreateFollowups < ActiveRecord::Migration
  def change
    create_table :followups do |t|
      t.integer :broadcast_id
      t.string :subject
      t.text :content
      t.integer :delay_days

      t.timestamps
    end
  end
end
