class RenameSegmentTable < ActiveRecord::Migration
  def change
    rename_table :segments_tables, :segments
  end
end
