class ChangeSentMailsFields < ActiveRecord::Migration
  def change
    change_table :sent_mails do |t|
      t.change :opened, :text
      t.change :clicks, :text
      t.change :unsubscribed, :text
      t.remove :ip_address
    end
  end
end
