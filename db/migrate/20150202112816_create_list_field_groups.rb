class CreateListFieldGroups < ActiveRecord::Migration
  def change
    create_table :list_field_groups do |t|
      t.integer :list_id
      t.string :label

      t.timestamps
    end
  end
end
