class CreateSegmentTypes < ActiveRecord::Migration
  def change
    create_table :segment_types do |t|
      t.text :description
      t.text :base_sql
      t.text :variables

      t.timestamps null: false
    end
  end
end
