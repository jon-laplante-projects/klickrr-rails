class CreateFollowupQueues < ActiveRecord::Migration
  def change
    create_table :followup_queues do |t|
      t.integer :list_id
      t.integer :list_field_group_id
      t.integer :set_id
      t.integer :followup_id
      t.string :email

      t.timestamps null: false
    end
  end
end
