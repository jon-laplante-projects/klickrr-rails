class ChangeScopeIdToFollowupScopeIdOnFollowupScopesTriggers < ActiveRecord::Migration
  def change
    rename_column :followup_scopes_triggers, :scope_id, :followup_scope_id
  end
end
