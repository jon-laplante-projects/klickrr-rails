class CreateTableOpenedMails < ActiveRecord::Migration
  def change
    create_table :opened_mails do |t|
      t.references :message, polymorphic: true, index: true
      t.string :country
      t.string :city
      t.string :timezone
      t.string :browser
      t.string :operating_system
      t.timestamps null: false
    end
  end
end
