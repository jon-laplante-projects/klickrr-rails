class AlterOpenedAndDroppedAndDeliveredAndBouncedAndUnsubscribedAndSpamAndBlocked < ActiveRecord::Migration
  def change
  	#alter_table :sent_mails do |t|
	  	remove_column :sent_mails, :opened
	  	remove_column :sent_mails, :dropped
	  	remove_column :sent_mails, :delivered
	  	remove_column :sent_mails, :bounced
	  	remove_column :sent_mails, :unsubscribed
	  	remove_column :sent_mails, :spam
	  	remove_column :sent_mails, :blocked

	  	add_column :sent_mails, :opened, :datetime
	  	add_column :sent_mails, :dropped, :datetime
	  	add_column :sent_mails, :delivered, :datetime
	  	add_column :sent_mails, :bounced, :datetime
	  	add_column :sent_mails, :unsubscribed, :datetime
	  	add_column :sent_mails, :spam, :datetime
	  	add_column :sent_mails, :blocked, :datetime
  	#end
  end
end
