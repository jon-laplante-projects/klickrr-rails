class AddIpAddressToSentMails < ActiveRecord::Migration
  def change
    add_column :sent_mails, :ip_address, :string
  end
end
