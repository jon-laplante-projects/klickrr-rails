class RenameTimezoneIdToTimezoneOnBroadcast < ActiveRecord::Migration
  def change
  	rename_column :broadcasts, :timezone_id, :timezone
  end
end
