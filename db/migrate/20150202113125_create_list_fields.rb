class CreateListFields < ActiveRecord::Migration
  def change
    create_table :list_fields do |t|
      t.integer :list_field_group_id
      t.string :label
      t.string :field_type
      t.boolean :required
      t.text :field_options
      t.text :description
      t.boolean :include_other
      t.boolean :include_blank
      t.boolean :integer_only
      t.integer :min
      t.integer :max

      t.timestamps
    end
  end
end
