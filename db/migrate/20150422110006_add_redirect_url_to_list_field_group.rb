class AddRedirectUrlToListFieldGroup < ActiveRecord::Migration
  def change
    add_column :list_field_groups, :redirect_url, :string
  end
end
