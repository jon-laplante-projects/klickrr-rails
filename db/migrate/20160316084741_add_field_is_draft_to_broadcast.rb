class AddFieldIsDraftToBroadcast < ActiveRecord::Migration
  def change
    change_table :broadcasts do |t|
      t.boolean :is_draft
      t.integer :user_id, index: true
    end
  end
end
