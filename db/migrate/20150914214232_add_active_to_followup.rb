class AddActiveToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :active, :boolean, default: false
  end
end
