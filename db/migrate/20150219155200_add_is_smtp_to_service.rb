class AddIsSmtpToService < ActiveRecord::Migration
  def change
    add_column :services, :is_smtp, :boolean
  end
end
