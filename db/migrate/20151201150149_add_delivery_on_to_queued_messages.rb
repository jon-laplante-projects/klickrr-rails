class AddDeliveryOnToQueuedMessages < ActiveRecord::Migration
  def change
    add_column :queued_messages, :delivery_on, :datetime
  end
end
