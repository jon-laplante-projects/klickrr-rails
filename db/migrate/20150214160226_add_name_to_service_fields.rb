class AddNameToServiceFields < ActiveRecord::Migration
  def change
    add_column :service_fields, :name, :string
  end
end
