class CreateSegmentsTable < ActiveRecord::Migration
  def change
    create_table :segments_tables do |t|
      t.integer :segment_operator_id
      t.integer :field_id
      t.integer :list_id
      t.integer :user_id
      t.string :condition_value
      t.text :sql
    end
  end
end
