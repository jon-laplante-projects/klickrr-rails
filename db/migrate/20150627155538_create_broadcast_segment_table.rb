class CreateBroadcastSegmentTable < ActiveRecord::Migration
  def change
    create_view :broadcast_segments, 'SELECT id, user_id, segment_id, action_id AS "broadcast_id", created_at, updated_at FROM segments WHERE seg_type=\'broadcast\';'
  end
end
