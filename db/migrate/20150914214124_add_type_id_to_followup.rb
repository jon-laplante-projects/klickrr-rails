class AddTypeIdToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :type_id, :integer
  end
end
