class AddReferenceToMessageForSegments < ActiveRecord::Migration
  def change
  	change_table :segments do |t|
      t.references :message, polymorphic: true, index: true
    end
  end
end
