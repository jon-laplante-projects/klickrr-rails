class AddFieldsToListFieldGroups < ActiveRecord::Migration
  def up
    add_column :list_field_groups, :inline_styles, :text
    add_column :list_field_groups, :custom_email_message, :text
    add_column :list_field_groups, :is_old_form, :boolean, default: false
    ListFieldGroup.update_all(is_old_form: true)
  end

  def drown
    remove_column :list_field_groups, :inline_styles
    remove_column :list_field_groups, :custom_email_message
    remove_column :list_field_groups, :is_old_form
  end
end
