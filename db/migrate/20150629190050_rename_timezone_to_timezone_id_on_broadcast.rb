class RenameTimezoneToTimezoneIdOnBroadcast < ActiveRecord::Migration
  def change
  	rename_column :broadcasts, :timezone, :timezone_id
  end
end
