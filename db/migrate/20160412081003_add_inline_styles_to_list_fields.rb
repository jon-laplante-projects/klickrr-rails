class AddInlineStylesToListFields < ActiveRecord::Migration
  def change
    add_column :list_fields, :inline_styles, :text
  end
end
