class CreateBroadcasts < ActiveRecord::Migration
  def change
    create_table :broadcasts do |t|
      t.integer :list_id
      t.string :subject
      t.text :content
      t.datetime :push_at
      t.datetime :sent

      t.timestamps
    end
  end
end
