class CreateFollowupSegmentView < ActiveRecord::Migration
  def change
    create_view :followup_segments, 'SELECT id, user_id, segment_id, action_id AS "followup_id", created_at, updated_at FROM segments WHERE seg_type=\'followup\';'
  end
end
