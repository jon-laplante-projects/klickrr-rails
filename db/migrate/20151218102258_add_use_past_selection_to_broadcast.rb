class AddUsePastSelectionToBroadcast < ActiveRecord::Migration
  def change
    add_column :broadcasts, :use_past_selection, :boolean, default: false
  end
end
