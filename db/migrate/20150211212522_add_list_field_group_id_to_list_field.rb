class AddListFieldGroupIdToListField < ActiveRecord::Migration
  def change
    add_column :list_fields, :list_field_group_id, :integer
  end
end
