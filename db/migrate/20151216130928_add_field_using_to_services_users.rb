class AddFieldUsingToServicesUsers < ActiveRecord::Migration
  def change
    add_column :services_users, :used, :boolean
  end
end
