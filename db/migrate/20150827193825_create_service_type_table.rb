class CreateServiceTypeTable < ActiveRecord::Migration
  def change
    create_table :service_types do |t|
      t.string :label
      t.string :name
    end
  end
end
