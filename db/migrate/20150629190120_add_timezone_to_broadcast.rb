class AddTimezoneToBroadcast < ActiveRecord::Migration
  def change
    add_column :broadcasts, :timezone, :decimal
  end
end
