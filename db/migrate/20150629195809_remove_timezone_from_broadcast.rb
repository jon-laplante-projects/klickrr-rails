class RemoveTimezoneFromBroadcast < ActiveRecord::Migration
  def change
  	remove_column :broadcasts, :timezone
  end
end
