class AddMessageTypeToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :message_type, :string
  end
end
