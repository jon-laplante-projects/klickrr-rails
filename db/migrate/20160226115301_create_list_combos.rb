class CreateListCombos < ActiveRecord::Migration
  def change
    create_table :list_combos do |t|
      t.string :label

      t.timestamps null: false
    end
  end
end
