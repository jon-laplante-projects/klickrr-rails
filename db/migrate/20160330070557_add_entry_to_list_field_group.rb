class AddEntryToListFieldGroup < ActiveRecord::Migration
  def change
    change_table :list_field_groups do |t|
      t.integer :entries
    end  
  end
end
