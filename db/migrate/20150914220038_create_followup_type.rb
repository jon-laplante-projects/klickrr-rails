class CreateFollowupType < ActiveRecord::Migration
  def change
    create_table :followup_types do |t|
      t.text :desc_html
      t.string :name
      t.boolean :multipart
    end
  end
end
