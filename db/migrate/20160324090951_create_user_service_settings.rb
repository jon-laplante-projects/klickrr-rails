class CreateUserServiceSettings < ActiveRecord::Migration
  def up
    create_table :user_services_settings do |t|
      t.integer :rotation, default: 0
      t.boolean :autodeactivate, default: true
      t.belongs_to :user, index: true
    end
    User.find_each {|user| user.create_services_settings }
  end

  def down
    drop_table :user_services_settings
  end
end
