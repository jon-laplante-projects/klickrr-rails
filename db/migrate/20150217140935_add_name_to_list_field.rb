class AddNameToListField < ActiveRecord::Migration
  def change
    add_column :list_fields, :name, :string
  end
end
