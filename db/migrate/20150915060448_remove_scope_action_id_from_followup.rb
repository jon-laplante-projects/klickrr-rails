class RemoveScopeActionIdFromFollowup < ActiveRecord::Migration
  def change
    remove_column :followups, :scope_action_id, :integer
  end
end
