class CreateSegmentOperation < ActiveRecord::Migration
  def change
    create_table :segment_operations do |t|
      t.string :label
      t.string :expression
    end
  end
end
