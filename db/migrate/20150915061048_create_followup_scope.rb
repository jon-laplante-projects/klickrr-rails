class CreateFollowupScope < ActiveRecord::Migration
  def change
    create_table :followup_scopes do |t|
      t.string :label
      t.string :name
    end
  end
end
