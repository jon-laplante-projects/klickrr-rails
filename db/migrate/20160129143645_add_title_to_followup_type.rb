class AddTitleToFollowupType < ActiveRecord::Migration
  def change
    add_column :followup_types, :title, :string
    
    type1 = FollowupType.where(name: "one_time_msg").last
    type1.update_attribute(:title, "One time") if type1

    type2 = FollowupType.where(name: "multi_part_msg").last 
    type2.update_attribute(:title, "Multi-part") if type2
  end
end
