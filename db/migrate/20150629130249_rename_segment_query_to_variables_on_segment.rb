class RenameSegmentQueryToVariablesOnSegment < ActiveRecord::Migration
  def change
  	rename_column :segments, :segment_query, :variables
  end
end
