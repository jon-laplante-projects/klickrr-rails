class AddNegativeToSegmentType < ActiveRecord::Migration
  def change
    add_column :segment_types, :negative, :boolean
  end
end
