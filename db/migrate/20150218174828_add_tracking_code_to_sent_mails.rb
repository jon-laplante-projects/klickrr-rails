class AddTrackingCodeToSentMails < ActiveRecord::Migration
  def change
    add_column :sent_mails, :tracking_code, :string
  end
end
