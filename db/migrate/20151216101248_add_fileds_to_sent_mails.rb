class AddFieldsToSentMails < ActiveRecord::Migration
  def change
  	add_column :sent_mails, :country, :string
  	add_column :sent_mails, :city, :string
  	add_column :sent_mails, :timezone, :string
  	add_column :sent_mails, :browser, :string
  	add_column :sent_mails, :operating_system, :string
  end
end
