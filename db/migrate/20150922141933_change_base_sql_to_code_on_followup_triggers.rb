class ChangeBaseSqlToCodeOnFollowupTriggers < ActiveRecord::Migration
  def change
    rename_column :followup_triggers, :base_sql, :code
  end
end
