class AddTriggerTypeIdToFollowupTriggers < ActiveRecord::Migration
  def change
    add_column :followup_triggers, :trigger_type_id, :integer
  end
end
