class ChangeTriggerIdToFollowupTriggerIdOnFollowupScopesTriggers < ActiveRecord::Migration
  def change
    rename_column :followup_scopes_triggers, :trigger_id, :followup_trigger_id
  end
end
