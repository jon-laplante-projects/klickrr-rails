class RemoveScopeTypeFromFollowup < ActiveRecord::Migration
  def change
    remove_column :followups, :scope_type, :string
  end
end
