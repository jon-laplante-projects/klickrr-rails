class ChangeBroadcastIdToMessageIdOnFollowup < ActiveRecord::Migration
  def change
  	rename_column :followups, :broadcast_id, :message_id
  end
end
