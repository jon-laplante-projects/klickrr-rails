class RenameNameToLabelInServicesTable < ActiveRecord::Migration
  def change
  	rename_column :services, :name, :label
  end
end
