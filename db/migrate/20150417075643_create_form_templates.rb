class CreateFormTemplates < ActiveRecord::Migration
  def change
    create_table :form_templates do |t|
      t.string :name
      t.string :label
      t.text :markup

      t.timestamps null: false
    end
  end
end
