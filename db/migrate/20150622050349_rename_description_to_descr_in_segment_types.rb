class RenameDescriptionToDescrInSegmentTypes < ActiveRecord::Migration
  def change
  	rename_column :segment_types, :description, :descr
  end
end
