class AddSenderAndSenderNameAndDomainToList < ActiveRecord::Migration
  def change
    add_column :lists, :sender, :string
    add_column :lists, :sender_email, :string
    add_column :lists, :domain, :string
  end
end
