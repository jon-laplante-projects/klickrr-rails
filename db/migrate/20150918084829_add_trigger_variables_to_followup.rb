class AddTriggerVariablesToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :trigger_variables, :string
  end
end
