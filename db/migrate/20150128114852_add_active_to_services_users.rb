class AddActiveToServicesUsers < ActiveRecord::Migration
  def change
    add_column :services_users, :active, :boolean
  end
end
