class AddUtcSendOnToBroadcast < ActiveRecord::Migration
  def change
    add_column :broadcasts, :utc_send_on, :datetime
  end
end
