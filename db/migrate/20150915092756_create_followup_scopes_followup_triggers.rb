class CreateFollowupScopesFollowupTriggers < ActiveRecord::Migration
  def change
    create_table :followup_scopes_followup_triggers do |t|
      t.integer :scope_id
      t.integer :trigger_id
    end
  end
end
