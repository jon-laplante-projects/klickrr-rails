class AddScopeIdToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :scope_id, :integer
  end
end
