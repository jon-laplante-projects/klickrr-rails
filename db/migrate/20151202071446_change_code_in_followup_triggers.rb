class ChangeCodeInFollowupTriggers < ActiveRecord::Migration
  def change
    FollowupTrigger.where(descr: "No action on sent email in {{x}} days.").find_each do |trigger|
      trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}, active: true).find_each do |followup|
        variables = eval(followup.trigger_variables)
        sent_during_range = followup.sent_mails.where(\"clicks IS NULL AND opened IS NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at > ? AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)\", Time.now - variables[\"x\"].to_i.days, followup.id)
        if sent_during_range.present?
          followup.followup_message_parts.each do |msg|
            queued_message = QueuedMessage.new
            queued_message.addr = followup.user.email
            queued_message.followup_id = followup.id
            queued_message.subject = msg.subject
            queued_message.message = msg.message
            queued_message.send_on = Time.now
            queued_message.save
          end
        end
      end")
    end  

    FollowupTrigger.where(descr: "Link clicked.").find_each do |trigger|
      trigger.update_attribute(:code, "mail.followups.where(trigger_id: #{trigger.id}).find_each do |followup|
        followup.followup_message_parts.each do |msg|
          queued_message = QueuedMessage.new
          queued_message.addr = followup.user.email
          queued_message.followup_id = followup.id
          queued_message.subject = msg.subject
          queued_message.message = msg.message
          queued_message.send_on = Time.now
          queued_message.save
        end
      end")
    end  

    FollowupTrigger.where(descr: "Email opened, no links clicked.").find_each do |trigger|
        trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}).find_each do |followup|
          if followup.sent_mails.where(clicks: nil).present?
            followup.followup_message_parts.each do |msg|
              queued_message = QueuedMessage.new
              queued_message.addr = followup.user.email
              queued_message.followup_id = followup.id
              queued_message.subject = msg.subject
              queued_message.message = msg.message
              queued_message.send_on = Time.now
              queued_message.save
            end
          end
        end")
    end  


    FollowupTrigger.where(descr: "Email opened, no links clicked.").find_each do |trigger|
        trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}).find_each do |followup|
          variables = eval(followup.trigger_variables)
          sent_mails = followup.user.sent_mails.where(\"sent_mails.created_at > ?\", Time.now - variables[\"x\"].to_i.days)
          if sent_mails.present?
            followup.followup_message_parts.each do |msg|
              queued_message = QueuedMessage.new
              queued_message.addr = followup.user.email
              queued_message.followup_id = followup.id
              queued_message.subject = msg.subject
              queued_message.message = msg.message
              queued_message.send_on = Time.now
              queued_message.save
            end
          end
        end")
    end    


    FollowupTrigger.where(descr: "Subscriber unsubscribed from list.").find_each do |trigger|
      trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}).find_each do |followup|
        followup.followup_message_parts.each do |msg|
          queued_message = QueuedMessage.new
          queued_message.addr = followup.user.email
          queued_message.followup_id = followup.id
          queued_message.subject = msg.subject
          queued_message.message = msg.message
          queued_message.send_on = Time.now
          queued_message.save
        end
      end")
    end    

    FollowupTrigger.where(descr: "{{x}} or more links in an email clicked within {{y}} days.").find_each do |trigger|
      trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}).find_each do |followup|
        variables = eval(followup.trigger_variables)
        sent_mails = followup.user.sent_mails.where(\"sent_mails.created_at < ?\", Time.now - variables[\"y\"].to_i.days).select { |sent_mail| sent_mail.clicks && eval(sent_mail.clicks).count >= variables[\"x\"].to_i }
        if sent_mails.present?
          followup.followup_message_parts.each do |msg|
            queued_message = QueuedMessage.new
            queued_message.addr = followup.user.email
            queued_message.followup_id = followup.id
            queued_message.subject = msg.subject
            queued_message.message = msg.message
            queued_message.send_on = Time.now
            queued_message.save
          end
        end
      end")
    end  

    FollowupTrigger.where(descr: "{{x}} or more links in an email clicked after {{y}} days.").find_each do |trigger|
      trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}).find_each do |followup|
        variables = eval(followup.trigger_variables)
        sent_mails = followup.sent_mails.where('sent_mails.created_at > ?',  Time.now - variables[\"y\"].to_i.days).select { |sent_mail| sent_mail.clicks && eval(sent_mail.clicks).count >= variables[\"x\"].to_i }
        if sent_mails.present?
          followup.followup_message_parts.each do |msg|
            queued_message = QueuedMessage.new
            queued_message.addr = followup.user.email
            queued_message.followup_id = followup.id
            queued_message.subject = msg.subject
            queued_message.message = msg.message
            queued_message.send_on =
            queued_message.save
          end
        end
      end")
    end  

    FollowupTrigger.where(descr: "{{x}} days since broadcast was sent.").find_each do |trigger|
      trigger.update_attribute(:code, "Followup.where(trigger_id: #{trigger.id}, active: true).find_each do |followup|
        variables = eval(followup.trigger_variables)
        sent_during_range = followup.sent_mails.where(\"clicks IS NULL AND opened IS NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at > ? AND message_type = 'broadcast' AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)\", Time.now - variables[\"x\"].to_i.days, followup.id)
        if sent_during_range.present?
          followup.followup_message_parts.each do |msg|
            queued_message = QueuedMessage.new
            queued_message.addr = followup.user.email
            queued_message.followup_id = followup.id
            queued_message.subject = msg.subject
            queued_message.message = msg.message
            queued_message.send_on = Time.now
            queued_message.save
          end
        end
      end")
    end

  end
end
