class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :trans_type, :string
    add_column :users, :hash_key, :string
    add_column :users, :trans_gateway, :string
    add_column :users, :trans_date, :datetime
    add_column :users, :trans_testmode, :boolean
    add_column :users, :seller_id, :string
    add_column :users, :seller_email, :string
    add_column :users, :name, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :country, :string
    add_column :users, :status, :string
  end
end
