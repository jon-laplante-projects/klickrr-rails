class CreateSegments < ActiveRecord::Migration
  def change
    create_table :segments do |t|
      t.integer :user_id
      t.integer :segment_id
      t.integer :list_id
      t.text :segment_query

      t.timestamps null: false
    end
  end
end
