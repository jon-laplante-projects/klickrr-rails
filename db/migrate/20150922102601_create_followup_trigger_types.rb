class CreateFollowupTriggerTypes < ActiveRecord::Migration
  def change
    create_table :followup_trigger_types do |t|
      t.string :name
    end
  end
end
