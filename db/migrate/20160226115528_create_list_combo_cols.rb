class CreateListComboCols < ActiveRecord::Migration
  def change
    create_table :list_combo_cols do |t|
      t.integer :list_combo_id
      t.string :label
      t.string :name

      t.timestamps null: false
    end
  end
end
