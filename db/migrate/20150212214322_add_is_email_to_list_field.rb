class AddIsEmailToListField < ActiveRecord::Migration
  def change
    add_column :list_fields, :is_email, :boolean
  end
end
