class CreateListValues < ActiveRecord::Migration
  def change
    create_table :list_values do |t|
      t.integer :list_field_id
      t.text :field_value

      t.timestamps
    end
  end
end
