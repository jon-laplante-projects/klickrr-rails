class CreateQueuedMessages < ActiveRecord::Migration
  def change
    create_table :queued_messages do |t|
      t.integer :followup_id
      t.string :subject
      t.text :message
      t.string :addr
      t.date :send_on
    end
  end
end
