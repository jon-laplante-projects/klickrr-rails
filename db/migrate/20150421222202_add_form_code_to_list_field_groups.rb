class AddFormCodeToListFieldGroups < ActiveRecord::Migration
  def change
    add_column :list_field_groups, :form_code, :string
  end
end
