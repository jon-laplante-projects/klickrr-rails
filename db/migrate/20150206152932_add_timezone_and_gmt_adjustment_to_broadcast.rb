class AddTimezoneAndGmtAdjustmentToBroadcast < ActiveRecord::Migration
  def change
    add_column :broadcasts, :timezone, :integer
    add_column :broadcasts, :gmtAdjustment, :decimal
  end
end
