class AddSentAndResponseMessageToSentMail < ActiveRecord::Migration
  def change
    add_column :sent_mails, :sent, :boolean
    add_column :sent_mails, :response_message, :string
  end
end
