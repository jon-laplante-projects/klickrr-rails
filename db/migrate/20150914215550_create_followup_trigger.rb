class CreateFollowupTrigger < ActiveRecord::Migration
  def change
    create_table :followup_triggers do |t|
      t.text :descr
      t.text :base_sql
      t.text :variables
      t.boolean :negative
    end
  end
end
