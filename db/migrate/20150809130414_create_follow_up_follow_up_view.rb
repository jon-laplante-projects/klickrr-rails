class CreateFollowUpFollowUpView < ActiveRecord::Migration
  def change
    create_view :followup_followups, "SELECT id, message_id AS \"followoup_id\", subject, content, delay_days, created_at, updated_at FROM followups WHERE message_type = 'followup';"
  end
end
