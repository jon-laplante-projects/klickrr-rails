class AddListFieldGroupIdToListValue < ActiveRecord::Migration
  def change
    add_column :list_values, :list_field_group_id, :integer
  end
end
