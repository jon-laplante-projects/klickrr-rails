class RenameFollowupScopesFollowupTriggersToFollowupScopesTriggers < ActiveRecord::Migration
  def change
    rename_table :followup_scopes_followup_triggers, :followup_scopes_triggers
  end
end
