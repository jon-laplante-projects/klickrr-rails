class AddListIdToListField < ActiveRecord::Migration
  def change
    add_column :list_fields, :list_id, :integer
  end
end
