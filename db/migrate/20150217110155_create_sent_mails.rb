class CreateSentMails < ActiveRecord::Migration
  def change
    create_table :sent_mails do |t|
      t.integer :user_id
      t.integer :service_id
      t.integer :message_id
      t.string :message_type
      t.string :email
      t.boolean :opened
      t.text :clicks
      t.boolean :dropped
      t.boolean :delivered
      t.boolean :bounced
      t.boolean :unsubscribed
      t.boolean :spam
      t.boolean :blocked

      t.timestamps
    end
  end
end
