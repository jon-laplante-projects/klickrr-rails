class AddLinkStringToList < ActiveRecord::Migration
  def change
    add_column :lists, :link_string, :string
  end
end
