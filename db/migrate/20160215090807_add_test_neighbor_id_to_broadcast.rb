class AddTestNeighborIdToBroadcast < ActiveRecord::Migration
  def change
  	add_column :broadcasts, :test_neighbor_id, :integer
  	add_index :broadcasts, :test_neighbor_id
  end
end
