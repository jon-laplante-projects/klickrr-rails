class CreateServicesUsers < ActiveRecord::Migration
  def change
    create_table :services_users do |t|
      t.integer :user_id
      t.integer :service_id
      t.text :usage

      t.timestamps
    end
  end
end
