class RenameDelayToDelayDaysOnFollowupMessageParts < ActiveRecord::Migration
  def change
    rename_column :followup_message_parts, :delay, :delay_days
  end
end
