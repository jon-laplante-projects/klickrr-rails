class AddScopeTypeAndScopeActionIdToFollowup < ActiveRecord::Migration
  def change
    add_column :followups, :scope_type, :string
    add_column :followups, :scope_action_id, :integer
  end
end
