class RenameUtcSendOnToUtcPushAtOnBroadcast < ActiveRecord::Migration
  def change
  	rename_column :broadcasts, :utc_send_on, :utc_push_at
  end
end
