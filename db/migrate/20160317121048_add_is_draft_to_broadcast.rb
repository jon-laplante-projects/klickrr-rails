class AddIsDraftToBroadcast < ActiveRecord::Migration
  def change
    change_column :broadcasts, :is_draft, :boolean, default: false
  end
end
