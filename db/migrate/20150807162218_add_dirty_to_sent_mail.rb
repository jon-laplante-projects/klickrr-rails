class AddDirtyToSentMail < ActiveRecord::Migration
  def change
    add_column :sent_mails, :dirty, :boolean
  end
end
