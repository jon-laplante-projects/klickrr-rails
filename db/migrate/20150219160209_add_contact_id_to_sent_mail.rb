class AddContactIdToSentMail < ActiveRecord::Migration
  def change
    add_column :sent_mails, :contact_id, :integer
  end
end
