class CreateServiceFields < ActiveRecord::Migration
  def change
    create_table :service_fields do |t|
      t.integer :service_id
      t.string :label
      t.string :field_type
      t.boolean :required
      t.text :field_options
      t.text :description
      t.integer :min
      t.integer :max

      t.timestamps
    end
  end
end
