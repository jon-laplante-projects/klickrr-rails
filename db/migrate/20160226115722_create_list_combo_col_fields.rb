class CreateListComboColFields < ActiveRecord::Migration
  def change
    create_table :list_combo_col_fields do |t|
      t.integer :list_combo_col_id
      t.integer :list_field_id

      t.timestamps null: false
    end
  end
end
