class AddServicesToBroadcasts < ActiveRecord::Migration
  def change
    change_table :broadcasts do |t|
      t.string :services
    end 
  end
end
