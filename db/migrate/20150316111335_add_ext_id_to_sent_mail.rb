class AddExtIdToSentMail < ActiveRecord::Migration
  def change
    add_column :sent_mails, :ext_id, :string
  end
end
