class ChangeSegmentQueryToSegTypeInSegment < ActiveRecord::Migration
  def change
  	rename_column :segments, :list_id, :action_id
  end
end
