class CreateBroadcastFollowUpView < ActiveRecord::Migration
  def change
    create_view :broadcast_followups, "SELECT id, message_id AS \"broadcast_id\", subject, content, delay_days, created_at, updated_at FROM followups WHERE message_type = 'broadcast';"
  end
end
