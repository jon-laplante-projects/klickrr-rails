class AddPreviewImageToFormTemplate < ActiveRecord::Migration
  def change
    add_column :form_templates, :preview_image, :string
  end
end
