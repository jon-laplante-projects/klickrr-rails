class CreateUnsubscribes < ActiveRecord::Migration
  def change
    create_table :unsubscribes do |t|
      t.string :set_id

      t.timestamps null: false
    end
  end
end
