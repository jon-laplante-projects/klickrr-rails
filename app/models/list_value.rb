# == Schema Information
#
# Table name: list_values
#
#  id                  :integer          not null, primary key
#  list_field_id       :integer
#  field_value         :text
#  created_at          :datetime
#  updated_at          :datetime
#  set_id              :integer
#  list_field_group_id :integer
#

class ListValue < ActiveRecord::Base
	belongs_to :list_field
	has_one :list, through: :list_field
end
