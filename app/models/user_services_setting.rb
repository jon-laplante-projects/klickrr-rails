class UserServicesSetting < ActiveRecord::Base
  enum rotation: [:series, :evenly]

  validates :user_id, presence: true
end
