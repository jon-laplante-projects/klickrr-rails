class Service < ActiveRecord::Base
	belongs_to :service_type
	has_many :service_fields, dependent: :destroy
	has_many :services_users, dependent: :destroy
	has_many :sent_mails, dependent: :restrict_with_exception

	def sent_mails_from(user)
		sent_mails.sent_from(user)
	end
end
