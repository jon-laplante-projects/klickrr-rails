class Followup < ActiveRecord::Base
  include ApplicationHelper
  belongs_to :type, foreign_key: :type_id, class_name: "FollowupType"
  belongs_to :trigger, foreign_key: :trigger_id, class_name: "FollowupTrigger"
  belongs_to :scope, foreign_key: :scope_id, class_name: "FollowupScope"
  has_many :followup_message_parts, dependent: :destroy
  has_many :queued_messages, dependent: :destroy
  has_many :sent_mails, through: :user
  belongs_to :user
  has_many :opened_mail, as: :message
  has_many :sent_email_followups, dependent: :destroy
  belongs_to :message, polymorphic: true

  def trigger_description
    description = self.trigger.try(:descr)
    variables = self.trigger_variables
    read_vars(variables).each { |key, value| p key; p value; description.gsub!("{{#{key}}}", value.to_s) } if description.present? && variables.present?
    description
  end

  def parse_trigger_variables
    read_vars(self.trigger_variables)
  end

  def inheritance_services
    if message_type = 'Broadcast'
      update_attribute(:services, message.services)
    else
      update_attribute(:services, message.list.services)
    end     
  end  

  def services_instances(attrs='*')
    Service.where(id: services.try(:split,',')).select(attrs)
  end  

end
