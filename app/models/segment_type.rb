# == Schema Information
#
# Table name: segment_types
#
#  id         :integer          not null, primary key
#  descr      :text
#  base_sql   :text
#  variables  :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  negative   :boolean
#

class SegmentType < ActiveRecord::Base
end
