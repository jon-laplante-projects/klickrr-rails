# == Schema Information
#
# Table name: followup_message_parts
#
#  id          :integer          not null, primary key
#  followup_id :integer
#  delay       :integer
#  subject     :string
#  message     :text
#

class FollowupMessagePart < ActiveRecord::Base
  belongs_to :followup
end
