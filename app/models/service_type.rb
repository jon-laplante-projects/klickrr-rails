# == Schema Information
#
# Table name: service_types
#
#  id    :integer          not null, primary key
#  label :string
#  name  :string
#

class ServiceType < ActiveRecord::Base
	has_many :services
end
