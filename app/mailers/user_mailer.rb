class UserMailer < ActionMailer::Base

  def followup_delivery(queued_message)
    mail(to: queued_message.addr,
         body: queued_message.message,
         content_type: "text/html",
         subject: queued_message.subject)
  end

end
