class SmtpMailer < ActionMailer::Base
  def smtp_test(email_to, email_from)
  	mail(to: email_to,
		from: email_from,
		body: "Hey there! I'm a message that has been sent from Klickrr!",
		content_type: "text/html",
		subject: "It's a message from Klickrr!")
  end
end
