class WebhooksController < ApplicationController
	def incoming
		########################################
		## When a user sets up a service, activate
		## the webhooks via API.
		## ?? - Do this at the model level???
		########################################
		unless params[:service].blank?
			case params[:service]
				when "sendgrid"
					mail = SentMail.find(params[:mail_id])
					case params[:event]
						when "Processed"
							# Does this need to be tracked?
						when "Dropped"
							mail.dropped = Time.now
						when "Delivered"
							mail.delivered = Time.now
						when "Deferred"
							# Not sure what to do with this.
							# Is it a common metric?
						when "Bounce"
							mail.bounced = Time.now
						when "Open"
							mail.opened = Time.now
						when "Click"
							mail_arr = mail.clicks.split(",")
							mail_arr.push(Time.now)
							mail.clicks = mail_arr.join(",")
							eval(FollowupTrigger.find(2).code)
						when "Spam Report"
							mail.spam = Time.now
						when "Unsubscribe"
							mail.unsubscribe = Time.now
						when "Group Unsubscribe"
							# Consider tracking group events sometime in the future.
							mail.unsubscribe = Time.now
						when "Group Resubscribe"
							# Not sure what to do with this.
							# Is it a common metric?
					end
					
					mail.save
				when "mandrill"
					comment = Feedback.new
						comment.name = "Jon La Plante"
						comment.email = "uberjon@gmail.com"
						comment.message = params.to_json
					comment.save

					# Allow us to access the params using strings instead of symbols.
					params.with_indifferent_access

					unless params["mandrill_events"]["_id"].blank?
						sent_mail = SentMail.find(params["mandrill_events"]["_id"])

						unless params[:opens_detail].blank?
							sent_mail.opened = DateTime.strptime("#{params["mandrill_events"]["opens_detail"]}", "%s")
						end

						unless params[:clicks_detail].blank?
							click = {url: "#{ params["mandrill_events"]["clicks_detail"] }", datetime: DateTime.strptime("#{params["mandrill_events"]["opens_detail"]}", "%s").to_s}
							curr_clicks = sent_mail.clicks ? eval(sent_mail.clicks) : []
							curr_clicks << click
							sent_mail.clicks = curr_clicks.to_json
							eval(FollowupTrigger.find(2).code)
						end

						sent_mail.save
					end

				when "mailgun"

				when "mailjet"

				when "ses" # Does SES provide message metrics?
					# 1) Set up the webhook(s) in the services_users model.
						# a) Set up verification the webhook was created.
					# 2) Make any required changes in the application_controller, at the email send level.
					# 3) Add handling to webhooks_controller.

				when "postage"

				when "socketlabs"

				when "elasticemail"

			end
		end

		render status: 200, json: @controller.to_json
	end
end