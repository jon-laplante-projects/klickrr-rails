class ServicesUsersController < ApplicationController
  layout false

  def update
    @services_user = current_user.services_users.find_by_id(params[:id])
    @services_user.update(services_user_params)
    head :ok
  end

  private
  def services_user_params
    params.require(:services_user).permit(:used, :limit)
  end
end
