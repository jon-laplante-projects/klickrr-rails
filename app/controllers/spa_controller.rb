require 'csv'
require "browser"
require "http"

class SpaController < ApplicationController
	include SpaHelper
  before_action :authenticate_user!, :except => "process_submitted_webform"

  IMAGE_PATH = File.join(Rails.root, "app", "assets", "images", "open.png")

  PAGE_SIZES = [10, 25, 50, 100]

  before_action :broadcast_paginate, only: [:index, :broadcast_list, :broadcast_total_pages]
  before_action :followup_paginate, only: [:index, :followup_list, :followup_total_pages]
	before_action :lists_paginate, only: [:index, :lists_list, :lists_total_pages]
  before_action :webform_paginate, only: [:index, :webform_list, :webform_total_pages]
  before_action :get_additional_data, only: [:aggregate_payload, :index]
  after_action :check_list_services, only: [:delete_service_conf]
  before_action :aggregate_payload, only: [:index]

  def index
  	# Services activity.
  	@services = Service.all.order(:id)
    @services_users = current_user.services_users.includes(:service).order(:id).to_a
    @services.each do |service|
      unless @services_users.any? { |services_user| services_user.service_id == service.id }
        @services_users.push(ServicesUser.new(service: service, user: current_user))
      end
    end

    @steps_count = 3

    @service_types = ServiceType.all.order(:id)
  	# End services activity.
		@user_id = current_user.id

    # Webform data.
    @styles = FormTemplate.all

    # @segment_types = SegmentType.all

		# Get followup scopes.
		@followup_scopes = FollowupScope.all.order(:id)

		# Followup types.
		@followup_types = FollowupType.all.order(:id)

    @values = {}

		# Get the lead stats for the dashboard load.
		@lead_stats = leads_stats(nil)

    # @lists = List.where(user_id: current_user.id)
    gon.jbuilder template: 'app/views/spa/lists_collection.json.jbuilder'
    gon.webformStepsCount = @steps_count
		gon.webformSubmitUrl = root_url + 'webform/process/'
	end

  def webform_list
    render partial: "partials/webform_manager"
  end

  def webform_total_pages
    render json: { total_pages: @webforms.total_pages }
  end

  def webform_paginate
    @webforms = paginate!(ListFieldGroup, [:list, :list_fields])
  end

  def webforms_for_list
    @list = List.includes(list_field_groups: :list_fields).find_by(id: params[:list_id])
  end

  def followup_list
    render partial: "partials/followup_manager"
	end

	def get_followups
		@followups = current_user.followups.includes(:followup_message_parts)
	end

  def followup_total_pages
    render json: { total_pages: @followups.total_pages }
  end

  def followup_paginate
    @followups = paginate!(Followup, [:trigger, :scope, :followup_message_parts])
	end

	def lists_list
		render partial: "partials/list_manager"
	end

	def lists_total_pages
		render json: { total_pages: @lists.total_pages }
	end

	def lists_paginate
    get_additional_data
		@lists = paginate!(List)
	end

	# Toggle follow ups on and off from the follow up manager screen.
	def followup_state_toggle
		unless params[:current_state].blank? || params[:followup_id].blank?
			followup = Followup.find(params[:followup_id])

			if followup.present?
				followup.active = params[:current_state]
				followup.save
			end

			render json: { success: true, message: "Follow up state has been toggled.", active: followup.active }, status: 200
		else
			render json: { success: false, message: "No params sent." }, status: 200
		end
	end

  def broadcast_list
    render partial: "partials/broadcast_manager"
	end

	def followup_load
			@followup = Followup.find(params[:id])
	end

  def broadcast_total_pages
    render json: { total_pages: @broadcasts.total_pages }
  end

  def broadcast_paginate
    @broadcasts = paginate!(Broadcast, [:segments])
  end

  def service_conf
  	#uid = params[:user_id]
  	conf = current_user.services_users.as_json
  	conf.each do |c|
  		service = Service.find(c["service_id"])
  		c["service_name"] = service.name
  	end

  	render json: conf
	end

  def save_service_conf
  	items = Hash.new
		if params[:services_user_id].present?
			rec = current_user.services_users.find(params[:services_user_id])
		else
			rec = current_user.services_users.new(user_id: current_user.id, service_id: params[:service_id], active: true)
      rec.used = true
		end

  	params.each do |k,v|
        unless k == "utf8" || k == "authenticity_token" || k == "action" || k == "controller" || k == "user_id"
					case k
        		# when "user_id"
        		# 	rec.user_id = v
						when "service_id"
							rec.service_id = v
						when "active"
							rec.active = v
						when "services_user_id"
  					else
              if k == "password" && Service.find(params[:service_id]).name == 'sendgrid'
                rec.api_password = v
              else
							  items[k] = "#{v}"
              end
        	end
        end
      end

		rec.usage = items.to_json

		rec.save

    count_users = ServicesUser.where(service_id: params[:service_id], user_id: current_user.id).count

		if params[:services_user_id].present?
			render json: { success: true }
		else
			render json: {
							 success: true,
               count_users: count_users,
							 service_name: rec.service.name,
							 services_user: rec,
							 partial: render_to_string(partial: "partials/servises_user", locals: {service: rec.service, services_user: rec})
						 }
		end
  rescue => e
    render json: { success: false, error: e.message }
	end

	def delete_service_conf
		rec = ServicesUser.find(params[:services_user_id])
    last_service = ServicesUser.where(user_id: rec.user_id, service_id: rec.service_id).count == 1
    if rec
			rec.destroy
      render json: { success: true, service_name: rec.service.name, service_last: last_service }
    else
      render json: { error: true }
		end
	end

  def active_list_services
  	service_ids = current_user.lists.select(:services).find(params[:list_id])

  	unless service_ids.services.blank?
			list_service_ids = service_ids.services.split(",").map { |i| i.to_i}
			service_labels = Service.where(id: list_service_ids).pluck(:label)
		else
			service_labels = ""
		end

  	render json: { active_list_services: service_ids, list_service_labels: service_labels }
  end

  def active_services
		service_ids = current_user.services_users.where(used: true).pluck(:service_id)

		if service_ids.present?
    	services = Service.find_by_sql(["SELECT s.id, s.label, s.name, t.name AS type FROM services s LEFT OUTER JOIN service_types t ON t.id = s.service_type_id WHERE s.id IN (?)", service_ids])
		end

		render json: services
  end

  def update_list_services
  	unless params[:list_id].blank?
  		list = List.find(params[:list_id])

  		list.services = "#{params[:services]}"
  		list.save

  		render json: { success: true }
  	else
  		render json: { success: false }
  	end
  end

  def save_webform
  	# TODO: Add error trapping here!!!
  	if params[:list_field_group_id].blank?
      group = ListFieldGroup.new
    else
      group = ListFieldGroup.find(params[:list_field_group_id])
      old_fields = ListField.where(list_field_group_id: params[:list_field_group_id])
      old_fields.destroy_all
    end

    group.list_id = params[:list_id]
		group.label = "#{params[:form_name]}"
    #group.form_code = SecureRandom.hex(9)
    group.form_code = params[:form_code]
    group.redirect_url = params[:redirect_url]

    unless params[:form_style].blank?
      group.form_style = params[:form_style]
    end

		group.inline_styles = params[:form_inline_styles] if params[:form_inline_styles].present?
		group.custom_email_message = params[:custom_email_text] if params[:custom_email_text].present?

	group.save

  	params[:fields].each do |field_coll|
      next if field_coll[:label].underscore == 'email'
  		field = ListField.new
			field.cid = field_coll[:cid]
			field.label = field_coll[:label]
			field.field_type = field_coll[:field_type]

      # Replace hash rockets in field_options
      field_options = field_coll[:field_options].to_s.gsub("=>", ":")
      if field_options.blank?
        field_options = field_coll[:field_options]
      end

			field.field_options = field_options
			field.description = field_coll[:description]
			field.include_other = field_coll[:include_other]
			field.include_blank = field_coll[:include_blank]
			field.integer_only = field_coll[:integer_only]
			field.max = field_coll[:max].to_i
			field.min = field_coll[:min].to_i
			field.required = field_coll[:required]
      field.list_id = params[:list_id]
      field.list_field_group_id = group.id
			field.inline_styles = field_coll[:inline_styles] if field_coll[:inline_styles].present?
			field.list_values << ListValue.new(field_value: field_coll[:value], list_field_group_id: group) if field_coll[:value].present?

      unless field_coll[:name].blank?
        field.name = field_coll[:name]
      end

      if field_coll[:is_email] == true
        field.is_email = true
      end

		field.save
  	end

  	unless group.blank?
  		render json: { group_id: group.id, success: true }
  	else
  		render json: {success: false}
  	end
  end

  def clone_webform
    success = false
    if params[:id].present?
      webform = ListFieldGroup.find(params[:id])
      success = (webform.present? && webform.dup.save)
    end
    render json: { success: success }
  end

  def load_webform
    field_group = ListFieldGroup.find(params[:list_field_group_id])
    fields = ListField.includes(:list_values).where(list_field_group_id: field_group.id)

    form = Hash.new

    form["label"] = field_group[:label]
    form["id"] = field_group[:id]
    form["style"] = field_group[:form_style]
    form["redirect_url"] = field_group[:redirect_url]
    form["form_code"] = field_group[:form_code]
		form["submit_url"] = root_url + 'webform/process/'
    form[:inline_styles] = field_group.inline_styles
		form[:list_id] = field_group.list_id
		form[:is_old_form] = field_group.is_old_form
    form["fields"] = Array.new

    fields.find_each do |f|
      field = Hash.new
      field[:id] = f.id.to_i
      field[:label] = f.label
      field[:field_type] = f.field_type
      field[:required] = f.required
      field[:field_options] = f.field_options
      field[:description] = f.description
      field[:include_other] = f.include_other
      field[:include_blank] = f.include_blank
      field[:integer_only] = f.integer_only
      field[:min] = f.min.to_i
      field[:max] = f.max.to_i
      field[:cid] = f.cid
      field[:list_id] = f.list_id.to_i
      field[:list_field_group_id] = f.list_field_group_id.to_i
      field[:is_email] = f.is_email
      field[:name] = f.name
      field[:inline_styles] = f.inline_styles
      list_value = f.list_values.first
      field[:value] = list_value.field_value if list_value.present?

      form["fields"] << field
    end

    render json: form
  end

  def process_submitted_webform
    group = ListFieldGroup.find_by(form_code: params[:form_code])
    fields = group.list_fields

      counter = 0
      set_id = nil

      # Allow us to access the params using strings instead of symbols.
      params.with_indifferent_access

      fields.each do |f|
        unless f.field_type == "hr" || f.field_type == "p"
          if f.field_type == "checkbox"
            options = JSON.parse(f.field_options)
            options.each do |opt|
              vals = ListValue.new
              vals.list_field_id = f.id
              vals.list_field_group_id = group.id

              unless params["#{opt["name"]}"].blank?
                vals.field_value = "#{opt["label"]}".strip
                unless vals.field_value == ""
                  vals.save
                end
              end

              if counter == 0
                set_id = vals.id
              end

              unless vals.blank? || vals.field_value.blank?
                vals.set_id = set_id
                vals.save
              end

              counter = counter + 1
            end
          else
            vals = ListValue.new
            vals.list_field_id = f.id
            vals.list_field_group_id = group.id
            vals.field_value = params["#{f.name}"]
            vals.save

            if counter == 0
              set_id = vals.id
            end

            unless vals.blank?
              vals.set_id = set_id
              vals.save
            end

            counter = counter + 1
          end
        end
      end

    group.increment(:entries).save
		if group.redirect_url.include?("http://") || group.redirect_url.include?("https://")
    	redirect_to url_for(group.redirect_url)
		else
			redirect_to url_for("http://" + group.redirect_url)
		end
  end

  def destroy_webform
    success = false
    if params[:group_id].present?
      group = ListFieldGroup.find(params[:group_id])
      success = (group.present? && group.destroy)
    end

    if success
      render json: { success: true, message: "Your selected form has been deleted." }
    else
      render json: { success: false, message: "The correct parameters for this action have not been provided." }
    end
  end

  def load_form_template
    name = params[:template_name]

    style = FormTemplate.find_by(name: name)

    render plain: style.markup
  end

  #def save_file
  #	render json: { success: true }
  #end

  def list_upload
  	# Handling for new list creation - still need to handle adding to existing list.

  	render json: { success: true }
  end

  def gen_link_string
  	link_string = SecureRandom.hex(7)

  	test = List.find_by(link_string: link_string)

  	unless test.blank?
  		gen_link_string
  	else
  		return link_string
  	end
  end


  def update_list
    list = List.find(params[:list_id])
    list.update_attribute(:title, params[:title])
    list.save
    render json: { success: true }
  end

  def create_list
  	list = List.new
  		list.title = params[:title]
  		list.user_id = current_user.id
  		list.link_string = gen_link_string
  	list.save

  	render json: { id: list.id, email_cid: nil, first_name_cid: nil, last_name_cid: nil, success: true }
  	#render json: { id: list.id, email_cid: email_field.cid, first_name_cid: fn_field.cid, last_name_cid: ln_field.cid, success: true }
  end

  def upload_list
  	list = current_user.lists.find( params[:list_id] )
  	cols_arr = Array.new
    if list.list_field_groups.exists?
      field_group = list.list_field_groups.first
    else
    	field_group = list.list_field_groups.new
  		field_group.list_id = list.id
  		field_group.label = list.title
  	  field_group.save
    end
	  Thread.new do
  		iteration = 0
  		set_id = nil
  	  params[:data].each do |field_coll|
			  email_id = nil
			  if field_coll == params[:data].first
  				field_coll.each do |f|
  					existing_col = field_group.list_fields.find_by(label: f.to_s)
  					unless existing_col.present?
  						field = field_group.list_fields.new
  						field.list_id = list.id
  						field.list_field_group_id = field_group.id
  						field.label = f
  						field.field_type = "paragraph"
  						field.field_options = ""
  						field.description = ""
  						field.include_other = false
  						field.include_blank = false
  						field.integer_only = false
  						field.max = nil
  						field.min = 0
  						field.required = true

  						if f.to_s == "email"
  							field.is_email = true
  						end
  						field.save

  						if field.is_email == true
  							email_id = field.id
  						end

  						# I'm not even sure what the cid is for!
  						field.cid = "c#{field.id}"
  						field.save
  					else
  						field = existing_col
  					end

					  cols_arr.push(field.id)
				end
			else
				#puts "Dealing with data rows..."
				counter = 0
				cols_arr.sort

				duplicate = false
				field_coll.each do |c|
					unless email_id.blank?
						if ListField.find(cols_arr[counter]) == email_id
							dups = ListValues.where(field_value: "#{c}")

							#puts "Dups: #{dups}"
							if dups.count > 0
								dups.each do |dup|
									if dup.list_fields.list_id == list.id
										duplicate = true
									end
								end
							end
						end
					end

					#puts "Duplicate: #{duplicate}"
					#unless duplicate == true
						#set_id_recs = list.list_fields.pluck(:id)
						#unless set_id_recs.list_values.last.nil?
						#	set_id = list.list_fields.list_values.last.set_id
						#end
						#unless field_coll.blank?
  						unless c.blank?
  							column = ListField.find(cols_arr[counter])
  							val = ListValue.new
  					#			puts "Val: #{val}"
  					#			#val.list_field_id =
  								#puts "Cols_arr: #{cols_arr}"
  								#puts "Column ID: #{column.id}"
  								val.list_field_id = column.id
  								val.field_value = "#{c}"
  								val.list_field_group_id = field_group.id
  							val.save

  							if column.id == cols_arr[0]
  								set_id = val.id
  							end

  							val.set_id = set_id
  							val.save
  						end
  					#end
  					counter = counter + 1
  				end
  			end
  			iteration = iteration + 1
  		end
  	end

  	render json: { success: true }
  end

  def destroy_list
  	list = List.find(params[:id])

  	Thread.new do
  		list.destroy
  	end

  	render json: {data_deleted: true}
  end

  def save_list_domain
  	list = List.find(params[:list_id])

  	list.domain = params[:domain]
  	list.save

  	render json: { success: true }
  end

	def list_combos

	end

	def list_segments
		unless params[:list_id].blank?
			@list = List.find(params[:list_id])
      @segments = Segment.where(message_id: params[:message_id], message_type: params[:message_type])
      @segment_operations = SegmentOperation.all
			render partial: "partials/list_segments"
		end
	end

	def save_list_segments
		unless params[:segment_rules].blank?
      save_segments(params[:segment_rules])
      render json: { success: true }
    else
      render json: { success: false }
		end
	end

  def save_segments(segments, message_id = nil, message_type = nil)
    segments.each do |value|
      segment = Segment.find_or_initialize_by(id: value[:id])
      segment.list_id = value['list_id']
      segment.segment_operator_id = SegmentOperation.where(label: value['operator']).first.id
      segment.field_id = value['field']
      segment.condition_value = value['val']
      segment.message_id = message_id || value['message_id']
      segment.message_type = message_type || value['message_type']
      segment.user_id = current_user.id
      segment.set_sql
      segment.save
    end
  end

	def delete_broadcast_segments
		unless params[:broadcast_id].blank?
			segments = current_user.lists.find(params[:list_id]).broadcasts.find(params[:broadcast_id]).segments
			segments.destroy_all

			unless segments.exists?
				render json: { success: true, message: "Segments have been removed from this broadcast." }, status: 200
			else
				render json: { success: false, message: "Segments could not be removed from this broadcast." }, status: 200
			end
		else
			render json: { success: false, message: "Cannot delete segments. No broadcast provided." }, status: 200
		end
	end

	def fields_list
		unless params[:list_id_arr].blank?
			@list_ids = params[:list_id_arr]
		else
			render json: { success: false, message: "No lists have been selected" }, status: 200
		end
	end

  def destroy_list_domain
  	# Placeholder for later implmentation.
  end

  def save_list_sender_email
  	list = List.find(params[:list_id])

  	list.sender_email = params[:sender_email]
  	list.save

  	render json: { success: true }
  end

  def destroy_list_sender_email
  	# Placeholder for later implmentation.
  end

  def save_list_sender_name
  	list = List.find(params[:list_id])

  	list.sender = params[:sender]
  	list.save

  	render json: { success: true }
  end

  def destroy_list_sender_name
  	# Placeholder for later implementation.
  end

  def list_subscriber_count
  	field_count = ListField.find_by_sql("SELECT id FROM list_fields WHERE list_id = #{params[:list_id]}").count
		if field_count > 0
  		subcount = ListValue.find_by_sql("SELECT COUNT(DISTINCT set_id) FROM list_values WHERE list_field_id IN (SELECT id FROM list_fields WHERE list_id = #{params[:list_id]})")

  		subCount = subcount[0]["count"]
  	else
  		subCount = 0
  	end

  	render json: { subCount: subCount }
  end

  def list_collections
    cols = ListFieldGroup.where(list_id: params["list_id"])

    render json: cols
  end

	# This method is used to *SCHEDULE* broadcasts
  def create_broadcast
    bc = params[:id].blank? ? Broadcast.new : Broadcast.find(params[:id])

    if params[:list_id].present?
      bc.list_id = params[:list_id]
    else
      bc.user_id = current_user.id
    end

  	bc.subject = params[:title]
  	bc.content = params[:body]
  	bc.timezone = params[:timezone]
  	bc.gmtAdjustment = params[:gmtAdjustment]
    bc.use_past_selection = params[:use_past_selection]
    bc.is_draft = params[:is_draft]
    bc.services = params[:services]

  	unless params[:send_on].blank?
      bc.push_at = params[:send_on].to_datetime

      # Add the timezone to the send time and save as UTC.
      utc_push_at = params[:send_on].to_datetime
      utc_push_at = utc_push_at - bc.gmtAdjustment.hour

      bc.utc_push_at = utc_push_at
  	end
  	#bc.sent = params[:]
    bc.save

    bc.inheritance_services unless bc.services.present?

    if params[:segments].present?
      save_segments(params[:segments], bc.id, 'Broadcast')
    end

    # # Save any segments.
    # unless params[:segment_ids].blank?
    #   save_segments("broadcast", bc.id, params[:segment_ids])
    # end
    # # End segments

  	render json: { id: bc.id, success: true }
  end

	# This method is used to *IMMEDIATELY SEND* broadcasts.
  def create_and_send_broadcast
    def render_false
      render json: { success: false, message: 'Some required data was missing to create and send a broadcast.' }
    end
  	if [:list_id, :title, :body].all? {|key| params[key].present? }
      broadcast_params = params
      .permit(:list_id, :timezone, :gmtAdjustment,
                                       :use_past_selection, :is_draft, :services)
      .merge(subject: params[:title], content: params[:body])
	  	broadcast = Broadcast.new(broadcast_params)
      list = List.find(params[:list_id])
      if params[:send_on].present?
	  		broadcast.push_at = DateTime.now
        broadcast.sent = DateTime.now
        # Add the timezone to the send time and save as UTC.
        utc_push_at = params[:send_on].to_datetime
        utc_push_at = utc_push_at - bc.gmtAdjustment.hour

        broadcast.utc_push_at = utc_push_at
	  	end
	  	broadcast.save
      broadcast.inheritance_services unless broadcast.services.present?
    	save_segments(params[:segments], broadcast.id, 'Broadcast') if params[:segments].present?

      mail_tasks = MailTasks.new
      mail_tasks.message_sender(broadcast.id, 'broadcast', { user_id: current_user.id })

      page = Broadcast.order(:id).page(1).per(params[:page_size]).total_pages
	  	render json: { id: broadcast.id, success: true, message: 'Broadcast successfully created and sent.', page: page }
	  else
	  	render_false
	  end
  rescue MailSender::NoAvailableServices => e
    render_false
  end

  def delete_broadcast
    bc = Broadcast.find(params[:id])
    if bc.destroy
      render json: { status: :ok }, status: 200
    else
      render json: { errors: 'Something wrong' }, status: 200
    end
  end

	def delete_followup
		unless params[:id].blank?
			followup = Followup.find(params[:id])

			if followup.destroy
				render json: { success: true, message: "Follow up has been permanently deleted." }, status: 200
			else
				render json: { success: false, message: "Could not delete followup." }, status: 200
			end

		else
			render json: { success: false, message: "You didn't specify which follow up to delete." }, status: 200
		end
	end

  def check_test_broadcast
    test_message_a = Broadcast.where(list_id: params[:list_id], test_type: "a").try(:last)
    test_message_b = Broadcast.where(list_id: params[:list_id], test_type: "b").try(:last)
    render json: { a: test_message_a, b: test_message_b }
  end

  def send_test_broadcast
    bc_a = Broadcast.create_test_broadcast(params[:list_id], params[:a])
    bc_b = Broadcast.create_test_broadcast(params[:list_id], params[:b])
    bc_a.update_attribute(:test_neighbor_id, bc_b.id)
    bc_b.update_attribute(:test_neighbor_id, bc_a.id)

    args = { user_id: current_user.id, test_message_b: bc_b }
    MailTasks.new.message_sender(bc_a.id, "Broadcast", args)
    render json: { success: true }
  end

  def send_test_message_from_broadcast
    mail_tasks = MailTasks.new
    args = Hash.new
    args["subject"] = params[:title]
    args["content"] = params[:body]
    args["email"] = params[:email]
    args["user_id"] = current_user.id
    args['list_id'] = params[:list_id]

    send_type = (params[:type] == 'broadcast' ? 'Broadcast' : params[:type])
    mail_tasks.message_sender(nil, send_type, args)
    render json: { success: true }
  end


  def save_followup
		unless params.blank?
			# Get the active scope name.
			message_type = FollowupScope.find(params[:active_scope_trigger])

      if params[:id].present?
        fu = Followup.find(params[:id])
      else
			   fu = Followup.new
      end

			fu.user_id = current_user.id
			fu.message_id = params["active_scope_target"]
			fu.message_type = message_type.name.titleize
			fu.scope_id = params["active_scope"]
			fu.trigger_id = params["active_trigger"]
			fu.trigger_variables = params["active_trigger_variables"].reduce Hash.new, :merge if params["active_trigger_variables"]
			fu.active = params["active"]
			fu.list_id = params["list_id"]
      fu.services = params[:services] if params[:services].present?
			fu.save

      fu.inheritance_services unless fu.services.present?
      message_parts_array = []
			params["message_parts"].each do |part|
        if part['id'].present?
          fu_parts = FollowupMessagePart.find(part['id'])
        else
          fu_parts = FollowupMessagePart.new
        end

				fu_parts.followup_id = fu.id
				fu_parts.subject = part["subject"]
				fu_parts.message = part["body"]
				fu_parts.delay_days = part["interval"]
				fu_parts.save
        message_parts_array << fu_parts.id
			end

		end

  	render json: { success: true, followup_id: fu.id, message_parts_array: message_parts_array}
	end

  def clone_broadcast
  	unless params[:bc_id].blank?
  		bc = Broadcast.find(params[:bc_id])

  		broadcast = bc.dup
  		broadcast.sent = nil
  		broadcast.push_at = nil
  		broadcast.save
  	end

  	render json: { success: true }
  end

  def clone_followup
  	unless params[:fu_id].blank?
  		fu = Followup.find(params[:fu_id])

  		followup = fu.dup
  		followup.save
  	end

  	render json: { success: true }
  end

  def update_broadcast
  	unless params.blank?
  		broadcast = Broadcast.find(params[:id])
      unless broadcast.sent
        broadcast.list_id = params[:list_id]
  			broadcast.subject = params[:title]
		  	broadcast.content = params[:body]
		  	broadcast.timezone = params[:timezone]
		  	broadcast.gmtAdjustment = params[:gmtAdjustment]
	  		broadcast.push_at = params[:send_on]
        broadcast.use_past_selection = params[:use_past_selection]
    		broadcast.save

        if params[:segments].present?
          save_segments(params[:segments], bc.id, 'Broadcast')
        end

    		render json: { success: true, message: "Broadcast has been updated." }
      else
        render json: { success: false, message: "Broadcast has already been sent." }
      end
  	else
  		render json: { success: false, message: "No data sent." }
  	end
	end

	def update_and_send_broadcast
		unless params.blank?
			broadcast = Broadcast.find(params[:bc_id])

			unless broadcast.sent
        broadcast.list_id = params[:list_id]
				broadcast.subject = params[:title]
				broadcast.content = params[:body]
				broadcast.timezone = params[:timezone]
				broadcast.gmtAdjustment = params[:gmtAdjustment]
				broadcast.push_at = params[:send_on] || DateTime.new
				broadcast.use_past_selection = params[:use_past_selection]
				broadcast.is_draft = params[:is_draft]
				broadcast.save

				# # Save any segments.
				if params[:segments].present?
					save_segments("broadcast", broadcast.id, params[:segment_ids])
				end
				# # End segments

				args = { user_id: current_user.id }
				mail_tasks = MailTasks.new

				mail_tasks.message_sender(broadcast.id, "Broadcast", args)

				render json: { id: broadcast.id, success: true, message: "Broadcast has been updated." }
			else
				render json: { success: false, message: "Broadcast has already been sent." }
			end
		else
			render json: { success: false, message: "No data sent." }
		end
	end

  def update_broadcast_services
    if params[:id].present? && br = Broadcast.find(params[:id])
      br.update_attribute(:services, params[:services])
      labels = br.services.present? ? br.services_instances.pluck(:label).join(',') : ''
      render json: { success: true, labels: labels }
    else
      render json: { success: false }, status: 404
    end
  end

  def update_followup_services
    if params[:id].present? && fu = Followup.find(params[:id])
      fu.update_attribute(:services, params[:services])
      labels = fu.services.present? ? fu.services_instances.pluck(:label).join(',') : ''
      render json: { success: true, labels: labels }
    else
      render json: { success: false }, status: 404
    end
  end


	############################################
	## Followups
	############################################
  def update_followup
		followup = Followup.find(params[:id])
    followup.scope_id = params["active_scope"]
    followup.trigger_id = params["active_trigger"]
    followup.trigger_variables = params["active_trigger_variables"].reduce Hash.new, :merge if params["active_trigger_variables"]
    followup.active = params["active"]
    followup.save
    params[:message_parts].each do |message|
      message_part = FollowupMessagePart.find(message[:id])
      message_part.message = message[:body]
      message_part.subject = message[:subject]
      message_part.delay_days = message[:interval]
      message_part.save
    end
		render json: { success: true }
	end

	def followup_triggers_by_scope
		@scopes = FollowupScope.all.includes(:followup_triggers)
	end
	############################################
	## End followups
	############################################

	############################################
	## Analytics
	############################################
  def amount_vars(main_collection, added_collection)
    if added_collection.present?
      added_collection.each do |key, value|
        if main_collection[key].blank?
          main_collection[key] = value
        else
          amount_hash(main_collection[key], value)
        end
      end
    end
    main_collection
  end

  def amount_hash(main_hash, added_hash)
    if added_hash.present?
      added_hash.each do |k, v|
        main_hash[k] ||= 0
        main_hash[k] += v
      end
    end
    main_hash
  end


	def generate_analytics
    @values = {}
		unless params[:action_name].blank? || params[:action_id].blank?
      sent_mails = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id])
      @values[:opens] = 0
      @values[:clicks] = 0
      @values[:opens_country_hash] = {}
      @values[:all_clicks_country] = {}
      time_opens = {}
      time_clicks = {}
      time_unsub = {}
      time_spams = {}
      sent_mails.find_each do |sent_mail|
        amount_vars(@values[:all_clicks_country], sent_mail.count_of_key_by_key(:clicks, :url, :country))
        amount_hash(@values[:opens_country_hash], sent_mail.states_by_key(:opened, :country))
        @values[:opens] += 1 if sent_mail.opened.present?
        @values[:clicks] += 1 if sent_mail.clicks.present?
        amount_hash(time_opens, sent_mail.day_count(:opened, :opened_at))
        amount_hash(time_clicks, sent_mail.day_count(:clicks, :clicked_at))
        amount_hash(time_unsub, sent_mail.day_count(:unsubscribed, :unsubscribed_at))
        amount_hash(time_spams, sent_mail.check_spam)
      end

      @values[:graph] = [
        { name: 'Opens', data: time_opens.map{ |k, v| [k, v] } },
        { name: 'Clicks', data: time_clicks.map{ |k, v| [k, v] } },
        { name: 'Unsubs', data: time_clicks.map{ |k, v| [k, v] } },
        { name: 'Spams', data: time_spams.map{ |k, v| [k, v] } }
      ]


      @values[:opens_country] = [['Country', 'Popularity']] + @values[:opens_country_hash].map{ |k, v| [k, v] }

      @values[:sends] = ActionController::Base.helpers.number_with_delimiter(sent_mails.count.to_i)

      click_count = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id]).where.not(clicks: nil).count
      unless @values[:clicks] == 0 ||  @values[:opens] == 0
        ctr = (click_count / @values[:opens]) * 100
      else
        ctr = "0%"
      end

      @values[:ctr] = ctr.to_f

      @values[:drops] = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id]).where.not(dropped: nil).count
      @values[:delivers] = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id]).where.not(delivered: nil).count
      @values[:bounces] = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id]).where.not(bounced: nil).count
      @values[:unsubscribes] = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id]).where.not(unsubscribed: nil).count
      @values[:spam] = SentMail.where(message_type: params[:action_name].capitalize, message_id: params[:action_id]).where.not(spam: nil).count

      @values[:opens_unscr_spam] = {
        'Opens' => @values[:opens],
        'Unsubscribes' => @values[:unsubscribes],
        'Spams' => @values[:spam]
      }
      @values[:clicks_unscr_spam] = {
        'Clicks' => @values[:clicks],
        'Unsubscribes' => @values[:unsubscribes],
        'Spams' => @values[:spam]
      }

		end
    render partial: "partials/geo_analitics"
	end

  def get_pixel
    crypt = ActiveSupport::MessageEncryptor.new(Klickrr::Application.config.secret_key_base)
    encrypted_data = crypt.encrypt_and_sign(pixel_params)
    render json: { url: tracking_pixel_url(encrypted_data) }
  end

  def tracking
    data = {}
    body = HTTP.get("https://maps.googleapis.com/maps/api/timezone/json", params: { location: "#{request.location.coordinates[0]},#{request.location.coordinates[1]}", timestamp: 1331161200 }).body
    data[:timezone] = JSON.parse(body)['timeZoneId']
    data[:city] = request.location.city
    data[:country] = request.location.country
    data[:browser] = browser.name
    data[:operating_system] = browser.platform

    crypt = ActiveSupport::MessageEncryptor.new(Klickrr::Application.config.secret_key_base)
    decrypted_data = crypt.decrypt_and_verify(params[:token])
    data[:message_id] = decrypted_data[:action_id]
    data[:message_type] = decrypted_data[:action_name].capitalize

    OpenedMail.create(data)
    send_file IMAGE_PATH
  end
	############################################
	## End analytics
	############################################

  def aggregate_payload
		# @lists = List.where(user_id: current_user.id).includes(:list_fields, list_field_groups: [:list_fields], followups: [:followup_message_parts, :scope])
  	@lists = List.where(user_id: current_user.id).includes(:list_fields, list_field_groups: [:list_fields], broadcasts: [:sent_mails, :segments], followups: [:followup_message_parts, :scope])
  end

  def lists_collection
    @lists = current_user.lists
  end

  def get_list
    @list = current_user.lists.find(params[:id])
  end

  def get_broadcast
    @broadcast = Broadcast.find(params[:id])
  end

  def broadcasts_collection
    @broadcasts = current_user.lists.find(params[:list_id]).broadcasts
  end

  def get_followup
    @followup = Followup.where(user_id: current_user.id, id: params[:id]).includes(:followup_message_parts, :scope).first
  end

  def get_additional_data
    @subscribers = ListValue.select('distinct set_id').joins(:list).where('lists.user_id = ?', current_user.id).group(:list_id).count
    @drafts = Broadcast.where(list_id: nil, user_id: current_user.id)
  end

  def get_column_names(list)
    cols = list.list_fields.pluck(:label)
    col_uniqueness = Hash.new(0)
    cols.map! do |col|
      col = 'Untitled' if col.blank?
      col = col.gsub(' ', '_').underscore
      col = col.gsub('?', '_').underscore
      col_uniqueness[col] += 1
      col
    end

    col_string = "set_id int"
    cols.each do |col|
      if col_uniqueness["#{col}"] == 1
        col_string.concat(", #{col} text")
      else
        col_string.concat(", #{col}_#{SecureRandom.hex(1)} text")
      end
    end
    col_string
  end

  def subscriber_preview
    list = current_user.lists.find(params[:list_id])
    list_values_count = list.try(:list_fields).select { |item| item.list_values.exists? }.count
    if list && list_values_count > 0
      col_string = get_column_names(list)
			subscribers = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id LIMIT 50') AS ct(#{col_string})", params[:list_id].to_i ])
      response_hash = subscribers
    else
      response_hash = { success: false, message: "The selected list does not belong to the currently logged in user." }
    end

		respond_to do |format|
      format.json { render json: response_hash }
    end
  end

  def subscriber_columns
  	columns = get_names_columns(params[:list_id])
  	respond_to do |format|
  		format.json { render json: columns }
  	end
  end

  def get_names_columns(list_id)
    cols = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id", list_id])
    columns = Array.new

    cols.each do |c|
      columns << c.label unless columns.include? c.label
    end
    columns
  end

  def get_names_columns_with_id(list_id)
    ListField.where(list_id: list_id).select(:id, :label)
  end

  def add_new_subscriber
    @list = List.find(params[:list_id])
    render partial: 'partials/contact_fields'
  end

  def save_subscriber
    if params[:field_values].present?
      group_id = ListField.find(params[:field_values][0][:id]).list_field_group_id
      first_value = ListValue.create(
        list_field_id: params[:field_values][0][:id],
        field_value: params[:field_values][0][:value],
        list_field_group_id: group_id
      )
      first_value.update_attribute(:set_id, first_value.id)
      params[:field_values][1..-1].each do |sub|
        ListValue.create(
          list_field_id: sub[:id],
          field_value: sub[:value],
          set_id: first_value.id,
          list_field_group_id: group_id
        )
      end
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  def update_subscriber
    if params[:values].present?
      new_items = []
      params[:values].each do |item|
        if item[:value_id].present?
          list_value = ListValue.find(item[:value_id])
          list_value.update_attribute(:field_value, item[:field_value]) if list_value
        elsif item[:field_id]
          group_id = ListField.find(item[:field_id]).list_field_group_id
          list_value = ListValue.create(
            set_id: params[:id],
            field_value: item[:field_value],
            list_field_id: item[:field_id],
            list_field_group_id: group_id
          )
          new_items << { value_id: list_value.id, field_id: item[:field_id]}
        end
      end
      render json: { success: true, new_items: new_items }
    else
      render json: { success: false }, status: :bad_request
    end
  end

  def subscriber_search
    list = current_user.lists.find(params[:list_id])
  	if list.present?
      list_fields_array = list.list_fields.pluck(:id)
  		record_sets = ListValue.where("LOWER(field_value) LIKE '%#{params[:criterion].to_s.downcase}%' AND list_field_id IN (?)", list_fields_array).distinct.pluck(:set_id)
  		# subscribers = ListValue.find_by_sql("SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = #{params[:list_id].to_i} AND set_id IN (#{record_sets.join(",")}) ORDER BY list_values.id') AS ct(#{col_string})")
      subscribers = ListValue.where(set_id: record_sets).select(:id, :set_id, :field_value, :list_field_id).order(:list_field_id).group_by(&:set_id)

	  	response_hash = { subscribers: subscribers, columns: get_names_columns_with_id(list.id) }
	  else
	 	  response_hash = { success: false, message: "The selected list does not belong to the currently logged in user." }
  	end
		render json: response_hash
  end

  def subscriber_email_validate
    if params[:email].present? && params[:field_id].present?
      count = ListValue.where(list_field_id: params[:field_id], field_value: params[:email]).count
      render json: { success: count == 0 }
    else
      render json: { success: false }, status: :bad_request
    end
  end

  def remove_subscriber_from_list
  	set_id = params[:set_id]

  	vals = ListValue.where(set_id: set_id).destroy_all

  	render json: { success: true }
  end

  def subscriber_csv_export
    list = current_user.lists.find(params[:list_id])
    list_values_count = list.try(:list_fields).select { |item| item.list_values.exists? }.count
    if list && list_values_count > 0
      col_string = get_column_names(list)

			response_hash = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id') AS ct(#{col_string})", params[:list_id].to_i])
    else
      response_hash = { success: false, message: "The selected list does not belong to the currently logged in user." }
    end

    column_cols = get_names_columns(params[:list_id])
		respond_to do |format|

      columns = Array.new
      columns << "id"
      column_cols.each do |c|
        columns << c
      end
      format.csv { render text: csv(columns, response_hash.to_json) }
    end
  end

  def save_feedback
  	feedback = Feedback.new
  		unless params[:name].blank?
  			feedback.name = params[:name]
  		end

  		unless params[:email].blank?
  			feedback.email = params[:email]
  		end

  		unless params[:message].blank?
  			feedback.message = params[:message]
  		end

  	feedback.save

  	render json: { status: "Feedback saved." }
  end

  # def save_segments(action, action_id, segments)
  #   # Save any segments.
  #   unless segments.blank? || action.blank?
  #     segments.each do |segment_data|
  #       segment_data = segment_data.split(",")
  #
  #       seg = Segment.new
  #       seg.user_id = current_user.id
  #       seg.action_id = action_id
  #       seg.seg_type = "#{action}"
  #
  #       # If there are variables, capture therecm.
  #       arr = Array.new
  #
  #       segment_data.each do |data|
  #         key_val = data.split(":")
  #
  #         if key_val[0] == "id"
  #           seg.segment_id = key_val[1]
  #         else
  #           val_hash = Hash.new
  #
  #           val_hash["#{key_val[0]}"] = key_val[1]
  #           arr << val_hash
  #         end
  #       end
  #
  #       if arr.size > 0
  #         seg.variables = arr.to_json
  #       end
  #
  #       seg.save
  #     end
  #   end
  #   # End segments
  # end

  def broadcast_status
    broadcast = Broadcast.where(id: params[:id]).first
    return redirect_to :root, error: 'broadcast not found' unless broadcast

    render json: { state:   broadcast.state,
                   count:   broadcast.sent_count }
  end

  def broadcast_unschedule
    broadcast = Broadcast.sheduled.where.not(push_at: nil).find params[:id]
    broadcast.update(push_at: nil)

    head :ok
  end

  private

  def pixel_params
    params.permit(:action_name, :action_id)
	end

  def csv(cols, dataset)
  # cols = Array.new

  # dataset[0].each do |el|
  # 	cols << el
  # end
  count = 0

    CSV.generate do |csv|
  		if count == 0
        csv << cols

        count += 1
      end

      JSON.parse(dataset).each do |datum|
				csv << datum.except('id').values
				#csv << datum.attributes.values_at(*cols)

        count += 1
			end
    end
  end

  def check_list_services
    ids = ServicesUser.where(user_id: current_user.id, active: true, used: true).pluck(:service_id)
    List.find_each do |list|
      if list.services.present?
        list.services = list.services.split(',').reject { |id| !ids.include? id.to_i }.to_s.delete '[]" '
        list.save
      end
    end
  end

  def paginate!(klass, includes = nil)
    @page = params[:page] || 1
    @page_size = params[:page_size] || 10
    @order = params[:order] || 'updated_at DESC'
    scope = includes ? klass.includes(includes) : klass
    scope = if params[:list_id].present?
      @list_title = List.find(params[:list_id]).title
      scope.where(list_id: params[:list_id])
		else
			lists = current_user.lists.pluck(:id)
      case klass.name
        when 'List'
          scope.where(["id IN (?)", lists])
        when 'ListFieldGroup'
          scope.where(list_id: lists)
        else
				  scope.where(["list_id IN (?) OR user_id = ?", lists, current_user.id])
			end
    end
    scope = scope.order(@order).page(@page).per(@page_size)
    @page_sizes = PAGE_SIZES
    scope
  end
end
