class RegistrationsController < Devise::RegistrationsController
	private
 
	def sign_up_params
#		unless params[:name]?
#			name = params[:first_name].to_s + " " + params[:last_name].to_s
#			params[:name = name
#		end
#
#		params[:status] = "active"

		params.require(:user).permit(:first_name, :last_name, :name, :email, :password, :password_confirmation, :trans_type, :hash_key, :trans_gateway, :trans_date, :trans_testmode, :seller_id, :seller_email, :country, :status)
	end

	def account_update_params
		params.require(:user).permit(:first_name, :last_name, :name, :email, :password, :password_confirmation, :current_password)
	end
end