class AccountsController < ApplicationController
		skip_before_action :verify_authenticity_token
		#skip_before_filter  :verify_authenticity_token

	def zaxaa_zpn
		#render :text => "Kindle Trend echo.", :status => 200, :content_type => 'text/html'
		trans_type = params[:trans_type]
		logger.info "trans_type = #{trans_type}"
		hash_key = params[:hash_key]
		logger.info "hash_key = #{hash_key}"
		trans_gateway = params[:trans_gateway]
		logger.info "trans_gateway = #{trans_gateway}"
		trans_date = params[:trans_date]
		logger.info "trans_date = #{trans_date}"
		trans_testmode = params[:trans_testmode]
		logger.info "trans_testmode = #{trans_testmode}"
		seller_id = params[:seller_id]
		logger.info "seller_id = #{seller_id}"
		seller_email = params[:seller_email]
		logger.info "seller_email = #{seller_email}"
		cust_email = params[:cust_email]
		logger.info "cust_email = #{cust_email}"
		cust_firstname = params[:cust_firstname]
		logger.info "cust_firstname = #{cust_firstname}"
		cust_lastname = params[:cust_lastname]
		logger.info "cust_lastname = #{cust_lastname}"
		cust_address = params[:cust_address]
		logger.info "cust_address = #{cust_address}"
		cust_state = params[:cust_state]
		logger.info "cust_state = #{cust_state}"
		cust_city = params[:cust_city]
		logger.info "cust_city = #{cust_city}"
		cust_country = params[:cust_country]
		logger.info "cust_country = #{cust_country}"


		# Trans types: SALE, BILL, RFND, CGBK, INSF, CANCEL-REBILL, UNCANCEL-REBILL
		pwd = SecureRandom.hex(8)

		# Spawn a new User object...
		usr = User.new
			usr.password = pwd
			usr.password_confirmation = pwd
			usr.email = cust_email
			usr.trans_type = trans_type
			usr.hash_key = hash_key
			usr.trans_gateway = trans_gateway
			usr.trans_date = trans_date
			usr.trans_testmode = trans_testmode
			usr.seller_id = seller_id
			usr.seller_email = seller_email
			usr.email = cust_email
			usr.first_name = cust_firstname
			usr.last_name = cust_lastname
			usr.cust_country = cust_country

			trans = trans_type.upcase
			case trans
			when "SALE"
				acct_status = "approved"
			when "JV_SALE"
				acct_status = "approved"
			when "TEST_SALE"
				acct_status = "approved"
			when "TEST_JV_SALE"
				acct_status = "approved"
			when "RFND"
				acct_status = "frozen"
			when "TEST_RFND"
				acct_status = "frozen"
			when "CANCEL-REBILL"
				acct_status = "cancelled"
			when "UNCANCEL-REBILL"
				acct_status = "cancelled"
			when "CGBK"
				acct_status = "frozen"
			end

			usr.status = acct_status
		usr.save

		#AccountMailer.jv_join_welcome(cust_email, pwd).deliver
		jv_welcome_mail(usr.email, usr.name, pwd)

		# Build a response string.
		params_str = ""
		params.each do |p|
			params_str.concat("#{p} | ")
		end

		params_str.concat("6Pa0wh3qjq3_f_4")

		respond_to do |format|
			#format.html {render :jvzoo_instant_notification, :layout => false}
			format.html do
				logger.info "Returning #{params_str}"
				#render text: params_str
				render text: 'FVZ2GCP5171TZY17'
			end
		end
		#head 200, :content_type => 'text/html'
		#render :html => layout: false, :status => 200
	end

	def spawn_user
	usr_token = Hash.new

	unless params[:email].blank?
		#existing = User.find_by(email: "#{params[:email]}")
		#unless existing.nil?
		if params[:password].blank?
			pwd = SecureRandom.hex(8)
		else
			pwd = params[:password]
		end

			usr = User.new
			usr.password = pwd
			usr.password_confirmation = pwd
			usr.first_name = params[:first_name]
			usr.last_name = params[:last_name]
			usr.email = params[:email]
			usr.save

			usr_token["token"] = "Success!"

			jv_welcome_mail(usr.email, usr.name, pwd)
		#else
		#	usr_token["token"] = "existing_user"
		#end
	else
		usr_token["token"] = "params_failure"
	end

	render json: usr_token
	end

	def jv_welcome_mail(recipient, recipient_name, pwd)
		content = <<-CONTENT
			<p>Welcome to Klickrr!</p>

		<p>To get started, visit <a href="http://www.klickrr.net" target="_BLANK">http://www.klickrr.net</a>, and login with the following credentials:<br/>
            <strong>email:</strong> #{ recipient }<br/>
            <strong>password:</strong> #{ pwd }<br/><br/>

		<p>Thanks for joining Klickrr!</p>

		<p>Sincerely,</p>
		The Klickrr Team
		CONTENT

		subject = "Klickrr login credentials"

		begin
		    mandrill = Mandrill::API.new "5U-aZ4Fp5LoaknZJUQP_ZA"
		    template_content = [{}]
		    message = {"from_name"=>"Klickrr",
		    # "metadata"=>{"website"=>"www.example.com"},
		    # "google_analytics_domains"=>["example.com"],
		    # "merge"=>true,
		    # "url_strip_qs"=>nil,
		    # "track_clicks"=>nil,
		    "subject"=>"#{subject}",
		    "html"=>"#{content}",
		    # "images"=>
		    #    [{"name"=>"IMAGECID", "content"=>"ZXhhbXBsZSBmaWxl", "type"=>"image/png"}],
		    # "google_analytics_campaign"=>"message.from_email@example.com",
		    # "preserve_recipients"=>nil,
		    # "tags"=>["password-resets"],
		    # "return_path_domain"=>nil,
		    # "important"=>false,
		     "to"=>
		        [{"email"=>"#{recipient}",
		            "name"=>"#{recipient_name}",
		            "type"=>"to"}],
		    # "recipient_metadata"=>
		    #    [{"values"=>{"user_id"=>123456}, "rcpt"=>"recipient.email@example.com"}],
		    # "inline_css"=>nil,
		    # "auto_html"=>nil,
		    # "subaccount"=>"customer-123",
		    # "tracking_domain"=>nil,
		    # "text"=>"Example text content",
		    # "signing_domain"=>nil,
		    # "bcc_address"=>"message.bcc_address@example.com",
		    # "view_content_link"=>nil,
		    # "track_opens"=>nil,
		     "headers"=>{"Reply-To"=>"welcome@klickrr.com"},
		     "from_email"=>"welcome@klickrr.com"}
		    async = true
		    ip_pool = "Main Pool"
		    send_at = {}
		    result = mandrill.messages.send message, async, ip_pool, send_at
		        # [{"status"=>"sent",
		        #     "reject_reason"=>"hard-bounce",
		        #     "email"=>"recipient.email@example.com",
		        #     "_id"=>"abc123abc123abc123abc123abc123"}]
		    
		rescue Mandrill::Error => e
		    # Mandrill errors are thrown as exceptions
		    puts "A mandrill error occurred: #{e.class} - #{e.message}"
		    # A mandrill error occurred: Mandrill::UnknownSubaccountError - No subaccount exists with the id 'customer-123'    
		    raise
		end
	end

		def password_reminder_email(email, token)
			begin
				mandrill = Mandrill::API.new 'cOK-ihNyBEVbt9BuL73Rmg'
				template_name = "klickrr-change-of-password"
				template_content = [{}]
				message = {"from_name"=>"Klickrr App",
									 "to"=>
											 [{"email"=>"#{email}",
												 "name"=>"#{email}",
												 "type"=>"to"}],
									 "merge_vars"=>
											 [{ "vars" => [{ "name" => "uid", "content" => "#{email}"}, {"name" => "token", "content" => "#{token}" }],
													"rcpt"=>"#{email}"}],
									 "merge_language"=>"mailchimp",
									 "headers"=>{"Reply-To"=>"admin@klickrr.net"},
									 "from_email"=>"admin@klickrr.net"}
				async = true
				ip_pool = "Main Pool"
				send_at = {}
				result = mandrill.messages.send_template template_name, template_content, message, async, ip_pool, send_at
					# [{"status"=>"sent",
					#     "reject_reason"=>"hard-bounce",
					#     "email"=>"recipient.email@example.com",
					#     "_id"=>"abc123abc123abc123abc123abc123"}]

			rescue Mandrill::Error => e
				# Mandrill errors are thrown as exceptions
				puts "A mandrill error occurred: #{e.class} - #{e.message}"
				# A mandrill error occurred: Mandrill::UnknownSubaccountError - No subaccount exists with the id 'customer-123'
				raise
			end
		end
end