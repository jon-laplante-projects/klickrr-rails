class OutsideController < ApplicationController
	def subscribe
		unless params[:list_group_id] == nil
			list_field_group = ListFieldGroup.find(params[:list_group_id])

			#make sure that formbuilder is writing cid and app is not generating them. 
			@fields = list_field_group.list_fields.order(:cid)
		end
	end

	def unsubscribe_me
		unless params[:tracking_code].blank?
			# Let's get the list_id and set_id...
			sent_mail = SentMail.find_by(tracking_code: params[:tracking_code])

			if sent_mail.message_type.downcase == "broadcast"
				action = Broadcast.find(sent_mail.message_id)
			else
				action = FollowUp.find(sent_mail.message_id)
			end

			list_id = action.list_id

			list_field_ids = ListField.where(list_id: list_id).pluck(:id)

			value_rec = ListValue.where(["list_field_id IN (?) AND field_value = ?", list_field_ids, sent_mail.email]).first
			# End list_id and set_id retrieval...

			sent_mail.unsubscribed = Time.now
			sent_mail.save

			unsub = Unsubscribe.new
			unsub.set_id = value_rec.set_id
			unsub.save
		end
	end
end