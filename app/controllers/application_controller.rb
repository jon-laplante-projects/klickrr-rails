require "resolv-replace.rb"

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :null_session
  #########################################
	unless Rails.env.development?
  		#protect_from_forgery with: :exception
  		#protect_from_forgery with: :null_session
	end
  #########################################
  #force_ssl unless Rails.env.development?

	layout :layout_by_resource

	def layout_by_resource
		if devise_controller? && resource_name == :user && action_name == "new"
			false
		else
			nil
		end
	end
end
