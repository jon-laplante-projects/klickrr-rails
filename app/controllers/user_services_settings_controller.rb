class UserServicesSettingsController < ApplicationController
  layout false

  def update
    @services_settings = current_user.services_settings
    @services_settings.update(services_settings_params)
    head :ok
  end

  private
  def services_settings_params
    params.require(:services_settings).permit(:rotation, :autodeactivate)
  end
end
