class AnalyticsController < ApplicationController
  include SpaHelper

  def list_analytics
      list_id = params[:list_id] || nil

      stats = leads_stats(list_id)

      render json: { success: true, stats: stats}, status: 200
  end

  def broadcast_send_count
    #Delivered
    #Bounced
    #Droped
    #Unsubscribed
    #Spam

    unless params[:broadcast_id].blank?
      sent = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]}").count
      delivered = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND delivered IS NOT NULL").count
      bounced = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND bounced IS NOT NULL").count
      dropped = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND dropped IS NOT NULL").count
      unsubbed = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND unsubscribed IS NOT NULL").count
      spam = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND spam IS NOT NULL").count

      # Will need to do some parsing here...
      opened_recs = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND opened IS NOT NULL").pluck(:opened)
      opened = calculate_opens(opened_recs)

      clicks_recs = current_user.sent_mails.where("(message_type='broadcast' OR message_type='Broadcast') AND message_id = #{params[:broadcast_id]} AND clicks IS NOT NULL").pluck(:clicks)
      clicks = calculate_clicks(clicks_recs)

      render json: { success: true, sent: sent, delivered: delivered, bounced: bounced, dropped: dropped, unsubscribed: unsubbed, spam: spam, clicks: clicks, opened: opened }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  private
  def calculate_clicks(clicks)
    click_count = 0

    clicks.each do |click|
      # Clicks should never be null, but just in case...
      unless click.blank?
        if (click[0] === "[" && click[6..7] === "=>") || click[2] === ":"
          click_arr = eval(click)
        elsif click[0] === "{" || (click[0..1] === "[{" && click[7] === ":")
          click_arr = JSON.parse(click)
        end

        # In case there are multiple clicks for this, iterate through all.
        click_arr.each do |clk|
          click_count += 1
        end
      end
    end
    return click_count
  end

  def calculate_opens(opens)
    open_count = 0
    opens.each do |o|
      # Open should never be null, but just in case...
      unless o.blank?
        open_count += 1
      end
    end

    return open_count
  end
end