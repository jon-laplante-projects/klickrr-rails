require 'mandrill'

class MailController < ApplicationController
	include MailHelper
	require 'RubySpamAssassin'
	include RubySpamAssassin

	def send_message
		mail_tasks = MailTasks.new

		if params[:send_type] == "email_preview"
			args = Hash.new
			args["subject"] = params[:subject]
			args["content"] = params[:content]
			args["email"] = params[:email]
			args["user_id"] = current_user.id
			mail_tasks.message_sender(0, params[:send_type], args)
			# mail_tasks.message_sender(params[:id], params[:send_type], args)
		elsif params[:send_type] == "conn_test"
			args = Hash.new
			args["service"] = params[:service]
			args["user_id"] = current_user.id

			mail_tasks.message_sender(nil, nil, args)
		else
			args["user_id"] = current_user.id
			send_type = (params[:send_type] == 'broadcast' ? 'Broadcast' : params[:send_type])
			mail_tasks.message_sender(params[:id], send_type, args)
		end

		#render json: json_arr
		render json: { success: true }
	end

	def test_service
		mail_tasks = MailTasks.new
		config = Hash.new

		service = Service.find_by(name: params[:service])

		#config["subject"] = params[:subject]
		#config["content"] = params[:]
		config["subject"] = "Klickrr :: Testing #{service.label} Connection"
		config["content"] = "<p>This is a test of the selected service in Klickrr.</p><p>If you've received this email, you have successfully configured #{service.label}.</p>"
		config["to_email"] = params[:email]
		config["from_email"] = params[:sender]
		config["service"] = params[:service]
		config["address"] = params[:address]
		config["port"] = params[:port]
		config["domain"] = params[:domain]
		config["user_name"] = params[:user_name]
		config["sender"] = params[:user_name]
		config["password"] = params[:password]
		config["authentication"] = params[:authentication]
		config["sender"] = params[:sender]
		config["to_email"] = params[:recipient]
		config["api_key"] = params[:api_key]
		config["api_name"] = params[:api_name]
		config["sendmail"] = true

		sent_mail = SentMail.new
			sent_mail.user_id = current_user.id
			sent_mail.service_id = service#s[service_index].id
			sent_mail.message_id = nil
			sent_mail.contact_id = nil
			sent_mail.message_type = "service_test"
			sent_mail.email = params[:to_email]
			sent_mail.sent = false
			sent_mail.response_message = "This contact has already been sent this message."
		sent_mail.save

		config["sent_mail_id"] = sent_mail.id
		if params[:service] != "personal"
			begin
			# if mail_tasks.send_api(config) == true
				result = mail_tasks.send_api(config)
				sent_mail.reload
				if result && sent_mail.sent
					sent_mail.response_message = "Successfully connected to service."
					sent_mail.save

					render json: { success: true, message: sent_mail.response_message }
				else
					render json: { success: false, message: sent_mail.response_message }
				end
			rescue => e
			# else
				sent_mail.response_message = e.message
				sent_mail.save

				render json: { success: false, message: "#{e.message}" }
			# end
			end
		else
			begin
			# if mail_tasks.send_smtp(config) == true
				result = mail_tasks.send_smtp(config)

				sent_mail.reload
				if result && sent_mail.sent
					sent_mail.response_message = "Successfully connected to service."
					sent_mail.save

					render json: { success: true, message: sent_mail.response_message }
				else
					render json: { success: false, message: sent_mail.response_message }
				end
			rescue => e
			# else
				sent_mail.response_message = e.message
				sent_mail.save

				render json: { success: false, message: "#{e.message}" }
			#end
			end
		end
	end

	def mailing_progress
		unless params[:message_id].blank? || params[:message_type].blank?
			mailing_progress = Array.new

			mailing_progress << { success: true }

			##########################################
			## TODO: Replace this with SQL process. ##
			##########################################
			Service.all.find_each do |service|
				mailing_activity = {}
				mailing_activity["service"] = service.label

				mailing_activity["true_count"] = SentMail.where(service_id: service.id, sent: true, message_type: params[:message_type], message_id: params[:message_id]).count
				mailing_activity["true_message"] = SentMail.select(:response_message).where(service_id: service.id, sent: true, message_type: params[:message_type], message_id: params[:message_id]).first
				mailing_activity["false_count"] = SentMail.where(service_id: service.id, sent: false, message_type: params[:message_type], message_id: params[:message_id]).count
				mailing_activity["false_message"] = SentMail.select(:response_message).where(service_id: service.id, sent: false, message_type: params[:message_type], message_id: params[:message_id]).first

				unless mailing_activity["true_count"] == 0 && mailing_activity["false_count"] == 0
					mailing_progress << mailing_activity
				end
			end

			render json: mailing_progress.to_json
		else
			render json: { success: false, message: "Not enough data sent to check mailing progress." }
		end
	end

	def click_tracking
		sent_mail = SentMail.where(tracking_code: params[:tracking_code]).first
		if sent_mail
		  sent_mail.save_open(request.remote_ip) if sent_mail.opened.blank?
		  sent_mail.save_click(params[:url], request.remote_ip)
		  AwsSqs.instance.send_message(sent_mail.id, request.remote_ip)
		# Test for Follow up triggers.
		  triggers_test("click", sent_mail.id)
		# Send the visitor on to the link.
	    end
		redirect_to params[:url]
	end

	def open_pixel
		sent_mail = SentMail.where(tracking_code: params[:tc]).first
		if sent_mail
		  sent_mail.save_open(request.remote_ip) if sent_mail.opened.blank?
		  AwsSqs.instance.send_message(sent_mail.id, request.remote_ip)
		  # Test for Follow up triggers.
		  triggers_test("open", sent_mail.id)
		end
		image_url = "app/assets/images/open.png"

		respond_to do |format|
			format.png do
				File.open(image_url, 'rb') do |f|
					send_data f.read, :type => "image/png", :disposition => "inline"
				end
			end
		end
	end

	def raw_mail
		subject = params[:subject].to_s
		content = params[:content].to_s

		sender = current_user.email
		recipient = "noreply@klickrr.net"

		message = <<-CONTENT
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
					</head>
					<body>
					<img src="http://www.klickrr.net/open.png" />
					#{content}
					<hr style="margin: 15px 0 15px 0;"/><div style="text-align: center; font-size: 0.5em">If you no longer wish to receive these emails, please <a href="http://www.klickrr.net/unsubscribe/me/some-tracking-code">unsubscribe here</a>.</div><div style="text-align: center; font-size: 0.5em">Provided by <a href="klickrr.net">Klickrr.net</a>.</div>
					</body>
					</html>
					CONTENT

		raw_mail = Mail.new do
		  from     "#{sender}"
		  to       "#{recipient}"
		  subject  "#{subject}"

		  text_content = "#{ActionController::Base.helpers.sanitize(content)}"
		  text_part do
		  	body "#{text_content}"
		  end

		  html_part do
		  	content_type 'text/html; charset=UTF-8'
		  	body "#{message}"
		  end
		end

		payload = Hash.new

		payload["email"] = raw_mail.to_s
		payload["options"] = "long"

		#puts payload
		report = SpamClient.new("127.0.0.1", "783").report(raw_mail.to_s)
		mail_result = SpamClient.new("127.0.0.1", "783").check(raw_mail.to_s)
		render json: { success: !eval(mail_result.spam.downcase), score: mail_result.score, report: report }
		# mail_result = HTTParty.post("http://spamcheck.postmarkapp.com/filter",:body => payload )

		# render json: { success: mail_result["success"], score: mail_result[:score], report: mail_result["report"] }
	end

end
