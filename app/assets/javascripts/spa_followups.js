// ****************************************
// ****************************************
// FOR FU INTEGRATION.
// Ensure the Klickrr object has been instantiated.
var Klickrr = Klickrr || {};

// Define the FollowUps for the Klickrr obj.
Klickrr.FollowUps = {
  id: null, // ID assigned when written to the database.
  active: false, // Boolean - whether this trigger should be evaluated in production.
  active_trigger: null, // Track the trigger that is currently selected.
  active_trigger_variables: [], // Array for the variables attached to this trigger.
  types: [], // Collection of followup types - array of hashes.
  active_type: null,
  message_parts: [
    {
      position: 0,
      interval: null,
      subject: null,
      body: null
    }
  ],
  currentInstance: null,
  tempInstance: {},
  active_message: null,
  active_scope: null,
  active_scope_target: null,
  scopes: [{
    id: null,
    name: null,
    label: null,

    triggers: [
      {
        id: null,
        desc: null
      }
    ]

  }]
}

Klickrr.followupPagination.list_id = selected_list;

function hoverIconContainer(message_part_id, action) {
  var that = $("#message_part_" + message_part_id);

  if( action === "enter" ){
    $(that).addClass("followup-icon-hover");
    $("div", that).addClass("followup-icon-hover");
  } else {
    $(that).removeClass("followup-icon-hover");
    $("div", that).removeClass("followup-icon-hover");
  }
}

// Init function - add the scopes and triggers to the Klickrr.FollowUps object.
function loadScopedTriggers(){
  Klickrr.FollowUps.scopes = [];

  $.getJSON("/followup/triggers_by_scope.json", function(data){
    Klickrr.FollowUps.scopes = data["scopes"];
  });
}

function toggleFollowupState(){
  if (Klickrr.FollowUps.active === true) {
    $("#active_followup_switch").bootstrapSwitch('state', true);
  } else {
    $("#active_followup_switch").bootstrapSwitch('state', false);
  }
}

function toggleSaveAsButton(){
  if (Klickrr.FollowUps.id != null) {
    $("#save_as_followup_btn").show();
  } else {
    $("#save_as_followup_btn").hide();
  }
}
// ****************************************
// ****************************************

function updateFollowupUi(){
  followup_scope_available();
  followup_trigger_available();
  activate_followup_types();
  generate_fu_field_btns();
  selBroadcastTitles();
  selWebformTitles();
  followupServersCheck(); 
} 

// ****************************************
// TODO:
//  ID handling when saving and
//  Updating followups.
// ****************************************
function loadSelectedFollowup(followup_id){
  Klickrr.FollowUps.message_parts = [];
  Klickrr.FollowUps.id = followup_id;
  selected_followup = followup_id;
  // var followups = getNode("followups");
   $.getJSON('/followup?id=' + followup_id, function(followup_data){
          $.each(followup_data.messageParts, function(index, item){
            var newItem = item;
            newItem.position = index + 1;
            Klickrr.FollowUps.message_parts.push(newItem);
          })
          Klickrr.FollowUps.active_message = 1;
          Klickrr.FollowUps.currentInstance = followup_data.currentInstance;
          $("#" + followup_data.type).trigger('click');
          followup_trigger = $('ul.triggers li[data-id="' + followup_data.triggerId +'"]');
          updateFollowupUi();
          setTimeout(function(){
            followup_trigger.find('input[type="radio"]').trigger('click');
            if (followup_data.message_type == 'broadcast') {
              $("#broadcast_followup").trigger('click');
              $('#broadcast_sel').val(followup_data.messageId);
            }
            if (followup_data.message_type == 'webform') {
               $("#webform_followup").trigger('click');
            }
            // Set the active Follow up type.

            if (Klickrr.FollowUps.message_parts.length > 1) {
              $("#multi_part_msg").trigger('click');
            } else {
              $("#one_time_msg").trigger('click');
            }
            $('#message_part_count').val(followup_data.messageParts.length);
            $('#message_part_count').trigger('keyup');

            $("#fu_subject_line").val(followup_data.messageParts[0].subject);

            $("#followup_message").code(followup_data.messageParts[0].body);
          }, 400)

            // Hide the trigger warning.
            $("#trigger_warning").hide();

            // Set the subject.
            if (followup_data.variables){
                $.each(followup_data.variables, function(key, value){
                    followup_trigger.find('input.' + key).val(value);
                })
            }

          $("#followup_load").modal("hide");

          $(".save-followup").hide();
          $(".update-followup").show();
          $("#clear_followup_btn").show();
        }
      )    
}

function clearFollowupUI() {
  $("#fu_subject_line").val("");
  $("#followup_message").code('');
  $("#message_part_count").val(1);

  $("#fu_subject_line").attr("disabled", true);
  $('#followup_message').attr("disabled", true);
  $(".btn-followup-ops").attr("disabled", true);
  $('.note-editable.panel-body').attr('contenteditable', false);
  $("#followup_parts").hide();
  $("#delay_before_send").hide();
  $(".save-followup").show();
  $(".update-followup").hide();
  $("#clear_followup_btn").hide();
  $("#save_as_followup_btn").hide();
  $('#trigger_warning').show();

  followup_triggers = $('ul.triggers li');
  $(followup_triggers.get(0)).find('input[type="radio"]').trigger('click');
  followup_triggers.each(function(){
    $(this).find('input[type="text"]').val('');
  });
}

function editFollowups(followup_id){
  selected_followup = followup_id;
  loadSelectedFollowup(followup_id);

  $("#function_tabs a[href='#follow_up']").tab("show");
}

function saveFollowup(){
  if (Klickrr.FollowUps.message_parts[0].body != null && Klickrr.FollowUps.message_parts[0].subject != null) {
    // Inject the list ID into the FollowUps object.
    Klickrr.FollowUps.list_id = selected_list;
    Klickrr.FollowUps.active_scope_trigger = $('.followup-scope li input[checked]').attr('data-scope-id');
    if (!$.isEmptyObject(Klickrr.FollowUps.tempInstance)) {
      Klickrr.FollowUps.services = Klickrr.FollowUps.tempInstance.services;
    } 
    var payload = JSON.stringify(Klickrr.FollowUps);
    $.ajax({
      url: "followup/save",
      type: "POST",
      data: payload,
      contentType: "application/json",
      success: function(data){
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Followups',
          message: 'Follow up saved', type: 'success'};
        Klickrr.FollowUps.id = data.followup_id;
        Klickrr.FollowUps.tempInstance = {};
        $.each(Klickrr.FollowUps.message_parts, function(index, elem){
          elem.id = data.message_parts_array[index];
        })
        // updateListContent();
        $('#followup_page').trigger('page', [Klickrr.followupPagination.page]);
        Klickrr.Helpers.notify(notify_args);
      },
      error: function(){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Followups',
          message: 'Follow up could not be saved.', type: 'danger'};
        Klickrr.Helpers.notify(notify_args);
      }
    });
  } else {
    var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Followups',
      message: 'Follow up could not be saved. Please enter follow up subject and message.', type: 'danger'};
    Klickrr.Helpers.notify(notify_args);
  }
}

//function saveFollowup(){
//  if ( $("#sel_followup_broadcasts option:selected").val() != "Select a broadcast..." && $("#sel_followup_broadcasts option:selected").val() != null ) {
//    if ( $("#followup_subj_line").val() != null && $("#followup_message").val() != null ) {
//      var title = $("#followup_subj_line").val();
//      var body = $("#followup_message").val();
//      var delay;
//
//      if ( $("input[name=followup_options]:checked").val() == "now" ) {
//        delay = null;
//      } else {
//        delay = $("#sel_followup_days").val();
//      }
//      var fu = {
//      //last_bc_id++;
//
//      "id": 99,
//      "title": title,
//      "body": body,
//      "placement": 1,
//      "delay": delay
//    }
//
//    var payload = fu;
//    payload.message_id = $("#sel_followup_broadcasts option:selected").attr("id");
//    payload.message_type = "broadcast";
//
//    // Push the followup to the server.
//    $.ajax({
//      url: "http://localhost:3000/followup/create",
//      //url: "https://www.klickrr.net/webform/save",
//      type: "POST",
//      data: JSON.stringify(payload),
//      dataType: "json",
//      beforeSend: function(xhr) {
//        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
//      },
//      contentType: "application/json",
//      success: function(data){
//        console.log(data);
//
//        // Set the id, based on return from the server.
//        fu.id = data.id;
//      },
//      error: function(){
//        console.log("This didn't seem to work.");
//      }
//    });
//
//    selected_broadcast = $("#sel_followup_broadcasts option:selected").attr("id");
//    var broadcast = getNode("broadcasts");
//    var followups = broadcast[0].followups;
//
//    followups.push( fu );
//
//    $("#followup_subj_line").val("");
//    $("#followup_message").val("");
//    } else {
//      alert("Sorry. Subject and body are required.");
//    };
//
//  } else {
//
//  };
//}

function updateFollowup(){
  if (Klickrr.FollowUps.message_parts[0].body != null && Klickrr.FollowUps.message_parts[0].subject != null) {
    var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Followups', message: 'Updating followup...', type: 'info'};

    Klickrr.Helpers.notify(notify_args);

    var payload = JSON.stringify(Klickrr.FollowUps);

    $.ajax({
      url: "followup/update",
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      dataType: "json",
      contentType: "application/json",
      data: payload,
      success: function(data){
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Followups', message: 'Follow up saved', type: 'success'};
        // updateListContent();
        $('#followup_page').trigger('page', [Klickrr.followupPagination.page]);
        Klickrr.Helpers.notify(notify_args);
      },
      error: function(data){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Followups', message: 'Follow up could not be saved.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      }
    });

    $("#followup_subj_line").val("");
    $("#followup_message").val("");

    $(".save-followup").show();
    $(".update-followup").hide();
    $("#clear_followup_btn").hide();
  } else {
    var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Followups',
      message: 'Follow up could not be saved. Please enter follow up subject and message.', type: 'danger'};
    Klickrr.Helpers.notify(notify_args);
  }
}

function toggleSaveReadiness(msg_type){
  if ( msg_type == "followup" ) {
    if ( $("#sel_followup_broadcasts option:selected").val() != "Select a broadcast..." && $("#sel_followup_broadcasts option:selected").val() != "Select a list to load associated broadcasts..." && $("#followup_subj_line").val() != "" && $("#followup_message").val() != "" ) {

      $(".save-followup").removeAttr("disabled");

      $("#followup_broadcast").removeClass("incomplete-action");
      $("#followup_broadcast").addClass("completed-action");

    } else {
      $(".save-followup").attr("disabled", "disabled");

      $("#followup_broadcast").removeClass("completed-action");
      $("#followup_broadcast").addClass("incomplete-action");
    };
  } else {
    function disable_action_buttons() {
      $("#broadcast_now").attr("disabled", "disabled");
      $(".save-broadcast").attr("disabled", "disabled");
      $("#queue_broadcast_btn").attr("disabled", "disabled");
    }

    if ( $("#master_lists_sel option:selected").val() != "Select a contact list...") {
      if ($("#broadcast_subj_line").val() != "" && $("#broadcast_message").val() != "" && list && list.length > 0 && $('#txt_broadcast_servers').val() != '') {
        $(".save-broadcast").removeAttr("disabled");
        $("#queue_broadcast_btn").removeAttr("disabled");
        $("#broadcast_now").removeAttr("disabled");
      } else {
        disable_action_buttons();
      }

      $('#broadcast_edit_servers').removeAttr('disabled');
    } else {
      disable_action_buttons();
      $('#broadcast_edit_servers').attr('disabled');
    };
  };

  if(list && list.length > 0 && list[0].active_services == "") {
    $('#active_services_is_empty').show();
  } else {
    $('#active_services_is_empty').hide();
  }


  return;
}

  //========================================================
  // Create the links to add fields into broadcast.
  //========================================================
  function generate_fu_field_btns(){
    if ( selected_list != null && String(selected_list) != "undefined" ) {
      var arr = [];
      $.getJSON("list/" + selected_list + "/subscribers/columns.json", function(data){
        //arr.push('<button id="sign_up_link_field" class="btn btn-default" type="button" onClick="followup_insert_token(\'{{sign_up_link}}\');" style="margin: 5px;">sign_up_link</button>');

        $.each(data, function(key,val){
          //arr.push('<button id="' + val["name"] + '_field" class="btn btn-default" type="button" onClick="followup_insert_token(\'{{' + val["name"] + '}}\');" style="margin: 5px;">' + val["name"] + '</button>')
          arr.push('<button id="' + val + '_field" class="btn btn-default" type="button" onClick="followup_insert_token(\'{{' + val + '}}\');" style="margin: 5px;">' + val + '</button>')
        });

        $("#followup_field_tokens").html(arr);
      });
    };
  }

  function followup_insert_token(token){
      var msg = $("#followup_message").val();
      var currentPos = $("#followup_message").getCursorPosition();
      var strLeft = msg.substring(0, currentPos);
      var strMiddle = token;
      // CKEDITOR.instances['followup_message'].insertText( strLeft + strMiddle );
  }

  // Check if we've got a valid list selected.
  function followup_scope_available(){
    if ( selected_list === null ) {
      // Init the Klickrr.FollowUp scope and triggers...
      Klickrr.FollowUps.active_scope = null;

      $("input", ".followup-scope").attr("disabled", "disabled");
      $("select", ".followup-scope").attr("disabled", "disabled");

      $("input:radio", "#followup_action").attr("disabled", "disabled");
    } else {
      // Init the Klickrr.FollowUp scope and triggers...
      Klickrr.FollowUps.active_scope = Klickrr.FollowUps.scopes[0].id;

      $("input", ".followup-scope").attr("disabled", false);
      $("select", ".followup-scope").attr("disabled", false);

      $("input:radio", "#followup_action").attr("disabled", false);

      // Make the selected trigger input editable.
      var parent = $("input:radio", ".triggers").first().parent();

      $(".trigger-value", parent).removeClass("view");
      $(".trigger-value", parent).addClass("edit");
      $(".trigger-value", parent).attr("disabled", false);
    }
  }

  // If a list is selected, make triggers active.
  function followup_trigger_available(){
    if (selected_list === null) {
      // Init the Klickrr.FollowUp scope and triggers...
      Klickrr.FollowUps.active_trigger = null;

      $("input:radio", ".triggers").attr("disabled", "disabled");
      $("input:text", ".triggers").attr("disabled", "disabled");
    } else {
      // Init the Klickrr.FollowUp scope and triggers...
      Klickrr.FollowUps.active_trigger = Klickrr.FollowUps.scopes[0].triggers[0].id;

      $("input:radio", ".triggers").attr("disabled", false);
      // $("input:text", ".triggers").attr("disabled", false);
    }
  }

  // When a list is selected, enable the followup 'types' selector.
  function activate_followup_types(){
    $(".followup-type-disabled", "#followup_type").removeClass("followup-type-disabled").addClass("followup-type-enabled");
  }

  // When the selected scope changes, change the trigger set.
  $('input[name="followup_scope"]').change(function(){
    var scope_id = $(this).attr("data-scope-id");

    // Get the triggers for the current scope.
    var followup_triggers = $.grep(Klickrr.FollowUps.scopes, function(s){ return s.id == scope_id });

    // Let's build an array of strings for the triggers markup.
    var triggers = [];

    $.each(followup_triggers[0].triggers, function(key, val){
      triggers.push('<li class="radio">' + val.desc + '</li>');
    });

    $(".triggers").html( triggers );

    // Set the JS object active scope.
    Klickrr.FollowUps.active_scope = parseInt(scope_id);

    // Reset the JS object active trigger.
    Klickrr.FollowUps.active_trigger = followup_triggers[0].triggers[0].id;

    // When the scope type changes, also change the JS object active scope target.
    if (parseInt(Klickrr.FollowUps.active_scope) === parseInt(Klickrr.FollowUps.scopes[0].id)) {
      // User has selected "List" as the target.
      Klickrr.FollowUps.active_scope_target = null;
    } else {
      // Get the name of the active scope, and find the select value.
      var active_scope = $.grep(Klickrr.FollowUps.scopes, function(scopes){ return scopes.id === Klickrr.FollowUps.active_scope })
      Klickrr.FollowUps.active_scope_target = parseInt( $("#" + active_scope[0].name + "_sel").val() );
    }
  });

  // When one of the scope selects changes, set the active scope target in the JS object.
  $(document).on("change", ".scope-select", function(){
    // $(".scope-select").change(function(){
    Klickrr.FollowUps.active_scope_target = parseInt( $(this).val() );
  });

  //========================================================
  // END links to add fields into broadcast.
  //========================================================

$(function(){
  // ****************************************
  // ****************************************
  // FOR FU INTEGRATION.
  $('#followup_message').summernote({
    height: 300,                 // set editor height

    minHeight: null,             // set minimum height of editor
    maxHeight: null,             // set maximum height of editor

    focus: false,                 // set focus to editable area after initializing summernote
    onChange: function(e) {
      Klickrr.FollowUps.message_parts[ Klickrr.FollowUps.active_message - 1].body = $("#followup_message").code();
    }
  });

  // Init the Bootstrap switch.
  $("#active_followup_switch").bootstrapSwitch();

  // Init the follow up scope dropdowns.
  selBroadcastTitles();

  // When a multi-part icon has a hover event...
  $(".followup-icon-container").on({
    mouseenter: function(){
      $(this).addClass("followup-icon-hover");
      $("div", this).addClass("followup-icon-hover");
    },
    mouseleave: function(){
      $("div", this).removeClass("followup-icon-hover");
      $(this).removeClass("followup-icon-hover");
    }
  });

  // Change active state when the "Active" switch is toggled.
  $("#active_followup_switch").on('switchChange.bootstrapSwitch', function(event, state) {
    if (state === true) {
      Klickrr.FollowUps.active = true;
    } else {
      Klickrr.FollowUps.active = false;
    }
  });

  // Set a follow up type (i.e. one-off, thank-you, course, etc.) to active.
  $(".followup-type").click(function(){
    // Set the followup type in the JS object.

    Klickrr.FollowUps.active_type = $(this).attr("id");

    // Clean any already selected followup type.
    $(".followup-type").removeClass("followup-type-active");
    $(".followup-type-close").hide();

    // Set the selected type.
    $(this).addClass("followup-type-active");
    $(".followup-type-close", this).css("display", "block");

    // Enable the messages section.
    $("#fu_subject_line").attr("disabled", false);

    // Enabled the disabled Summernote editor.
    $('#followup_message').destroy();
    $('#followup_message').attr("disabled", false);
    $('#followup_message').summernote({
      height: 300,
      minHeight: null,
      maxHeight: null,
      focus: false,
      onChange: function(e) {
        Klickrr.FollowUps.message_parts[ Klickrr.FollowUps.active_message - 1].body = $("#followup_message").code();
      }
    });

    // Enable the message op buttons.
    $(".btn-followup-ops").attr("disabled", false);

    // Check to see if this is a multi-part message.
    if ( $(this).hasClass("multi-message") == true ) {
      $("#followup_parts").show();
      // $("#delay_before_send").show();

      // Simulate click to make first part of message active.
      setTimeout(function(){
      $("#message_part_count").keyup();
      $("#message_part_1").click();
      }, 200);
    } else {
      $("#followup_parts").hide();
      $("#delay_before_send").hide();

      // Let's set the JS object to null.
      // Klickrr.FollowUps.message_parts = [];

      // // Create the message part hash.
      // var msg_part = { position: 1, interval: 0, subject: null, body: null };

      // Klickrr.FollowUps.message_parts.push(msg_part);

      // Set the active message part to the first.
      Klickrr.FollowUps.active_message = 1;
    }

    // Clear message editing...
    $("#fu_subject_line").val("");
    $("#followup_message").code("");
    $("#message_part_count").val(1);
    $("#message_part_delay").val(1);

    // Clear background color on previously selected icon.
    $(".followup-icon-container").css("background-color", "#394754");
    $("div", ".followup-icon-container").css("background-color", "#394754");
  });

  // Close the currently selected followup type.
  $(".followup-type-close").click(function(event){
    // Null the active type in the JS object.
    Klickrr.FollowUps.active_type = null;

    event.preventDefault();
    event.stopPropagation();

    $(".followup-type").removeClass("followup-type-active");
    $(".followup-type-close").hide();

    // Hide followup parts.
    $("#followup_parts").hide();

    // Hide delay days.
    $("#delay_before_send").hide();

    // Disable the message subject.
    $("#fu_subject_line").attr("disabled", "disabled");

    // Disable the Summernote editor.
    $('#followup_message').destroy();
    $('#followup_message').attr("disabled", "disabled");
    $('#followup_message').summernote({
      height: 300,
      minHeight: null,
      maxHeight: null,
      focus: false,
      onChange: function(e) {
        Klickrr.FollowUps.message_parts[ Klickrr.FollowUps.active_message - 1].body = $("#followup_message").code();
      }
    });

    // Disable the message op buttons.
    $(".btn-followup-ops").attr("disabled", "disabled");
  });

  // Handle triggers.
  $('body').on('change', ".triggers input:radio", function(){
    var parent = $(this).parent();

    // Check to see if *ANY* radios are checked.
    if ( $("input:radio:checked", "#followup_action").length > 0 ){
      // Set the active trigger on the model.
      Klickrr.FollowUps.active_trigger = parseInt($(this).attr("data-trigger-id"));

      // Enabled the followup types.
      $(".followup-type-active", "#followup_type").removeClass("followup-type-active");
      $(".followup-type-disabled", "#followup_type").removeClass("followup-type-disabled").addClass("followup-type-enabled");

      // If there are variables, highlight them.
      if ( $(this).siblings().length > 0 ) {
        $.each($(this).siblings(), function(key, val){
          // $(val).css("border", "2px solid red");

        });

        // If the variables already have a value, get it.
      } else {
        // If there are no variables for this trigger, nullify in the JS object.
        Klickrr.FollowUps.active_trigger_variables = [];
      }

      // All text boxes are uneditable.
      $(".trigger-value").removeClass("edit");
      $(".trigger-value").addClass("view");
      $(".trigger-value").attr("disabled", "disabled");

      // Make the current text box editable.
      $(".trigger-value", parent).removeClass("view");
      $(".trigger-value", parent).addClass("edit");
      $(".trigger-value", parent).attr("disabled", false);
    } else {
      $(".followup-type-icon", "#followup_type").addClass("followup-type-active");
      $("p", "#followup_type").addClass("followup-type-disabled");
      $(".followup-type", "#followup_type").removeClass("followup-type-enabled").addClass("followup-type-disabled");

      // Make the current text box uneditable.
      $(".trigger-value", parent).removeClass("edit");
      $(".trigger-value", parent).addClass("view");
      $(".trigger-value", parent).attr("disabled", "disabled");

      // Disable the active followup type.
      $(".followup-type").removeClass("followup-type-active");
      $(".followup-type-close").hide();

      Klickrr.FollowUps.active_type = null;
      Klickrr.FollowUps.active_trigger = null;

      // Hide followup parts.
      $("#followup_parts").hide();

      // Hide delay days.
      $("#delay_before_send").hide();

      // Disable the message subject.
      $("#fu_subject_line").attr("disabled", "disabled");

      // Disable the Summernote editor.
      $('#followup_message').destroy();
      $('#followup_message').attr("disabled", "disabled");
      $('#followup_message').summernote({
        height: 300,
        minHeight: null,
        maxHeight: null,
        focus: false,
        onChange: function(e) {
          Klickrr.FollowUps.message_parts[ Klickrr.FollowUps.active_message - 1].body = $("#followup_message").code();
        }
      });

      // Disable the message op buttons.
      $(".btn-followup-ops").attr("disabled", "disabled");
    }
  });
  
  $('body').on('click', '#followup_edit_servers', function(){
    window.ActiveServices.showServices('followup', Klickrr.FollowUps.currentInstance, Klickrr.FollowUps.tempInstance);
  })

  // Make sure the trigger value is a number, and handle it when it changes.
  $('body').on('keyup', ".trigger-value", function(){
      // Check to see if there are multiple text fields for this trigger, or just one.
      var parent = $(this).parent();
      var var_count = $(".trigger-value", parent).size();

      var count = $(this).val();
      // Make sure our value is an integer greater than 1.
      if ( count === String(parseInt(count, 10)) && parseInt(count) >= 1 ) {
          // Let's set the JS object to null.
          Klickrr.FollowUps.active_trigger_variables = [];

          var vars = [];
          if (var_count > 0) {
              //***************************************
              // Manage the 1st (x) variable.
              //***************************************
              var var_x = { x: $(".x", parent).val() };
              var x_count = $(var_x).size();

              if ( x_count === parseInt(x_count, 10) && parseInt(x_count) >= 1 ) {
                  vars.push( var_x );
              } else {
                  $("#trigger_warning").show();
                  //return false;
              }
              //***************************************
              // End 1st var management.
              //***************************************

              //***************************************
              // Check for a 2nd (y) variable.
              //***************************************
              if ( var_count > 1 ) {
                  var var_y = { y: $(".y", parent).val() };
                  var y_count = $(var_y).size();

                  if ( y_count === parseInt(y_count, 10) && parseInt(y_count) >= 1 ) {
                      vars.push( var_y );

                      // Hide the warning message.
                      $("#trigger_warning").hide();
                  } else {
                      $("#trigger_warning").show();
                      //return false;
                  }
                  //***************************************
                  // End 2nd var management.
                  //***************************************
              }
          }

          Klickrr.FollowUps.active_trigger_variables = vars;

          // Hide the warning message.
          $("#trigger_warning").hide();
      } else {
          // Something is wrong - show the warning message.
          Klickrr.FollowUps.active_trigger_variables = [];

          $("#trigger_warning").show();
      }
  } );

  // Make sure the part count is a number, and handle it when it changes.
  $("#message_part_count").keyup(function(){
    var count = $("#message_part_count").val();
    // Make sure our value is an integer greater than 1.
    if ( count === String(parseInt(count, 10)) && parseInt(count) >= 1 ) {
      // Let's set the JS object to null.
      $("#message_part_icons").html("");
      $("#message_part_icons").append('<div style="float: left;" class="followup-type-icon followup-icon-container" id="message_part_1" data-message-part="1" onMouseOver="hoverIconContainer(1, \'enter\')" onMouseOut="hoverIconContainer(1, \'exit\')"><div class="followup-type-icon followup-icon followup-inner-border">1</div></div>');
      if (Klickrr.FollowUps.message_parts.length == 0) {
        // Create the FIRST message part hash.
        var msg_part = { position: 1, interval: 0, subject: null, body: null };
        // Update the FIRST Klickrr message array.
        Klickrr.FollowUps.message_parts.push(msg_part);
      }

      for (var i = 1; i < count; i++) {
        $("#message_part_icons").append('<div style="float: left;" class="followup-icon-arrow"><i class="fa fa-arrow-right"></i></div>');
        $("#message_part_icons").append('<div style="float: left;" class="followup-type-icon followup-icon-container" id="message_part_' + parseInt(i + 1) + '" data-message-part="' + parseInt(i + 1) + '" onMouseOver="hoverIconContainer(' + parseInt(i + 1) + ', \'enter\')" onMouseOut="hoverIconContainer(' + parseInt(i + 1) + ', \'exit\')"><div class="followup-type-icon followup-icon followup-inner-border">' + parseInt(i + 1) + '</div></div>');

        // Create the message part hash.
        if (Klickrr.FollowUps.message_parts.length < i + 1)  {
          var msg_part = { position: i + 1, interval: 0, subject: null, body: null };
          Klickrr.FollowUps.message_parts.push(msg_part);
        }
      }

      $("#message_part_icons").append('<div style="clear: both;"></div>');

      // Hide the warning message.
      $("#message_part_warning").hide();
    } else {
      // Something is wrong - show the warning message.
      $("#message_part_warning").show();
    }

    // Make the first message part active.
    $("#message_part_1").click();
  });

  // Change the follow up step we are editing, based on which icon has been clicked.
  $("body").on("click", ".followup-icon-container", function(){
    var msg_id = $(this).attr("data-message-part");

    // If we've got multiple parts, show the 'delay' box, too.
    var count = $("#message_part_count").val();
    if (parseInt(count) > 1 && msg_id > 1) { $("#delay_before_send").show() } else { $("#delay_before_send").hide() };

    // Set the active message.
    Klickrr.FollowUps.active_message = msg_id;

    // Set the current message.
    var msg = Klickrr.FollowUps.message_parts[parseInt(msg_id - 1)];

    // Clear background color on previously selected icon.
    $(".followup-icon-container").css("background-color", "#394754");
    $("div", ".followup-icon-container").css("background-color", "#394754");

    $(this).css("background-color", "firebrick");
    $("div", this).css("background-color", "firebrick");

    // Set the delay amount.
    $("#message_part_delay").val( msg.interval );

    // Set the current message subject.
    $("#fu_subject_line").val( msg.subject );

    // Set the current message body.
    $("#followup_message").code( msg.body );
  });

  // Update the model when the message_part_delay value changes.
  $("body").on("keyup", "#message_part_delay", function(){
    var count = $("#message_part_delay").val();

    // Make sure our value is an integer greater than 1.
    if ( count === String(parseInt(count, 10)) && parseInt(count) >= 1 ) {
      // Set the current message.
      var msg = Klickrr.FollowUps.message_parts[Klickrr.FollowUps.active_message - 1];

      msg.interval = $(this).val();

      // Hide the warning message.
      $("#message_delay_warning").hide();
    } else {
      // Something is wrong - show the warning message.
      $("#message_delay_warning").show();
    }
  });

  // Update the model when the message subject line changes.
  $("body").on("change", "#fu_subject_line", function(){
    // Set the current message.
    var msg = Klickrr.FollowUps.message_parts[ Klickrr.FollowUps.active_message - 1 ];

    msg.subject = $(this).val();
  });

  // Test the spam score of a followup message.
  $("#followup_spam_test_btn").click(function(){
    var subj = $("#fu_subject_line").val();
    var content = $("#followup_message").val();
    var payload = { "subject": subj, "content": content };

    $.ajax({
      url: "mail/raw",
      type: "POST",
      data: payload,
      success: function(data){
        $("#followup_test_results").html("");
        $("#followup_spam_score").text( data["score"] );

        var spam_issues = "<table><tr><thead><th>Points</th><th>Rule Name</th><th>Description</th></thead></tr>";
        $.each(data["report"].split("\n"), function(key, val){
          var cols = val.split(" ");

          //Drop the first element, as it is empty.
          cols.shift();

          if (isNaN(cols[0]) == true) {
            return;
          };

          var col_1 = cols.shift();
          var col_2 = cols.shift();
          var col_3 = cols.join(" ");

          spam_issues += '<tr><td>' + col_1 + '</td><td>' + col_2 + '</td><td>' + col_3 + '</td></tr>';
        });

        spam_issues += "</table>";

        $("#followup_test_results").html( spam_issues );

        var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Followups', message: 'Spam test was executed.', type: 'success'};
        Klickrr.Helpers.notify(notify_args);
      },
      error: function(data){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Followups', message: 'There was a problem. Email could not be retrieved.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      }
    });
  });

  // ******************************************************
  // TODO: Reintroduce followup test emails.
  // ******************************************************
  // Let the user preview this email by sending it to them.
  //$("#followup_test_modal").click(function(){
  //  var subj = $("#fu_subject_line").val();
  //  var content = $("#followup_message").val();
  //  var email = $("#followup_test_email_addr").val();
  //  var payload = {send_type: "email_preview", subject: subj, content: content, email: email};
  //
  //  $.ajax({
  //    url: "followup/test/send",
  //    type: "POST",
  //    data: payload,
  //    success: function(data){
  //      if ( data["success"] === true ) {
  //        var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Followups', message: 'Test email has been sent.', type: 'success'};
  //        Klickrr.Helpers.notify(notify_args);
  //      } else {
  //        var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Followups', message: 'Test email could not be sent.', type: 'warning'};
  //        Klickrr.Helpers.notify(notify_args);
  //      }
  //    },
  //    error: function(data){
  //      var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Followups', message: 'Test email could not be sent.', type: 'warning'};
  //      Klickrr.Helpers.notify(notify_args);
  //    }
  //  });
  //
  //  $("#broadcast_test").modal("hide");
  //});

  // Update the model when the content changes.
  // $("body").on("change", '#followup_message', function(){
  //$("body").on("keyup", "#followup_message", function(){
  //  Klickrr.FollowUps.message_parts[ Klickrr.FollowUps.active_message - 1].body = $("#followup_message").code();
  //});
  // ****************************************
  // ****************************************



  // Followup button click events.
  $("#add_broadcast").click(function(){
    $("#function_tabs a[href='#broadcast']").tab("show");
  });

  $("#followup_send_later").click(function(){
    $("#followup_btn_check").removeClass("fa-check-square-o");
    $("#followup_btn_check").addClass("fa-square-o");
    $("#followup_now").removeClass("active");
    $("#followup_now").attr("aria-pressed", "false");

    $("#followup_schedule").slideDown("fast");
  });

  //$("#followup_now").click(function(){
  $("input[name=followup_options]").change(function(){
    if ( $("input[name=followup_options]:checked").val() == "now" ) {
      console.log( $("input[name=followup_options]:checked").val() );
      $("#followup_btn_check").removeClass("fa-square-o");
      $("#followup_btn_check").addClass("fa-check-square-o");

      $("#followup_schedule").slideUp("fast");
    } else {
      console.log( $("input[name=followup_options]:checked").val() );
      $("#followup_btn_check").removeClass("fa-check-square-o");
      $("#followup_btn_check").addClass("fa-square-o");

      $("#followup_schedule").slideDown("fast");
    };
  });

  $("#followup_load_list").change(function(){
    if ( $("#followup_load_list").val() != "Select a contact list..." ) {
      selected_list = $("#followup_load_list").val();

      loadFollowupDialogInit();
    } else {
      $("#followup_load_broadcasts").html("<option>No contact list selected...</option>");
      $("#followup_load_message").html("<option>No broadcast selected...</option>");
    };

    $("#load_fu_message_btn").attr("disabled", true);
  });

  $("#followup_load_broadcasts").change(function(){
    selected_list = $("#followup_load_list").val();
    selected_broadcast = $("#followup_load_broadcasts option:selected").val();
    var broadcast = [];
    var followups = [];
    //followups.push("<option>Select a message to load...</option>");

    broadcast = getNode("broadcasts");

    $.each(broadcast[0].followups, function(key, val){
      // ******************************************
      // Unscheduled changes list-item styling if a broadcast or followup is not scheduled.
      // ******************************************
      var unscheduled;
      if ( val.delay == null ) { unscheduled = " list-group-item-danger" } else { unscheduled = "" };
      followups.push('<a href="#" class="list-group-item' + unscheduled + '" id="fu_item_' + val.id + '" onClick="loadSelectedFollowup(' + val.id + ')"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button></div><p class="list-group-item-text">' + val.body + '</p></a>');
      //followups.push('<option value="' + val.id + '" style="background: red;">' + val.title + '</option>');
    });

    $("#followup_load_message").html( followups );
    $("#load_fu_message_btn").attr("disabled", true);
  });

  $("#followup_load_message").change(function(){
    if ( $("#followup_load_message").val() != "No follow ups associated with the selected broadcast..."  && $("#followup_load_message").val() != "Select a message to load...") {
      $("#load_fu_message_btn").attr("disabled", false);
    } else {
      $("#load_fu_message_btn").attr("disabled", true);
    }
  });

  $("#load_fu_message_btn").click(function(){
    var selected = $("#followup_load_list").val();
    var broadcast = $("#followup_load_broadcasts").val();
    var followup = $("#followup_load_message").val();
    var list = [];
    var broadcasts = [];
    var followups = [];

    /*selected_broadcast = broadcast;
    selected_followup = followup;
    followups = getNode("followups");

    $("#followup_subj_line").val( followups[0].title );
    $("#followup_message").val( followups[0].body );*/

    loadSelectedFollowup();

    $("#followup_load").modal("hide");

    var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Followups',
      message: 'Follow up message loaded', type: 'success'};
    Klickrr.Helpers.notify(notify_args);
  });

  $("#clear_followup_btn").click(function(){
    $("#followup_subj_line").val("");
    $("#followup_message").val("");

    $(".save-followup").show();
    $(".update-followup").hide();
    $("#clear_followup_btn").hide();

    var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Followups', message: 'You are editing new follow up.', type: 'success'};
    Klickrr.Helpers.notify(notify_args);
  });

  $("#save_followup_btn").click(function(){
    saveFollowup();

    $('#followup_page').trigger('page', [Klickrr.followupPagination.page]);
  });

  $("#update_followup_btn").click(function(){
    updateFollowup();
  });

  $("#sel_followup_broadcasts").change(function(){
    if ( $("#sel_followup_broadcasts option:selected").val() != "Select a broadcast..." && $("#sel_followup_broadcasts option:selected").val() != "Select a list to load associated broadcasts..." ) {
      selected_broadcast = $("#sel_followup_broadcasts option:selected").attr("id");
    } else {
      selected_broadcast = null;
    }

      toggleSaveReadiness('followup');
    //} else {
    //  $("#followup_broadcast").removeClass("completed-action");
    //  $("#followup_broadcast").addClass("incomplete-action");
    //};
  });

  $("#followup_subj_line").change(function(){
    toggleSaveReadiness('followup');
  });

  //CKEDITOR.instances['followup_message'].on("change", function(){
  //  toggleSaveReadiness('followup');
  //});

  $("#save_followup").click(function(){
    // TODO: Determine if this is a new or existing record...
    saveFollowup();
  });

  $("#update_followup").click(function(){
    console.log("Hmmmm...");
    updateFollowup();
  });
  // End Followup button events.

  followupTotalPages({
    page: Klickrr.followupPagination.page,
    page_size: Klickrr.followupPagination.page_size,
    list_id: selected_list
  });

  // Fetch the contents of the current follow up list page.
  $('#followup_page').on("page", function(event, num){
    Klickrr.followupPagination.page = num;
    $.ajax({
      url: "/followup/list",
      type: "GET",
      data: Klickrr.followupPagination,
      dataType: "html",
      success: function(data){
        $('#followup_manager').html(data);
        followupTotalPages(Klickrr.followupPagination);
      }
    });
  });

  // Fetch total number of follow up pages from the server.
  function followupTotalPages(data_page){
    $.ajax({
          url: "/followup/total_pages",
          data: data_page,
          type: "GET",
          dataType: "json",
          success: function(data){
            Klickrr.followupPagination.last_page = data.total_pages;
            $('#followup_page').bootpag({
              total: Math.max.apply(null, [1, data.total_pages]),
              maxVisible: 3
            })
          }
        })
  }

  $('body').on('click', '#followup_page_size a', function(){
    Klickrr.followupPagination.page_size = $(this).attr('data-page-size');
    $('#followup_page').trigger('page', [1]);
    $('#followup_page_size li').removeClass('active');
    $('#followup_page_size a[data-page-size="' + Klickrr.followupPagination.page_size + '"]').parent().addClass('active');
  })

  $('body').on('click', 'a.edit-followup', function(){
    clearFollowupUI();
    $('#master_lists_sel').val($(this).attr('data-list-id'));
    $('#master_lists_sel').trigger('change');
    editFollowup($(this).attr('data-id'));
  })

  function editFollowup(followup_id) {
    loadSelectedFollowup(followup_id);
    $("a[href='#follow_up']").tab("show");
  }

  $('body').on('click', 'a[id^="clone_followup_"]', function(){
    cloneFollowup($(this).attr('data-id'));
    $('#followup_page').trigger('page', [Klickrr.followupPagination.page]);
  })

  $('body').on('click', 'a[id^="test_followup_"]', function(){
    $('#master_lists_sel').val($(this).attr('data-list-id'));
    $('#master_lists_sel').trigger('change');
    loadSelectedFollowup($(this).attr('data-id'));
    $('#' + $(this).attr('data-message-type')).trigger('click');
    $("#followup_spam_test_btn").trigger('click');
  })

  $('body').on('click', 'a[href="#follow_up"]', function(){
    clearFollowupUI();
    Klickrr.FollowUps.currentInstance = null;
    if (selected_list) {
      setTimeout(function(){
        $('#master_lists_sel').trigger('change');
      }, 200);
    }
  })

  // Update the order of the broadcast list, based on the column header clicked.
  $('body').on('click', 'a.followup_order', function(){
    Klickrr.followupPagination.order = $(this).attr('data-field') + ' ' + $(this).attr('data-order-status');
    $('#followup_page').trigger('page', [Klickrr.followupPagination.page]);
  })

    // Delete follow ups from the 'Follow up Manager' list.
    $(document).on ("click", ".delete-followup-link", function(){
        $("#delete_followup_modal_verify").attr("data-id", $(this).attr("data-id") );
    });

    // Delete follow ups from the 'Follow up Manager' list verification.
    $(document).on ("click", "#delete_followup_modal_verify", function(){
        var followup_id = $(this).attr("data-id");

        $('tr[data-followup-id="' + followup_id + '"]').hide();

        $.ajax({
            url: "followup/delete/" + followup_id,
            type: "DELETE",
            async: true,
            success: function(data){
                if ( data.success === true ) {
                    // Delete the hidden table row.
                    $('tr[data-followup-id="' + followup_id + '"]').destroy();
                    $('#followup_page').trigger('page', [Klickrr.followupPagination.page]);
                } else {
                    // Notify of failure.
                    var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Followups', message: data.message, type: 'warning'};

                    Klickrr.Helpers.notify(notify_args);

                    // Unhide the hidden table row.
                    $('tr[data-followup-id="' + followup_id + '"]').show();
                }

            },
            error: function(data){
                // Notify of failure.
                var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Followups', message: "Cannot connect to the internet. Changes are unsaved.", type: 'warning'};

                Klickrr.Helpers.notify(notify_args);

                // Unhide the hidden table row.
                $('tr[data-followup-id="' + followup_id + '"]').show();
            }
        });
    });

    // Toggle a follow up active and inactive from the 'Follow up Manager' list.
    $(document).on("click", ".toggle-followup-state-link", function(){
        var that = this;
        var state;

        if ( $(this).attr("data-active") === "true" ) {
            state = false;
            $(this).attr("data-active", false);
            $("i", this).removeClass("fa-toggle-on");
            $("i", this).addClass("fa-toggle-off");
        } else {
            state = true;
            $(this).attr("data-active", true);
            $("i", this).addClass("fa-toggle-on");
            $("i", this).removeClass("fa-toggle-off");
        }

        $.ajax({
            url: "followup/state/toggle",
            type: "POST",
            data: { current_state: state, followup_id: $(that).attr("data-id") },
            async: true,
            // success: function(data){},
            error: function(data){
                // Notify of failure.
                var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Followups', message: data.message, type: 'warning'};

                Klickrr.Helpers.notify(notify_args);

                // Revert the new values.
                if ( state === true ) {
                    $(that).attr("data-active", false);
                    $("i", that).removeClass("fa-toggle-on");
                    $("i", that).addClass("fa-toggle-off");
                } else {
                    $(that).attr("data-active", true);
                    $("i", that).addClass("fa-toggle-on");
                    $("i", that).removeClass("fa-toggle-off");
                }
            }
        });
    });
});
