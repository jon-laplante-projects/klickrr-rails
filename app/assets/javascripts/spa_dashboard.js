var Klickrr = Klickrr || {};

Klickrr.Statistics = {
  BroadcastProgress: function(message_id, message_type){
    var stats = $.getJSON("mail/progress/" + message_id + "/" + message_type);

    return JSON.stringify(stats);
  }
};

Klickrr.Dashboard = {
  UpdateBroadcastProgress: function(bc_id){
    var stat_str = "";

    $.getJSON("mail/progress/" + bc_id + "/broadcast", function(data){
      $.each(data, function(key, val){
        if (key > 0) {
          stat_str += '<table class="table table-condensed">';
          stat_str += '<tr>';
          stat_str += '<thead>';
          stat_str += '<th colspan="2">' + val.service + '</th>';
          stat_str += '</thead>';
          stat_str += '</tr>';
          
          if ( parseInt(val.true_count) > 0 ) {
            stat_str += '<tr class="success">';
            stat_str += '<td>' + val.true_count + ' sent</td>';
            stat_str += '<td>' + val.true_message.response_message + '</td>';
            stat_str += '</tr>';  
          };
          
          if ( parseInt(val.false_count) > 0 ) {
            stat_str += '<tr class="danger">';
            stat_str += '<td>' + val.false_count + ' failed</td>';
            stat_str += '<td>' + val.false_message.response_message + '</td>';
            stat_str += '</tr>';
          };
          
          stat_str += '<tr>';
          stat_str += '<td>' + parseInt(val.true_count + val.false_count) + ' attempted</td>';
          stat_str += '<td></td>';
          stat_str += '</tr>';
          stat_str += '</table>';
        };
      });

      $("#broadcast_progress_stats_container").html(stat_str);
    });
  }
};

  // Dashboard button click events
    $("#dash_add_list").click(function(){
      $("#function_tabs a[href='#lists']").tab("show");
    });

    $(".followup_sortable").on("drop", function(event){
      /*event.preventDefault();  
      event.stopPropagation();*/
      console.log("Dropped...");
    });

    $("input[name='dash_broadcast_timeframe']:radio").on("change", function(){
      if ( $("input[name='dash_broadcast_timeframe']:checked").attr("id") == "dash_broadcast_sched" ) {
        $("#sched_broadcasts").show();
        $("#hist_broadcasts").hide();
      } else {
        $("#sched_broadcasts").hide();
        $("#hist_broadcasts").show();
      };
    });
    
    function quickStatsInit(bc_id){
      Klickrr.Dashboard.UpdateBroadcastProgress(bc_id);

      $("#broadcast_quick_stats_mod").modal("show");
    }
    /*$("#copy_sched_broadcast_1").click(function(){
      
      $("#sched_broadcasts").append( $("#sched_broadcast_1").clone() );
    });
    $("#copy_sched_broadcast_2").click(function(){
      
      $("#sched_broadcasts").append( $("#sched_broadcast_2").clone() );
    });
    $("#copy_sched_broadcast_3").click(function(){
      
      $("#sched_broadcasts").append( $("#sched_broadcast_3").clone() );
    });*/

    /*$("#copy_sched_followup_1").click(function(){
      $("#dash_followups_list").append( $("#sched_followup_1").clone() );
    });
    $("#copy_sched_followup_2").click(function(){
      $("#dash_followups_list").append( $("#sched_followup_2").clone() );
    });
    $("#copy_sched_followup_3").click(function(){
      $("#dash_followups_list").append( $("#sched_followup_3").clone() );
    });*/
    // End Dashboard button events


      // Dashboard functions.
  var dash_followups = $('#dash_followups_list');

  dash_followups.sortable({
      // Only make the .panel-heading child elements support dragging.
      // Omit this to make then entire <li>...</li> draggable.
      //handle: '.panel-heading', 
      update: function() {
          $('.followup_sortable', dash_followups).each(function(index, elem) {
               var $listItem = $(elem),
                   newIndex = $listItem.index();

                   console.log($listItem);

               // Persist the new indices.
               var ids = $listItem.context.id.split("_");
               selected_followup = ids[ids.length - 1];
               var followup = getNode("followups");

               followup[0].placement = index + 1;
          });
      }
  });


  $("#sel_dashboard_lists").change(function(){
      getSelectedLists( $("#sel_dashboard_lists option:selected").val() );
  });

  // Get the dashboard list analytics.
    function listAnalytics(){
        var list_id = { list_id: selected_list };
        $.ajax({
            url: "dashboard/list/analytics",
            type: "POST",
            data: list_id,
            async: true,
            success: function(data){
                // console.log("Data: " + data);
            },
            error: function(data){

            }
        });
    }

$(function(){
    // Handle dashboard tab launch buttons.
    $(".launch-services").click(function(){
        $("#function_tabs a[href='#services']").tab("show");
    });

    $(".launch-analytics").click(function(){
        $("#function_tabs a[href='#analytics']").tab("show");
    });

    $(".launch-broadcasts").click(function(){
        $("#function_tabs a[href='#broadcast_list']").tab("show");
    });

    $(".launch-follow-ups").click(function(){
        $("#function_tabs a[href='#followup_list']").tab("show");
    });

    $(".launch-lists").click(function(){
        $("#function_tabs a[href='#lists_list']").tab("show");
    });

    $(".launch-webforms").click(function(){
        $("#function_tabs a[href='#web_form']").tab("show");
    });
});
  // End dashboard functions.