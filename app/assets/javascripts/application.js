// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require vendor
//= require jquery
//= require jquery_ujs
//= require jquery-ui
// require bootstrap-sprockets
// require jquery.prettyPhoto
//= require jquery.bootpag.min
//= require sortable
//= require modernizr
//= require placeholder
//= require moment
//= require bootstrap-datetimepicker.min
// require ckeditor-jquery
//= require jquery.csv-0.71.min
// require formbuilder
//= require jquery-fileupload.min
//= require jQuery.succinct.min
//= require papaparse.min
//= require jquery.feedback_me
//= require jquery.backstretch.min
//= require jquery.wheelcolorpicker
//= require bootbox.min
//= require bootstrap-notify.min
//= require bootstrap-switch.min
//= require bootstrap-colorpicker.min
//= require bootstrap-colorpicker-plus.min
//= require summernote.min
//= require formValidation.popular.min
//= require formValidation.bootstrap.min
// require Chart
// require excanvas
//= require spa
//= require spa_analytics
//= require spa_broadcasts
//= require spa_dashboard
//= require spa_followups
//= require spa_lists
//= require spa_services
//= require spa_services_rotation
//= require spa_webforms
//= require webform/sortable_items
//= require webform/form_constructor
//= require webform/validation
//= require webform/colorpicker
//= require webform/manager
//= require webform/steps
//= require helpers
//= require broadcast_status_updater
//= require router
//= require clipboard
//= require active_services
