fb = new Formbuilder({
  selector: '.fb-main',
  bootstrapData: [
    {
      "label": "Do you have a website?",
      "field_type": "website",
      "required": false,
      "field_options": {},
      "cid": "c1"
    },
    {
      "label": "Please enter your clearance number",
      "field_type": "text",
      "required": true,
      "field_options": {},
      "cid": "c6"
    },
    {
      "label": "Security personnel #82?",
      "field_type": "radio",
      "required": true,
      "field_options": {
          "options": [{
              "label": "Yes",
              "checked": false
          }, {
              "label": "No",
              "checked": false
          }],
          "include_other_option": true
      },
      "cid": "c10"
    },
    {
      "label": "Medical history",
      "field_type": "file",
      "required": true,
      "field_options": {},
      "cid": "c14"
    }
  ]
});

fb.on('save', function(payload){
  alert("Savin'!");
  // Turn JSON back to object.
  var payload_obj = JSON.parse(payload);

  payload_obj.user_id = $("#uid").val();
  payload_obj.form_name = $("#webform_name").val();
  payload_obj.list_id = $(selected_list);
  console.log("payload_obj: " + payload_obj);

  //payload = JSON.stringify(payload_obj);
  payload = payload_obj;
  console.log(payload);

  $.ajax({
    url: "/webform/save",
    //url: "https://www.klickrr.net/webform/save",
    type: "POST",
    data: payload,
    dataType: "json",
    success: function(data){
      console.log("Success returned.");
    },
    error: function(){
      console.log("This didn't seem to work.");
    }
  });
})