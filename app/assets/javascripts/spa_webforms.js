var Klickrr = Klickrr || {};

Klickrr.Webform = {
	group_id: null,
	form_code: null,
	validatejs: null,
	load: function(group_id){
		////////////////////////////////////////////////////////////
		// TODO: Load and persist the list_field_group.id so we
		// can easily track where this data lives for updates
		// and/or deletion.
		////////////////////////////////////////////////////////////
    $('[id^="webform_field_switch_"]').bootstrapSwitch('state', false)
		
		$.getJSON("webform/" + group_id + "/load", function(data){

      var data_style = '';
			if ( data.style !== "main-style" && data.style != null ) {
				$.ajax({
					url: "webform/template/" + data.style,
					dataType: "text",
					async: true,
					success: function(data){
						data_style = data;
					},
					error: function(){
						console.log("Didn't work.");
					}
				});
			};
			Klickrr.Webform.loadFromJson(data, data_style);
		});
	},

	loadFromJson: function(data, data_style) {
		$('[id^="webform_field_switch_"]').bootstrapSwitch('state', false)
		$.each(data.fields, function(key, val){
			if (data.is_old_form){
				buildField(val);
			} else {
				Klickrr.Webform.FormConstructor.loadFiled(val)
			}
		});

		// Save the group_id.
		Klickrr.Webform.group_id = data.id;
		Klickrr.Webform.form_code = data.form_code;
					Klickrr.Webform.submitUrl = data.submit_url;

		$("#form_div").addClass( data.style );
		$("#webform_preview").attr('style', data.inline_styles)
		$("#form_name").val( data.label );
		$("#redirect_url").val( data.redirect_url );
		$("#webform_list_selection [value='"+ data.list_id + "']").attr("selected", "selected");
    if (data_style != '' && data.style != null) {
			$("#curr_form_style").html(data_style);
		};
		$('#webform_width').val(parseInt($("#webform_preview").css('min-width').replace('px','')));
    $('#webform_height').val(parseInt($("#webform_preview").css('min-height').replace('px','')));
		// Load the form template style only if we're not using the default.
	},

	save: function(){
		$.ajax({
			url: "webform/save",
			type: "POST",
			dataType: "json",
			contentType: "application/json",
			beforeSend: function(xhr) {
		    	xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
		    },
			data: Klickrr.Webform.generateJSON(),
			success: function(data){
				if (data.success == "true") {
					console.log("Form saved.");
				} else {
					console.log("Form not saved.");
				};

				var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webforms', message: 'Your form has been saved.', type: 'success'};
  				Klickrr.Helpers.notify(notify_args);

				load_list_collections(selected_list);
			},
			error: function(data){

			}
		});
	},

	update: function(){
		$.ajax({
			url: "webform/" + Klickrr.Webform.group_id + "/save",
			type: "POST",
			dataType: "json",
			contentType: "application/json",
			beforeSend: function(xhr) {
		    	xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
		    },
			data: Klickrr.Webform.generateJSON(),
			success: function(data){
				if (data.success == "true") {
					console.log("Form saved.");
				} else {
					console.log("Form not saved.");
				};
			},
			error: function(data){

			}
		});
	},

	delete: function(){
		if (Klickrr.Webform.group_id != null) {
			$.ajax({
				url: "webform/delete",
				type: "post",
				datatype: "json",
				data: { group_id: Klickrr.Webform.group_id },
				success: function(data){
					if ( data.success == true ) {
						var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webforms', message: 'The selected form has been deleted.', type: 'success'};
  						Klickrr.Helpers.notify(notify_args);

  						load_list_collections(selected_list);
					} else {
						var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webforms', message: 'Form deletion could not be completed.', type: 'danger'};
  						Klickrr.Helpers.notify(notify_args);
					};
				},
				error: function(data){
					var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webforms', message: 'Form deletion could not be completed.', type: 'danger'};
  					Klickrr.Helpers.notify(notify_args);
				}
			});
		} else {
			var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webforms', message: 'No form selected for deletion.<p/>Please choose a form from the dropdown', type: 'danger'};
  			Klickrr.Helpers.notify(notify_args);
		};
	},

	generateForm: function(){
		var form_script = "";
		var form_arr = $("#form_elements li").clone();

		form_script = "<style>" + $("#curr_form_style").html() + "</style>";
		form_script += '<form method="post" id="klickrr_form" action="' + Klickrr.Webform.submitUrl + Klickrr.Webform.form_code + '" class="' + $("#form_div").attr("class") + '"' + ' style="' + $("#webform_preview").attr('style') + '"' + '>';

		//form_script += '<form method="post" action="http://localhost:3000/webform/process/' + Klickrr.Webform.form_code + '" class="' + $("#form_div").attr("class") + '">';

		$(form_arr).each(function(){
			$(".remove-el-btn", this).remove();

			if ( $(".required-field", this).css("display") == "none" ) {
				$(".required-field", this).remove();
			};

			form_script += $(this).html();
		});

		// Build the validation JS.
		var validation_arr = [];

		$(".webform-field").each(function(key, val){
			var validation_obj = {};

			if ( $(val).prop("required") == true ) {
				validation_obj["name"] = $(val).attr("name");
				validation_obj["rules"] = "required";

				validation_arr.push(validation_obj);
			};
		});

		var err_handling = '\n\nfunction(errors, evt) {\n' +
			'var err = document.getElementById("error_box");\n' +
			'err.style.border = "2px dotted goldenrod";\n' +
			'if (errors.length > 0) {\n' +
				'var err_msg = "";\n' +
				'for(var i = 0; i < errors.length; i++){\n' +
					'err_msg += "<li>" + errors[i].message + "</li>";' +
				'}\n' +
				'err.innerHTML = "<ul>" + err_msg + "</ul>";\n' +
				'err.style.display = "block";\n' +
			'} else {\n' +
				'err.style.display = "none";\n' +
			'}\n' +
			'if (evt && evt.preventDefault) {\n' +
		    	'evt.preventDefault();\n' +
		    '} else if (evt) {\n' +
		    	'evt.returnValue = false;\n' +
		    '}\n' +
		'});\n\n';

		form_script = '<script type="text/javascript">' + Klickrr.Webform.validatejs + "var validator = new FormValidator('klickrr_form',)" + JSON.stringify(validation_arr) + ',' + err_handling + '</script><div id="error_box"></div>' + form_script;
		return form_script;
	},
	generateJSON: function(){
		var form_json_obj = {};
		var json_arr = [];

		var unsubscribeListsIds = [];
		var unsubscribeFollowupIds = [];

		function setUnsubscribeListIds(){
			if ($("#unsubscribe_other_lists").bootstrapSwitch('state')){
				unsubscribeListsIds = $("#webform_unsubscribe_lists option:selected").not('.followup-select-item').map(function(i,v){
					if ($.isNumeric($(this).val())) { return $(this).val() };}).toArray();
				if (form_json_obj["list_id"] != null) {
					unsubscribeListsIds.push(form_json_obj["list_id"]);};
				unsubscribeListsIds = $.unique(unsubscribeListsIds);
			} else {
				unsubscribeListsIds = [];
			};
		}

		function setUnsubscribeFollowUpIds(){
			if ($("#unsubscribe_other_lists").bootstrapSwitch('state')){
				unsubscribeFollowupIds = $("#webform_unsubscribe_lists option:selected.followup-select-item").map(function(i,v){
					if ($.isNumeric($(this).val())) { return $(this).val() };}).toArray();
				unsubscribeFollowupIds = $.unique(unsubscribeFollowupIds);
			} else {
				unsubscribeFollowupIds = [];
			};
		}

		if ( Klickrr.Webform.group_id != null ) {
			form_json_obj["id"] = Klickrr.Webform.group_id;
		};
    if ($.isNumeric($("#webform_list_selection option:selected").val())){
		  form_json_obj["list_id"] = $("#webform_list_selection option:selected").val()}
		else {
		  form_json_obj["list_id"] = null;
		};
		form_json_obj["form_name"] = $("#form_name").val();
		form_json_obj["form_style"] = $("#form_div").attr("class");
		form_json_obj['form_inline_styles'] = $('#webform_preview').attr('style'); //TODO backend
		form_json_obj['custom_email_text'] = $('#webform_custom_email_text').val(); //TODO backend
		form_json_obj["form_code"] = Klickrr.Webform.form_code;
		form_json_obj["redirect_url"] = $("#redirect_url").val();
		setUnsubscribeListIds();
		setUnsubscribeFollowUpIds();
		form_json_obj['unsubscribe_lists_ids'] = unsubscribeListsIds; //TODO backend
		form_json_obj['unsubscribe_followup_ids'] = unsubscribeFollowupIds; //TODO backend

		// Iterate through each element div.
		$(".el-div").each(function(k,v){
		//$(".webform-field").each(function(k,v){
			var json_obj = {};
			var field = $(".webform-field", v);
			if ( $(field).attr('required') == 'required' ) {
				json_obj['required'] = true;
			} else {
				json_obj['required'] = false;
			};

			// Iterate through each field in the element div (for checkboxes, radio, select)
			$.each(field, function(idx, obj){
				json_obj["cid"] = "c" + k;
				json_obj["label"] = $("label", v).text();
				json_obj["integer_only"] = false;
				json_obj["min"] = 0;
				json_obj["max"] = 256;
				json_obj['inline_styles'] = $(field).attr('style') //TODO backend

				$.each(obj.attributes, function() {
					// this.attributes is not a plain object, but an array
					// of attribute nodes, which contain both the name and value
					if ( $( $(obj) ).prop('tagName') == "INPUT" ) {
						json_obj["field_type"] = $( field ).attr("type");
            console.log($( obj ).attr("type"));
						switch( $( obj ).attr("type") ){
							case "checkbox":
								var checkbox_arr = [];

								// Need to move the scope back up from the web-field class.
								$.each( $(".checkbox-group", v), function(i, o){
									var checkbox_obj = {};

									checkbox_obj["label"] = $(o).text();

									if ($(".webform-field", o).attr("checked") == "checked") {
										checkbox_obj["checked"] = true;
									} else {
										checkbox_obj["checked"] = false;
									};

									checkbox_obj["name"] = Klickrr.Helpers.label2Name( $(".webform-field", o).attr("id") );

									checkbox_arr.push(checkbox_obj);
								});

								json_obj["name"] = Klickrr.Helpers.label2Name( $(obj).parent().parent().parent().find("label").text() );

								json_obj["field_options"] = checkbox_arr;
							break;

							case "text":
								json_obj["field_type"] = "text";

								if ( $(obj).attr("data-email") == "true" ) {
									json_obj["is_email"] = true;
								};

								json_obj["name"] = Klickrr.Helpers.label2Name( $(obj).attr("id") );
								json_obj["value"] = $(field).attr('placeholder')
							break;

							case "radio":
								var radio_arr = [];
								var name = "";

								// Need to move the scope back up from the web-field class.
								$.each( $(".radio-group", v), function(i, o){
									var radio_obj = {};

									radio_obj["label"] = $(o).text();
									radio_obj["value"] = $(o).text();

									name = $(".webform-field", o).attr("name");
									radio_obj["name"] = name;

									if ($(".webform-field", o).attr("checked") == "checked") {
										radio_obj["checked"] = true;
									} else {
										radio_obj["checked"] = false;
									};

									radio_arr.push(radio_obj);
								});

								json_obj["name"] = name;
								json_obj["field_options"] = radio_arr;
							break;

							default:
								if ( $( field ).prop('tagName') != null ) {
									json_obj["field_type"] = String($( field ).prop('tagName')).toLowerCase();
									json_obj["name"] = Klickrr.Helpers.label2Name( $(field).attr("id") );
								//} else {

								}
							break;
						};

					} else {
						if ( $( field ).prop('tagName') != null ) {
							json_obj["field_type"] = String($( field ).prop('tagName')).toLowerCase();
							json_obj["name"] = Klickrr.Helpers.label2Name( $(field).attr("id") );
						//} else {

						};
					};

          switch($( field ).prop('tagName')){
						case 'IMG':
							json_obj["value"] = $(field).attr('src')
						break;

						case 'BUTTON':
							json_obj["value"] = $(field).text()
						break;
				  };

					if ( $(obj).prop('tagName') == "SELECT" ) {
						var option_arr = [];

						$.each( $("option", obj), function(key, val) {
							var option_obj = {};

							if ( $(val).attr("selected") === "selected" ) {
								option_obj["selected"] = true;
							} else {
								option_obj["selected"] = false;
							};

							option_obj["label"] = val.text;

							option_arr.push(option_obj);
						});

						json_obj["name"] = Klickrr.Helpers.label2Name( $(obj).attr("id") );

						json_obj["field_options"] = option_arr;
				 	};

				 	// This is a description block.
					if ( $(obj).prop('tagName') == "P" ) {
						var desc_obj = {};
						var desc_arr = [];

						desc_obj["desc"] = $(obj).html();
						desc_arr.push(desc_obj);

						json_obj["field_options"] = desc_arr;
					};
				});
			});
			json_arr.push( json_obj );
		});

		form_json_obj["fields"] = json_arr;

		//console.log("Object for JSON: " + JSON.stringify(form_json_obj));

		return JSON.stringify(form_json_obj);
	},
	clear: function(){
		$('#generated_form_div').html('');
		$(".hidden-code-panel").hide();
		$('#list_form_fields').html('');
		$("#form_div").removeClass();
		$('#form_elements').html('');
		buildField({'field_type': 'text', 'is_email': true });
		$("#webform_form_list").val("");
		$("#form_name").val("");
		$("#redirect_url").val("");

		$(".form-options-group").hide();
		$("#load_form_name").hide();

		Klickrr.Webform.group_id = null;
		Klickrr.Webform.form_code = null;

		$(".element-btn").attr("disabled", true);
		$("#view_embed_script_btn").attr("disabled", true);

		$("#element_options_div").html("Choose an element to add to your form.");
	}
};

function buildField(val){
	switch(val.field_type){
		case "text":
			// Check to see if the field is email, and lay it out appropriately.
			if (val.is_email === true) {
				$("#form_elements").append('<li class="sortable-li old-form-field"><div class="el-div" data-active="text"><label for="email">Email</label><span class="required-field" style="color: firebrick;"> *</span><br/><input type="text" name="email" id="email" class="webform-field" /></div></li>');
			} else {
				$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#text_tmpl").html() + '</li>' );
				$(".webform-field").last().attr("id", val.name);
				$(".webform-field").last().attr("name", val.name);
			};

		break;

		// 'Paragraph' type kept for legacy forms.
		case "paragraph":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#paragraph_tmpl").html() + '</li>' );
		break;

		case "textarea":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#paragraph_tmpl").html() + '</li>' );
			$(".webform-field").last().attr("id", val.name);
			$(".webform-field").last().attr("name", val.name);
		break;

		case "checkbox":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#checkbox_tmpl").html() + '</li>' );
			var checkboxes = [];


			var field_options = JSON.parse(val.field_options);
			$.each(field_options, function(k, v){
				var chkbx = "";
				if ( v.checked === true ) {
					chkbx = '<span class="checkbox-group"><input class="webform-field" type="checkbox" id="' + v.name + '" checked /> ' + v.label + '</span><br/>';
				} else {
					chkbx = '<span class="checkbox-group"><input class="webform-field" type="checkbox" id="' + v.name + '" /> ' + v.label + '</span><br/>';
				};
				checkboxes.push( chkbx );
			});

			$(".el-div.active .checkboxes").html("");
			$(".el-div.active .checkboxes").html( checkboxes );

			$(".webform-field").last().attr("id", val.name);
			$(".webform-field").last().attr("name", val.name);
		break;

		case "radio":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#multiple_choice_tmpl").html() + '</li>' );
			var radios = [];

			var field_options = JSON.parse(val.field_options);
			$.each(field_options, function(k, v){
				var rdio = "";
				if ( v.checked === true ) {
					rdio = '<span class="radio-group"><input type="radio" name="test" value="' + val.label + '" class="webform-field" checked /> ' + v.label + '</span><br/>';
				} else {
					rdio = '<span class="radio-group"><input type="radio" name="test" value="' + val.label + '" class="webform-field" /> ' + v.label + '</span><br/>';
				};
				radios.push( rdio );
			});

			$(".el-div.active .radios").html("");
			$(".el-div.active .radios").html( radios );

			$(".webform-field").last().attr("name", val.name);
		break;

		case "select":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#dropdown_tmpl").html() + '</li>' );
			var dropdowns = [];

			var field_options = JSON.parse(val.field_options);
			$.each(field_options, function(k, v){
				var drpdwn = "";
				if ( v.selected === true ) {
					drpdwn = '<option selected>' + v.label + '</option>';
				} else {
					drpdwn = '<option>' + v.label + '</option>';
				};
				dropdowns.push( drpdwn );
			});

			$(".el-div.active select").html("");
			$(".el-div.active select").html( dropdowns );

			$(".webform-field").last().attr("id", val.name);
			$(".webform-field").last().attr("name", val.name);
		break;

		case "hr":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#break_tmpl").html() + '</li>' );
		break;

		case "p":
			$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#desc_tmpl").html() + '</li>' );

			$(".el-div.active .webform-field").html( val["field_options"][0]["desc"] );
		break;
	}
	if (val.required === true) {
		$(".el-div.active .required-field").show();
		$(".el-div.active .webform-field").prop("required", true);

	} else {
		$(".el-div.active .required-field").hide();
		$(".el-div.active .webform-field").prop("required", false);

	};

	// Populate the field label.
	$(".el-div.active > label").text( val.label );

	$(".el-div").removeClass("active");
}

function refresh_checkboxes(){
	var checkboxes = [];

	$.each( $(".checkbox-editor"), function(){
		if ( $(this).find("input[type='text']").val() != "" ) {
			var chkbx = "";
			if ( $(this).find("input[type='checkbox']").prop("checked") == true ) {
				chkbx = '<span class="checkbox-group"><input class="webform-field" type="checkbox" id="' + Klickrr.Helpers.label2Name( $(this).find("input[type='text']").val() ) + '" checked /> ' + $(this).find("input[type='text']").val() + '</span><br/>';
			} else {
				chkbx = '<span class="checkbox-group"><input class="webform-field" type="checkbox" id="' + Klickrr.Helpers.label2Name( $(this).find("input[type='text']").val() ) + '" /> ' + $(this).find("input[type='text']").val() + '</span><br/>';
			};
			checkboxes.push( chkbx );
		};
	} );

	$(".el-div.active .checkboxes").html("");
	$(".el-div.active .checkboxes").html( checkboxes );
}

function refresh_radios(){
	var radios = [];

	$.each( $(".radio-editor"), function(){
		if ( $(this).find("input[type='text']").val() != "" ) {
			var rdio = "";
			if ( $(this).find("input[type='radio']").prop("checked") === true ) {
				rdio = '<span class="radio-group"><input type="radio" name="' + Klickrr.Helpers.label2Name( $(".el-div.active > label").text() ) + '" id="' + Klickrr.Helpers.label2Name( $(this).find("input[type='text']").val() ) + '" value="' + $(this).find("input[type='text']").val() + '" class="webform-field" checked /> ' + $(this).find("input[type='text']").val() + '</span><br/>';
			} else {
				rdio = '<span class="radio-group"><input type="radio" name="' + Klickrr.Helpers.label2Name( $(".el-div.active > label").text() ) + '" id="' + Klickrr.Helpers.label2Name( $(this).find("input[type='text']").val() ) + '" value="' + $(this).find("input[type='text']").val() + '" class="webform-field" /> ' + $(this).find("input[type='text']").val() + '</span><br/>';
			};
			radios.push( rdio );
		};
	} );

	$(".el-div.active .radios").html("");
	$(".el-div.active .radios").html( radios );
}

function refresh_dropdowns(){
	var dropdowns = [];

	$.each( $(".select-editor"), function(key, val){
		if ( $(val).find("input[type='text']").val() != "" ) {
			var drpdwn = "";
			if ( $(val).find("[name='selected_group']").prop("checked") == true ) {
				drpdwn = '<option selected>' + $(val).find("input[type='text']").val() + '</option>';
			} else {
				drpdwn = '<option>' + $(val).find("input[type='text']").val() + '</option>';
			};
			dropdowns.push( drpdwn );
		};
	} );

	$(".el-div.active select").html("");
	$(".el-div.active select").html( dropdowns );

	$(".el-div.active select").attr("id", Klickrr.Helpers.label2Name( $(".el-div.active > label").text() ));
	$(".el-div.active select").attr("name", Klickrr.Helpers.label2Name( $(".el-div.active > label").text() ));
}

function active_editor_data(){
	var element = $(".el-div.active .webform-field");

	if ( typeof element != "undefined" && element != null && element.length > 0 ) {
		// Handle dropdowns.
		if ( $(element).is("select") ) {
			if ( $("option", element).length - 3 > 0 ) {
				for (var i = 1; i <= $("option", element).length - 3; i++) {
					$(".select-editor").last().after( $("#dropdown_editor_tmpl").html() );
				};
			};

			$.each($("option", element), function(key, val){
				$(".dropdown-option-text:eq(" + key + ")").val( $(val).text() );
			});
		};

		// Handle checkboxes.
		if ( $(element).is("input[type='checkbox']") ) {
			if ( $(element).length - 3 > 0 ) {
				for (var i = 1; i <= $(element).length - 3; i++) {
					$(".checkbox-editor").last().after( $("#checkbox_editor_tmpl").html() );
				};
			};

			$.each($(".checkbox-group"), function(key, val){
				$(".checkbox-option-text:eq(" + key + ")").val( $(val).text() );

				if ( $(".webform-field", this).prop("checked") == true ) {
					$(".checkbox-default-val:eq(" + key + ")").prop("checked", true);
				};
			});
		};

		// Handle radios.
		if ( $(element).is("input[type='radio']") ) {
			if ( $(element).length - 3 > 0 ) {
				for (var i = 1; i <= $(element).length - 3; i++) {
					$(".radio-editor").last().after( $("#radio_editor_tmpl").html() );
				};
			};

			$.each($(".radio-group"), function(key, val){
				$(".radio-option-text:eq(" + key + ")").val( $(val).text() );

				if ( $(".webform-field", this).prop("checked") == true ) {
					$("[name='radio_group']:eq(" + key + ")").prop("checked", true);
				};
			});
		};

		// Handle descriptions.
		if ( $(element).is("p") ) {
			$("#edit_descriptive_text").val( $(element).html() );
		};
	};
}

function get_element_attributes(el){
	$(xml).find('item').each(function() {
	  $.each(this.attributes, function(i, attrib){
	     var name = attrib.name;
	     var value = attrib.value;
	     // do your magic :-)
	  });
	});
}

function load_list_collections(list){
	if ( list !== null && list != "undefined" ) {
		$.ajax({
			url: "list/" + list + "/collections",
			type: "get",
			datatype: "json",
			success: function(data){
				var options = [];

				$.each(data, function(key, val){
					options.push('<option data-list-id="' + val.list_id + '" value="' + val.id + '">' + val.label + '</option>');
				});

				$("#webform_form_list").html("<option></option>" + options);
				Klickrr.Webform.group_id = null;
				Klickrr.Webform.form_code = null;
				$('#webform_form_list').trigger('loadItems');
			},
			error: function(data){

			}
		});
	};
}

function load_list_fields(listId){
	if ( listId ) {
		$('#hidden_temp_list_fields').html('');
		var listFields = $.grep(lists, function(list){ return list.list_id == listId })[0].listFields;
		$('#list_form_fields').html('');
		if (listFields && listFields.length > 0) {
			$.each(listFields, function(index, el){
				$('#list_form_fields').append('<button class="btn btn-default button-add-list-field" type="button" data-field-id="' + el.id + '">' + el.label + '</button>');
			})
		}
	}
}

// Create the <options/> for a <select/> containing webform titles for a specific list.
function selWebformTitles(){
	if ( list.length > 0 ) {
	//if ( list !== null && list != "undefined" ) {
		$.ajax({
			url: "list/" + list[0].list_id + "/collections",
			type: "get",
			datatype: "json",
			success: function(data){
				var options = [];

				if( data.length > 0 ){
					$.each(data, function(key, val){
						options.push('<option value="' + val.id + '">' + val.label + '</option>');
					});

					$(".webform-title-select").html("<option>Select a webform</option>" + options);
				} else {
					$(".webform-title-select").html("<option>No webforms for this list.</option>");
				}

				Klickrr.Webform.group_id = null;
				Klickrr.Webform.form_code = null;
			},
			error: function(data){

			}
		});
	};
}

function update_webforms_ui(){
	Klickrr.Webform.clear();
	if ( selected_list != null && typeof selected_list != "undefined" && selected_list != "Select a contact list..." ) {
		$("#form_options_group").slideDown("fast");
		$("#save_form_btn").prop("disabled", false);
	} else {
		$("#form_options_group").slideUp("fast");
		$("#save_form_btn").prop("disabled", "disabled");
	}
}

$(function(){
	$.get("/validate.min.js", function(data){
		Klickrr.Webform.validatejs = data;
	});

	$("#form_elements").sortable();
	$("#form_elements").disableSelection();

	$(".element-btn").attr("disabled", true);

	$("#test").click(function(){
		console.log("Beginning to examine...");

		$("ul#form_elements>li").each(function(){
			console.log("ID: " + $(this).attr("data-id"));
		});
	});

	$(".close-panel").click(function(){
		$(this).parent().parent().parent().slideUp("fast");
	});

	$(document).on("click",".element-btn", function(){
		// Clear currently active element.
		$(".el-div.active").removeClass("active");

		var el = $(this).attr("data-template");
		$("#form_elements").append( '<li class="sortable-li old-form-field">' + $("#" + el + "_tmpl").html() + '</li>' );
		$("#element_options_div").html( $("#" + el + "_options_tmpl").html() );
	});

	$("body").on("click", ".el-div", function(){
		// Clear currently active element.
		$(".el-div.active").removeClass("active");

		// Make the clicked div active.
		$(this).addClass("active");

		// Find out which element was clicked.
		var el = $(this).attr("data-active");

		$("#element_options_div").html( $("#" + el + "_options_tmpl").html() );

		// Set the editor data.
		$("#element_options_div > div > p > input[type='text']").val( $(".el-div.active > label").text() );

		if ( $(".el-div.active .required-field").css("display") != "none" ) {
			$("#element_options_div > div > p > input[type='checkbox']").attr("checked", true);
		} else {
			$("#element_options_div > div > p > input[type='checkbox']").attr("checked", false);
		};

		active_editor_data();
	});

	$("body").on("click", ".remove-el-btn", function(){
		$(this).parent().remove();
	});

	$("body").on("keyup", ".edit-text-label", function(e){
		$(".el-div.active > label").text( $(this).val() );
		$(".el-div.active > input").attr("id", Klickrr.Helpers.label2Name( $(this).val() ));
		$(".el-div.active > input").attr("name", Klickrr.Helpers.label2Name( $(this).val() ));
		$(".el-div.active > textarea").attr("id", Klickrr.Helpers.label2Name( $(this).val() ));
		$(".el-div.active > textarea").attr("name", Klickrr.Helpers.label2Name( $(this).val() ));
	});

	$("body").on("change", ".required-field-checkbox", function(){
		$(".el-div.active .required-field").toggle();

		if ( $(".el-div.active .webform-field").prop("required") == true ) {
			$(".el-div.active .webform-field").prop("required", false);
			console.log("required");
		} else {
			$(".el-div.active .webform-field").prop("required", true);
			console.log("Not required");
		};
	});

	// Handle checkboxes.
	$("body").on("keyup", ".checkbox-option-text", function(){
		refresh_checkboxes();
	});

	$("body").on("click", ".remove-checkbox-btn", function(){
		$(this).parent().parent().remove();

		refresh_checkboxes();
	});

	$("body").on("click","#add_checkbox_btn", function(){
		$(".checkbox-editor").last().after( $("#checkbox_editor_tmpl").html() );
	});

	$("body").on("change", ".checkbox-default-val", function(){
		refresh_checkboxes();
	});
	// End checkbox handling.

	// Add templates styles to formbuilder.
	$(".webform-theme").click(function(){
		$("#form_div").removeClass();

		var theme = $(this).attr("data-theme");

		$("#form_div").addClass( theme );

		$.ajax({
			url: "webform/template/" + $("#form_div").attr("class"),
			dataType: "text",
			async: true,
			success: function(data){
				$("#curr_form_style").html(data);

				// Klickrr.Webform.generateForm();
			},
			error: function(data){
				console.log("Didn't work.");
			}
		});
	});

	$("#clear_tmpl_btn").click(function(){
		$("#form_div").removeClass();

		$("#generated_form_div").html("");
        $(".hidden-code-panel").hide();
	});
	// End template handling.

	// Handle radio (multiple choice)
	$("body").on("keyup", ".radio-option-text", function(){
		refresh_radios();
	});

	$("body").on("click", ".remove-radio-btn", function(){
		$(this).parent().parent().remove();

		refresh_radios();
	});

	$("body").on("click","#add_radio_btn", function(){
		$(".radio-editor").last().after( $("#radio_editor_tmpl").html() );
	});

	$("body").on("change", "[name='radio_group']", function(){
		refresh_radios();
	});
	// End radio handling.

	// Handle select dropdown
	$("body").on("keyup", ".dropdown-option-text", function(){
		refresh_dropdowns();
	});

	$("body").on("click", ".remove-dropdown-btn", function(){
		$(this).parent().parent().remove();

		refresh_dropdowns();
	});

	$("body").on("change", "[name='selected_group']", function(){
		refresh_dropdowns();
	});

	$("body").on("click","#add_dropdown_btn", function(){
		$(".select-editor").last().after( $("#dropdown_editor_tmpl").html() );
	});

	$("body").on("change", "[name='dropdown_group']", function(){
		refresh_dropdowns();
	});
	// End dropdown handling.

	// Handle desc block.
	$("body").on("keyup", "#edit_descriptive_text", function(){
		$(".el-div.active > p").html( $("#edit_descriptive_text").val() );
	});
	// End desc block handling.

	// Handle form loading.
	$("#save_form_btn").click(function(){
		if (Klickrr.Webform.group_id != null) {
			Klickrr.Webform.update();
		} else {
			Klickrr.Webform.save();
		};
	});

	$("#delete_form_btn").click(function(){
	 	$("#mod_delete_webform").modal("show");
	});

	$("#confirm_delete_webform_btn").click(function(){
		Klickrr.Webform.delete();

		$("#mod_delete_webform").modal("hide");
	});

	$("#view_embed_script_btn").click(function(){
        Klickrr.Webform.generateForm();
	});

	$("#webform_form_list").change(function(){
		console.log("webform-options changed");
		Klickrr.Webform.load( $("#webform_form_list option:selected").val() );
		//$("#form_name").val( $("#webform_form_list option:selected").text() );

		if ($("#webform_form_list option:selected").val() != "" && $("#webform_form_list option:selected").val() != null) {
			$(".element-btn").attr("disabled", false);
			$("#view_embed_script_btn").attr("disabled", false);
			Klickrr.Webform.group_id = $("#webform_form_list option:selected").val();
		} else {
			Klickrr.Webform.group_id = null;
			Klickrr.Webform.form_code = null;
			$(".element-btn").attr("disabled", true);
			$("#view_embed_script_btn").attr("disabled", true);
		};
	});
	// End form loading.

	// Form saving.
	$("#new_form_name_btn").click(function(){
		Klickrr.Webform.clear();
		$(".form-options-group").show();

		$(".element-btn").attr("disabled", false);
		$("#view_embed_script_btn").attr("disabled", false);

        // Reload the form tokens.
        load_list_fields(selected_list);

        // Set form code.
        Klickrr.Webform.form_code = Klickrr.Helpers.randomString(12, 'aA#');
	});

	$("#reset_form_name_btn").click(function(){
		Klickrr.Webform.clear();
	});

	$("#load_form_name_btn").click(function(){
		Klickrr.Webform.clear();

		$("#load_form_name").show();
		$(".form-options-group").show();
	});

	$('body').on('click', '.button-add-list-field', function(){
		var listFields = $.grep(lists, function(list){ return list.list_id == selected_list })[0].listFields;
		var id = $(this).attr('data-field-id');
		var field = $.grep(listFields, function(field){ return field.id == id })[0];
		buildField(field);
	})

	$('body').on('click', '.delete-webform', function(){
    if (confirm('Are you sure to delete this webform?')) {
      var data = {group_id: $(this).attr('data-id')};
      $.ajax({
        url: "webform/delete",
        type: "POST",
        dataType: "json",
        data: data,
        success: function(data){
          var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webform', message: 'Delete successful.', type: 'success'};
          $('#webform_page').trigger("page", [Klickrr.webformPagination.page]);
          Klickrr.Helpers.notify(notify_args);
        },
        error: function(){
          var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Webform', message: 'Unable to delete the webform.', type: 'danger'};
          Klickrr.Helpers.notify(notify_args);
        }
      });
    }
  })

  $('body').on('click', '.duplicate-webform', function(){
  	var data = {id: $(this).attr('data-id')};
    $.ajax({
      url: "webform/clone",
      type: "POST",
      dataType: "json",
      data: data,
      success: function(data){
      	if (data.success) {
	        var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Webform', message: 'Duplicate successful.', type: 'success'};
	        $('#webform_page').trigger("page", [Klickrr.webformPagination.page]);
	        Klickrr.Helpers.notify(notify_args);
	      } else {
	      	var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Webform', message: 'Unable to duplicate the webform.', type: 'danger'};
        	Klickrr.Helpers.notify(notify_args);
	      }
      }
    });
  })

  $('#webform_page').on("page", function(event, num){
    Klickrr.webformPagination.page = num;
    $.ajax({
      url: "/webform/list",
      type: "GET",
      data: Klickrr.webformPagination,
      dataType: "html",
      success: function(data){
        $('#webform_manager').html(data);
        webformTotalPages();
      }
    });
  });

  function webformTotalPages(){
    $.ajax({
          url: "/webform/total_pages",
          data: Klickrr.webformPagination,
          type: "GET",
          dataType: "json",
          success: function(data){
            $('#webform_page').bootpag({
              total: Math.max.apply(null, [1, data.total_pages]),
              maxVisible: 10
            })
          }
        })
  }

  $('body').on('click', 'a.webform_order', function(){
    Klickrr.webformPagination.order = $(this).attr('data-field') + ' ' + $(this).attr('data-order-status');
    $('#webform_page').trigger('page', [Klickrr.webformPagination.page]);
  })

  $('a[href="#webform_list"]').on('shown.bs.tab', function(){
    updateListContent();
    $('#webform_page').trigger('page', [Klickrr.webformPagination.page]);
  })

  $('body').on('click', '#webform_page_size a', function(){
    Klickrr.webformPagination.page_size = $(this).attr('data-page-size');
    $('#webform_page').trigger('page', [1]);
    $('#webform_page_size li').removeClass('active');
    $('#webform_page_size a[data-page-size="' + Klickrr.webformPagination.page_size + '"]').parent().addClass('active');
  })

	// End form saving.
});
