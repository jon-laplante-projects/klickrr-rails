class ActiveServices
  constructor: ->
    $(document).ready =>
      $('body').on 'click', modalSel + ' .btn-save', =>
        @saveButtonHandler()

  showServices: (@type, @currentInstance, @tempInstance) ->
    build(@currentInstance, @tempInstance)
    openModal()
   
  saveButtonHandler: => 
    saveButton = $(modalSel + ' .btn-save')
    serviceInput = $(serviceInputSelectors[@type]) 
    checkedButtons = $(modalSel + ' .list-service .btn.active')
    activeServicesIds = checkedButtons.map ->
      $(@).data('service-id')
    .get()

    strActiveServicesIds = activeServicesIds.join(',')

    if !$.isEmptyObject(@currentInstance)
      postData = 
        id: @currentInstance.id,
        services: strActiveServicesIds

      currentInstance = @currentInstance  
      $.post @type + '/services/update', postData, (data) ->
        currentInstance.services = strActiveServicesIds
        currentInstance.servicesLabels = data.labels
        serviceInput.val(data.labels)
        toggleSaveReadiness()
    else
      @tempInstance.services = strActiveServicesIds
      @tempInstance.servicesLabels = checkedButtons.map ->
        $(@).find('.service-label').text()
      .get().join(',')
      serviceInput.val(@tempInstance.servicesLabels)
      toggleSaveReadiness()

    $(modalSel).modal('hide')

  modalSel = '.active-service-modal'

  serviceInputSelectors = 
    broadcast: '#txt_broadcast_servers',
    followup: '#txt_followup_servers'
      
  openModal = ->
    $(modalSel).modal('show')

  getListServices = (id) ->
    $.get '/list/services/' + id

  getIds = (str) ->
    if str then str.split(',') else []

  getAvailableServices = (ids) ->
    $.get 'services/list/active', (data) ->
      renderServices(data, ids)

  renderServices = (services, ids) ->
    modal = $(modalSel)
    template = modal.find('.service-template').first().html()
    modal.find('.list-services>li').remove()

    if services && services.length > 0
      modal.find('.services-not-found').hide()
      $.each services, (_, service) ->
        isActive = $.inArray(service.id.toString(), ids) >= 0
        serviceWiew = $(template)
        serviceWiew.find('.btn').attr('data-service-id', service.id)
        serviceWiew.find('.service-label').text(service.label)
        if isActive
          activateServiceButton(serviceWiew.find('.btn'))
        serviceWiew.appendTo('.list-services')
    else 
      modal.find('.services-not-found').show()
        
  activateServiceButton = (button) ->
    button.addClass('active')
    icon = button.find('i')
    icon.removeClass('fa-square-o')
    icon.addClass('fa-check-square-o')

  build = (currentInstance, tempInstance) ->
    if !$.isEmptyObject(currentInstance)
      getAvailableServices(currentInstance.services)
    else 
      getListServices(selected_list).then (data) ->
        activeServicesIds = if !$.isEmptyObject(tempInstance)
          getIds(tempInstance.services)
        else
          getIds(data.active_list_services.services) 

        getAvailableServices(activeServicesIds)

window.ActiveServices = new ActiveServices        
  