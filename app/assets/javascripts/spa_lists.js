// When a user is importing a CSV list, they can opt out of
// bringing in certain data columns.
function omitCSVCol(csv_arr){
  // Build array of deselected columns.
  var omitted = [];

  $(".csv_include_field").each(function(){
    if ( $(this).prop("checked") === false ) {
      omitted.push( $(this).attr("data-field-id") );
    };
  });

  //console.log("Omitted len: " + omitted.length);

  if ( omitted.length > 0 ) {
    // Remove the unchecked columns from each row.
    $(csv_arr).each(function(){
      for (var i = omitted.length - 1; i >= 0; i--) {
        this.splice(omitted[i], 1);
      };
    });
  };

  return csv_arr;
}

// Upload the selected .csv data.
function csvUpload(){
  $("input#list_csv_upload_file").parse({
    config: {
      //worker: true,
      step: true,
      complete: function(results, file) {
        //console.log("This file done:", file, results);
        addSubscribersToList(results);
      }
    },
    complete: function() {
      // Hide the modal.
      if (Klickrr.newSubscribers != null) {
        addSubscribersToList(Klickrr.newSubscribers);
      }
      $("#mod_csv_parser").modal("hide");
      $("#list_csv_upload_file").val("");
      $("#list_csv_upload_file").text("");

      // Reset modal data.
      $("#csv_included_fields").html("<tr><th>Include field?</th><th>Email?</th></tr>");
    }
  });
}

function addSubscribersToList(results) {
  var data_obj = {};
  data_obj.list_id = $('#master_lists_sel').val();

  if ( $("[name='email_field_radio']").is(":checked") != false ) {
    var index = results["data"][0].indexOf( $("input[name='email_field_radio']:checked").attr("data-field-name") );

    if (index !== -1) {
        results["data"][0][index] = "email";
    }
  };

  data_obj.data = omitCSVCol(results["data"]);

  var payload = JSON.stringify( data_obj );

  var conctacts_count = parseInt(data_obj.data.length) - 1;
  var notify_args = { icon: 'fa fa-upload', title: 'Klickrr :: Lists', message: 'Uploading ' + conctacts_count + ' contacts...', type: 'info'};


  Klickrr.Helpers.notify(notify_args);

  $.ajax({
    url: "/list/upload",
    //url: "https://www.klickrr.net/webform/save",
    type: "POST",
    data: payload,
    dataType: "json",
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    },
    contentType: "application/json",
    success: function(data){
      console.log(data);
    },
    error: function(){
      console.log("This didn't seem to work.");

      var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'Could not upload list at this time.', type: 'danger '};

      Klickrr.Helpers.notify(notify_args);
    }
  });
}

// This should be fired when selected list changes
// and when service button CRUD happens.
function list_service_btn_setup(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    $("#list_services").slideDown("fast");
    var service_btns = [];
    var btns;
    var active_btns;

    $.ajax({
      url: "/services/list/active",
      async: false,
      type: "get",
      dataType: "json",
      success: function(data){
        btns = data;
      }
    });

    $.ajax({
      url: "/list/services/" + selected_list,
      async: false,
      type: "get",
      success: function(data){
        if (data["active_list_services"]["services"] != null) {
          active_btns = data["active_list_services"]["services"].split(",");

          // Convert array elements to integers so inArray will work.
          for(var i=0; i<active_btns.length;i++) active_btns[i] = +active_btns[i];
        } else {
          active_btns = [];
        };

      }
    });
  } else {
    $("#list_services").slideUp("fast");

    active_btns = [];
  };

  $("#list_service_btns").html("");

  if ( btns != null && typeof btns != "undefined" ) {
    $.each(btns, function(key, val){
      var is_active = "";
      var btn_icon = "fa fa-square-o";

      if ( $.inArray( val.id, active_btns ) != -1 ) {
        is_active = " active";
        btn_icon = "fa fa-check-square-o";
      };

      service_btns.push('<li class="list_service_li" style="margin: 0 10px 5px 10px;"><button type="button" class="btn btn-default list-service-btn' + is_active + '" data-toggle="button" aria-pressed="false" autocomplete="off" data-service="' + val.id + '"><i id="' + val.name + '_btn_check" class="fa ' + btn_icon + '"></i>&nbsp;' + val.label + '</button></li>');
    });

    $("#list_service_btns").html(service_btns);
  };
}

function updateList(list){
    var tmp = {
        "title": list,
        list_id: selected_list
    }
    $.ajax({
        url: "list/update",
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        data: JSON.stringify(tmp),
        dataType: "json",
        contentType: "application/json",
        success: function(data){
            var notify_args = { icon: 'fa fa-list', title: 'Klickrr :: Lists', message: 'List has been updated.', type: 'success' };

            Klickrr.Helpers.notify(notify_args);

            tmp.list_id = data["id"];
            if (tmp.forms && tmp.forms.length > 0 && tmp.forms[0].fields.length > 0) {
              tmp.forms[0].fields[0].cid = data["email_cid"];
              tmp.forms[0].fields[1].cid = data["first_name_cid"];
              tmp.forms[0].fields[2].cid = data["last_name_cid"];
            }
        },
        error: function(data){
            var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'Cannot update list.', type: 'danger'};

            Klickrr.Helpers.notify(notify_args);
        },
        complete: function(data){
            lists.push( tmp );
            selected_list = tmp.list_id;
            loadSelectedList();
            // updateUI();

        }
    });
}

function loadSelectedList(){
  update_webforms_ui();
  toggleListManagementButtons();
  updateListImportUI();
  updateListServiceData();
  list_service_btn_setup();
  updateCountListSubscribers();
  updateListsBroadcasts();
  analytics_data_update("broadcast", selected_broadcast);

  if ($("#broadcast").is(":visible")){
    updateBroadcastUi();
  }

  if ($("#follow_up").is(":visible")){
    updateFollowupUi();
  }
  $('#sel_lists_management').val($('#master_lists_sel').val());
}

function loadListsCollection(){
  return $.getJSON('lists/collection', function(data){
    lists = data.lists;
    updateListsSelects();
  })
}

function newList(title){
  var notify_args = { icon: 'fa fa-list', title: 'Klickrr :: Lists', message: 'Creating your list...', type: 'info'};

  Klickrr.Helpers.notify(notify_args);

  var next_id = lists[lists.length - 1] + 1;

    var tmp = {
      "title": title,
      "subCount": 0,
      "forms": [
        {
        "name": title,
        "fields": [
          { //"cid": "c90",
            "label": "First Name",
            "field_type": "text",
            "required": true,
            "field_options": {"size":"small"},
            "description": "",
            "include_other": "",
            "include_blank": "",
            "integer_only": false,
            "min": 0,
            "max": 256
          },{ //"cid": "c90",
            "label": "Last Name",
            "field_type": "text",
            "required": true,
            "field_options": {"size":"small"},
            "description": "",
            "include_other": "",
            "include_blank": "",
            "integer_only": false,
            "min": 0,
            "max": 256
          },{
            //"cid": "c90",
            "label": "Email",
            "field_type": "text",
            "required": true,
            "field_options": {"size":"small"},
            "description": "",
            "include_other": "",
            "include_blank": "",
            "integer_only": false,
            "min": 0,
            "max": 256
            }
        ],
      }],
      "broadcasts": new Array()
    };

    $.ajax({
      url: "list/create",
      //url: "https://www.klickrr.net/webform/save",
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      data: JSON.stringify(tmp),
      dataType: "json",
      contentType: "application/json",
      success: function(data){
        var notify_args = { icon: 'fa fa-list', title: 'Klickrr :: Lists', message: 'New list has been created.', type: 'success' };

        Klickrr.Helpers.notify(notify_args);

        tmp.list_id = data["id"];
        if (tmp.forms && tmp.forms.length > 0 && tmp.forms[0].fields.length > 0) {
          tmp.forms[0].fields[0].cid = data["email_cid"];
          tmp.forms[0].fields[1].cid = data["first_name_cid"];
          tmp.forms[0].fields[2].cid = data["last_name_cid"];
        }
      },
      error: function(data){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'Cannot save new list.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      },
      complete: function(data){
        lists.push( tmp );
        selected_list = tmp.list_id;
        loadListsCollection();
        loadSelectedList();
      }
    });



    //$("#txt_create_new_list").val("");

    //updateUI();
}

$(function(){
// List button click events.
  $('body').on('click', ".view-subscribers-btn", function(){
    var subscriber_cols = [];
    var subscribers = [];
    var subscriber_thead = "";
    var subscriber_tr = "";
    var subscriber_trs = [];

    // Test to see if there is a data-id on this node.
      if ( typeof $(this).attr("data-id") !== "undefined" ) {
          // Set the selected list.
          $("#master_lists_sel").val( $(this).attr("data-id") );
          $('#master_lists_sel').trigger('change');
      }

    $.getJSON("list/" + selected_list + "/subscribers/columns.json", function(data){
    //$.getJSON("list/" + selected_list + "/subscribers/columns.json", function(data){
      $.each(data, function(key, val){
      subscriber_cols.push( val.label );
      });
      subscriber_cols = $.map(data, function(val) { return val });
    });


    $.getJSON("list/" + selected_list + "/subscribers.json", function(data){
      subscribers = $.map(data, function(val) { return val });

      // TODO: Check to make sure both arrays are populated.
      if (subscribers.length > 0) {
        subscriber_thead = "<tr><thead>";
        $.each(subscriber_cols, function(key, val){
          subscriber_thead += "<th>" + val + "</th>";
        });
        subscriber_thead += "</thead></tr>";

        subscriber_trs.push(subscriber_thead);

        $.each(subscribers, function(idx, subscriber){
          subscriber_tr = "<tr>";
          $.each(subscriber, function(k, v){
            if ( k != "set_id" && k != "id") {
              subscriber_tr += "<td>" +  v + "</td>";
            };
            //console.log(k + ": " + v);
          });
          subscriber_tr += "</tr>";

          subscriber_trs.push(subscriber_tr);
          //console.log(subscriber_tr);
        });

        //console.log("Subscriber table: " + subscriber_trs);
        $("#subscriber_view_table").html( subscriber_trs );

        $("#mod_view_subscribers").modal("show");
      }
    });

    //console.log(subscribers);
  });

  //==========================================
  // Domain, sender mail, sender name
  //==========================================
  $("#list_domain_btn").click(function(){
    var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Lists', message: 'Saving list domain...', type: 'info'};

    Klickrr.Helpers.notify(notify_args);

    var domain_obj = {};

    domain_obj["list_id"] = selected_list;
    domain_obj["domain"] = $("#list_domain").val();

    $.ajax({
      url: "list/domain/save",
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(domain_obj),
      success: function(data){
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Lists', message: 'List saved.', type: 'info'};

        Klickrr.Helpers.notify(notify_args);
      },
      error: (function(){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'There was a problem saving your changes.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      })
    });
  });

  $("#list_sender_email_btn").click(function(){
    var sender_mail_obj = {};

    sender_mail_obj["list_id"] = selected_list;
    sender_mail_obj["sender_email"] = $("#list_sender_email").val();

    var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Lists', message: 'Saving list sender email...', type: 'info'};

    Klickrr.Helpers.notify(notify_args);

    $.ajax({
      url: "list/sender_email/save",
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(sender_mail_obj),
      success: function(data){
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Lists', message: 'Saved...', type: 'success'};

        Klickrr.Helpers.notify(notify_args);
      },
      error: (function(){
      var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'There was a problem saving your changes.', type: 'danger'};

      Klickrr.Helpers.notify(notify_args);
      })
    });
  });

  $("#list_sender_name_btn").click(function(){
    var sender_obj = {};

    sender_obj["list_id"] = selected_list;
    sender_obj["sender"] = $("#list_sender_name").val();

    var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Lists', message: 'Saving sender name...', type: 'info'};

    Klickrr.Helpers.notify(notify_args);

    $.ajax({
      url: "list/sender_name/save",
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(sender_obj),
      success: function(data){
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Lists', message: 'Saved...', type: 'success'};

        Klickrr.Helpers.notify(notify_args);
      },
      error: (function(){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'There was a problem saving your changes.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      })
    });
  });
  //==========================================
  // END Domain, sender mail, sender name
  //==========================================

  $("#txt_create_new_list").on("input propertychange", function(){
    if ( $("#txt_create_new_list").val().trim() != "" ) {
      $("#btn_create_new_list").removeAttr("disabled");
    } else {
      $("#btn_create_new_list").attr("disabled", "disabled");
    };
  });

  $("#btn_create_new_list").click(function(){
      if ( $("#master_lists_sel").val() != "Select a contact list..." ) {
          selected_list = $("#master_lists_sel option:selected").val();
          updateList( $("#txt_create_new_list").val());
      } else {
          newList( $("#txt_create_new_list").val());
          $('#btn_create_new_list').html('<i class="fa fa-check-circle"></i>&nbsp;Update')
      }

      $('#lists_page').trigger('page', [Klickrr.listPagination.page]);
      loadListsCollection().then(function(){
        loadSelectedList();
        $("#master_lists_sel").val(selected_list);
        $("#master_lists_sel").trigger('change');
      });
      // updateListContent();
  });

  $("#btn_create_first_list").click(function(){
    newList( $("#txt_create_first_list").val() );

    $("#txt_create_first_list").val("");
    $("#mod_no_list").modal("hide");
    loadListsCollection();
    loadSelectedList();
    // updateUI();
  });

  $("#upload_list_btn").click(function(){
    //$("#csv_included_fields").html("<tr><th>Include field?</th><th>Email?</th><th>List fields</th></tr>");
    csvUpload();
    loadSelectedList();
  });

  $("#btn_delete_list").click(function(){
    if( $("#sel_lists_management").val() != "Select a contact list..." ) {
      $("#mod_delete_list").modal("show");
    }
  });

  $("#btn_permanently_delete_list").click(function(){
    //$("#mod_delete_list").modal("hide");
    if( $("#sel_lists_management").val() != "Select a contact list..." ) {
      // Send a POST to the server to begin deleting the list and associated records.
      var dead_list = {};
      dead_list["id"] = selected_list;
      //$.post("list/destroy", deadlist);
      $.ajax({
        type: "POST",
        url: "list/destroy",
        data: dead_list,
        success: function(result){
            // updateListContent();
            // updateUI();
            $('#lists_page').trigger('page', [Klickrr.listPagination.page]);

            // Reset the list creation group back to "Create" and blank.
            $("#txt_create_new_list").val("");
            $('#btn_create_new_list').html('<i class="fa fa-check-circle"></i>&nbsp;Create')
            $('#btn_create_new_list').attr("disabled", "disabled");
            loadListsCollection().then(function(){
              selected_list = null;
              $('#master_lists_sel').trigger('change');
            });


        },
        //dataType: "JSON"
      });

      lists = clean_lists();
    }

    // Repopulate the dropdown lists.
    //$(".sel-lists").html( li_lists(lists) );
  });

  $("#csv_first_row_header").change(function(){
    if ($('#csv_first_row_header').is(':checked') == false) {
      $("#csv_preview_cols").hide();
      $("#csv_cols_inputs").show();
      $("#no_csv_cols").show();
    } else {
      $("#csv_cols_inputs").hide();
      $("#no_csv_cols").hide();
      $("#csv_preview_cols").show();
    };
  });

  $("#sel_lists_management").change(function(){
    getSelectedLists( $("#sel_lists_management option:selected").val() );
    // updateUI();
    loadSelectedList();
  });

  $("#sel_import_lists").change(function(){
    getSelectedLists( $("#sel_import_lists option:selected").val() );
    // updateUI();
    loadSelectedList();
  });
  // End List button events.

  // Lists functions.
  // Handle chosen list changes.
  $(".sel-lists").change(function(){
    /*setTimeout(function(){
      $.each(lists, function(idx, el){
        if (el[0] == $("#sel_lists_management").val()) {
          $("#lists_search_by").html("");
          $.each(el, function(i,e){
            if (i == 1) {
              $("#lists_subscriber_count").val(e);
            };

            if (i > 2) {
              $("#lists_search_by").append('<option>' + String(e) + '</option>');
            };
          });
        };
      });
    }, 200);*/
  });

  $("#search_contacts").click(function(){
    var notify_args = { icon: 'fa fa-search', title: 'Klickrr :: Lists', message: 'Searching...', type: 'info'};

    Klickrr.Helpers.notify(notify_args);

    var subscriber_cols = [];
    var subscribers = [];
    var subscriber_thead = "";
    var subscriber_tr = "";
    var subscriber_trs = [];

    var search_column = $("#lists_search_by option:selected").text();
    var criterion = $("#list_search_criterion").val();

    var data_obj = {list_id: selected_list, search_column: search_column, criterion: criterion};

    $.ajax({
      url: "list/search",
      type: "POST",
      data: data_obj,
      success: function(data){
        var notify_args = { icon: 'fa fa-list', title: 'Klickrr :: Lists', message: 'Results found!', type: 'success'};

        Klickrr.Helpers.notify(notify_args);

        // Populate the column names.
        /*$.getJSON("list/" + selected_list + "/subscribers/columns.json", function(data){
          $.each(data, function(key, val){
            subscriber_cols.push( val );
          });
          subscriber_cols = $.map(data, function(val) { return val });
          console.log("Subscriber cols: " + subscriber_cols);
        });

        // TODO: Check to make sure both arrays are populated.
        */
        subscriber_thead = "<tr><thead>";
        $.each(data.columns, function(index, elem){
          subscriber_thead += "<th>" + elem.label + "</th>";
        });
        subscriber_thead += "</thead></tr>";

        subscriber_trs.push(subscriber_thead);

        $.each(data.subscribers, function(idx, subscriber){
        //$.each(subscribers, function(idx, subscriber){
          subscriber_tr = '<tr data-set-id="' + idx + '">';
          $.each(data.columns, function(index, elem){
            item = $.grep(subscriber, function(value, index){
              return elem.id == value.list_field_id
            })
            if (item.length > 0) {
              if (item[0].field_value == null) { item[0].field_value == '-' }
              subscriber_tr += '<td data-value-id="' + item[0].id + '">' +  item[0].field_value + '</td>';
            } else {
              subscriber_tr += '<td data-field-id="' + elem.id + '">-</td>';
            }
          })

          subscriber_tr += '<td><button type="button" class="btn btn-default edit-searched-contact"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-info save-searched-contact" style="display: none;" data-set-id="' + idx+ '"><i class="fa fa-floppy-o"></i></button></td>';
          subscriber_tr += '<td><button type="button" class="btn btn-default delete-searched-contact" data-set-id="' + idx + '"><i class="fa fa-trash-o"></i></button></td>';
          subscriber_tr += "</tr>";

          subscriber_trs.push(subscriber_tr);
        });

        $("#contact_search_results_tbl").html( subscriber_trs );
      },
      error: function(data){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'No results found!', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      }
    });
  });

  $("#clear_search_contacts").click(function(){
    $("#list_search_criterion").val("");
    $("#contact_search_results_tbl").html("");
  });

  // Update the order of the list, based on the column header clicked.
  $('body').on('click', 'a.lists_order', function(){
      Klickrr.listPagination.order = $(this).attr('data-field') + ' ' + $(this).attr('data-order-status');
      $('#lists_page').trigger('page', [Klickrr.listPagination.page]);
  });

  $('body').on('click', '#lists_page_size a', function(){
    Klickrr.listPagination.page_size = $(this).attr('data-page-size');
    $('#lists_page').trigger('page', [1]);
    $('#lists_page_size li').removeClass('active');
    $('#lists_page_size a[data-page-size="' + Klickrr.listPagination.page_size + '"]').parent().addClass('active');
  })

    // Calculate page data for lists.
    listsTotalPages({
        page: Klickrr.followupPagination.page,
        page_size: Klickrr.followupPagination.page_size,
        list_id: selected_list
    });

    // listsTotalPages();

    // Get the current page of list results.
    $('#lists_page').on("page", function(event, num){
        Klickrr.listPagination.page = num;
        $.ajax({
            url: "/lists/list",
            type: "GET",
            data: Klickrr.listPagination,
            dataType: "html",
            success: function(data){
                $('#list_manager').html(data);
                listsTotalPages();
                $('[data-toggle="popover"]').popover();
            }
        });
    });

    // Get the total number of pages of lists from the server.
    function listsTotalPages(){
        $.ajax({
            url: "/lists/total_pages",
            data: Klickrr.listPagination,
            type: "GET",
            dataType: "json",
            success: function(data){
                Klickrr.listPagination.last_page = data.total_pages;
                $('#lists_page').bootpag({
                    total: Math.max.apply(null, [1, data.total_pages]),
                    maxVisible: 3
                })
            }
        })
    }

    // When the user clicks the Title/Name link in the list manager, change the selected list and open it in the editor.
    $(document).on("click", "a.edit-list", function(){
        // Set the selected list.
        $("#master_lists_sel").val( $(this).attr("data-id") );
        $('#master_lists_sel').trigger('change');
        // Navigate to the lists editor.
        $("a[href='#lists']").tab("show");

    });

    // When the user clicks the broadcast link in the list manager, change the selected list and open the broadcast.
    $(document).on("click", "a.list-broadcast", function(){
        // Set the selected list.
        $("#master_lists_sel").val( $(this).attr("data-id") );
        $('#master_lists_sel').trigger('change');

        // Navigate to the lists editor.
        $("a[href='#broadcast_list']").tab("show");
    });

    // When the user clicks the follow up link in the list manager, change the selected list and open the follow up.
    $(document).on("click", "a.list-followup", function(){
        // Set the selected list.
        $("#master_lists_sel").val( $(this).attr("data-id") );
        $('#master_lists_sel').trigger('change');

        // Navigate to the lists editor.
        $("a[href='#followup_list']").tab("show");
    });

    // When the user clicks the Webforms link in the list manager, change the selected list and open the Webforms.
    $(document).on("click", "a.list-webforms", function(){
        // Set the selected list.
        $("#master_lists_sel").val( $(this).attr("data-id") );
        $('#master_lists_sel').trigger('change');

        // Navigate to the lists editor.
        $("a[href='#web_form']").tab("show");
    });

    // When the user clicks the "New List" button, clear the editor and open it.
    $('body').on('click', "#new_list_btn", function(){
        selected_list = null;
        $("#master_lists_sel").val("Select a contact list...");
        $('#master_lists_sel').trigger('change');

        // Navigate to the lists editor.
        $("a[href='#lists']").tab("show");
    });


  $("body").on("click", ".delete-searched-contact", function(){
    //list/delete/contact
    var self = this;

    bootbox.confirm('<p class="lead text-danger">WARNING!</p><p>You are about to PERMANENTLY delete a contact from your list. This cannot be undone.</p>', function(result){
      if (result == true) {
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'Deleting chosen contact...', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);

        var deleted_set = { set_id: $(self).attr("data-set-id") };
        $.ajax({
          url: "list/delete/contact",
          type: "POST",
          data: deleted_set,
          success: function(data){
            $(self).parent().parent().remove();
            var notify_args = { icon: 'fa fa-trash-o', title: 'Klickrr :: Lists', message: 'Successfully deleted.', type: 'info'};

            Klickrr.Helpers.notify(notify_args);
          },
          error: function(){
            var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'ERROR. Could not delete contact.', type: 'danger'};

            Klickrr.Helpers.notify(notify_args);
          }
        });
      };
    });

    /*if (test == true) {
      $(this).parent().parent().remove();
    };*/
  });

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~ SET the selected list throughout the application
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  $("#master_lists_sel").change(function(){
    if ( $(this).val() != "Select a contact list..." ) {
      selected_list = $("#master_lists_sel option:selected").val();
      $('#show_segments_modal_btn').removeAttr('disabled');
    } else {
      selected_list = null;
      $('#show_segments_modal_btn').attr('disabled', 'disabled');
    }

    selected_broadcast = null;
    selected_followup = null;
    Klickrr.broadcastPagination.list_id = selected_list;
    Klickrr.followupPagination.list_id = selected_list;

    // updateUI();
    loadSelectedList();
    $('#broadcast_page').trigger("page", [Klickrr.broadcastPagination.page]);
    $('#followup_page').trigger("page", [Klickrr.followupPagination.page]);

  });
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  $("#get_more_leads_btn").click(function(){
    $("#mod_get_more_leads").modal("show");
  });


  // Handle CSV file uploads.
  $("input#list_csv_upload_file").on('fileselect', getListsFile);

  function getListsFile() {
    input = document.getElementById('list_csv_upload_file');
    //var input = $("#list_csv_upload_file");

    if (!input) {
      alert("Could not find the fileinput element.");
    }
    else if (!input.files) {
      alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input.files[0]) {
      alert("Please select a file before clicking 'Load'");
    }
    else {
      file = input.files[0];
      rdr = new FileReader();
      rdr.onload = loadListsFile;
      //fr.readAsText(file);
      rdr.readAsText(file);
    }
  }

  /*************************
  TODO: Add handling for import
  into lists that already have
  data.
  *************************/
  function loadListsFile() {
    var parsed = $.csv.toArrays(rdr.result);
    console.log("There are " + parsed.length + " results.")
    parsingSubscribers(parsed);
  }


  function parsingSubscribers(parsed) {
    var preview_cols_html = new String();
    var preview_data_html = new String();
    var no_csv_cols = new String();
    var csv_cols_inputs = new String();
    var fields_to_import = [];
    var field_options = [];
    var selected = $.grep(lists, function(l){ return l.list_id == selected_list });

    // Build header row for preview.
    csv_cols_inputs = '<tr id="csv_cols_inputs" style="display: none;">';
    no_csv_cols = '<tr id="no_csv_cols" style="display: none;">';
    preview_cols_html = '<tr id="csv_preview_cols">';

    if ( typeof selected[0] != "undefined" && selected[0].forms != null ) {
      $.each(selected[0].forms, function(key, form){
        $.each(form.fields, function(key, val){
          field_options.push('<option value="field_' + val.id + '">' + val.label + '</option>');
        });
      });
    };

    for (var i = 0; i <= parsed[0].length - 1; i++) {
      if (parsed[0][i] != "") {
        csv_cols_inputs += '<td class="csv_head_' + i + '" class="csv_no_header"><input type="text" class="form-control" /></td>';
        no_csv_cols += '<td class="csv_head_' + i + '" class="csv_no_header">' + parsed[0][i] + '</td>';
        preview_cols_html += '<th class="csv_head_' + i + '">' + parsed[0][i] + '</th>';

        //var field_import = '<form class="form-inline"><div class="form-group"><input type="checkbox" class="csv_include_field" data-field-id="' + i + '" id="field_' + i + '_chk" checked="true" onchange="toggle_fields(' + i + ');" />&nbsp;' + parsed[0][i] + '&nbsp;&nbsp;<select class="form-control"><option>Layout test...</option><option>New data</option></select></div></form>';

        //var field_import = '<tr><td style="width: 30%; padding-right: 20px;"><input type="checkbox" class="csv_include_field" data-field-id="' + i + '" id="field_' + i + '_chk" checked="true" onchange="toggle_fields(' + i + ');" />&nbsp;' + parsed[0][i] + '</td><td style="width: 20%; padding-right: 20px;"><input type="radio" data-field-name="' + parsed[0][i] + '" name="email_field_radio" id="radio_' + i + '" /></td><td style="width: 100%;"><select class="form-control existing_field_selects" id="sel_existing_fields_' + i + '"><option>New field</option>' + field_options + '</select></td></tr>';

        var field_import = '<tr><td style="width: 30%; padding-right: 20px;"><input type="checkbox" class="csv_include_field" data-field-id="' + i + '" id="field_' + i + '_chk" checked="true" onchange="toggle_fields(' + i + ');" />&nbsp;' + parsed[0][i] + '</td><td style="width: 20%; padding-right: 20px; text-align: center;"><input type="radio" data-field-name="' + parsed[0][i] + '" name="email_field_radio" id="radio_' + i + '" /></td></tr>';
        fields_to_import.push(field_import);
      };
    };

    csv_cols_inputs += '</tr>';
    no_csv_cols += '</tr>';
    preview_cols_html += '</tr>';

    $("#csv_preview_header").html( csv_cols_inputs + no_csv_cols + preview_cols_html );
    // Header row for preview complete.

    // Handle issue if list is too short (like for testing).
    var data_len;

    if (parsed.length > 4) {
      data_len = 3;
    } else {
      data_len = parseInt(parsed.length) - 1;
    };

    for (var i = 1; i <= data_len; i++) {
      preview_data_html += "<tr>";

      for (var x = 0; x <= parsed[i].length - 1; x++) {
        if (parsed[i][x] != "") {
          preview_data_html += '<td class="csv_data_' + x + '">' + parsed[i][x] + '</td>';
        };
      };

      preview_data_html += "</tr>";
    };

    $("#csv_preview_header").append( preview_data_html );

    $("#csv_included_fields").html( fields_to_import.join(" ") );

    $("#mod_csv_parser").modal("show");

    $("#csv_included_fields input[type='radio']").filter('[data-field-name="email"]').attr('checked', true);
    if ($("#csv_included_fields input[type='radio']:checked").length == 0){
      $("#csv_included_fields input[type='radio']").first().attr('checked', true);
    }
  }

  function progressOfListsFile() {

  }

  function errorParsingListsFile() {

  }

  $(document).on("click", ".list-service-btn", function(){
    var that = this;
    var services = [];

    if ( $(this).hasClass("active") === true ) {
      $("i", this).removeClass("fa-square-o");
      $("i", this).addClass("fa-check-square-o");
      $(that).addClass("active");
    } else {
      $("i", this).addClass("fa-square-o");
      $("i", this).removeClass("fa-check-square-o");
      $(that).removeClass("active");
    };

    // Save to the server.
    $(".list-service-btn").each(function(){
      //var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Lists', message: 'Updating selected services for this list.', type: 'info'};

      //Klickrr.Helpers.notify(notify_args);

      if ( $(this).hasClass("active") == true ) {
        services.push( $(this).attr("data-service") );
      };
    });

    var payload = {};

    payload["services"] = services.join(",");
    payload["list_id"] = selected_list;

    $.ajax({
      url: "list/services/update",
      type: "post",
      data: payload,
      success: function(data){
        $.getJSON("/list/services/" + selected_list, function(data){
          list[0]["active_services"] = data["list_service_labels"];

            // Update the list manager.
            $('#lists_page').trigger("page", [Klickrr.listPagination.page]);

          broadcastServersCheck();
          followupServersCheck();
        });

        //var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Lists', message: 'Successfully updated services.', type: 'success'};

        //Klickrr.Helpers.notify(notify_args);
      },
      error: function(data){
        //var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Lists', message: 'WARNING! Cannot update services at this time.', type: 'danger'};

        //Klickrr.Helpers.notify(notify_args);
      }
    });
  });

  $('body').on('click', '#list_csv_upload_file',function(){
    $("#csv_included_fields").html('<tr><th>Import?</th><th>Email?</th></tr>');
  })

  $('body').on('click', '#mod_csv_parser button[data-dismiss="modal"]', function(){
    $('#list_csv_upload_file').val('');
  })

    /******************************************
    * Combo list functions.
    * ******************************************/
  $(document).on("change", ".combo-list-sel", function(){
      if ( $(this).is(":checked") ) {
          // Check to see if this section already exists in the DOM.
          if ( $('section.fields-section[data-id="' + $(this).attr("data-id") + '"]') === true ) {
              $('section.fields-section[data-id="' + $(this).attr("data-id") + '"]').show();
          } else {
              // If hasn't been created yet, so let's do that now.
              var list_id_arr = {list_id_arr: [ $(this).attr("data-id") ] };

              // Fetch the JSON fields list via HTTP POST.
              $.ajax({
                  url: "list/fields.json",
                  type: "POST",
                  data: list_id_arr,
                  async: true,
                  success: function(data){
                      var lists_arr = [];
                      var combo_field_map_hdr = [];
                      var combo_fields_sel = [];

                      // Iterate the lists.
                      $.each(data["lists"], function(list_idx, lst){
                          lists_arr.push('<section class="fields-section" data-id="' + lst["id"] + '" style="float: left; margin-left: 15px;"><label>' + lst["label"] + ' fields</label><div class="bootstrap-form-with-validation"><div class="form-group">');
                          combo_field_map_hdr.push('<th data-id="' + lst["id"] + '">' + lst["label"] + '</th>');
                          combo_fields_sel.push('<td><select class="combo-fields-sel">');

                          var fields_arr = [];
                          var options_arr = [];
                          // Iterate the list fields.
                          $.each(lst["fields"], function(key, val){
                              var checkbox;
                              if ( val["is_email"] === true ) {
                                  checkbox = '<label class="control-label" for="checkbox-input">Email</label>';
                              } else {
                                  checkbox = '<label class="control-label"><input type="checkbox" class="list-fields-check-input" data-id="' + val["id"] + '" data-list-id="' + lst["id"] + '">' + val["label"] + '</label>';
                              }

                              fields_arr.push('<div class="checkbox">' + checkbox + '</div>');
                              options_arr.push('<option val="' + val["id"] +  '">' + val["label"] + '</option>');
                          });

                          combo_fields_sel.push( options_arr.join("") );
                          combo_fields_sel.push( "</select></td>" );

                          lists_arr.push( fields_arr.join("") );
                          lists_arr.push("</div></div></section>");
                      });

                      // Build the fields string.
                      $("#list_fields_selection").append( lists_arr.join("") );
                      $('.combo-field-name-hdr').before( combo_field_map_hdr.join("") );
                      $('.combo-field-name').before( combo_fields_sel.join("") );

                  },
                  error: function(data){
                      console.log("error with ajax call.");
                  }
              });
          }
      } else {
          $('section.fields-section[data-id="' + $(this).attr("data-id") + '"]').hide();

          console.log("List is unselected.");
      }
  });

  $(document).on("change", ".list-fields-check-input", function(){
      if ( $(this).is(":checked") ) {
          console.log("Field is checked.");
      } else {
          console.log("Field is unchecked.");
      }
  });


  $(document).on("click", ".remove-mapper-row", function(){
      $(this).parent().parent().remove();
  });

  $(document).on("click", ".add-mapper-row", function(){
      $("tbody", "#fields_mapper_table").append( $(".field-mapper-tr").html() );
  });


  $(document).on("click", ".add-mapper-row", function(){
    $("tbody", "#fields_mapper_table").append( $(".field-mapper-tr").html() );
  });

  $(document).on("click", ".edit-segments", function(){
      showListSegmentationDialog(this);
  });

  $(document).on("click", ".add-list-segment-btn", function(){
    $("#segment_collection").prepend('<div class="segment-item">' +  $("#segment_option_tmpl").html() + '</div>');
    var newItem = $($("#segment_collection .segment-item").get(0));
    newItem.find("select.segment-list-sel").val($("#segment_option_tmpl select.segment-list-sel").val());
    newItem.find("select.segment-operator-sel").val($("#segment_option_tmpl select.segment-operator-sel").val());
    newItem.find("input.segment-val").val($("#segment_option_tmpl input.segment-val").val());
    newItem.find(".add-list-segment-btn").after('<button type="button" class="btn btn-sm btn-primary del-list-segment-btn"><i class="fa fa-close"></i></button>');
    newItem.find(".add-list-segment-btn").hide();
  });

  $(document).on("click", ".del-list-segment-btn", function(){
      $(this).parent().remove();
  });

  $('a[href="#lists_list"]').on('shown.bs.tab', function(){
    // updateListContent();
    $('#lists_page').trigger('page', [Klickrr.listPagination.page]);
  })

  $('body').on('click', '#add_new_contacts', function(){
    $.ajax({
      url: "list/" + selected_list + "/add_new_subscriber",
      type: "GET",
      success: function(data){
        $('#add_new_contact_modal .modal-body').html(data);
        $('#new_contact_error_message').hide();
        $('#validate_email_error_message').hide();
        $('#add_new_contact_modal').modal('show');
      }
    })
  })

  $('body').on('click', '#save_new_contact_btn', function(){
    var values = {};
    values.field_values = [];
    var allowing = true;
    $('#add_new_contact_modal .modal-body input').each(function(index, elem){
      if ( ($(elem).val() == '' || $(elem).val() == null) && $(elem).attr('required') ) {
        $('#new_contact_error_message').show();
        allowing = false;
        return false;
      } else {
        values.field_values.push({id: $(elem).attr('data-id'), value: $(elem).val()})
      }
    })

    var email_field = $('#add_new_contact_modal .modal-body input[data-email]');
    var validate_value = {email: email_field.val(), field_id: email_field.attr('data-id')};
    if (allowing) {
      $.ajax({
        url: "list/" + selected_list + "/subscriber_email_validate",
        type: "POST",
        data: validate_value,
        success: function(data){
          if (!data.success) {
            $('#validate_email_error_message').show();
          } else {
            $.ajax({
              url: "list/" + selected_list + "/save_subscriber",
              type: "POST",
              dataType: 'json',
              contentType: 'application/json',
              data: JSON.stringify(values),
              success: function(data){
                var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Lists', message: 'Successfully created subscriber.', type: 'success'};
                Klickrr.Helpers.notify(notify_args);
                $('#add_new_contact_modal').modal('hide');
              }
            })
          }
        }
      })
    }
  })


  $('body').on('click', ".edit-searched-contact", function(){
    $(this).hide();
    $(this).siblings('.save-searched-contact').show();
    $(this).parent().siblings('td:not(:has(button))').each(function(index, elem){
      var elemVal = $(elem).text();
      var dataValueId = $(elem).attr('data-value-id') || '';
      var dataFieldId = $(elem).attr('data-field-id') || '';
      $(elem).html('<input class="form-control" type="text" value="' + elemVal + '" data-value-id="' + dataValueId + '" data-field-id="' + dataFieldId + '"></input>');
    });
  });

  $('body').on('click', ".save-searched-contact", function(){
    var button = $(this);
    values = [];
    button.parents('tr').find('input').each(function(index, elem){
      var value = {}
      if ($(elem).attr('data-value-id')) {
        value.value_id = $(elem).attr('data-value-id');
      }
      if ($(elem).attr('data-field-id')) {
        value.field_id = $(elem).attr('data-field-id');
      }
      value.field_value = $(elem).val();
      values.push(value);
    })

    $.ajax({
      url: "list/update_subscriber/" + button.attr('data-set-id'),
      type: "POST",
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({values: values}),
      success: function(data){
        button.hide();
        button.siblings('.edit-searched-contact').show();
        button.parent().siblings('td:not(:has(button))').each(function(index, elem){
          $(elem).html($(elem).find('input').val());
        });
        $.each(data.new_items, function(index, item){
          elem =  button.parent().siblings('td:not(:has(button))[data-field-id="' + item.field_id + '"]')
          elem.attr('data-value-id', item.value_id);
          elem.removeAttr('data-field-id');
        })
      }
    })
  });

  $('body').on('click', '#copy_paste_leads', function(){
    var parsed = [];
    $.each($('#textarea_with_new_subscribers').val().split('\n'), function(index, elem){
      if (elem != '') {
        parsed.push(elem.split(','));
      }
    })
    parsingSubscribers(parsed);
    Klickrr.newSubscribers = {data: parsed};
    $("#copy_paste_subscibers_modal").modal("hide");
  })

  $('#copy_paste_subscibers_modal').on('shown.bs.modal', function() {
    $('#textarea_with_new_subscribers').val('');
    $.getJSON("list/" + selected_list + "/subscribers/columns.json", function(data){
      $('#textarea_with_new_subscribers').val(data.toString());
    });
  })
});
