$.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
});

// Set up global namespace.
//(function($){
  var Klickrr = Klickrr || {};

  Klickrr.broadcastPagination = {
    page: 1,
    page_size: 10,
    order: 'updated_at DESC',
    list_id: null,
  }

  Klickrr.followupPagination = {
    page: 1,
    page_size: 10,
    last_page: 1,
    list_id: null
  }

  Klickrr.listPagination = {
    page: 1,
    page_size: 10,
    last_page: 1
  }

  Klickrr.webformPagination = {
    page: 1,
    page_size: 10,
    order: 'updated_at DESC',
    list_id: null,
  }

  Klickrr.Helpers = {
      notify: function(args){
        if (args["delay"] == null) {
          args["delay"] = 2000;
        };

        return $.notify({
          // options
          icon: args["icon"],
          title: "<strong>" + args["title"] + "</strong><br/><br/>",
          message: args["message"]
        },{
          // settings
          type: args["type"],
          delay: args["delay"]
        });
      },

      html2text: function(html) {
        var tag = document.createElement('div');
        tag.innerHTML = html;

        //return tag.innerText;
        return $(tag).text();
      },

      commaSeparateNumber: function(val){
          if ( val != null ) {
              while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
              }
          }

          return val;
      },

      capitalize: function(str){
        str = str.toLowerCase();
        return str.replace(/([^ -])([^ -]*)/gi,function(v,v1,v2){ return v1.toUpperCase()+v2; });
      },

      randomString: function(length, chars) {
        var mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        var result = '';
        for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];

        return result;
      },

      label2Name: function(label){
        if (label != null) {
          var name_arr = label.split(" ");
          var tmp_arr = [];
          var name = "";

          $.each(name_arr, function(key, val){
            val = val.replace(/\W/g, '');
            val = val.toLowerCase();

            tmp_arr.push(val);
          });

          return tmp_arr.join("_");
        };
      }
    };

  Klickrr.spam_test = function(subj, msg){
    var payload = { "subject": subj, "content": msg };
    var spam_test = [];

    $.ajax({
      url: "mail/raw",
      type: "POST",
      data: payload,
      success: function(data){
        console.log("Success entered.");
        //$("#test_results").html("");
        spam_test["success"] = true;
        spam_test["notify_args"] = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Broadcasts', message: 'Success! Raw email format retrieved.', type: 'success'};

        // Klickrr.Helpers.notify(notify_args);

        // console.log(data);
        spam_test["spam_score"] = data["score"];
        // $("#spam_score").text( data["score"] );

        var spam_issues = "<table><tr><thead><th>Points</th><th>Rule Name</th><th>Description</th></thead></tr>";
        $.each(data["report"].split("\n"), function(key, val){
          var cols = val.split(" ");

          //Drop the first element, as it is empty.
          cols.shift();

          if (isNaN(cols[0]) == true) {
            return;
          };

          var col_1 = cols.shift();
          var col_2 = cols.shift();
          var col_3 = cols.join(" ");

          spam_issues += '<tr><td>' + col_1 + '</td><td>' + col_2 + '</td><td>' + col_3 + '</td></tr>';
        });

        spam_issues += "</table>";

        spam_test["test_results"] = spam_issues;
        //$("#test_results").html( spam_issues );
        console.log("End of success function");
      },
      error: function(data){
        spam_test["success"] = false;
        spam_test["notify_args"] = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'There was a problem. Email could not be retrieved.', type: 'danger'};
      }
    });

    return spam_test;
  }
//})();

// Update the subscribers count field.
// setInterval(function(){
//   if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
//     var list_id = {list_id: selected_list};
//     $.ajax({
//       url: "/list/subscribers/count",
//       type: "post",
//       async: true,
//       data: list_id,
//       success: function(data){
//         $(".sub-count-span").text(data["subCount"]);
//         $(".sub-count").val( data["subCount"] );
//         $(".sub-count").attr("disabled", false);
//         $(".sub-count").attr("disabled", true);
//         $(".sub-count").text( Klickrr.Helpers.commaSeparateNumber(data["subCount"]) );
//       }
//     });
//   };
// }, 3000);

function updateCountListSubscribers(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    var list_id = {list_id: selected_list};
    $.ajax({
      url: "/list/subscribers/count",
      type: "post",
      data: list_id,
      success: function(data){
        $(".sub-count-span").text(data["subCount"]);
        $(".sub-count").val( data["subCount"] );
        $(".sub-count").attr("disabled", false);
        $(".sub-count").attr("disabled", true);
        $(".sub-count").text( Klickrr.Helpers.commaSeparateNumber(data["subCount"]) );
      }
    });
  };
}


uid = new Number();
service_conf = [];
services = [];
broadcasts = [];
followups = [];
lists = [];
list = [];
curr_lists = [];
selected_list = null;
selected_broadcast = null;
analytics_broadcast = null;
selected_followup = null;
analytics_followup = null;
selected_services = [];
last_bc_id = 8;
last_fu_id = 9;


function updateListContent(){
    $.ajax({
        url:'/payload.json',
        type: "POST",
        async: true,
        success: function(data){
            lists = data.lists;
            drafts = data.drafts;
        },
        error: function(data){
            var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Canot connect to server',
                message: 'Cannot retrieve list contents. Are you having network problems?', type: 'danger'};
            Klickrr.Helpers.notify(notify_args);
        }
    }).fail(function(xhr){
            console.log(xhr);
        })
        .promise()
        .done(function(){
            updateListsSelects();
        });

    //$.post('/payload.json', function(data){
    //// JSON to JS array of objs.
    //lists = $.map(data, function(val) { return val });
    //}).fail(function(xhr){
    //  console.log(xhr);
    //})
    //.promise()
    //.done(function(){
    //  updateListsSelects();
    //});
  }

  $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
  numFiles = input.get(0).files ? input.get(0).files.length : 1,
  label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

jQuery.fn.extend({
  getCursorPosition: function() {
          var el = $(this).get(0);
          var pos = 0;
          if('selectionStart' in el) {
              pos = el.selectionStart;
          } else if('selection' in document) {
              el.focus();
              var Sel = document.selection.createRange();
              var SelLength = document.selection.createRange().text.length;
              Sel.moveStart('character', -el.value.length);
              pos = Sel.text.length - SelLength;
          }
          return pos;
      }
})

function getNode(nodeType) {
  list = $.grep(lists,function(l){ return l.list_id == selected_list });
  if( selected_broadcast != null ){
    if (list.length > 0) {
      var broadcast = $.grep(list[0].broadcasts, function(b){ return b.id == selected_broadcast });
      if (broadcast.length == 0){
        broadcast = $.grep(drafts, function(b){ return b.id == selected_broadcast });
      }
    } else {
      var broadcast = $.grep(drafts, function(b){ return b.id == selected_broadcast });
    }

      if( selected_followup != null && selected_broadcast != null ){
        var followup = $.grep(broadcast[0].followups, function(f){ return f.id == selected_followup });
      }
  }

  if (selected_followup != null) {
    followup = $.grep(list[0].followups, function(followup){
      return followup.id == selected_followup
    });
  }

  switch (nodeType){
    case "lists":
      // selected_broadcast = null;
      // selected_followup = null;
      return list;
    break;

    case "broadcasts":
      selected_followup = null;
      return broadcast;
    break;

    case "followups":
      return followup;
    break;
  }
}

function txt_lists(arr){
  var txt_items = "";

  $.each(arr, function(idx,itm){
    if (idx === arr.length - 1) {
      txt_items = txt_items + (itm);
    } else {
      txt_items = txt_items + (itm + ", ");
    }
  });

  return txt_items;
}

// Remove any duplicates from the master list.
function clean_lists(){
  arr = $.grep(lists, function(a) {
    return a.list_id != selected_list;
  });

  selected_list = null;

  return arr;
}

// Toggle CSV fields on and off for inclusion in import.
function toggle_fields(field_id) {
  if ( $("#field_" + field_id + "_chk").prop("checked") == false ) {
    $(".csv_head_" + field_id).css("display", "none");
    $(".csv_data_" + field_id).css("display", "none");
    $("#sel_existing_fields_" + field_id).val("New field");
    $("#sel_existing_fields_" + field_id).attr("disabled", "disabled");
  } else {
    $(".csv_head_" + field_id).css("display", "table-cell");
    $(".csv_data_" + field_id).css("display", "table-cell");
    $("#sel_existing_fields_" + field_id).removeAttr("disabled");
  };
}
// End CSV toggling.

// Function to watch for data changes.
function checkDirty(){
  //if (curr_lists != null) {
  //  if(curr_lists != lists) {
  //    // Walk through the data and update the UI as needed.
  //    /**/
  //
  //    $.when( updateUI() ).then( curr_lists = lists );
  //    updateUI();
  //  }
  // } else {
    curr_lists = lists;
  // };
}
// END function to watch for data changes.

function updateUI(){
  setTimeout(function(){
    update_webforms_ui();
    load_list_collections(selected_list);
    load_list_fields(selected_list);
    updateListsSelects();
    updateListsChildren();
    updateListsBroadcasts();
    updateListImportUI();
    toggleListManagementButtons();
    updateListServiceData();
    updateBroadcastsFollowups(selected_broadcast);
    updateFollowupBroadcastsList();
    analyticsListsChange();
    loadBroadcastDialogList();
    loadFollowupDialogInit();
    generate_bc_field_btns();
    generate_fu_field_btns();
    toggleSaveReadiness('broadcast');
    toggleSaveReadiness('followup');
    list_service_btn_setup();
    broadcastServersCheck();
    followupServersCheck();
    analytics_data_update("broadcast", selected_broadcast);

    // Init the follow up scope dropdowns.
    selBroadcastTitles();
    selWebformTitles();
    followup_scope_available(); // Toggle enabled/disabled on followup scope radios.
    followup_trigger_available(); // Toggle enabled/disabled on followup trigger radios.
    activate_followup_types(); // Activate followup types, since a radio will always already be chosen by default.
    toggleFollowupState(); // Toggles the active/inactive switch on the followups.
  }, 250);
}

function setRadioButtonLists(lists){
  var tmp = [];

  $.each(lists, function(key, val){
    if (val.list_id == selected_list) {
      tmp.push('<label><input type="radio" name="sel_broadcast_lists" value="' + val.list_id + '" checked> ' + val.title + '</label><br>');
    } else {
      tmp.push('<label><input type="radio" name="sel_broadcast_lists" value="' + val.list_id + '"> ' + val.title + '</label><br>');
    }
  });

  $('.radio-button-lists').html(tmp);

  $('input[type="radio"][name="sel_broadcast_lists"]').change(function() {
    getSelectedLists($(this).val());
    return true;
  });
}

function updateListsSelects(){
  //console.log("Updating <select> lists...");
  var tmp = [];

  if (selected_list == null) {
    tmp.push('<option selected>Select a contact list...</option>');
  } else {
    tmp.push('<option>Select a contact list...</option>');
  };

  $.each(lists, function(key, val){
      if (val.list_id == selected_list) {
        tmp.push('<option value="' + val.list_id + '" selected>' + val.title + '</option>');
      } else {
        tmp.push('<option value="' + val.list_id + '">' + val.title + '</option>');
      }
  });

  $(".sel-lists").html( tmp );

  setRadioButtonLists(lists);
}

function updateListsChildren(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    var selected = $.grep(lists, function(l){ return l.list_id == selected_list });
    var field_options = [];

    if ( typeof selected[0] != "undefined" && selected[0].forms != null ) {
      $.each(selected[0].forms, function(key, form){
        $.each(form.fields, function(key, val){
          field_options.push('<option>' + val.label + '</option>');
        });
      });
    };

    $("#lists_search_by").html( field_options );

    $("#export_subscribers_btn").attr("href", "list/subscribers/" + selected_list + "/export.csv");

    // Set subscriber count.
    $("#lists_subscriber_count").val( Klickrr.Helpers.commaSeparateNumber(selected[0].subCount) );
  } else {
    $("#lists_search_by").html( [] );
    $("#lists_subscriber_count").val("");
  };
}

// Updates broadcast list groups on list change, across entire app.
function updateListsBroadcasts(broadcast_sort_type){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    var selected = $.grep(lists, function(l){ return l.list_id == selected_list });
    var sched_broadcasts = [];
    var hist_broadcasts = [];
    var analytics_broadcasts = [];
    var now = new Date();

    // Sort our broadcasts.
    getBroadcasts().then(function(data){
      var selected_sorted = data.broadcasts.sort(function(a,b){ return b.id-a.id });

      if (broadcast_sort_type != null) {
        switch(broadcast_sort_type) {
          case "old_to_new":
            selected_sorted = selected[0].broadcasts.sort(function(a,b){ return a.id-b.id });
            break;

          case "new_to_old":
            selected_sorted = selected[0].broadcasts.sort(function(a,b){ return b.id-a.id });
            break;

          case "ctr_high":
            selected_sorted = selected[0].broadcasts.sort(function(a,b){ return b.ctr-a.ctr });
            break;

          case "clicks_high":
            selected_sorted = selected[0].broadcasts.sort(function(a,b){ return b.clicks-a.clicks });
            break;

          case "opens_high":
            selected_sorted = selected[0].broadcasts.sort(function(a,b){ return b.opens-a.opens });
            break;

          default:
            selected_sorted = selected[0].broadcasts.sort(function(a,b){ return b.id-a.id });
            break;
        }
      }

      $.each(selected_sorted, function(key, val){
        if ( val.sent_on != null ) {
        //if ( val.sent_on != null && new Date(val.sent_on) > now ) {
          hist_broadcasts.push('<a href="#" class="list-group-item" data-id="' + val.id + '" id="sched_broadcast_' + val.id + '" onClick="quickStatsInit(' + val.id + ');"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary" onClick="editBroadcast(' + val.id + ');"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-primary" id="copy_sched_broadcast_' + val.id + '" onClick="cloneBroadcast(' + val.id + ',' + selected_list + ');"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
          analytics_broadcasts.push('<a href="#" class="list-group-item" data-id="' + val.id + '" id="analytics_broadcast_' + val.id + '"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
          $('body').on('click', '#analytics_broadcast_' + val.id, function(){
            showAnalyticsDetails('broadcast', val, selected_sorted);

            analytics_data_update('broadcast', $(this).attr("data-id"));
          })
          //hist_broadcasts.push('<a href="#" class="list-group-item" id="sched_broadcast_' + val.id + '" data-toggle="modal" data-target="#broadcast_quick_stats_mod"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary" onClick="editBroadcast(' + val.id + ');"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-primary" id="copy_sched_broadcast_' + val.id + '" onClick="cloneBroadcast(' + val.id + ',' + selected_list + ');"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
        } else {
          sched_broadcasts.push('<a href="#" class="list-group-item" data-id="' + val.id + '" id="sched_broadcast_' + val.id + '" onClick="quickStatsInit(' + val.id + ');"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary" onClick="editBroadcast(' + val.id + ');"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-primary" id="copy_analytics_broadcast_' + val.id + '" onClick="cloneBroadcast(' + val.id + ',' + selected_list + ');"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
          //sched_broadcasts.push('<a href="#" class="list-group-item" id="sched_broadcast_' + val.id + '" data-toggle="modal" data-target="#broadcast_quick_stats_mod"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary" onClick="editBroadcast(' + val.id + ');"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-primary" id="copy_sched_broadcast_' + val.id + '" onClick="cloneBroadcast(' + val.id + ',' + selected_list + ');"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
        };
      });
      
      $("#dash_bc_instructions").hide();
      $("#dash_broadcasts").show();
      addBroadcasts(sched_broadcasts, hist_broadcasts, analytics_broadcasts); 
    })
  }  

  else {
    // Dashboard, broadcast, and analytics - broadcasts are null.
    $("#dash_bc_instructions").show();
    $("#dash_broadcasts").hide();
    addBroadcasts(sched_broadcasts, hist_broadcasts, analytics_broadcasts);
  }
}

function addBroadcasts(sched_broadcasts, hist_broadcasts, analytics_broadcasts) {
  if (sched_broadcasts != null && sched_broadcasts.length > 0) {
    $("#sched_broadcasts").html( sched_broadcasts );
  } else {
    $("#sched_broadcasts").html( "No broadcasts scheduled for this list." );
  }

  if (sched_broadcasts != null && hist_broadcasts.length > 0) {
    $(".broadcast-history").html( hist_broadcasts );
    $(".broadcast-analytics").html( analytics_broadcasts );
  } else {
    $(".broadcast-history").html( "No broadcasts have been sent for this list." );
    $(".broadcast-analytics").html( "No broadcasts have been sent for this list." );
  };

  $(".list-group-item > .list-group-item-text").succinct({ size: 220 });
  $('#analytics_broadcasts').trigger('loadItems');
}

// Checks to see if the services assigned to an upcoming Broadcast are in fact active.
function broadcastServersCheck() {
    if ( txt_lists(selected_services) != "" && txt_lists(selected_services) != null ) {
        if (Klickrr.Broadcasts.currentInstance) {
          $("#txt_broadcast_servers").val(Klickrr.Broadcasts.currentInstance.servicesLabels);
        } else {
          if (list[0] != null && list[0]["active_services"] != null) {
            $("#txt_broadcast_servers").val( list[0]["active_services"] );
          } else {
            $("#txt_broadcast_servers").val( txt_lists(selected_services) );
          }
        }
        $("#active_services").removeClass("incomplete-action");
        $("#active_services").addClass("completed-action");
        $("#broadcast_send_later").removeAttr("disabled");
        toggleSaveReadiness('broadcast');
    } else {
        $("#txt_broadcast_servers").val( "" );
        $("#active_services").removeClass("completed-action");
        $("#active_services").addClass("incomplete-action");
        toggleSaveReadiness('broadcast');
        $("#broadcast_send_later").attr("disabled", "disabled");
    }
}

function ChangeTitle(title) {
    if ( $("#master_lists_sel").val() != "Select a contact list..." ) {
        if (title.options[title.selectedIndex]){
          var selectedTitle = title.options[title.selectedIndex].text;
          document.getElementById("txt_create_new_list").value = selectedTitle;
          document.getElementById("btn_create_new_list").innerHTML = '<i class="fa fa-check-circle"></i>&nbsp;Update';
        }
    } else {
        document.getElementById("txt_create_new_list").value = '';
        document.getElementById("btn_create_new_list").innerHTML = '<i class="fa fa-check-circle"></i>&nbsp;Create';
    }
}

function updateFollowupBroadcastsList(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    getBroadcasts().then(function(data){
      var select_options = [];

      select_options.push('<option>Select a broadcast...</option>');

      $.each(data.broadcasts, function(key, val){
        select_options.push('<option id="' + val.id + '">' + val.title + '</option>');
      });
      setSelectFollowUpBroadcasts(select_options);
    })
  } else {
    setSelectFollowUpBroadcasts('<option>Select a list to load associated broadcasts...</option>');
  };
}

function setSelectFollowUpBroadcasts(select_options){
  $("#followup_broadcast").removeClass("completed-action");
  $("#followup_broadcast").addClass("incomplete-action");

  $("#sel_followup_broadcasts").html( select_options );

  toggleSaveReadiness('followup');
}

function updateBroadcastsFollowups(bc_id){
  if ( bc_id != null) { selected_broadcast = bc_id };

  if ( selected_list != null && selected_list != "undefined" && selected_broadcast != null ) {

    getBroadcast(bc_id).then(function(data){
      var fu = [];
      var sorted = [];

      if ( data.followups && data.followups.length > 0 ) {
        data.followups.sort(function(a, b){
          return a.placement - b.placement;
        });

        $.each(data.followups, function(key, val){
          fu.push('<a href="#" class="list-group-item followup_sortable" id="sched_followup_' + val.id + '"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary" onClick="editFollowups(' + val.id + ');"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-primary" id="copy_sched_followup_' + val.id + '" onClick="cloneFollowup(' + val.id + ',' + bc[0].id + ');"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
        });
      }

      $("#dash_fu_instructions").hide();
      $("#dash_followups").show();
      $("#dash_followups_list").html( fu );
      $(".list-group-item > .list-group-item-text").succinct({ size: 220 });
    })
    
  } else {
    // Dashboard, followups, and analytics - followups are null.
    $("#dash_fu_instructions").show();
    $("#dash_followups").hide();
  };
}

function getSelectedLists(sel_val){
  if(sel_val != "Select a contact list..."){
      selected_list = sel_val;
      var selected = getNode("lists");
      var field_options = [];
      var list_columns = [];

      if ( selected[0] != "undefined" && selected[0].forms != null ) {
        $.each(selected[0].forms, function(key, form){
          $.each(form.fields, function(key, val){
            field_options.push('<option value="' + val.id + '">' + val.label + '</option>');
          });
        });
      };

      // Set list 'Search By' options
      $("#lists_search_by").html( field_options );

      // Set subscriber count.
      $("#lists_subscriber_count").val( Klickrr.Helpers.commaSeparateNumber(String(selected[0].subCount)) );

      // Run the functions to update the user interface.

      updateListsSelects();

    } else {
      selected_list = null;
    }
}

function updateListImportUI(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    $("#import_emails").slideDown("fast");
  } else {
    $("#import_emails").slideUp("fast");
  }
}


function toggleListManagementButtons(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    $("#add_new_contacts").removeAttr("disabled");
    $("#clear_search_contacts").removeAttr("disabled");
    $("#search_contacts").removeAttr("disabled");
    $("button.view-subscribers-btn").removeAttr("disabled");
    $("#btn_delete_list").removeAttr("disabled");

    $("#export_subscribers_btn").show();
  } else {
    $("#clear_search_contacts").attr("disabled", "disabled");
    $("#add_new_contacts").attr("disabled", "disabled");
    $("#search_contacts").attr("disabled", "disabled");
    $("button.view-subscribers-btn").attr("disabled", "disabled");
    $("#btn_delete_list").attr("disabled", "disabled");

    $("#export_subscribers_btn").hide();
  }
}

function updateListServiceData(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    getNode("lists");
    //setTimeout(function(){
      $("#list_domain").val( list[0]["domain"] );
      $("#list_sender_name").val( list[0]["sender_name"] );
      $("#list_sender_email").val( list[0]["sender_email"] );
      $("#list_service_data").slideDown("fast");
    //}, 200);
  } else {
    $("#list_service_data").slideUp("fast");
  }
}

function cloneBroadcast(bc_id, list_id){
  var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Broadcasts', message: 'Cloning broadcast...', type: 'info'};

  Klickrr.Helpers.notify(notify_args);

  // Copy the broadcast into the Payload.
  for (var i = lists.length - 1; i >= 0; i--) {
    if( lists[i].id == list_id ){
      var curr_bc = $.grep(lists[i].broadcasts, function(arr){ return arr.id == bc_id });
      curr_bc[0].id = last_bc_id + 1;
      last_bc_id++;

      lists[i].broadcasts.push( curr_bc[0] );
    }
  }

  var clone_obj = {bc_id: bc_id};

  $.ajax({
    url: "broadcast/clone",
    type: "POST",
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    },
    dataType: "json",
    async: true,
    contentType: "application/json",
    data: JSON.stringify(clone_obj),
    success: function(data){
      var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Broadcasts', message: 'Clone successful.', type: 'success'};

      $('#broadcast_page').trigger("page", [Klickrr.broadcastPagination.page]);
      Klickrr.Helpers.notify(notify_args);
    },
    error: function(data){
      var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Unable to clone the broadcast.', type: 'danger'};

      Klickrr.Helpers.notify(notify_args);
    }
  });
  // updateListContent();
  // updateUI();
}

function cloneFollowup(fu_id){
  var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Followups', message: 'Cloning followup...', type: 'info'};

  Klickrr.Helpers.notify(notify_args);

  selected_followup = fu_id;

  var clone_obj = {fu_id: fu_id};

  $.ajax({
    url: "followup/clone",
    type: "POST",
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    },
    dataType: "json",
    async: true,
    contentType: "application/json",
    data: JSON.stringify(clone_obj),
    success: function(data){
      var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Followups', message: 'Clone successful.', type: 'success'};

      Klickrr.Helpers.notify(notify_args);
    },
    error: function(data){
      $("#loader_text").text("Unable to clone this follow up.");

      setTimeout(function(){
        $("#loader").slideUp("fast");
      }, 2000);

      var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Followups', message: 'Clone successful.', type: 'info'};

      Klickrr.Helpers.notify(notify_args);
    }
  });

  updateFollowupUi();
}

function loadFollowupDialogInit(){
  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    var bcs = [];
    bcs.push("<option>Select a message to load...</option>");

    var list = getNode("lists");

    if ( typeof list[0] != "undefined" && list[0].broadcasts.length > 0 ) {
      $.each(list[0].broadcasts, function(key, val){
        bcs.push('<option value="' + val.id + '">' + val.title + '</option>');
      });

      $("#followup_load_broadcasts").html( bcs );
      $("#followup_load_message").html("<option>No broadcast selected...</option>");

      $("#followup_load_message").html("Select a broadcast...");

    };
  };
}

//********************************
// Segmentation dialog builder.
//********************************
function showListSegmentationDialog(that, id, type){
  var list_id;

  // Make the call work regardless of where it is coming from.
  if ( typeof $(that).attr("data-id") !== "undefined" ) {
      list_id = $(that).attr("data-id");
  } else {
      list_id = selected_list;
  }

  if(!Klickrr.Broadcasts.SegmentsCollection || Klickrr.Broadcasts.current) {
    $("#list_segments_modal_body").load("list/segments/" + list_id + "?message_id=" + id + "&message_type=" + type);
  }
}
//********************************
// End segmentation dialog builder.
//********************************

//*****************************************
// Save segments built with dialog builder.
//*****************************************
function saveSegments(item_id ,type){
    var firstItem = $('#segment_option_tmpl');
    var firstValue = firstItem.find(".segment-val").val();

    if (firstValue && firstValue != "") {
      var rules_arr = [{
        list_id: selected_list,
        field: firstItem.find(".segment-list-sel").val(),
        operator: firstItem.find(".segment-operator-sel").val(),
        val: firstValue,
        message_id: item_id,
        message_type: type,
        id: firstItem.find('.segment-item').attr('data-id'),
      }];
    } else {
      rules_arr = []
    }

    // Iterate the added rules.
    $.each($(".form-inline", "#segment_collection"), function(key, rule_row){
        var rule_obj = {};

        if ( $(".segment-val", rule_row).val() !== "" )  {
            rule_obj["list_id"] = selected_list;
            rule_obj["field"] = $(".segment-list-sel", rule_row).val();
            rule_obj["operator"] = $(".segment-operator-sel", rule_row).val();
            rule_obj["val"] = $(".segment-val", rule_row).val();
            rule_obj["message_id"] = item_id;
            rule_obj["message_type"] = type;
            rule_obj["id"] = $(rule_row).parent().attr('data-id');
            rules_arr.push(rule_obj);
        }
    });

    // if (item_id == null){
      Klickrr.Broadcasts.SegmentsCollection = rules_arr;
      $('#list_segments_modal').modal('hide');
    // } else {

      if ( rules_arr.length > 0 ) {
          var payload = { segment_rules: rules_arr };

          // Post the segment changes.
          $.ajax({
              url: "list/segments/save",
              type: "POST",
              dataType: 'json',
              contentType: 'application/json',
              data: JSON.stringify(payload),
              async: true,
              success: function(data){
                if (data.success) {
                  $('#list_segments_modal').modal('hide');
                  var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Segments was updated', type: 'success'};

                  Klickrr.Helpers.notify(notify_args);

                    // Update the hasSegments flag on the broadcast, and show/hide the checkmark.
                    bc = $.grep(list[0].broadcasts, function(b){ return b.id == selected_broadcast });
                    bc[0].hasSegments = true;
                    $(".bc-has-segments-icon").show();

                } else {
                  var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: "Segments wasn't updated", type: 'danger'};

                  Klickrr.Helpers.notify(notify_args);
                }

              },
              error: function(data){
                var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: "Segments wasn't updated", type: 'danger'};

                Klickrr.Helpers.notify(notify_args);

              }
          });
      } else {
          var payload = { broadcast_id: selected_broadcast, list_id: selected_list };

          $.ajax({
              url: "broadcast/segments/delete",
              type: "DELETE",
              data: payload,
              async: true,
              success: function(data){
                  if ( data.success ) {
                      // Update the hasSegments flag on the broadcast, and show/hide the checkmark.
                      bc = $.grep(list[0].broadcasts, function(b){ return b.id == selected_broadcast });
                      bc[0].hasSegments = false;
                      $(".bc-has-segments-icon").hide();
                  } else {
                      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: "Segments could not be removed.", type: 'danger'};

                      Klickrr.Helpers.notify(notify_args);
                  }
              },
              error: function(data){
                  var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: "Segments could not be removed. No broadcast provided.", type: 'danger'};

                  Klickrr.Helpers.notify(notify_args);
              }
          });
      }
    // }
}
//*****************************************
// Save segments built with dialog builder.
//*****************************************

function analyticsListsChange(){
  //if( $("#sel_analytics_lists").val() != "Select a contact list..." ) {
  //  selected_list = $("#sel_analytics_lists option:selected").val();
  //};

  if ( selected_list != null && selected_list != "undefined" && selected_list != "Select a contact list..." ) {
    var selected = getNode("lists");
    var broadcasts = [];
    var broadcast_options = [];

    broadcast_options.push("<option>No broadcast selected...</option>");
    getBroadcasts().then(function(data){
      if (data.broadcasts && data.broadcasts.length > 0 ) {
        $.each(data.broadcasts, function(key, val){
          broadcasts.push('<a href="#" class="list-group-item" id="analytics_broadcast_' + val.id + '" onClick="analytics_data_update(\'broadcast\', ' + val.id + ');"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary"><i class="fa fa-expand"></i></button><button type="button" class="btn btn-primary"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + val.body + '</p></a>');
          broadcast_options.push('<option value="' + val.id + '">' + val.title + '</option>');
        });
      };
      $("#analytics_bc_lists").html( broadcasts );
      $("#analytics_followups_sel").html( broadcast_options );

      $(".list-group-item > .list-group-item-text").succinct({ size: 220 });
    })
  }   
}

$.fn.digits = function(){
    return this.each(function(){
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
    })
}


jQuery(function ($) {
  // INITIALIZERS.
  uid = $("#uid").val();
  // Watch data for changes.
  setInterval(function(){ checkDirty() }, 150);
    // END watch data.

    // Update Analytics on Dashboard and Analytics tab.
    // setInterval(function(){
    //     analytics_data_update("broadcast", selected_broadcast);
    // }, 15000);

  setTimeout(function(){
    if (lists.length == 0) {
      $(window).load(function(){
        $('#mod_no_list').modal('show');
      });
    };
  }, 300);

  // Get scope and trigger data for followup builder.
  $(document).ready(function(){
    loadScopedTriggers();
  })

  // End scope and trigger data.

  //$("#analytics_temp").backstretch("/assets/analytics_placeholder_bg.jpg");
  $("#webform_temp").backstretch("/assets/formbuilder_bg.jpg");

  $('[data-toggle="popover"]').popover();

  $('#broadcast_message').ckeditor({
    toolbarGroups: [
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'colors' },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'others' },
      { name: 'styles' },
      { name: 'links' },
        { name: 'tools' },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
      { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
      // { name: 'insert' },
    ]
  });


  //$('#followup_message').ckeditor({
  //  toolbarGroups: [
  //    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
  //    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
  //    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
  //    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
  //    { name: 'links' },
  //    { name: 'insert' },
  //    { name: 'styles' },
  //    { name: 'colors' },
  //    { name: 'tools' },
  //    { name: 'others' }
  //  ]
  //});

  $('#mod_bc_split_test').on('show.bs.modal', function () {
      summernote_config = {
      height: 200,
      toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ]
    };
    // $('#test_bc_message_a').summernote(summernote_config);
    $('#test_bc_message_b').summernote(summernote_config);
    // $.getJSON("list/" + selected_list + "/check_test_broadcast", function(data){
    //   $.each(data, function(key, value) {
    //     if (value !== null) {
    //       $("#test_bc_subj_line_" + key).val(value.subject);
    //       $("#test_bc_message_" + key).next('.note-editor').find('.note-editable.panel-body').html(value.content);
    //     }
    //   });
    // });
  });

  // $('#bc_message_a').ckeditor({
  //   toolbarGroups: [
  //     { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
  //     { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
  //     { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
  //     { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
  //     { name: 'links' },
  //     { name: 'insert' },
  //     { name: 'styles' },
  //     { name: 'colors' },
  //     { name: 'tools' },
  //     { name: 'others' }
  //   ]
  // });

  // $('#bc_message_b').ckeditor({
  //   toolbarGroups: [
  //     { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
  //     { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
  //     { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
  //     { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
  //     { name: 'links' },
  //     { name: 'insert' },
  //     { name: 'styles' },
  //     { name: 'colors' },
  //     { name: 'tools' },
  //     { name: 'others' }
  //   ]
  // });
  //$(".sel-lists").html( li_lists(lists) );
  broadcastServersCheck();

  var curr_date = moment()
  var one_month_from_now = moment().add(1, "month")
  var tomorrow = moment().add(1, "day").set('minute', 0).set('hour', 12);

  $("#broadcast_schedule_datetime").datetimepicker({
    pickDate: true,                 //en/disables the date picker
    pickTime: true,                 //en/disables the time picker
    useMinutes: false,               //en/disables the minutes picker
    useSeconds: false,               //en/disables the seconds picker
    useCurrent: false,               //when true, picker will set the value to the current date/time
    defaultDate: tomorrow,
    minDate: curr_date,               //set a minimum date
    maxDate: one_month_from_now,                      //set a maximum date (defaults to today +100 years)
    showToday: true,                 //shows the today indicator
    language:'en',                  //sets language locale
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down"
    },
    useStrict: false,               //use "strict" when validating dates
    sideBySide: true
  });

  //====================================================================
  //== Init the comments widget.
  //====================================================================
  //set up some minimal options for the feedback_me plugin
    fm_options = {
        position: "right-top",
        bootstrap: true,
        show_email: true,
        name_required: false,
        email_required: false,
        email_label: "Email",
        name_placeholder: "Your Name",
        email_placeholder: "MyName@example.com",
        message_placeholder: "We want Klickrr to be *THE* goto product for email marketing. If you have found a bug, have comments, or a great idea, we need to hear it!",
        feedback_url: "feedback/save",
        custom_params: {
            csrf: "my_secret_token",
            user_id: "john_doe",
            feedback_type: "bootstrap"
        },
        delayed_options: {
            success_color: "#5cb85c",
            fail_color: "#d2322d",
            delay_success_milliseconds: 3500,
            send_success : "Thanks for helping make Klickrr awesome!"
        }
    };

    //init feedback_me plugin
    fm.init(fm_options);
  //====================================================================
  //== End init for comments widget.
  //====================================================================
  // updateListContent();
  lists = gon.lists;
  updateListsSelects();

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Set up the data for each service on the 'Services' tab.
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  $.getJSON('json/service_conf', function(data){
    // Map the service_name and usage.
    service_conf = $.map(data, function(val){ return val });
    $.each(data, function(k, conf){
      // If the service is active, check the active checkbox on 'Services' tab.
      if (conf.active && conf.used) {
        $("#check_" + conf.service_name).prop("checked", true);
        toggleService( conf.service_name );
      } else {
        $("#check_" + conf.service_name).prop("checked", false);
      };
      var usage = JSON.parse(conf.usage);
      $.each(usage, function(key,val){
        // Iterate through the service fields and populate data if present.
        // Finding fields on 'Services' with #id.classname format.

        // If it's a select, handle it differently...
        if ( $("#" + key + "_" + conf.id).is("select") ) {
          $("#" + key + "_" + conf.id).val( val );
        } else {

          // ...likewise if it is a checkbox.
          if ( $("#" + key + "_" + conf.id + "." + conf.service_name).hasClass("checkbox") ) {
            if (val === "true") {
                $("#" + key + "_" + conf.id + "." + conf.service_name).attr( "checked", "checked" );
            }
          } else {
            $("#" + key + "_" + conf.id + "." + conf.service_name).val( val );
          }
        }
      });
    });
  });

  // END INITIALIZERS...
});
