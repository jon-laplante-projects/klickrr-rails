$ ->
  BROADCAST_STATUS_URL = '/broadcasts/status'
  UPDATE_INTERVAL      = 5000
  TOP_MESSAGE          = 'Sending broadcast to list...'

  class BroadcastStatusUpdater
    start: (id)->
      @id = id
      @interval = setInterval(
        => @update()
        UPDATE_INTERVAL)

      @showStatusMessage()

    stop: ->
      stopUpdating()

    showStatusMessage: ->
      notify_args = { icon: 'fa fa-paper-plane', title: 'Klickrr :: Broadcasts', message: TOP_MESSAGE, type: 'info', delay: 0 }
      @notification = Klickrr.Helpers.notify(notify_args)

    update: ->
      $.getJSON "#{BROADCAST_STATUS_URL}/#{@id}", (data) =>
        @updateInformation(data)

    updateInformation: (data) ->
      @isFinished  = data.state == 'completed'
      @countOfSent = data.count

      return @onFinished() if @isFinished

      @updateCount()

    updateCount: ->
      @notification.update('message', "#{TOP_MESSAGE}<br>#{@countOfSent} emails has been sent.")

    onFinished: ->
      @stopUpdating()
      @showSuccess()
      @hideStatusMessage()

    showSuccess: ->
      @notification.update 'icon',    'fa fa-paper-plane'
      @notification.update 'message', 'Success! Broadcast is being sent to list.'
      @notification.update 'type',    'success'

    stopUpdating: ->
      clearInterval(@interval)

    hideStatusMessage: ->
      setTimeout(
        => @notification.close()
        2000
      )

  window.broadcastStatusUpdater = new BroadcastStatusUpdater

  $('input.switch').bootstrapSwitch
    onText: 'Past selection'
    offText: 'Common selection'
    size: 'normal'