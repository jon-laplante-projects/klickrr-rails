var Klickrr = Klickrr || {};

Klickrr.Analytics = {
  selected_action: "broadcast",
  selected_action_id: null
};

function analytics_dashboard_update(){

};

function analytics_tab_update(){

};

function analytics_data_update(parent_type, id) {
    selected_broadcast = id;

    if (selected_list != null) {
        list_id = selected_list;
    } else {
        list_id = null;
    }

    if (parent_type == "broadcast") {
        selected_broadcast = id;
        // var analytics = getNode("broadcasts");
        var payload = { list_id: list_id };
        var total_leads, new_leads, unsubscribes, opens, clicks;

        $.ajax({
            url: "dashboard/list/analytics",
            type: "POST",
            data: payload,
            async: true,
            success: function(data){
                var payld = { broadcast_id: selected_broadcast };

                $.ajax({
                    url: "analytics/broadcast/count",
                    type: "POST",
                    data: payld,
                    async: true,
                    success: function(d){
                        if (d["success"] === true) {
                            $("#analytics_sends").html( Klickrr.Helpers.commaSeparateNumber(d["sent"]) );

                            // Update the analytics tab.
                            $("#analytics_opens").html( Klickrr.Helpers.commaSeparateNumber(d["opened"]) );
                            $("#analytics_clicks").html( Klickrr.Helpers.commaSeparateNumber(d["clicks"]) );
                            if ( d["opened"] != null && d["clicks"] != null && d["opened"] != 0) {
                                $("#analytics_ctr").html( String(Math.round(parseFloat(parseInt(d["clicks"])/parseInt(d["opened"] ) ) * 100)) + "%" );
                            } else {
                                $("#analytics_ctr").html("<i class='fa fa-minus'></i>");
                            }

                            $("#delivered_cell").html(d["delivered"]);
                            $("#bounced_cell").html(d["bounced"]);
                            $("#dropped_cell").html(d["dropped"]);
                            $("#unsubscribed_cell").html(d["unsubscribed"]);
                            $("#spam_cell").html(d["spam"]);

                        } else {
                            console.log("Cannot connect to get sent count.");
                        }

                    },
                    error: function(d){
                        console.log("Could not retrieve sent count.");
                    }
                });

                // Dashboard analytic ranges.
                var ranges = ["today", "7days", "30days", "90days", "year", "alltime"];

                $.each(ranges, function(key, val){
                    // Update the dashboard analytics.
                    total_leads = data["stats"]["total_leads"];
                    $("." + val + "-total-leads").html( data["stats"]["total_leads"] );
                    new_leads = data["stats"][val]["new_leads"];
                    $("." + val + "-new-leads").html( data["stats"][val]["new_leads"] );
                    unsubscribes = data["stats"][val]["unsubscribes"];
                    $("." + val + "-unsubscribes").html( data["stats"][val]["unsubscribes"] );
                    opens = data["stats"][val]["opens"];
                    $("." + val + "-opens").html( data["stats"][val]["opens"] );
                    clicks = data["stats"][val]["clicks"];
                    $("." + val + "-clicks").html( data["stats"][val]["clicks"] );
                });
            },
            error: function(data){

            }
        });
    } else {
        selected_broadcast = $("#analytics_followups_sel").val();
        selected_followup = id;

        var analytics = getNode("followups");
    }
}

function analytics_bc_fu_swap(clicked_btn){

  if ( $("#analytics_broadcasts").hasClass("sect_show") ) {
      if ( clicked_btn == "analytics_bc_btn" ) {
          // Turn on broadcasts active state.
          Klickrr.Analytics.selected_action = "broadcast";
          $("#analytics_broadcasts").removeClass("sect_show");
          $("#analytics_followups").addClass("sect_show");
          $("#analytics_followups").hide();
          $("#analytics_webforms").hide();
          $("#analytics_broadcasts").show();

          // Turn off followups and webforms active state.
          $("#analytics_fu_btn").removeClass("btn-info");
          $("#analytics_wf_btn").removeClass("btn-info");
          $("#analytics_bc_btn").addClass("btn-info");
          $("#analytics_fu_btn>i").removeClass("fa-circle");
          $("#analytics_wf_btn>i").removeClass("fa-circle");
          $("#analytics_bc_btn>i").removeClass("fa-circle-o");
          $("#analytics_bc_btn>i").addClass("fa-circle");
          $("#analytics_fu_btn>i").addClass("fa-circle-o");
          $("#analytics_wf_btn>i").addClass("fa-circle-o");
      }
  } else if ( clicked_btn == "analytics_wf_btn" ) {
      $("#analytics_followups").removeClass("sect_show");
      $("#analytics_followups").removeClass("sect_show");
      $("#analytics_broadcasts").addClass("sect_show");
      $("#analytics_broadcasts").hide();
      $("#analytics_followups").show();
      $("#analytics_followups").show();
      $("#analytics_wf_btn").removeClass("btn-info");
      $("#analytics_bc_btn").addClass("btn-info");
      $("#analytics_fu_btn").addClass("btn-info");
      $("#analytics_wf_btn>i").removeClass("fa-circle");
      $("#analytics_fu_btn>i").removeClass("fa-circle-o");
      $("#analytics_bc_btn>i").removeClass("fa-circle-o");
      $("#analytics_bc_btn>i").addClass("fa-circle");
      $("#analytics_fu_btn>i").addClass("fa-circle");
      $("#analytics_wf_btn>i").addClass("fa-circle-o");
  } else {
    if ( clicked_btn == "analytics_fu_btn" ) {
        Klickrr.Analytics.selected_action = "followup";
        $("#analytics_followups").removeClass("sect_show");
        $("#analytics_broadcasts").addClass("sect_show");
        $("#analytics_broadcasts").hide();
        $("#analytics_followups").show();
        $("#analytics_bc_btn").removeClass("btn-info");
        $("#analytics_fu_btn").addClass("btn-info");
        $("#analytics_bc_btn>i").removeClass("fa-circle");
        $("#analytics_fu_btn>i").removeClass("fa-circle-o");
        $("#analytics_fu_btn>i").addClass("fa-circle");
        $("#analytics_bc_btn>i").addClass("fa-circle-o");
    }
    //$("analytics_fu_btn").addClass();
  };
}

// Update the analytics squares, based on selected bc or fu.
function showAnalyticsDetails(action_name, obj, broadcasts){
  var payload = { action_name: action_name, action_id: obj.id };
  $('#tracking_pixel').attr("data-action-name", action_name);
  $('#tracking_pixel').attr("data-action-id", obj.id);
  $("#tracking_pix_code_div").text('');
  if (obj.testType != null){
    var neighbor_broadcast = $.grep(broadcasts, function(broadcast){ return broadcast.id == obj.testNeighborId })[0];
    $('#test_pasrts').show();
    $('#activate_test_' + obj.testType).attr('disabled', true);
    var other_type = $.grep(['a', 'b'], function(type){return type != obj.testType })[0];
    $('#activate_test_' + other_type).removeAttr('disabled');
    $('#activate_test_' + other_type).click(function(){
      showAnalyticsDetails('broadcast', neighbor_broadcast, broadcasts)
    })
  } else {
    $('#test_pasrts').hide();
  }

  $.ajax({
    url: "analytics/data",
    type: "POST",
    data: payload,
    success: function(data){
      $("#analitics_subject_line").html('<h4>Subject Line: ' + obj.title + '</h4>');
      $("#geo_analitics_render").html(data);
    },
    error: function(data){
      alert("FAIL!");
    }
  });
}

// Pop a modal detailing a broadcasts analytics.
function expandBroadcast(broadcast_id){
  var payload = { action_name: "broadcast", action_id: broadcast_id };

  $.ajax({
    url: "analytics/data",
    type: "POST",
    data: payload,
    success: function(data){
      console.log("analyticsData success!");

      $("#analytics_modal_sent").text(data.sends);
      $("#analytics_modal_opened").text(data.opens);
      $("#analytics_modal_clicked").text(data.clicks);
      $("#analytics_modal_ctr").text(data.ctr);
      $("#analytics_modal_dropped").text(data.drops);
      $("#analytics_modal_delivered").text(data.delivers);
      $("#analytics_modal_bounced").text(data.bounces);
      $("#analytics_modal_unsubscribed").text(data.unsubscribes);
      $("#analytics_modal_spam").text(data.spam);
      $("#analytics_modal_blocked").text(data.blocks);

      $("#broadcast_stats_modal").modal("show");
    },
    error: function(data){
      console.log("analyticsData failure");

    }
  });
}

// On the analytics tab, when followups are selected, change the followups list based on chosen type.
$(".analytics-type-sel").change(function(){
  if ( isNaN( parseInt($(this).val()) ) === false && $("[name='followup_analytics']").val("broadcast") ) {
    $("#analytics_fu_lists").show();
    console.log( parseInt($(this).val()) );
  } else {
    $("#analytics_fu_lists").hide();
  }
});

jQuery(function ($) {
	  // Analytics button click events.
    $("#analytics_bc_btn").click(function(){
      analytics_bc_fu_swap("analytics_bc_btn");
    });

    $("#analytics_fu_btn").click(function(){
      analytics_bc_fu_swap("analytics_fu_btn");
    });

    $("#analytics_wf_btn").click(function(){
        analytics_bc_fu_swap("analytics_wf_btn");
    });

    $("#sel_analytics_lists").change(function(){
      selected_list = $("#sel_analytics_lists option:selected").val();

      analyticsListsChange();

      updateUI();
    });

  $("#analytics_followups_sel").change(function(){
    if( $("#analytics_followups_sel").val() != "No broadcast selected..." ){
      var followups = [];
      selected_broadcast = $("#analytics_followups_sel").val();
      var broadcasts = getNode("broadcasts");

      $.each(broadcasts[0].followups, function(key, val){
        followups.push('<a href="#" class="list-group-item followup_sortable" id="sched_followup_' + val.id + '" onClick="analytics_data_update(\'followup\', ' + val.id + ');"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></button><button type="button" class="btn btn-primary" id="copy_sched_followup_' + val.id + '"><i class="fa fa-files-o"></i></button></div><p class="list-group-item-text">' + val.body + '</p></a>');
      });

      $("#analytics_fu_lists").html( followups );

      $(".list-group-item > .list-group-item-text").succinct({ size: 220 });
    }
  });

  // Update the broadcast/followup list group, based on the order set in the analytics select.
  $("#analytics_sort_sel").change(function(){

    if (Klickrr.Analytics.selected_action === "broadcast") {
      updateListsBroadcasts( $(this).val() );
    } else {

    }
  });

    //
  $("#tracking_pixel_btn").click(function(){
    var payload = { 
      action_name: $('#tracking_pixel').attr("data-action-name"),
      action_id: $('#tracking_pixel').attr("data-action-id")
    };

    $.ajax({
      url: "analytics/get_pixel",
      type: "POST",
      data: payload,
      success: function(data){
        var code = '<img src="' + data.url + '" alt="" />';
        $("#tracking_pix_code_div").text( code );

        $("#tracking_pix_code_div").slideDown("fast");
      },
      error: function(data){
        alert("FAIL!");
      }
    });
  });

  $('body').on('click', ".analytics-data-view-btn", function(){
    $.each($(".analytics-data-view-btn"), function(key, val){
        if ($(val).hasClass("active")) {
            $(val).removeClass("active");
            // $(val).attr("disabled", "disabled");
        } else {
            $(val).addClass("active");
            // $(val).attr("disabled", false);
        }
    });

    $(".analytics-details-section").toggle();
    window.dispatchEvent(new Event('resize'));
  });
// End Analytics button events.
});