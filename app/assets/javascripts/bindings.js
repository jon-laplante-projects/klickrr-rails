$(function(){
	// listFields contains ONLY the current fields.
	listFields = new Array();
	// Contact list model.
	function ContactList(listData) {
		var self = this;

		self.id = ko.observable(listData.id)
		self.title = ko.observable(listData.title);
		self.subCount = ko.observable(listData.subCount);
		self.fields = ko.observable(listData.fields);

		fields = new Array();
		fields.push(listData.id);
		fields.push(listData.subCount);
		fields.push(listData.title);
		$.each(listData.fields, function(idx, el){
			fields.push(el.name);
			//fields.push("<option>" + el.name + "</option>");
		});
		
		listFields.push(fields);
		console.log("listFields:");
		console.log(JSON.stringify(listFields));
	};

	function fieldsList(listData) {
	    var self = this;
	    self.text = listData.text;
	    self.childOptions = listData.childOptions;
	}

	// Broadcasts model.
	function Broadcast(broadcastData) {
		var self = this;


	};

	//var lists = ko.mapping.fromJS(data);

	// List binding.
	var contactListBindings = function(listItems) {
		var self = this;

		self.listToAdd = ko.observable('');
		self.lists = ko.observableArray(listItems);

		self.addList = function(){
			if ( self.listToAdd() != "" ) {
				var last_id = parseInt(self.lists().length);
				self.lists.push( new ContactList({id: last_id, title: self.listToAdd(), subCount: 0, fields: [{name: "Name"}, {name: "Email"}]}) );
				self.listToAdd("");
			};
		};

		self.removeList = function(){
			var listsArr = self.lists();
			for (var i = listsArr.length - 1; i >= 0; i--) {
				if ( listsArr[i].id == $("#sel_lists_selection option:selected").val() ) {
					listsArr.splice( listsArr.indexOf(listsArr[i]), 1 );
					
					self.lists.valueHasMutated();
					break;
				};
			};
		};
	};

	// Server binding.
	var serverBindings = function(serverItems) {
		var self = this;
		//servers = ko.observableArray(servers);

		//this.serverString = servers().join(",");
	};

	// Broadcast binding.
	var broadcastBindings = function(broadcastItems) {
		var self = this;

		self.broadcasts = ko.observableArray(broadcastItems);
	};

	var viewModel = {
		contactLists: new contactListBindings([new ContactList({id: 0, title: "This Is List #1", subCount: 4321, fields: [{name: "First name"}, {name: "Last name"}, {name: "Email"}]}), new ContactList({id: 1, title: "List #2", subCount: 9876, fields: [{name: "Name"}, {name: "Email"}]}), new ContactList({id: 2, title: "jList #3", subCount: 1234, fields: [{name: "FIRSTNAME"}, {name: "LASTNAME"}, {name: "EMAIL"}, {name: "PHONE"}]}) ]),
		selectedList: ko.observable(),
		selectedField: ko.observable(),
		//contactLists: new contactListBindings([new ContactList({id: 0, title: "This Is List #1", subCount: 4321, fields: {"First name", "Last name", "Email"}}), new ContactList({id: 1, title: "List #2", subCount: 9876, fields: {"Name", "Email"}}), new ContactList({id: 2, title: "jList #3", subCount: 1234, fields: {"FIRSTNAME", "LASTNAME", "EMAIL", "PHONE"}}) ]),
		//contactLists: new contactListBindings(["This Is List #1", "List #2", "jList #3"]),
		broadcastLists: new broadcastBindings(["jBroadcast #1", "Broadcast #2", "Broadcast #3", "Broadcast #4"])
	};

	viewModel.subCount = ko.computed(function(){
		return viewModel.selectedList() ? viewModel.selectedList().subCount : null;
	});

	ko.applyBindings(viewModel);
});