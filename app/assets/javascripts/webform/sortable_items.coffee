$ ->
  Klickrr.Webform.SortableItems = {}

  Klickrr.Webform.SortableItems.init = (init_events = true) ->
    elements = $('#webform_fields_container').find('.sortable')
    Klickrr.Webform.SortableItems.count = elements.length

    $.each elements, (index, element) ->
      $(element).attr('data-sortable-position', index)
      Klickrr.Webform.SortableItems.toggleArrows(element)

    if init_events
      Klickrr.Webform.SortableItems.initEvents()

  Klickrr.Webform.SortableItems.initEvents = ->
    $('.webform-field-down').click ->
      Klickrr.Webform.SortableItems.changePosition($(this).parents('.webform-field-container').first(), 1)
    $('.webform-field-up').click ->
      Klickrr.Webform.SortableItems.changePosition($(this).parents('.webform-field-container').first(), -1)

    $('.webform_field_switch_sortable').on 'switchChange.bootstrapSwitch', (event, state) ->
      parent = $(this).parents('.webform-field-container').first()
      if state
        parent.addClass('sortable')
        parent.find('.required-and-arrows').show()
      else
        parent.removeClass('sortable')
        parent.find('.required-and-arrows').hide()
      Klickrr.Webform.SortableItems.init(false)

  Klickrr.Webform.SortableItems.getElementPosition = (jqueryElement) ->
    return parseInt(jqueryElement.attr('data-sortable-position'))

  Klickrr.Webform.SortableItems.toggleArrows = (element) ->
    currentPosition = Klickrr.Webform.SortableItems.getElementPosition($(element))
    switch
      when Klickrr.Webform.SortableItems.count == 1
        $(element).find('.webform-field-down').hide()
        $(element).find('.webform-field-up').hide()
      when currentPosition == 0
        $(element).find('.webform-field-down').show()
        $(element).find('.webform-field-up').hide()
      when currentPosition == Klickrr.Webform.SortableItems.count - 1
        $(element).find('.webform-field-down').hide()
        $(element).find('.webform-field-up').show()
      else
        $(element).find('.webform-field-down').show()
        $(element).find('.webform-field-up').show()

  Klickrr.Webform.SortableItems.moveElement = (element, diff, currentPosition) ->
    # TODO refactor
    currentPosition = Klickrr.Webform.SortableItems.getElementPosition($(element))
    movedElement = ''
    if diff > 0
      movedElement = $(element).nextAll('.sortable').first()
    else
      movedElement = $(element).prevAll('.sortable').first()
    Klickrr.Webform.SortableItems.exchangeElements(element, movedElement, diff)
    Klickrr.Webform.SortableItems.exchangeElements($($(element).attr('data-field-template-id')),
                                                   $(movedElement.attr('data-field-template-id')),
                                                   diff)
    Klickrr.Webform.SortableItems.exchangeElements($($(element).attr('data-texts-id')),
                                                   $(movedElement.attr('data-texts-id')),
                                                   diff)
    $(element).attr('data-sortable-position', currentPosition + diff)
    $(movedElement).attr('data-sortable-position', currentPosition)
    return movedElement

  Klickrr.Webform.SortableItems.exchangeElements = (element, movedElement, direction) ->
    if direction > 0
      movedElement.after(element)
    else
      movedElement.before(element)

  Klickrr.Webform.SortableItems.changePosition = (element, diff, currentPosition) ->
    currentPosition = Klickrr.Webform.SortableItems.getElementPosition($(element))
    if (currentPosition + diff) >= 0 && ((currentPosition + diff) <= Klickrr.Webform.SortableItems.count)
      movedElement = Klickrr.Webform.SortableItems.moveElement($(element), diff, currentPosition)
      Klickrr.Webform.SortableItems.toggleArrows($(element))
      Klickrr.Webform.SortableItems.toggleArrows(movedElement)
