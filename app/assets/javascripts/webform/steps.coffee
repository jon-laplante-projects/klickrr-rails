$ ->
  Klickrr.Webform.toggleUnsubscribeList = (state) ->
    if state
      $('#webform_unsubscribe_lists').show()
    else
      $('#webform_unsubscribe_lists').hide()

  Klickrr.Webform.toggleStepsView = (step_to_show) ->
    for step_id in [1..Klickrr.Webform.stepsCount]
      if step_id != step_to_show
        $('#step_' + step_id).hide()
        $('#status_step_' + step_id).addClass('btn-grey')
      else
        $('#step_' + step_id).show()
        $('#status_step_' + step_id).removeClass('btn-grey')

  Klickrr.Webform.changeStep = (to_step_id) ->
    switch
      when to_step_id == 1
        Klickrr.Webform.toggleStepsView(to_step_id)
        $('#webform_cancel_btn').show()
        $('#webform_previous_step_btn').hide()
        $('#webform_next_step_btn').show()
        $('#webform_save_btn').hide()
      when to_step_id > 0 && to_step_id < Klickrr.Webform.stepsCount
        Klickrr.Webform.toggleStepsView(to_step_id)
        $('#webform_cancel_btn').hide()
        $('#webform_previous_step_btn').show()
        $('#webform_next_step_btn').show()
        $('#webform_save_btn').hide()
      when to_step_id == Klickrr.Webform.stepsCount
        Klickrr.Webform.toggleStepsView(to_step_id)
        $('#webform_step3_html_code').hide()
        $('#webform_step3_presets').show()
        $('#webform_cancel_btn').hide()
        $('#webform_previous_step_btn').show()
        $('#webform_next_step_btn').hide()
    if to_step_id > 0 && to_step_id <= Klickrr.Webform.stepsCount
      Klickrr.Webform.currentStep = to_step_id

  Klickrr.Webform.addUnsubscribeList = ->
    unsubscribe_list = $('.unsubscribe_list_container').first().clone(true)
    number = String($('#webform_unsubscribe_lists').children().length + 1)
    unsubscribe_list.find('#sel_webform_unsubscribe_list_1').attr('id', 'sel_webform_unsubscribe_list_' + number)
    $('#webform_unsubscribe_lists').append(unsubscribe_list)

  Klickrr.Webform.addFollowUpsToUnsubscribeList = () ->
    $.ajax
      url: '/followups'
      type: 'GET'
      dataType: 'json'
      success: (result) ->
        $('#sel_webform_unsubscribe_list_1').append($('<option></option>').attr('disabled', 'disabled').text('Follow Ups:'))
        $.each result.followups, (index, followup) ->
          value = followup.id
          if followup.messageParts[0]? && followup.messageParts[0]['subject']?
            text = followup.messageParts[0]['subject']
          else
            text = 'Followup with no subject'
          $('#sel_webform_unsubscribe_list_1').append($('<option></option>').attr('value', value).text(text).addClass('followup-select-item'))

  Klickrr.Webform.toggleStyleTabs = (identifier) ->
    $('.webform_style_tabs').hide()
    $('.' + identifier).show()
    $("[id^='webform_style_tab']").addClass('btn-grey')
    $('#' + identifier).removeClass('btn-grey')

  Klickrr.Webform.initCallbacks = ->
    $('.webform-checkbox').bootstrapSwitch onText: 'Yes', offText: 'No', size: 'normal'

    $('#unsubscribe_other_lists').on 'switchChange.bootstrapSwitch', (event, state) ->
      Klickrr.Webform.toggleUnsubscribeList(state)

    $('#webform_next_step_btn').click ->
      Klickrr.Webform.changeStep(Klickrr.Webform.currentStep + 1)

    $('#webform_previous_step_btn').click ->
      Klickrr.Webform.changeStep(Klickrr.Webform.currentStep - 1)

    $('#add_unsubscribe_list').on 'click', Klickrr.Webform.addUnsubscribeList

    $("[id^='webform_style_tab']").click ->
      Klickrr.Webform.toggleStyleTabs($(this).attr('id'))

    $('#webform_save_btn').click ->
      if Klickrr.Webform.group_id != null
        Klickrr.Webform.update()
      else
        Klickrr.Webform.save()  

    $('.webform-field-label-text').on 'keyup', ->
      Klickrr.Webform.FormConstructor.handleLabelText($(this))

    $('.webfrom-field-placeholder-text').on 'keyup', ->
      Klickrr.Webform.FormConstructor.handlePlaceholderText($(this))

    $('#webform_generate_form_code').on 'click', ->
      $('#webform_step3_html_code ').show()
      $('#webform_save_btn').show()
      $('#webform_step3_presets').hide()
      $("#webform_generated_form_code").val('')
      $("#webform_generated_form_code").val(Klickrr.Webform.generateForm())

    $('#webform_copy_form_code').on 'click', ->
      Klickrr.Helpers.copyToClipboard($('#webform_generated_form_code').val())

    $('#webform_form_double_confirm_subscription').on 'switchChange.bootstrapSwitch', (event, state) ->
      if state
        $('#webform_customize_email_row').show()
      else
        $('#webform_customize_email_row').hide()

  Klickrr.Webform.initWebFormSteps = ->
    Klickrr.Webform.stepsCount = gon.webformStepsCount
    Klickrr.Webform.changeStep(1)
    Klickrr.Webform.toggleUnsubscribeList(false)
    Klickrr.Webform.addFollowUpsToUnsubscribeList()
    Klickrr.Webform.SortableItems.init()
    Klickrr.Webform.Validation.init()
    Klickrr.Webform.FormConstructor.init()
    Klickrr.Webform.toggleStyleTabs('webform_style_tab_fields')
    Klickrr.Webform.initCallbacks()

    $('.webform-field-label-text').keyup()  #to set preview...
    $('.webfrom-field-placeholder-text').keyup() #to set preview...

    $('#webform_customize_email_row').hide()

  Klickrr.Webform.initWebFormSteps()
