$ ->
  $('body').on 'click', '#new_webform', ->
    Klickrr.Webform.FormConstructor.clear()
    Klickrr.Webform.form_code = Klickrr.Helpers.randomString(12, 'aA#')

  $('body').on 'click', '.edit-webform', ->
    Klickrr.Webform.FormConstructor.clear()
    Klickrr.Webform.load($(this).attr('data-id'))

  $('body').on 'click', '.copy-html-to-clipboard', ->
    Klickrr.Webform.FormConstructor.clear()
    Klickrr.Webform.load($(this).attr('data-id'))
    Klickrr.Webform.changeStep(3)
    setTimeout ->
      $('#webform_generate_form_code').click()
    , 1000
