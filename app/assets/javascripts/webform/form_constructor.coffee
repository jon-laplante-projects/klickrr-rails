$ ->
  Klickrr.Webform.FormConstructor = {}

  Klickrr.Webform.FormConstructor.init = ->
    Klickrr.Webform.FormConstructor.initPreviewList()
    Klickrr.Webform.FormConstructor.toggleElements()
    Klickrr.Webform.FormConstructor.initStylesItemSelect()
    Klickrr.Webform.submitUrl = gon.webformSubmitUrl if !Klickrr.Webform.submitUrl?

    $("[id^='webform_field_switch']").on 'switchChange.bootstrapSwitch', (event, state) ->
      if state
        Klickrr.Webform.FormConstructor.addElement($(this).parents('.webform-field-container'))
      else
        Klickrr.Webform.FormConstructor.removeElement($(this).parents('.webform-field-container'))

    $("[id^='webform_field_required_switch']").on 'switchChange.bootstrapSwitch', (event, state) ->
      template = $($(this).parents('.webform-field-container').first().attr('data-field-template-id'))
      if state
        template.find('.webform-field').attr('required', 'required')
      else
        template.find('.webform-field').attr('required', false)

    $('#webform_styles_hex_code').on 'change', ->
      additional_selector = ' .webform-field'
      additional_selector = '' if $('#webform_styles_item_select').val() == '#webform_preview'
      $($('#webform_styles_item_select').val() + additional_selector).css $('#webform_styles_item_attribute_select').val(), $('#webform_styles_hex_code').val()

    $('#webform_width').on 'keyup', ->
      $('#webform_preview').css('min-width', $('#webform_width').val() + 'px')
    $('#webform_height').on 'keyup', ->
      $('#webform_preview').css('min-height', $('#webform_height').val() + 'px')
    $('#webform_form_adjust_automatically').on 'switchChange.bootstrapSwitch', (event, state) ->
      if state
        $('#webform_preview').css('min-width', 'auto').css('min-height', 'auto')
        $('#webform_width').attr('placeholder', '-').val('').attr('disabled','disabled')
        $('#webform_height').attr('placeholder', '-').val('').attr('disabled','disabled')
      else
        $('#webform_width').attr('placeholder', '').removeAttr('disabled')
        $('#webform_height').attr('placeholder', '').removeAttr('disabled')


  Klickrr.Webform.FormConstructor.initPreviewList = () ->
    for i in [0..$("#webform_fields_container").children().length]
      $('#form_elements').append($('<li>').attr('id', 'preview_field_position_' + i))


  Klickrr.Webform.FormConstructor.toggleElements = ->
    elements = $('#webform_fields_container').children()
    $.each elements, (index, element) ->
      $(element).attr('data-position', index)
      if $(element).find("[id^='webform_field_switch']").bootstrapSwitch('state')
        Klickrr.Webform.FormConstructor.addElement(element)
      else
        Klickrr.Webform.FormConstructor.removeElement(element)

  Klickrr.Webform.FormConstructor.addElement = (element) ->
    template = $(element).attr('data-template')
    template_id = $(element).attr('data-field-template-id').replace('#', '')
    tag_for_preview = $($('#' + template + "_tmpl").html()).attr('id', template_id )
    tag_for_preview.find('.webform-field').attr('data-' + $(element).attr('data-field-name'), true)
    $('#preview_field_position_' + $(element).attr('data-position')).append(tag_for_preview)
    Klickrr.Webform.FormConstructor.setPreviewItemTexts(element)

  Klickrr.Webform.FormConstructor.removeElement = (element) ->
    template = $(element).attr('data-template')
    $($(element).attr('data-field-template-id')).remove()

  Klickrr.Webform.FormConstructor.handleLabelText = (webformFieldLabelElement) ->
    parent = $(webformFieldLabelElement).parents('.webform_field_texts_container')
    text = $(webformFieldLabelElement).val()
    text = 'undefined' if !text?
    $(parent.attr('data-field-template-id') + ' .webform-field').first().attr('id', text.toLowerCase().replace(' ', '_')).attr('name', text.toLowerCase().replace(' ', '_'))
    switch $(parent.attr('data-field-id')).attr('data-template')
      when 'text'
        # TODO !!!
        $(parent.attr('data-field-id')).find('.webform-item-name-label').text(text)
        $(parent.attr('data-field-template-id')).find('label').first().text(text)
      when 'p_break'
        $(parent.attr('data-field-id')).find('.webform-item-name-label').text(text)
      when 'image'
        $(parent.attr('data-field-id')).find('.webform-item-name-label').text(text)
      when 'button'
        #do something
      else
        console.log 'There is no such template type to process!'


  Klickrr.Webform.FormConstructor.handlePlaceholderText = (webformFieldPlaceholderElement) ->
    parent = $(webformFieldPlaceholderElement).parents('.webform_field_texts_container')
    switch $(parent.attr('data-field-id')).attr('data-template')
      when 'text'
        $(parent.attr('data-field-template-id')).find('.webform-field').first().attr('placeholder', $(webformFieldPlaceholderElement).val())
      when 'p_break'
        $(parent.attr('data-field-template-id')).find('label').first().text($(webformFieldPlaceholderElement).val())
      when 'image'
        if Klickrr.Webform.Validation.isValideHiddenUrl($(webformFieldPlaceholderElement).val())
          $(parent.attr('data-field-template-id')).find('img').first().attr('src', $(webformFieldPlaceholderElement).val())
      when 'button'
          $(parent.attr('data-field-template-id')).find('button').first().text($(webformFieldPlaceholderElement).val())
      else
        console.log 'There is no such template type to process!'

  Klickrr.Webform.FormConstructor.setPreviewItemTexts = (element) ->
    $($(element).attr('data-texts-id')).find('.webform-field-label-text').keyup()
    $($(element).attr('data-texts-id')).find('.webfrom-field-placeholder-text').keyup()

  Klickrr.Webform.FormConstructor.initStylesItemSelect = ->
    Klickrr.Webform.FormConstructor.addStylesSelectItem('#webform_preview', 'Form')
    $.each $('.webform-field-container').not('.webform_field_container_for_image'), (_index, element) ->
      Klickrr.Webform.FormConstructor.addStylesSelectItem $(element).attr('data-field-template-id'),
                                                          $(element).find('.webform-item-name-label').text()


  Klickrr.Webform.FormConstructor.addStylesSelectItem = (value, text) ->
    $('#webform_styles_item_select').append($("<option></option>").attr("value", value).text(text))

  Klickrr.Webform.FormConstructor.loadFiled = (field) ->
    $('#webform_field_switch_' + field.cid).bootstrapSwitch('state', true)
    if field.value?
      $('#webform_field_placeholder_' + field.cid).val(field.value)
      Klickrr.Webform.FormConstructor.handlePlaceholderText($('#webform_field_placeholder_' + field.cid))
    $('#webform_field_label_text_' + field.cid).val(field.name.replace('_', ' '))
    Klickrr.Webform.FormConstructor.handleLabelText($('#webform_field_label_text_' + field.cid))
    $('#webform_field_template_' + field.cid).find('.webform-field').attr('style', field.inline_styles)
    $('#webform_field_required_switch_'+ field.cid).bootstrapSwitch('state', field.required)


  Klickrr.Webform.FormConstructor.clear = () ->
    Klickrr.Webform.group_id = null
    $('#form_elements').children().remove()
    $('[id^="webform_field_switch_"]').bootstrapSwitch('state', false)
    Klickrr.Webform.FormConstructor.initPreviewList()
    $.each $('.webfrom-field-placeholder-text'), (index, field) ->
      $(field).val($(field).attr('data-default-placeholder')).click()
    $.each $('.webform-field-label-text'), (index, field) ->
      $(field).val($(field).attr('data-default-label')).click()
    $('[id^="webform_field_switch_"]').bootstrapSwitch('state', true)
    Klickrr.Webform.addFollowUpsToUnsubscribeList()
    $('#webform_preview').attr('style', $('#webform_preview').attr('data-default-style'))
    $('#form_name').val('')
    $('#redirect_url').val('')
    $('#webform_generated_form_code').val('')
    Klickrr.Webform.changeStep(1)
