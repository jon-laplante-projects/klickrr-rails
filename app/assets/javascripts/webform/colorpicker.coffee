$ ->
  $('#webform_palette').colorpickerembed()
  $('.colorpicker-element').parent().hide()

  $('#webform_palette').on 'changeColor', ->
      $('#webform_styles_hex_code').val($('.colorpicker-element').val()).css('background-color', $('.colorpicker-element').val())
      $('#webform_styles_hex_code').change()

  $('#webform_wheel_colorpicker').wheelColorPicker(cssClass: 'wheelcolorpicker', layout: 'block')
  $('#webform_wheel_colorpicker').on 'sliderup', ->
    $('#webform_styles_hex_code').val('#' + $(this).wheelColorPicker('value')).css('background-color', '#' + $(this).wheelColorPicker('value'))
    $('#webform_styles_hex_code').change()

  $('#webform_styles_hex_code').on 'change', ->
    $('#webform_wheel_colorpicker').wheelColorPicker('value', $(this).val())
