// Ensure the Klickrr object has been instantiated.
var Klickrr = Klickrr || {};

// Define the FollowUps for the Klickrr obj.
Klickrr.Services = {
    // Placeholder for building out a services obj.
}

  function updateServices() {
    var bttns = [];
    var divs = [];
  };

function serviceClick(id) {
  var service = $("#" + id + "_btn").attr("data-service");

  $(".service-test-btn").removeClass("btn-info");
  $(".service-test-btn").removeClass("active");
  $(".service-test-btn").attr("aria-pressed", false);
  $("#" + id + "_btn").addClass("btn-info");
  $("#" + id + "_btn").addClass("active");
  $("#" + id + "_btn").attr("aria-pressed", true);
  $(".service_details").css("display", "none");
  $("#" + service).css("display", "block");
}

function toggleService(id) {
  var service = $("#check_" + id).attr("data-service");

  if ( $("#check_" + id).is(':checked') ) {
    $("#" + service + "_btn_check").removeClass("fa-square-o");
    $("#" + service + "_btn_check").addClass("fa-check-square-o");
    selected_services.push(service);
  } else {
    $("#" + service + "_btn_check").removeClass("fa-check-square-o");
    $("#" + service + "_btn_check").addClass("fa-square-o");

    $.each(selected_services, function(key, val){
      if ( val == id ) {
        selected_services.splice( key, 1);
      };
    });
  }
  //$("#txt_broadcast_servers").val( txt_lists(selected_services) );
  broadcastServersCheck();
}

//// Checks to see if the services assigned to an upcoming Broadcast are in fact active.
//function broadcastServersCheck() {
//  if ( txt_lists(selected_services) != "" && txt_lists(selected_services) != null ) {
//    if (list[0] != null && list[0]["active_services"] != null) {
//      $("#txt_broadcast_servers").val( list[0]["active_services"] );
//    } else {
//      $("#txt_broadcast_servers").val( txt_lists(selected_services) );
//    }
//
//    $("#active_services").removeClass("incomplete-action");
//    $("#active_services").addClass("completed-action");
//    $("#broadcast_send_later").removeAttr("disabled");
//    toggleSaveReadiness('broadcast');
//  } else {
//    $("#txt_broadcast_servers").val( "" );
//    $("#active_services").removeClass("completed-action");
//    $("#active_services").addClass("incomplete-action");
//    toggleSaveReadiness('broadcast');
//    $("#broadcast_send_later").attr("disabled", "disabled");
//  }
//}

function followupServersCheck() {
  if ( txt_lists(selected_services) != "" && txt_lists(selected_services) != null ) {
      if (list[0] != null && list[0]["active_services"] != null) {
          $("#txt_followup_servers").val( list[0]["active_services"] );
      } else {
          $("#txt_followup_servers").val( txt_lists(selected_services) );
      };

      $("#followup_active_services").removeClass("incomplete-action");
      $("#followup_active_services").addClass("completed-action");
  } else {
      $("#txt_followup_servers").val( "" );
      $("#followup_active_services").removeClass("completed-action");
      $("#followup_active_services").addClass("incomplete-action");
  };
}

/*function serviceSelected(sel){
  var service = $(sel).attr("data-service");

  if ( $(sel).is(':checked') ) {
    $("#" + service + "_btn_check").removeClass("fa-square-o");
    $("#" + service + "_btn_check").addClass("fa-check-square-o");
    services.push(service);
  } else {
    $("#" + service + "_btn_check").removeClass("fa-check-square-o");
    $("#" + service + "_btn_check").addClass("fa-square-o");
    services = clean_arr(servers, service);
  }

  $("#txt_broadcast_servers").val( txt_lists(selected_services) );
}*/

$(function(){
    // Initializers for form validation for services.
    //$("form.services-form").formValidation();
    //
    // When the email test field is populated for services, enable the btn.
    //$("form.services-form").on('success.field.fv', function(e, data) {
    //    $(".new-service-save-btn").attr("disabled", false);
    //});

    // When the email test field is invalid again, disable the btn.
    //$("form.services-form").on('err.field.fv', function(e, data) {
    //    $(".new-service-save-btn").attr("disabled", "disabled");
    //});

    // If the email test field is empty, disable the btn.
    //$('form.services-form > input[type="email"]').keyup(function(){
    //    if ( $(this).val() == "" ) {
    //        $(".new-service-save-btn").attr("disabled", "disabled");
    //    }
    //});
    //========================================================
    //== END form/field validation.
    //========================================================

	// Service button click events
	$(".service-btn").click(function(){
		var service = $(this).attr("data-service");

		$(".service-btn").removeClass("btn-info");
		$(this).addClass("btn-info");
		$(".server_details").css("display", "none");
		$("#" + service).css("display", "block");
	});
	// End service button events.

  $('body').on('click', ".service-test-btn", function(){
    $("#send_service_test_btn").attr("data-service", $(this).attr("data-service") );

      // Set the form identifier.
      $("#send_service_test_btn").attr("data-services-user-id", $(this).parent().attr("data-services-user-id") );

      $("input#service_test_from_email", "#mod_service_test").val( $(this).parent().find('[name="from_email"]').val() );
    $("#mod_service_test").modal("show");
  });

  $("#send_service_test_btn").click(function(){
    var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Services', message: 'Attempting to send test message...', type: 'info'};

    Klickrr.Helpers.notify(notify_args);

    var payload = {};
    var service = $("#send_service_test_btn").attr("data-service");
    var receiver = $("#service_test_email_addr").val();
    var sender = $("#service_test_from_email").val();

    var parent = $( 'form[data-services-user-id="' + $(this).attr("data-services-user-id") + '"]' );

    $.each( $("." + service, parent), function(){
    // $.each( $("#" + service + " ." + service, parent), function(){
      var id = $(this).attr("id").replace(/_[0-9]+$/g, '');
      var val;
        if ( $(this).is("input") ) {
            val = $(this).val();
        } else if ( $(this).is("select") ) {
            val = $(this).has("option:selected").val();
        }

      payload[id] = val;
    });

    payload["service"] = service;
    payload["recipient"] = receiver;
    payload["sender"] = sender;

    var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Services' };

    $.ajax({
      type: "POST",
      url: "/api/test",
      data: payload,
      success: function(result){
        if (result.success) {
          notify_args["type"] = 'success';
          notify_args["message"] = result["message"]

          Klickrr.Helpers.notify(notify_args);
        } else {
          notify_args["type"] = 'danger';
          notify_args["message"] = result["message"]

          Klickrr.Helpers.notify(notify_args);
        }

      },
      error: function(result){
        notify_args["type"] = 'danger';
        notify_args["message"] = result["message"]

        Klickrr.Helpers.notify(notify_args);
      }
    });

    $("#mod_service_test").modal("hide");
  });

  $(".required").blur(function(){
    var service = $(this).attr("data-service");

    $(".required." + service).each(function(){
      if ( $(this).val().trim() == null || typeof $(this).val() == undefined || $(this).val() == "" ) {
        //alert("Found one!");
        $("#check_" + service).prop("checked", false);
        $("#check_" + service).attr("disabled", true);
      } else {
        $("#check_" + service).attr("disabled", false);
      };
    });
  });

  // $(".test_btn").click(function(){
  //   // Init POST data object.
  //   var service_obj = {};

  //   $.each( $(".mailgun"), function(key, val){
  //     if( $( val ).attr( "id" ) != null ) {
  //       var id = $( val ).attr( "id" );
  //       var data = $( val ).val();

  //       service_obj[id] = data;
  //       console.log("Pair - " + id + ": " + data);
  //     }
  //   });

  //       service_obj["service"] = "mailgun"

  //   console.log(service_obj);

  //   $.ajax({
  //     type: "POST",
  //     url: "/api/send",
  //     data: service_obj,
  //     success: function(result){
  //       console.log("Done.");
  //     },
  //     dataType: "JSON"
  //   });

  //   alert("Sending!");
  // });

  // On/off handling for services.
  $("body").on("change", "service-chkbx", function(){
    //serviceSelected(this);
    toggleService( $(this).prop("id") );
  });

  $("body").on("click", '.add-new-service-acct', function(){
      id_modal = '#new_account_service_forms_' + $(this).attr('data-service-id')
      $(id_modal + ' .service-fields input').val('');
      $(id_modal + ' .resource-error').hide();
      $(id_modal).on('show.bs.modal', function (e) {
          $(id_modal + ' input:visible').val(null);
      });
      $(id_modal).modal('show');
  });


    $("[id^='new_account_service_forms_'] form").submit(function(event){
        event.preventDefault();
        this_modal = $(this)

        // Init POST data object.
        var service_obj = {};

        // Add User ID.
        service_obj.user_id = $("#uid").val();

        // Iterate fields in the submitted form.
        $.each( $(this).find(":input"), function(key, val){
            if( $( val ).attr( "id" ) != null ) {
                var id = $( val ).attr( "id" );
                if ( $( val ).prop("type") != "checkbox" ) {
                    var data = $( val ).val();
                } else {
                    if ( $( val ).prop("id").split("_")[0] == "check" ) {
                        id = "active";
                    };

                    var data = $( val ).is(":checked");
                };

                service_obj[id] = data;
            }
        });

        $.ajax({
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            url: "/save_service_conf",
            data: service_obj,
            success: function(result){
                if (result.success) {
                  service_div = $('#' + result.service_name);
                  last_form = service_div.find('form').last();
                  if (last_form.length == 0) {
                      service_div.find('.services-users').html(result.partial);
                  }
                  else {
                    last_form.after(result.partial);
                  }
                  var service_user = result.services_user
                  var usage = JSON.parse(result.services_user.usage);
                  $.each(usage, function(key,val){
                      $("#" + key + "_" + service_user.id + "." + result.service_name).val( val );
                  });
                  this_modal.parents('.modal.fade.in').modal('hide');
                  if (result.count_users > 0) {
                    $("#" + result.service_name + "_btn_check").removeClass("fa-square-o");
                    $("#" + result.service_name + "_btn_check").addClass("fa-check-square-o");
                    list_service_btn_setup();
                  }
                } else {
                  this_modal.find('.resource-error').show();
                  this_modal.find('.resource-error .error-message').text(result.error);
                }
            },
            dataType: "JSON"
        });

        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Services', message: 'Saving service preferences.', type: 'info'};

        Klickrr.Helpers.notify(notify_args);
    });


  // Handle the submission and addition of a msging service.
  $("[id^='services_user_']").submit(function(event){
    event.preventDefault();

    // Init POST data object.
    var service_obj = {};

    // Add User ID.
    service_obj.user_id = $("#uid").val();

    // Iterate fields in the submitted form.
    $.each( $(this).find(":input"), function(key, val){
      if( $( val ).attr( "name" ) != null ) {
        var id = $( val ).attr( "name" );
        if ( $( val ).prop("type") != "checkbox" ) {
          var data = $( val ).val();
        } else {
          if ( $( val ).prop("name").split("_")[0] == "check" ) {
            id = "active";
          };

          var data = $( val ).is(":checked");
        };

        service_obj[id] = data;
      }
    });

    console.log(service_obj);

    $.ajax({
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      url: "/save_service_conf",
      data: service_obj,
      success: function(result){
        console.log("Done.");
        if (typeof result.service_name !== 'undefined' && typeof result.services_user.id !== 'undefined') {
          setUpServiceFields(result.service_name, result.services_user.id);
        };
      },
      dataType: "JSON"
    });

    var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Services', message: 'Saving service preferences.', type: 'info'};

    Klickrr.Helpers.notify(notify_args);
  });

  function setUpServiceFields(serviceName, servicesUserId){
    $('#' + serviceName).find('#services_user_id').val(servicesUserId);
    selected_services.push(serviceName);
    setUpServiceFieldId(serviceName, 'services_user_', servicesUserId);
    setUpServiceFieldId(serviceName, 'use_services_user_', servicesUserId);
    setUpServiceFieldId(serviceName, 'delete_services_user_', servicesUserId);
  };

  function setUpServiceFieldId(serviceName, fieldId, servicesUserId){
    parent = $('#' + serviceName)
    parent.find('#' + fieldId).attr('id', fieldId + servicesUserId);
    parent.find('#' + fieldId + servicesUserId).attr('data-services-user-id', servicesUserId);
  }

  // Handle deletion of an instance of a msging service.
    $("body").on("click", '[id^="delete_services_user_"]', function(){
        id = $(this).attr('data-services-user-id')
        servicesUser = { services_user_id: id }
        $.ajax({
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            url: "/delete_service_conf",
            data: servicesUser,
            success: function(result){
                $('#services_user_' + id).remove();
                list_service_btn_setup();
                if (result.service_last){
                    $("#" + result.service_name + "_btn_check").removeClass("fa-check-square-o");
                    $("#" + result.service_name + "_btn_check").addClass("fa-square-o");
                 }
            },
            dataType: "JSON"
        });
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Services', message: 'Deleting service preferences.', type: 'info'};

        Klickrr.Helpers.notify(notify_args);
    });
  // End on/off handling for services.
});
