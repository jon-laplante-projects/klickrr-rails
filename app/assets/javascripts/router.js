Path.map("#/dashboard").to(function(){
    $("a[href='#dashboard']").tab("show");
});

Path.map("#/services").to(function(){
    $("a[href='#services']").tab("show");
});

Path.map("#/lists").to(function(){
    $("a[href='#lists_list']").tab("show");
});

Path.map("#/broadcasts").to(function(){
    $("a[href='#broadcast_list']").tab("show");
});

Path.map("#/followups").to(function(){
    $("a[href='#followup_list']").tab("show");
});

Path.map("#/webforms").to(function(){
    $("a[href='#web_form']").tab("show");
});

Path.map("#/analytics").to(function(){
    $("a[href='#analytics']").tab("show");
});

$(function(){
    Path.listen();

    $('a[href="#dashboard"]').click(function(){
        document.location.hash = "/dashboard";
    });

    $('a[href="#services"]').click(function(){
        document.location.hash = "/services";
    });

    $('a[href="#lists_list"]').click(function(){
        document.location.hash = "/lists";
    });

    $('a[href="#broadcast_list"]').click(function(){
        document.location.hash = "/broadcasts";
    }); 

    $('a[href="#followup_list"]').click(function(){
        document.location.hash = "/followups";
    });

    $('a[href="#web_form"]').click(function(){
        document.location.hash = "/webforms";
    });

    $('a[href="#analytics"]').click(function(){
        document.location.hash = "/analytics";
    });
});