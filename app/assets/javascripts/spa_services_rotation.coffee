class ServicesRotation
  constructor: ->
    @$container = jQuery('#services_rotation')
    if @$container.length > 0
      @handleSortable()
      @handleLimitChange()
      @handleServiceSettingsChange()

  handleSortable: ->
    @$container.find('.sortable').sortable
      revert: true
      connectWith: '.sortable'
      receive: (event, ui)=>
        @toggleActive(ui.item)

  updateServicesUser: ($servicesUser, data)->
    jQuery.ajax
      url: "/services_users/#{$servicesUser.data().id}"
      method: 'PUT'
      data:
        services_user: data

  toggleActive: ($servicesUser)->
    toggleIcon = ->
      $icon = $servicesUser.find('.icon-check')
      $icon.toggleClass('fa-check-square-o')
      $icon.toggleClass('fa-square-o')
    toggleIcon()
    $servicesUser.data('is-active', !$servicesUser.data().isActive)
    data =
      used: $servicesUser.data().isActive
    @updateServicesUser($servicesUser, data)

  handleLimitChange: ->
    _.each @$container.find('.services-user'), ($servicesUser)=>
      $servicesUser = jQuery($servicesUser)
      $inputLimit = $servicesUser.find('input#services_user_limit')
      $inputLimit.on 'change', =>
        limit = $inputLimit.val()
        maxLimit = $inputLimit.attr('max')
        $inputLimit.val(maxLimit) if parseInt(limit) > parseInt(maxLimit)
        @updateServicesUser $servicesUser,
          limit: $inputLimit.val()

  handleServiceSettingsChange: ->
    $servicesSettings = @$container.find('.services-settings')
    id = $servicesSettings.data().servicesSettingsId
    $servicesSettings.find('input').on 'change', ->
      $input = jQuery(@)
      val = if $input.attr('type') == 'checkbox'
        $input.prop('checked')
      else
        $input.val()
      name = $input.data().name
      jQuery.ajax
        url: '/user_services_settings'
        method: 'PUT'
        data:
          services_settings:
            "#{name}": val

jQuery ->
  new ServicesRotation()
