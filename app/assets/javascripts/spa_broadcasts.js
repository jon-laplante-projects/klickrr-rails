var Klickrr = Klickrr || {};

Klickrr.Broadcasts = {
  Segments: {
    save_locked: false,
    active_var_count: 0
  },
  SegmentsCollection: null,
  current: null,
  currentInstance: null,
  tempInstance: {}
};

function newBroadcastButtons(){
  $('#bc_activity_div *').hide();
  $('.btn-back-to-list').show();
  $('#broadcast_save_as_draft_btn').show();
  $('#send_broadcast_btn').show();
  $('.update-broadcast').hide();
  $('#queue_broadcast_btn').hide();
}

function draftBroadcastButtons(){
    setTimeout(function(){
      $('#bc_activity_div *').hide();
      $('.btn-back-to-list').show();
      $('#broadcast_save_as_draft_btn').hide();
      $('#send_broadcast_btn').hide();
      $('.update-broadcast').show();
      $('#queue_broadcast_btn').hide();
      $('#broadcast_update_draft_btn').show();
      $('#broadcast_update_draft_btn').removeAttr('disabled');
    }, 200);
}

function sentBroadcastButtons(){
  $('#bc_activity_div *').hide();
  $('.btn-back-to-list').show();
  $('#broadcast_save_as_draft_btn').show();
  $('#send_broadcast_btn').hide();
  $('.update-broadcast').hide();
  $('#queue_broadcast_btn').hide();
}

function scheduledBroadcastButtons(){
  $('#bc_activity_div *').hide();
  $('.btn-back-to-list').show();
  $('#broadcast_save_as_draft_btn').hide();
  $('#send_broadcast_btn').hide();
  $('.update-broadcast').show();
  $('#queue_broadcast_btn').show();
}

function updateBroadcastUi(){
  updateListsSelects();
  generate_bc_field_btns();
  toggleSaveReadiness('broadcast');
  broadcastServersCheck();
}

  $("#use_past_selection_switch").bootstrapSwitch();

  // Change active state when the "Active" switch is toggled.
  $("#use_past_selection_switch").on('switchChange.bootstrapSwitch', function(event, state) {
    if (state === true) {
      Klickrr.Broadcasts.use_past_selection = true;
    } else {
      Klickrr.Broadcasts.use_past_selection = false;
    }
  });

  $("#use_past_selection_switch").bootstrapSwitch('state', false);

    // Broadcast functions.
  $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

    var input = $(this).parents('.input-group').find(':text'),
    log = numFiles > 1 ? numFiles + ' files selected' : label;

    if( input.length ) {
      input.val(log);
    } else {
      if( log ) alert(log);
    }

  });

  // Title only list of broadcasts.
  function selBroadcastTitles(){
    if ( selected_list != null && selected_list != "Select a contact list..." ) {
      // list = getNode("lists");
      getBroadcasts().then(function(data){
        broadcasts = [];
        if (data.broadcasts.length > 0 ) {
          $.each(data.broadcasts, function(key, val){
            broadcasts.push('<option id="broadcast_' + val.id + '" value="' + val.id + '">' + val.title + '</option>');
          });

          $(".broadcast-title-select").html( '<option>Select a broadcast.</option>' + broadcasts );

          $(".list-group-item > .list-group-item-text").succinct({ size: 150 });
        } else {
          $(".broadcast-title-select").html( '<option>No broadcasts for this list.</option>' );
        }
      });
    }
  }

  // Init the list of broadcasts found inside the "load" modal.
  function loadBroadcastDialogList(){
    if ( selected_list != null && selected_list != "Select a contact list..." ) {
      broadcasts = [];
      getBroadcasts().then(function(data){
        if (data.broadcasts.length > 0 ) {
          $.each(list[0].broadcasts, function(key, val){
            // ******************************************
            // Unscheduled changes list-item styling if a broadcast or followup is not scheduled.
            // ******************************************
            var unscheduled;
            if ( val.send_on == null ) { unscheduled = " list-group-item-danger" } else { unscheduled = "" };
            broadcasts.push('<a href="#" class="list-group-item' + unscheduled + '" id="bc_item_' + val.id + '" onClick="loadSelectedBroadcast(' + val.id + ')"><h4 class="list-group-item-heading">' + val.title + '</h4><div class="btn-group pull-right" role="group"><button type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button></div><p class="list-group-item-text">' + Klickrr.Helpers.html2text(val.body) + '</p></a>');
          });

          $("#broadcast_load_message").html( broadcasts );

          $(".list-group-item > .list-group-item-text").succinct({ size: 150 });
        };
      })
    };
  }

  function saveBroadcast(isDraft){
    var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Saving broadcast...', type: 'info'};
    Klickrr.Helpers.notify(notify_args);

    if ($('#broadcast').attr('data-split-test')){
      sendBroadcastSplitTest();
    } else {
      var payload = getNewBroadcastValues();
      payload.list_id = selected_list;
      payload.is_draft = isDraft;
      payload.id = selected_broadcast;

      Klickrr.broadcastPagination.list_id = selected_list;

      // Push the broadcast to the server.
      $.ajax({
        url: "broadcast/create",
        //url: "https://www.klickrr.net/webform/save",
        type: "POST",
        data: JSON.stringify(payload),
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        dataType: "json",
        contentType: "application/json",
        success: function(data){
          var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Broadcast successfully saved.', type: 'success'};
          var newDraft = data;
          Klickrr.Helpers.notify(notify_args);
          if (isDraft){
            draftBroadcastButtons();
            $('#clear_editing_broadcast').show();
            Klickrr.Broadcasts.current = newDraft.id;
            selected_broadcast = newDraft.id;
            loadSelectedBroadcast(newDraft.id);
          }
        },
        error: function(){
          var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Error! Cannot save broadcast at this time.', type: 'danger'};

          Klickrr.Helpers.notify(notify_args);
        }
      });

      // Copy the broadcast into the Payload.

    };
  }

  function updateBroadcastWithoutSend(isDraft){
    var payload = getNewBroadcastValues();
    payload.list_id = selected_list;
    payload.is_draft = isDraft;
    if (!selected_broadcast) {
      selected_broadcast = Klickrr.Broadcasts.current;
    }
    Klickrr.broadcastPagination.list_id = selected_list;

    payload.id =  Klickrr.Broadcasts.currentInstance.id;

    $.ajax({
      url: "broadcast/update",
      type: "POST",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(payload),
      success: function(data){
        var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Broadcast updated.', type: 'success'};

        Klickrr.Helpers.notify(notify_args);
      },
      error: function(data){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Warning!<br/>Could not update broadcast.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      }
    });
  }

  function getBroadcasts(){
    return $.getJSON("list/" + selected_list + "/broadcasts");
  }

  function getBroadcast(broadcast_id){
    return $.getJSON('broadcast?id=' + broadcast_id);
  }

  function updateBroadcast(isDraft){
    if( $("#broadcast_subj_line").val() != null && $("#broadcast_message").val() != null ){
      //var notify_args = { icon: 'fa fa-pencil', title: 'Klickrr :: Broadcasts', message: 'Updating broadcast...', type: 'info'};

      //Klickrr.Helpers.notify(notify_args);

      var is_draft;
        if ( isDraft === true ) {
            is_draft = true;
        } else {
            is_draft = false;
        }

      var title = $("#broadcast_subj_line").val();
      var body = $("#broadcast_message").val();
      var send_on = $("#broadcast_send_on").val();
      var tz_id = $("#scheduled_broadcast_timezone option:selected").attr("timeZoneId");
      var gmt_adjust = $("#scheduled_broadcast_timezone option:selected").attr("gmtAdjustment");

      var bc_obj = {
        "list_id": selected_list,
        "bc_id": Klickrr.Broadcasts.currentInstance.id,
        "title": title,
        "body": body,
        "send_on": moment(send_on).format("YYYY-MM-DD HH:mm"),
        "timezone": tz_id,
        "gmtAdjustment": gmt_adjust,
        "segment_ids": null,
        "use_past_selection": Klickrr.Broadcasts.use_past_selection,
        "is_draft": is_draft
      }

      // Check to see if there are any segments selected.
      if ( $("input[name='seg_id']").length > 0 ) {
        var seg_ids = [];

        $.each($("input[name='seg_id']"), function(key, val){
          seg_ids.push( $(val).val() );
        });

        bc_obj["segment_ids"] = seg_ids;
      };
      //var bc_obj = {bc_id: bc[0].id, subject: title, content: body, push_at: send_on, timezone: tz_id, gmtAdjustment: gmt_adjust};

      $.ajax({
        url: "broadcast/update_and_send",
        type: "POST",
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(bc_obj),
        success: function(data){
          var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Broadcast saved.', type: 'success'};
          Klickrr.Helpers.notify(notify_args);

          $('#broadcast_page').trigger("page", [Klickrr.broadcastPagination.page]);
          Klickrr.broadcast_list_page = data.page;
          clearBroadcastUI();
          $('a[href="#broadcast_list"]').tab('show');
          window.broadcastStatusUpdater.start(data.id);


            Klickrr.Broadcasts.currentInstance.title = title;
            Klickrr.Broadcasts.currentInstance.body = body;
            Klickrr.Broadcasts.currentInstance.send_on = moment(send_on).format("MM/DD/YYYY HH:mm");
            Klickrr.Broadcasts.currentInstance.timezone = tz_id;
            Klickrr.Broadcasts.currentInstance.gmtAdjustment = gmt_adjust;

            $("#broadcast_subj_line").val("");
            $("#broadcast_message").val("");

            //$("#save_broadcast_btn").show();
            $(".save-broadcast").show();
            $("#broadcast_save_as_draft_btn").show();
            $("#queue_broadcast_btn").hide();
            $(".update-broadcast").hide();
            $("#clear_broadcast_btn").hide();
            $("#broadcast_update_draft_btn").hide();
          /////////////////////////////////////////////////////
          // What's the best way to handle this???
          /////////////////////////////////////////////////////
        },
        error: function(data){
          var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Warning!<br/>Could not save broadcast.', type: 'danger'};

          Klickrr.Helpers.notify(notify_args);
        }
      });
    }
  }

  function getNewBroadcastValues(){
    var title = $("#broadcast_subj_line").val();
    var body = $("#broadcast_message").val();
    if ($('#broadcast_schedule').css('display') == 'block') {
      if( $("#broadcast_send_on").val() !== "" ) {
        var send_on = moment($("#broadcast_send_on").val()).format("YYYY-MM-DD HH:mm");
      }
      var tz_id = $("#scheduled_broadcast_timezone option:selected").attr("timeZoneId");
      var gmt_adjust = $("#scheduled_broadcast_timezone option:selected").val();
    }
    var is_draft = $('#broadcast_is_draft').val();
    last_bc_id++;


    var bc = {
      "list_id": selected_list,
      "title": title,
      "body": body,
      "followups": [],
      "send_on": send_on,
      "timezone": tz_id,
      "gmtAdjustment": gmt_adjust,
      "segments": Klickrr.Broadcasts.SegmentsCollection,
      "use_past_selection": Klickrr.Broadcasts.use_past_selection,
      'page_size': Klickrr.broadcastPagination.page_size,
      'is_draft': is_draft
    }
    if (!$.isEmptyObject(Klickrr.Broadcasts.tempInstance)) {
      bc.services = Klickrr.Broadcasts.tempInstance.services;
    }
    // Check to see if there are any segments selected.
    if ( $("input[name='seg_id']").size() > 0 ) {
      var seg_ids = [];

      $.each($("input[name='seg_id']"), function(key, val){
        seg_ids.push( $(val).val() );
      });

      bc["segment_ids"] = seg_ids;
    };
    return bc;
  }

  function loadSelectedBroadcast(broadcast_id) {
    //selected_list = $("#broadcast_load_list").val();
    selected_broadcast = broadcast_id;
    // broadcast_data = getNode("broadcasts");
    Klickrr.Broadcasts.currentList = selected_list;
    getBroadcast(broadcast_id).then(function(broadcast_data){
      Klickrr.Broadcasts.currentInstance = broadcast_data;
      updateBroadcastUi();
      if ( broadcast_data.send_on != null ) {
        if ( broadcast_data.timezone != "" ) {
          $("#scheduled_broadcast_timezone option[timeZoneId=" + broadcast_data.timezone + "]").prop('selected', true);
        } else {
          $("#scheduled_broadcast_timezone option[timeZoneId=30]").prop('selected', true);
        };

        send_on = moment(broadcast_data.send_on, 'YYYY-MM-DD HH:mm').utc();

        var gmt = parseFloat(broadcast_data.gmtAdjustment);
        var gmt_h = Math.floor(gmt);
        var gmt_m = Math.floor(60*(gmt - gmt_h));
        var gmt_sign = gmt > 0 ? '+' : '-';
        var timezone_str = 'GMT' + gmt_sign + gmt_h;
        if(gmt_m > 0){
           timezone_str += ':' + gmt_m
        }

        $("#broadcast_send_on").val(send_on.format("MM/DD/YYYY HH:mm A"));
        $('.scheduled-for').text(send_on.format("[Scheduled for] LT [on] dddd MMMM D, YYYY"));
        $('.scheduled-for').removeClass('hidden');
        $('.schedule-info .scheduled-date').show().text(send_on.format('[Send at] h:mm A [on] M/D/YYYY ') + timezone_str);
        $('.schedule-info .close').show();

        $("#broadcast_schedule").slideDown("fast");
        $("#queue_broadcast_btn").show();
        $(".btn-lg.update-broadcast").hide();

        if(broadcast_data.sent_on == null){
          scheduledBroadcastButtons();
        }
      } else {
        $('.schedule-info .scheduled-date').hide();
        $('.schedule-info .close').hide();
        $(".update-broadcast").show();
      }

      if (!broadcast_data.isDraft){
        $("#broadcast_update_draft_btn").hide();
      }

      $("#broadcast_load").modal("hide");

      //$("#update_broadcast_btn").show();
      $("#clear_broadcast_btn").show();

      // Populate the subject and message sections with the selected data.
      $("#broadcast_subj_line").val( broadcast_data.title );
      setTimeout(function(){
        CKEDITOR.instances['broadcast_message'].setData(broadcast_data.body);
      }, 400);
    })
  }
  // End broadcast functions.

  function editBroadcast(broadcast_id) {
    Klickrr.Broadcasts.current = broadcast_id;
    $("a[href='#broadcast']").tab("show");
    loadSelectedBroadcast(broadcast_id);
  }

  function clearBroadcastUI() {
    $("#broadcast_subj_line").val("");
    $("#broadcast_message").val("");
    $("#test_bc_message_b").code('');
    $("#test_bc_subj_line_b").val('');
    $('label[for="broadcast_subj_line"]').text('Subject Line ');
    $('label[for="broadcast_message"]').text('Main message ');
    $("#scheduled_broadcast_timezone option[value='0']").prop('selected', true);
    $("#broadcast_send_on").val( moment().format("MM/DD/YYYY hh:mm A") );
    $(".save-broadcast").show();
    $("#queue_broadcast_btn").hide();
    $(".update-broadcast").hide();
    $("#clear_broadcast_btn").hide();
    $('#slit_test_ind').hide();
    $("#broadcast_schedule").slideUp("fast");
    $("#broadcast_update_draft_btn").hide();
    $("#broadcast_save_as_draft_btn").show();
    $('#clear_editing_broadcast').hide();
    newBroadcastButtons();
  }

  //========================================================
  // Create the links to add fields into broadcast.
  //========================================================
  function generate_bc_field_btns(){
    if ( selected_list != null && selected_list != "undefined" ) {
      var arr = [];
      var forSubjectArr = [];
      var forSubjectSplitArr = [];
      var forBodySplitArr = [];

      $.getJSON("list/" + selected_list + "/subscribers/columns.json", function(data){
        //arr.push('<button id="sign_up_link_field" class="btn btn-default" type="button" onClick="broadcast_insert_token(\'{{sign_up_link}}\');" style="margin: 5px;">sign_up_link</button>');

        $.each(data, function(key,val){
            if ( val != null ) {
                forSubjectArr.push('<button data-field="' + val + '" data-token="{{' + val + '}}" data-subject-line="#broadcast_subj_line" class="btn btn-default subject-insert-token" type="button" style="margin: 5px;">' + val + '</button>')
                forSubjectSplitArr.push('<button data-field="' + val + '" style="border: 1px solid #ccc;" data-token="{{' + val + '}}" data-subject-line="#test_bc_subj_line_b" class="btn btn-default subject-insert-token" type="button" style="margin: 5px;">' + val + '</button>')
                forBodySplitArr.push('<button data-field="' + val + '" style="border: 1px solid #ccc;" data-token="{{' + val + '}}" data-body="#test_bc_message_b" class="btn btn-default body-insert-token" type="button" style="margin: 5px;">' + val + '</button>')
                arr.push('<button id="' + val + '_field" class="btn btn-default" type="button" onClick="broadcast_insert_token(\'{{' + val + '}}\');" style="margin: 5px;">' + val + '</button>')
            }
        });

        $("#subject_line_broadcast_field_tokens").html(forSubjectArr);
        $("#split_subject_line_broadcast_field_tokens").html(forSubjectSplitArr);
        $("#split_body_field_tokens").html(forBodySplitArr);
        $("#broadcast_field_tokens").html(arr);
      });
    };
  }

  $(document).on('click', '.subject-insert-token', function(){
    $($(this).attr('data-subject-line')).val(insertToken($(this).attr('data-token'), $(this).attr('data-subject-line')));
  })

  $(document).on('click', '.body-insert-token', function(){
    $($(this).attr('data-body')).summernote('insertText', $(this).attr('data-token'));
  })

  function insertToken(token, id){
    var currentPos = $(id).getCursorPosition();
    var msg = $(id).val();
    var strLeft = msg.substring(0, currentPos);
    var strMiddle = token;
    if (currentPos == msg.length - 1) {
      var strRight = '';
    } else {
      var strRight = msg.substring(currentPos, msg.length);
    }
    return strLeft + strMiddle + strRight;
  }

  function broadcast_insert_token(token){
    CKEDITOR.instances['broadcast_message'].insertText(token);
  }
  //========================================================
  // END links to add fields into broadcast.
  //========================================================


$(function(){
    // Init validation.
    $(".email-test-form").formValidation();

    // When the email test field is populated for services, enable the btn.
    $(".email-test-form").on('success.field.fv', function(e, data) {
        $(".send-email-test-btn").attr("disabled", false);
    });

    // When the email test field is invalid again, disable the btn.
    $(".email-test-form").on('err.field.fv', function(e, data) {
        $(".send-email-test-btn").attr("disabled", "disabled");
    });

    // If the email test field is empty, disable the btn.
    $(".test-email-addr").keyup(function(){
        if ( $(this).val() == "" ) {
            $(".send-email-test-btn").attr("disabled", "disabled");
        }
    });
    //========================================================
    //== END form/field validation.
    //========================================================


    // Broadcast button click events.
  $("#add_list").click(function(){
    $("#function_tabs a[href='#lists']").tab("show");
  });

  $('body').on('click', '#new-broadcast-link', function(){
    $('.scheduled-for').addClass('hidden');
    Klickrr.Broadcasts.currentInstance = null;
    Klickrr.Broadcasts.current = null; 
    Klickrr.Broadcasts.tempInstance = {};
    broadcastServersCheck();
  });

  $("#broadcast_send_later").click(function(){
    $("#broadcast_schedule").slideDown("fast");
    $("#send_broadcast_btn").hide();
    $("#update_broadcast_btn").hide();
    $("#queue_broadcast_btn").show();
    $('.update-broadcast.btn').hide();
  });

  $("#broadcast_now").click(function(){
    $("#broadcast_schedule").slideUp("fast");
    $("#update_broadcast_btn").hide();
    $("#queue_broadcast_btn").hide();
    $("#send_broadcast_btn").show();

    $("#broadcast_send_on").val( moment().format("MM/DD/YYYY hh:mm A") );
  });

  $("#save_broadcast_btn").click(function(){
    var title = $("#broadcast_subj_line").val();
    var body = $("#broadcast_message").val();

    if ( title != "" && body != "" ) {
      saveBroadcast();
      clearBroadcastUI();
      // updateListContent();
      $('#broadcast_page').trigger('page', [Klickrr.broadcastPagination.page]);
      $('a[href="#broadcast_list"]').tab('show');
    } else {
      alert("You need data!");
    };
  });

  // FIRED FROM MODAL - Save the current broadcast into the queue for later sending.
  $(".confirm-queue-broadcast-btn").click(function(){
    $("#queue_broadcast_send").modal("hide");

    var title = $("#broadcast_subj_line").val();
    var body = $("#broadcast_message").val();

    if ( title != "" && body != "" ) {
      saveBroadcast();
      clearBroadcastUI();

      $('a[href="#broadcast_list"]').tab('show');
    } else {
      alert("You need data!");
    };
  });

  $("#confirm_update_broadcast_btn").click(function(){
    $("#update_broadcast_mod").modal("hide");
    updateBroadcast();
    updateBroadcastUi();
    // updateUI();
  });

  $("#update_and_send_broadcast_btn").click(function(){
    updateBroadcast(false);
    updateBroadcastUi();
    // updateUI();
  });


  $("#confirm_send_broadcast_btn").click(function(){
    // Close the modal...
    $("#broadcast_send").modal("hide");
    if ($('#broadcast').attr('data-split-test')){
      sendBroadcastSplitTest();
    } else {

        // Keep working while the send is queueing.
        $('a[href="#broadcast_list"]').tab('show');

        // Let the user know we are queuing his/her messages.
        var notify_args = { icon: 'fa fa-info', title: 'Klickrr :: Broadcasts', message: 'Queuing messages for sending.<p/>Please wait, this may take a moment.', type: 'info'};

        Klickrr.Helpers.notify(notify_args);

      var payload = getNewBroadcastValues();
      payload.list_id = selected_list;
      Klickrr.broadcastPagination.list_id = selected_list;

      $.ajax({
        url: "broadcast/send",
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        async: true,
        //beforeSend: function(xhr) {
        //  xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        //},
        data: JSON.stringify(payload),
        success: function(data){
          $('#broadcast_page').trigger("page", [Klickrr.broadcastPagination.page]);
          Klickrr.broadcast_list_page = data.page;
          // $('a[href="#broadcast_list"]').tab('show');

          clearBroadcastUI();
          window.broadcastStatusUpdater.start(data.id).complete( updateBroadcastUi() );
        },
        error: function(data){
          var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'There was a problem. Broadcast could not be sent.', type: 'danger'};

          Klickrr.Helpers.notify(notify_args);
        }
      });
    }


    //var bc_id = {id: selected_broadcast};

  });

  // When the scheduled date for a broadcast changes, allow uses to add the bc to queue.
  $("#broadcast_send_on").change(function(){
    if ( $("#broadcast_send_on").val() != null && $("#broadcast_send_on").val() != "" && moment( $("#broadcast_send_on").val() ).isValid() == true ) {

      $("#queue_broadcast_btn").prop("disabled", false);
    } else {
      $("#queue_broadcast_btn").prop("disabled", "disabled");
    };
  });

  // When the scheduled date for a broadcast changes, allow uses to add the bc to queue.
  $("#broadcast_send_on").keyup(function(){
    if ( $("#broadcast_send_on").val() != null && $("#broadcast_send_on").val() != "" && moment( $("#broadcast_send_on").val() ).isValid() == true ) {
      $("#queue_broadcast_btn").prop("disabled", false);
    } else {
      $("#queue_broadcast_btn").prop("disabled", "disabled");
    };
  });

  // Test the spam score of a broadcast message.
  $("#broadcast_spam_test_btn").click(function(){
    var subj = $("#broadcast_subj_line").val();
    var content = $("#broadcast_message").val();
//
//    var test = Klickrr.spam_test(subj, content);

//    // Pop an alert about whether it was a success or failure.
//    Klickrr.Helpers.notify( test["notify_args"] );
//
//    // If we did a successful test, display the results.
//    if (test["success"] === true) {
//      $("#test_results").html("");
//
//      $("#test_results").html( test["test_results"] );
//    }
    var payload = { "subject": subj, "content": content };

    $.ajax({
      url: "mail/raw",
      type: "POST",
      data: payload,
      success: function(data){
        $("#test_results").html("");

        var notify_args = { icon: 'fa fa-envelope-o', title: 'Klickrr :: Broadcasts', message: 'Success! Raw email format retrieved.', type: 'success'};

        Klickrr.Helpers.notify(notify_args);

        $("#spam_score").text( data["score"] );

        var spam_issues = "<table><tr><thead><th>Points</th><th>Rule Name</th><th>Description</th></thead></tr>";
        $.each(data["report"].split("\n"), function(key, val){
          var cols = val.split(" ");

          //Drop the first element, as it is empty.
          cols.shift();

          if (isNaN(cols[0]) == true) {
            return;
          };

          var col_1 = cols.shift();
          var col_2 = cols.shift();
          var col_3 = cols.join(" ");

          spam_issues += '<tr><td>' + col_1 + '</td><td>' + col_2 + '</td><td>' + col_3 + '</td></tr>';
        });

        spam_issues += "</table>";

        $("#test_results").html( spam_issues );
      },
      error: function(data){
        var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'There was a problem. Email could not be retrieved.', type: 'danger'};

        Klickrr.Helpers.notify(notify_args);
      }
    });
  });

  $('#send_broadcast_btn, #save_as_new_and_send_broadcast_btn').click(function(){
    $('#broadcast_send').modal('show');
  });

  $('#save_as_draft_broadcast_btn').click(function(){
    $('input#broadcast_is_draft').val(true);
    $("#broadcast_send").modal("show");
  });

  $('#broadcast_send').on('hidden.bs.modal', function() {
    $('input#broadcast_is_draft').val(false)
  });

  $("#sel_broadcast_lists").change(function(){
    selected_list = $("#sel_broadcast_lists option:selected").val();
    updateBroadcastUi();
  });

  $("#broadcast_load_list").change(function(){
    selected_list = $("#broadcast_load_list").val();

    loadBroadcastDialogList();
    updateBroadcastUi();
  });

  $("#broadcast_load_message").change(function(){
    if ( $("#broadcast_load_message").val() != "No messages associated with the selected list..." && $("#broadcast_load_message").val() != "Select a message to load..." ) {
      $("#load_message_btn").attr("disabled", false);
    } else {
      $("#load_message_btn").attr("disabled", true);
    }
  });

  $("#clear_broadcast_btn").click(function(){
    $("#broadcast_subj_line").val("");
    $("#broadcast_message").val("");

    //$("#save_broadcast_btn").show();
    $(".save-broadcast").show();
    $("#queue_broadcast_btn").hide();
    $(".update-broadcast").hide();
    $("#clear_broadcast_btn").hide();
  });

  $("#send_test_email_btn").click(function(){
    if (selected_list == null)
    {
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'List wasn\'t selected ...', type: 'error'};
      Klickrr.Helpers.notify(notify_args);
    }
    else {

      var payload = set_test_payload_for_broadcast();
      data = $.extend({}, {list_id: selected_list}, payload);

      $.ajax({
          url: "/broadcast/send_test_message_from_broadcast",
          type: "POST",
          data: JSON.stringify(data),
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          },
          dataType: "json",
          contentType: "application/json",
          success: function(data){

            var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Test Broadcast message successfully sended.', type: 'success'};

            Klickrr.Helpers.notify(notify_args);

            // Set the id, based on return from the server.
          },
          error: function(){
            var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Error! Cannot send Test Broadcast message at this time.', type: 'danger'};

            Klickrr.Helpers.notify(notify_args);
          }
        });
      $('#mod_bc_split_test').modal('hide');
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Sending test message broadcast...', type: 'info'};

      Klickrr.Helpers.notify(notify_args);
    };


    $("#broadcast_test").modal("hide");
  });

  function set_test_payload_for_broadcast()
  {
    var title = $("#broadcast_subj_line").val();
    var doc = $('#cke_1_contents').find('iframe')[0].contentDocument
    var body = $(doc).find('body').html()
    var send_on = $("#broadcast_send_on").val();
    var tz_id = $("#scheduled_broadcast_timezone option:selected").attr("timeZoneId");
    var timezone = $("#scheduled_broadcast_timezone option:selected").val();
    var gmt_adjust = $("#scheduled_broadcast_timezone option:selected").val();
    var email = $("#broadcast_test").find('.modal-dialog .modal-body input').val();

    if ( title.length > 0 && body.length > 0 ) {
      var payload = {
        "title": title,
        "body": body,
        "followups": [],
        "send_on": moment(send_on).format("YYYY-MM-DD HH:mm"),
        "timezone": tz_id,
        "gmtAdjustment": gmt_adjust,
        "list_id": selected_list,
        "type": 'test_message_from_broadcast',
        "segment_ids": null,
        "email": email
      }
      // Check to see if there are any segments selected.
      if ( $("input[name='seg_id']").length > 0 ) {
        var seg_ids = [];

        $.each($("input[name='seg_id']"), function(key, val){
          seg_ids.push( $(val).val() );
        });

        payload["segment_ids"] = seg_ids;
      };
      return payload;
    } else {
      return null;
    }
  };

  // Handler for adding/removing a subject line.
  $("#broadcast_subj_line").change(function(){
      if ($(this).val() === "") {
          $("#bc_subj_required_alert").show();
      } else {
          $("#bc_subj_required_alert").hide();
      }

    toggleSaveReadiness('broadcast');
  });

  CKEDITOR.instances['broadcast_message'].on("change", function(){
    toggleSaveReadiness('broadcast');
  });

  // Show the "Manage Segments" dialog when button is clicked.
  $("#manage_segments_btn").click(function(){
    $("#broadcast_manage_segments").modal("show");
  });

    // Build the segments dialog.
  $("#show_segments_modal_btn").click(function(){
      showListSegmentationDialog(this, Klickrr.Broadcasts.current, 'Broadcast');
  });

    // Save the segment rules.
  $(document).on("click", "#save_segments_btn", function(){
    saveSegments(Klickrr.Broadcasts.current, 'Broadcast');
  });

  // Toggle segments active/disabled when buttons are clicked.
  $(".segment-type-chkbox").change(function(){
    if ( $(this).prop("checked") == true ) {
      $(this).parent().addClass("bg-success");

      // If there is a variable textbox, handle it.
      if ( $(this).siblings().length > 0 && $(this).siblings().val() == "" ) {
        $(".segment-type-value").prop("disabled", false);
        Klickrr.Broadcasts.Segments.active_var_count++;
      // } else {
      //   $(".segment-type-value").prop("disabled", false);
      //   Klickrr.Broadcasts.Segments.active_var_count--;
      };
    } else {
      $(this).parent().removeClass("bg-success");

      //
      if ( $(this).siblings().length > 0 ) {
        if ( Klickrr.Broadcasts.Segments.active_var_count != 0 ) {
          Klickrr.Broadcasts.Segments.active_var_count--;
        };

        $(this).siblings().val("");
        $(this).siblings().prop("disabled", false);
      };
    };

    // Check whether there are active vars that block saving.
    if ( Klickrr.Broadcasts.Segments.active_var_count > 0 ) {
      $("#save_segment_types_btn").prop("disabled", "disabled");
    } else {
      $("#save_segment_types_btn").prop("disabled", false);
    };
  });

  $(".segment-type-value").keyup(function(){
    if ( $(this).val() != "" ) {
      if ( Klickrr.Broadcasts.Segments.active_var_count != 0 ) {
        Klickrr.Broadcasts.Segments.active_var_count--;
      };

      // Check whether there are active vars that block saving.
      if ( Klickrr.Broadcasts.Segments.active_var_count > 0 ) {
        $("#save_segment_types_btn").prop("disabled", "disabled");
      } else {
        $("#save_segment_types_btn").prop("disabled", false);
      };
    };
  });

  // Check which segments are active, and generate SQL based on this.
  $("#save_segment_types_btn").click(function(){
    $("#broadcast_manage_segments").modal("hide");

    var selected = [];
    $.each( $(".segment-type-chkbox"), function(key, val){
      if ( $(val).prop("checked") == true ) {
        var desc = $(val).attr("data-desc");
        var seg_id = $(val).attr("data-id");
        var x = $(".segment-type-value.x[data-id='" + seg_id + "']").val();
        var y = $(".segment-type-value.y[data-id='" + seg_id + "']").val();

        // Add variables to the description on the broadcast page.
        desc = desc.replace("{{x}}", x);
        desc = desc.replace("{{y}}", y);

        // Build the "variables" for data-variables.
        var variables;

        if ( x != null && x != "" ) {
          variables = ",x:" + x;
        };

        if ( y != null && y != "" ) {
          variables = variables + ",y:" + y;
        };

        selected.push( '<tr><td>' + desc + '<input type="hidden" name="seg_id" value="id:' + seg_id + variables + '" /></td><td><button type="button" class="btn btn-default delete-selected-segment" data-id="' + seg_id + '"><i class="fa fa-trash-o"></i></button></td></tr>' );
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~~ Push the URLs to the server and save the
        //~~ full SQL statement??
        //~~ How should variables be handled.
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      };
    });

    $("#active_segments_tbl").html( selected );

    // Are segments selected? If yes, show the active segements table.
    if ( selected.length > 0 ) {
      $(".active-segments-tbl").css("display", "block");
    } else {
      $(".active-segments-tbl").css("display", "none");
    };
  });

  // Remove the selected segment.
  $("body").on("click", ".delete-selected-segment", function(){
    $(this).parent().parent().remove();

    // When a segment is removed, check and see if it's the last one.
    // If so, hide the segment display block.
    if ( $("input[name='seg_id']").length > 0 ) {
      $(".active-segments-tbl").css("display", "block");
    } else {
      $(".active-segments-tbl").css("display", "none");
    };
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //~~ Alert the server that this segment should be
    //~~ removed.
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  });

  function dataTestBroadcast(type)
  {
    var title = $("#test_bc_subj_line_" + type).val();

    var body = $("#test_bc_message_" + type).next('.note-editor').find('.note-editable.panel-body').html();

    var send_on = $("#broadcast_send_on").val();
    var tz_id = $("#scheduled_broadcast_timezone option:selected").attr("timeZoneId");
    var timezone = $("#scheduled_broadcast_timezone option:selected").val();
    var gmt_adjust = $("#scheduled_broadcast_timezone option:selected").val();

    if ( title.length > 0 && body.length > 0  ) {
      var payload = {
        "title": title,
        "body": body,
        "followups": [],
        "send_on": moment(send_on).format("YYYY-MM-DD HH:mm"),
        "timezone": tz_id,
        "gmtAdjustment": gmt_adjust,
        "list_id": selected_list,
        "type": type,
        "segment_ids": null
      }

      if (!$.isEmptyObject(Klickrr.Broadcasts.tempInstance)) {
        payload.services = Klickrr.Broadcasts.tempInstance.services;
      }

      // Check to see if there are any segments selected.
      if ( $("input[name='seg_id']").length > 0 ) {
        var seg_ids = [];

        $.each($("input[name='seg_id']"), function(key, val){
          seg_ids.push( $(val).val() );
        });

        payload["segment_ids"] = seg_ids;
      };
      return payload;
    } else {
      return null;
    }
  };

  function saveTestBroadcast(payload)
  {
      if (payload !== null ) {

      // Push the broadcast to the server.
      $.ajax({
        url: "/broadcast/save_test_message",
        //url: "https://www.klickrr.net/webform/save",
        type: "POST",
        data: JSON.stringify(payload),
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        dataType: "json",
        contentType: "application/json",
        success: function(data){

          var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Test Broadcast message successfully saved.', type: 'success'};

          Klickrr.Helpers.notify(notify_args);

          // Set the id, based on return from the server.
        },
        error: function(){
          var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Error! Cannot save Test Broadcast message at this time.', type: 'danger'};

          Klickrr.Helpers.notify(notify_args);
        }
      });

      // Copy the broadcast into the Payload.

    } else {
      var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Error! Please provide Test Broadcast message subject and content.', type: 'danger'};

      Klickrr.Helpers.notify(notify_args);
    };
  };


  $('body').on('click', '[id^="save_test_message_"]', function(){
    if (selected_list == null)
    {
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'List wasn\'t selected ...', type: 'error'};
      Klickrr.Helpers.notify(notify_args);
    }
    else {
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Saving test message broadcast...', type: 'info'};
      Klickrr.Helpers.notify(notify_args);
      var type = $(this).attr('data-message-number');
      var payload = dataTestBroadcast(type);
      saveTestBroadcast(payload);
    };
  })

  $('body').on('click', 'a[id^="edit_broadcast_"]', function(){
    clearBroadcastUI();
    data = $(this).data();
    if (data.listId) {
      $('#master_lists_sel').val(data.listId);
      $('#master_lists_sel').trigger('change');
    }  
    editBroadcast(data.id);
    if (data.sent && !data.isDraft ){
      broadcastSaveableAsNew();
      $('#message-broadcast-sent').removeClass('hidden');
    } else {
      broadcastUpdateable();
      $('#message-broadcast-sent').addClass('hidden');
    }

    if(data.isDraft) {
      draftBroadcastButtons();
    }
    if(data.sent) {
      sentBroadcastButtons();
    }
  })

  function broadcastSaveableAsNew(){
    $('.update-broadcast').hide();
    $('.save-broadcast').show()
    $('#send_broadcast_btn').hide();
  }

  function broadcastUpdateable(){
    $('.update-broadcast').show();
    $('.save-broadcast').hide();
  }

  $('body').on('click', 'a[id^="view_broadcast_analitics_"]', function(){
    $('a[href="#analytics"').tab('show');
    if (!$('#analytics_bc_btn').hasClass('btn-info')){
      $('#analytics_bc_btn').trigger('click');
    };
    $('#master_lists_sel').val($(this).attr('data-list-id'));
    var id = $(this).attr('data-id');
    $('#master_lists_sel').trigger('change');

    $('#analytics_broadcasts').on('loadItems', function(){
      $('a[data-id="' + id + '"][id^="analytics_broadcast_"]').trigger('click');
    })
  })

  $('body').on('click', 'a[id^="view_broadcast_webforms_"]', function(){
    $('a[href="#web_form"').tab('show');
    $('#master_lists_sel').val($(this).attr('data-list-id'));
    $('#master_lists_sel').trigger('change');
    var id = $(this).attr('data-list-id');
    $('#load_form_name_btn').trigger('click');
    $('#webform_form_list').on('loadItems', function(){
      $(this).val($(this).find('option[data-list-id="' + id + '"]').val());
    })
  })

  $('body').on('click', 'a[id^="test_broadcast_"]', function(){
    $('#master_lists_sel').val($(this).attr('data-list-id'));
    $('#master_lists_sel').trigger('change');
    loadSelectedBroadcast($(this).attr('data-id'));
    $("#broadcast_spam_test_btn").trigger('click');
  })

  $('body').on('click', 'a[id^="clone_broadcast_"]', function(){
    cloneBroadcast($(this).attr('data-id'), $(this).attr('data-list-id'));
  })

  $('body').on('click', 'a[id^="delete_broadcast_"]', function(){
    if (confirm('Are you sure to delete this broadcast?')) {
      var data = {id: $(this).attr('data-id')};
      $.ajax({
        url: "/broadcast/delete/" + $(this).attr('data-id'),
        type: "DELETE",
        dataType: "json",
        success: function(data){
          var notify_args = { icon: 'fa fa-files-o', title: 'Klickrr :: Broadcasts', message: 'Delete successful.', type: 'success'};
          $('#broadcast_page').trigger("page", [Klickrr.broadcastPagination.page]);
          Klickrr.Helpers.notify(notify_args);
        },
        error: function(){
          var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Unable to delete the broadcast.', type: 'danger'};
          Klickrr.Helpers.notify(notify_args);
        }
      });
    }
  })

  $('body').on('click', 'a[href="#broadcast"]', function() {
    clearBroadcastUI();
    broadcastUpdateable();
    Klickrr.Broadcasts.SegmentsCollection = null;
    Klickrr.Broadcasts.current = null;
    selected_broadcast = null;
  });

  broadcastTotalPages();

  $('#broadcast_page').on("page", function(event, num){
    Klickrr.broadcastPagination.page = num;
    $.ajax({
      url: "/broadcast/list",
      type: "GET",
      data: Klickrr.broadcastPagination,
      dataType: "html",
      success: function(data){
        $('#broadcast_manager').html(data);
        broadcastTotalPages();
      }
    });
  });

  function broadcastTotalPages(){
    $.ajax({
          url: "/broadcast/total_pages",
          data: Klickrr.broadcastPagination,
          type: "GET",
          dataType: "json",
          success: function(data){
            $('#broadcast_page').bootpag({
              total: Math.max.apply(null, [1, data.total_pages]),
              maxVisible: 10
            })
          }
        })
  }

  $('body').on('click', '#broadcast_page_size a', function(){
    Klickrr.broadcastPagination.page_size = $(this).attr('data-page-size');
    $('#broadcast_page').trigger('page', [1]);
    $('#broadcast_page_size li').removeClass('active');
    $('#broadcast_page_size a[data-page-size="' + Klickrr.broadcastPagination.page_size + '"]').parent().addClass('active');
  })

  // Update the order of the broadcast list, based on the column header clicked.
  $('body').on('click', 'a.broadcast_order', function(){
    Klickrr.broadcastPagination.order = $(this).attr('data-field') + ' ' + $(this).attr('data-order-status');
    $('#broadcast_page').trigger('page', [Klickrr.broadcastPagination.page]);
  })

  $('a[href="#broadcast_list"]').on('shown.bs.tab', function(){
    // updateListContent();
    $('#broadcast_page').trigger('page', [Klickrr.broadcastPagination.page]);
  })

  $('body').on('click', '#fix_broadcast_split_test', function(){
    $('label[for="broadcast_subj_line"]').text('Message A Subject Line ');
    $('label[for="broadcast_message"]').text('Message A Main message ');
    $('#broadcast_subj_line').val($('#test_bc_subj_line_b').val());
    $("#broadcast_message").val($("#test_bc_message_b").code());
    $('#split_test_ind').show();
    $('#broadcast').attr('data-split-test', true);
  })

  function sendBroadcastSplitTest(){
    if (selected_list == null)
    {
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'List wasn\'t selected ...', type: 'error'};
      Klickrr.Helpers.notify(notify_args);
    }
    else {

      var payloadA = getNewBroadcastValues();
      var payloadB = dataTestBroadcast('b');
      data = {list_id: selected_list, a: payloadA, b: payloadB};

      $.ajax({
          url: "/broadcast/send_test_message",
          type: "POST",
          data: JSON.stringify(data),
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          },
          dataType: "json",
          contentType: "application/json",
          success: function(data){

            var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Test Broadcast message successfully sended.', type: 'success'};
            $('#broadcast').removeAttr('data-split-test');
            Klickrr.Helpers.notify(notify_args);

            // Set the id, based on return from the server.
          },
          error: function(){
            var notify_args = { icon: 'fa fa-exclamation-triangle', title: 'Klickrr :: Broadcasts', message: 'Error! Cannot send Test Broadcast message at this time.', type: 'danger'};

            Klickrr.Helpers.notify(notify_args);
          }
        });
      $('#mod_bc_split_test').modal('hide');
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Sending test message broadcast...', type: 'info'};

      Klickrr.Helpers.notify(notify_args);
    };
  }

  $('body').on('click', '#save_test_message_2', function(){
    $('#mod_bc_split_test').modal('hide');
    $('#send_broadcast_btn').trigger('click');
  })

  $('body').on('click', '#remove_split_test', function(){
    $('#broadcast').removeAttr('data-split-test');
    $('#split_test_ind').hide();
    $('label[for="broadcast_subj_line"]').text('Subject Line ');
    $('label[for="broadcast_message"]').text('Main message ');
    $('#mod_bc_split_test').modal('hide');
  })

  function activateServiceButton(button) {
    button.addClass('active');
    var icon = button.find('i')
    icon.removeClass('fa-square-o');
    icon.addClass('fa-check-square-o');
  }

  function disableServiceButton(button) {
    button.removeClass('active');
    var icon = button.find('i')
    icon.addClass('fa-square-o');
    icon.removeClass('fa-check-square-o');
  }

  function updateServicesButtonDisabledState() {
    if(isNaN(parseInt(selected_list))) {
      $('#broadcast_edit_servers').prop('disabled', true);
    } else {
      $('#broadcast_edit_servers').prop('disabled', false);
    }
  }

  $('body').on('click', '#broadcast_edit_servers', function(){
    window.ActiveServices.showServices('broadcast', Klickrr.Broadcasts.currentInstance, Klickrr.Broadcasts.tempInstance);
  })

  $(document).on('shown.bs.tab', 'a[data-toggle="tab"][href="#broadcast"]', updateServicesButtonDisabledState);
  $(document).on('change', '#master_lists_sel', updateServicesButtonDisabledState);

  $(document).on('click', '.list-service .btn', function(e){
    var button = $(this);
    if (button.hasClass('active')) {
      activateServiceButton(button);
    } else {
      disableServiceButton(button);
    }
  });

  function clearScheduledState(){
    $('.schedule-info .scheduled-date').hide();
    $('.schedule-info .close').hide();
    $("#broadcast_schedule").slideUp("fast");
    $("#queue_broadcast_btn").hide();
    if (!isNaN(parseInt(Klickrr.Broadcasts.current))){
      $('.update-broadcast.btn').show();
    } else {
      $("#send_broadcast_btn").show();
    }

    newBroadcastButtons();
  }

  $(document).on('click', '#broadcast_schedule button.close', clearScheduledState);

  $(document).on('click', '#new-broadcast-link', clearScheduledState);

  $(document).on('click', '.schedule-info .close', function() {
    var broadcastId = Klickrr.Broadcasts.current;
    $.get('broadcasts/' + broadcastId + '/unschedule').success(function() {
      clearScheduledState();
      $('#broadcast_send_on').val('');
      var notify_args = { icon: 'fa fa-floppy-o', title: 'Klickrr :: Broadcasts', message: 'Broadcast has been unscheduled', type: 'info'};
      Klickrr.Helpers.notify(notify_args);
    }).error(function(){alert('error')});
  });

  $('body').on('click', ".confirm-broadcast-save-as-draft", function(){
    $("#broadcast_save_as_draft_modal").modal("hide");
    saveBroadcast(true);
  });

  $('body').on('click', "#broadcast_update_draft_btn", function(){
    if (Klickrr.Broadcasts.currentList && Klickrr.Broadcasts.currentList != selected_list) {
      $('#confirm_change_selected_list_modal').modal('show');
    } else {
      updateBroadcastWithoutSend(true);
      updateBroadcastUi();
      // updateUI();
    }
  });

  $('body').on('keyup', "#broadcast_subj_line", function(){
    if ($(this).val() == '') {
      $('#bc_subj_required_alert').show();
    } else {
      $('#bc_subj_required_alert').hide();
    }
  })

  CKEDITOR.instances['broadcast_message'].on('change', function(){
    if ($("#broadcast_message").val() == '') {
      $('#main_message_is_empty').show();
    } else {
      $('#main_message_is_empty').hide();
    }
  });

  $('body').on('click', '#cancel_change_selected_list', function(){
    $('#master_lists_sel').val(Klickrr.Broadcasts.currentList);
  })

  $('body').on('click', '#confirm_change_selected_list', function(){
    $('#confirm_change_selected_list_modal').modal('hide');
    var editedBroadcast;
    var newList;
    $.each(lists, function(index, item){
      if (Klickrr.Broadcasts.currentList == item.list_id){
        var oldIndex = $.grep(item.broadcasts, function(broadcast){ return broadcast.id == selected_broadcast })[0];
        editedBroadcast = item.broadcasts.splice(oldIndex, 1)[0];
      }
      if (selected_list == item.list_id){
        newList = item;
      }
    })
    newList.broadcasts.push(editedBroadcast);
    updateBroadcastWithoutSend(true);
    updateBroadcastUi();
    // updateUI();
  })

  $('body').on('click', '#clear_editing_broadcast', function(){
    clearBroadcastUI();
    Klickrr.Broadcasts.current = null;
    selected_broadcast = null;
  })


  /*$("#load_message_btn").click(function(){
    selected = $("#broadcast_load_list").val();
    broadcast = $("#broadcast_load_message").val();
    list = [];
    broadcasts = [];

    list = $.grep(lists, function(l){ return l.list_id == selected });
    broadcasts = $.grep(list[0].broadcasts, function(b){ return b.id == broadcast });

    $("#broadcast_subj_line").val( broadcasts[0].title );
    $("#broadcast_message").val( broadcasts[0].body );

    $("#broadcast_load").modal("hide");
  });*/
  // End Broadcast button events.
});
