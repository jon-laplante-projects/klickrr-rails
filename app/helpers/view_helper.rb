module ViewHelper

  def order_icon_class(order, field)
    if order.include? field
      order.include?('DESC') ? 'fa fa-angle-up' : 'fa fa-angle-down'
    end
  end

  def order_desc(order, field)
    order.include?(field) && order.include?('DESC') ? '' : 'DESC'
  end

  def sortable_th(opts)
    content_tag :th do
      a_attrs = {
        class: opts[:class],
        href: '#',
        data: {
          field: opts[:field],
          'order-status' => order_desc(@order, opts[:field])
        }
      }
      content = content_tag :a, opts[:text], a_attrs
      content += content_tag :i, '', class: order_icon_class(@order, opts[:field])
      content
    end
    .html_safe
  end

end
