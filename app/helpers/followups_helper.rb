module FollowupsHelper

  def followup_sortable_th(opts)
    sortable_th(opts.merge(class: 'followup_order'))
  end

end
