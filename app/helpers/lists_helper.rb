module ListsHelper

  def list_sortable_th(opts)
    sortable_th(opts.merge(class: 'lists_order'))
  end

end
