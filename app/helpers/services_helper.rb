module ServicesHelper

  def service_label_with_icon(services_user, opts = {})
    icon_class = services_user.used? ? 'fa-check-square-o' : 'fa-square-o'
    opts[:class] ||= ['fa', 'icon-check']
    opts[:class] << icon_class
    "#{content_tag(:i, '', opts)}&nbsp;#{services_user.service.label}".html_safe
  end
end
