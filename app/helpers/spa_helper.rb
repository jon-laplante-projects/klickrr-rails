module SpaHelper
  def leads_stats(list_id)
    stats = {}

    stats["today"] = {}
    stats["7days"] = {}
    stats["30days"] = {}
    stats["90days"] = {}
    stats["year"] = {}
    stats["alltime"] = {}

    list_ids = nil

    # Get the list(s) to display data for.
    if list_id.blank?
      list_ids = current_user.lists.all.pluck(:id).join(", ")
      list_ids = 'NULL' if list_ids.blank?
    else
      list_ids = list_id
    end

    unless list_ids.blank?
      # Define the vars we are about to populate.
      creation_date = nil
      updated_date = nil

      stats.each do |key, data|
        case key
          when "today"
            creation_date = "created_at::DATE = CURRENT_DATE"
            updated_date = "updated_at::DATE = CURRENT_DATE"
            interval = Time.now
          when "7days"
            creation_date = "created_at::DATE BETWEEN CURRENT_DATE - interval '7 days' AND CURRENT_DATE"
            updated_date = "updated_at::DATE BETWEEN CURRENT_DATE - interval '7 days' AND CURRENT_DATE"
            interval = Time.now - 7.days
          when "30days"
            creation_date = "created_at::DATE BETWEEN CURRENT_DATE - interval '30 days' AND CURRENT_DATE"
            updated_date = "updated_at::DATE BETWEEN CURRENT_DATE - interval '30 days' AND CURRENT_DATE"
            interval = Time.now - 30.days
          when "90days"
            creation_date = "created_at::DATE BETWEEN CURRENT_DATE - interval '90 days' AND CURRENT_DATE"
            updated_date = "updated_at::DATE BETWEEN CURRENT_DATE - interval '90 days' AND CURRENT_DATE"
            interval = Time.now - 90.days
          when "year"
            creation_date = "created_at::DATE BETWEEN CURRENT_DATE - interval '1 year' AND CURRENT_DATE"
            updated_date = "updated_at::DATE BETWEEN CURRENT_DATE - interval '1 year' AND CURRENT_DATE"
            interval = Time.now - 1.year
        end

        unless key === "alltime"
          # Since the SQL string contains single ticks, we need to escape our single tick creation_date.
          new_leads_creation_date = creation_date.gsub("'", "''")

          stats["#{key}"]["new_leads"] = List.find_by_sql("SELECT COUNT(set_id) AS rows from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN (SELECT * FROM list_values WHERE #{new_leads_creation_date}) AS list_values_table ON (list_values_table.list_field_id = list_fields.id) WHERE list_id IN (#{list_ids}) ORDER BY list_values_table.id LIMIT 50') AS ct(set_id int, first_name text, last_name text, email text);")[0].rows;
        else
          stats["#{key}"]["new_leads"] = List.find_by_sql("SELECT COUNT(set_id) AS rows from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN (SELECT * FROM list_values) AS list_values_table ON (list_values_table.list_field_id = list_fields.id) WHERE list_id IN (#{list_ids}) ORDER BY list_values_table.id LIMIT 50') AS ct(set_id int, first_name text, last_name text, email text);")[0].rows;
        end

        ####################################
        ## Unsubscribes
        ####################################
        unsub_count = 0
        # byebug
        unless current_user.sent_mails.blank?
          unsubs = current_user.sent_mails.where(["#{updated_date} AND unsubscribed IS NOT NULL"]).pluck(:unsubscribed)

          unsubs.each do |unsub|
            # Unsubscribed should never be null, but just in case...
            unless unsub.blank?
              if key === "today"
                if unsub.to_datetime.strftime('%Y-%m-%d') === Date.today
                  unsub_count += 1
                end
              elsif key === "alltime"
                unsub_count += 1
              else
                if unsub.to_datetime.strftime('%Y-%m-%d') >= interval
                  unsub_count += 1
                end
              end
            end
          end
        end

        stats["#{key}"]["unsubscribes"] = unsub_count
        ####################################
        ## End unsubscribes
        ####################################


        ####################################
        ## Opens
        ####################################
        opens = current_user.sent_mails.where(["#{updated_date} AND opened IS NOT NULL"]).pluck(:opened)
        open_count = 0
        opens.each do |o|
          # Open should never be null, but just in case...
          unless o.blank?
            if (o[0] === "[" && o[6..7] === "=>") || o[2] === ":"
              o = eval(o)
            elsif o[0] === "{" || (o[0..1] === "[{" && o[7] === ":")
              o = JSON.parse(o)
            end

            # Check to see if this is just a date string or array of hashes (legacy support).
            unless o.class === "String"
              opened_at = o[0][:opened_at]
            else
              opened_at = o
            end

            if key === "today"
              if opened_at.to_datetime.strftime('%Y-%m-%d') === Date.today
                open_count += 1
              end
            elsif key === "alltime"
              open_count += 1
            else
              unless opened_at.blank?
                if opened_at.to_datetime.strftime('%Y-%m-%d') >= interval
                  open_count += 1
                end
              end
            end
          end
        end

        stats["#{key}"]["opens"] = open_count
        ####################################
        ## End Opens
        ####################################

        ####################################
        ## Clicks
        ####################################
        clicks = current_user.sent_mails.where(["#{updated_date} AND clicks IS NOT NULL"]).pluck(:clicks)
        click_count = 0

        clicks.each do |click|
          # Clicks should never be null, but just in case...
          unless click.blank?
            if (click[0] === "[" && click[6..7] === "=>") || click[2] === ":"
              click_arr = eval(click)
            elsif click[0] === "{" || (click[0..1] === "[{" && click[7] === ":")
              click_arr = JSON.parse(click)
            end

            # In case there are multiple clicks for this, iterate through all.
            click_arr.each do |clk|
              # Check if we have a click date (legacy support)
              if (clk.include? "clicked_at")# || (clk.has_key?(:clicked_at)) || (clk.has_key?("clicked_at"))
                # Check to see if this is just a date string or array of hashes (legacy support).
                unless clk.is_a? String

                  puts "clk: #{clk}"
                  if clk.is_a? Hash
                    clicked_at = if clk.keys[0].is_a?(Symbol)
                                   clk[:clicked_at]
                                 else
                                   clk["clicked_at"]
                                 end
                    #end
                  else#if clk.is_a? Array
                    clicked_at = clk[1]
                  end
                else
                  clicked_at = clk
                end

                if key === "today"
                  if clicked_at.to_datetime.strftime('%Y-%m-%d') === Date.today
                    click_count += 1
                  end
                elsif key === "alltime"
                  click_count += 1
                else
                  unless clicked_at.blank?
                    if clicked_at.to_datetime.strftime('%Y-%m-%d') >= interval.to_datetime.strftime('%Y-%m-%d')
                      click_count += 1
                    end
                  end
                end
              end
            end
          end
        end

        stats["#{key}"]["clicks"] = click_count
        ####################################
        ## End clicks
        ####################################
      end

      # Get the total number of leads
      stats["total_leads"] = List.find_by_sql("SELECT COUNT(DISTINCT set_id) AS row_count FROM list_values WHERE list_field_group_id IN (SELECT ID FROM list_field_groups WHERE list_id IN (#{list_ids}))")[0].row_count;

    end

    return stats
  end
end
