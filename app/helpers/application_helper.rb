module ApplicationHelper
	def bootstrap_class_for flash_type
		case flash_type
		  when :success
		    "alert-success" # Green
		  when :error
		    "alert-danger" # Red
		  when :alert
		    "alert-warning" # Yellow
		  when :notice
		    "alert-info" # Blue
		  else
		    flash_type.to_s
		end
	end

	# Access devise resources anywhere. Will break multi-tier (i.e. company) logins, however.
	def resource_name
		:user
	end

	def resource
		unless current_user.blank?
			current_user
		else
			User.new
		end
		#@resource ||= User.new
	end

	def devise_mapping
		@devise_mapping ||= Devise.mappings[:user]
	end

	def read_vars(vars)
		eval vars 
	rescue Exception => exc
		JSON.parse vars	
	end
end