json.lists @list_ids.each do |list_id|
  json.id list_id

  json.label List.select(:title).find(list_id).title

  json.fields ListField.select(:id, :label, :name, :is_email).where(list_id: list_id).each do |field|
    json.id field.id
    json.label field.label
    json.name field.label
    json.is_email field.is_email
  end
end