json.followups @followups.find_each do |followup|
  json.id followup.id
  json.title followup.subject
  json.body followup.content
  json.active followup.active
  json.messageParts followup.followup_message_parts.find_each do |followup_message|
    json.subject followup_message.subject
  end
end
