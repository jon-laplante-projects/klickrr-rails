# Forms.
json.forms @list.list_field_groups.find_each do |form|
  json.name form.label
  json.fields form.list_fields.find_each do |field|
    json.cid field.cid
    json.label field.label
    json.field_type field.field_type
    json.required field.required
    json.field_options field.field_options
    json.description field.description
    json.include_other field.include_other
    json.include_blank field.include_blank
    json.integer_only field.integer_only
    json.min field.min
    json.max field.max
  end
end
