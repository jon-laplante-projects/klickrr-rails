json.lists @lists.each do |list|
  json.list_id list.id
  json.title list.title
  json.domain list.domain
  json.sender_email list.sender_email
  json.sender_name list.sender
  unless list.services.blank?
    json.active_services Service.where(id: list.services.split(',')).pluck(:label).join(", ")
    json.active_service_ids list.services
  else
    json.active_services ""
    json.active_service_ids ""
  end
end  