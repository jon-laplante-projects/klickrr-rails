json.followup
  json.id @followup.id
  json.active @followup.active
  json.active_trigger @followup.trigger_id
  json.message_id @followup.message_id
  json.created_at @followup.created_at
  json.updated_at @followup.updated_at
  json.scope_id
  json.active_trigger_variables

  json.active_message {
    json.active_scope @followup.scope_id
    json.active_type @followup.typ

  json.message_parts: FollowupMessagePart
    {
      position: 0,
      interval: null,
      subject: null,
      body: null
    }
end