json.broadcasts @broadcasts.each do |broadcast|
  json.id broadcast.id
  json.title broadcast.subject
  json.sent_on broadcast.sent
end  