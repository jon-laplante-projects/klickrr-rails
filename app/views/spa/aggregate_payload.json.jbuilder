json.lists @lists.each do |list|
  json.list_id list.id
  json.title list.title
  json.domain list.domain
  json.sender_email list.sender_email
  json.sender_name list.sender

  # Forms.
  json.forms list.list_field_groups.each do |form|
    json.name form.label
    json.fields form.list_fields.each do |field|
      json.cid field.cid
      json.label field.label
      json.field_type field.field_type
      json.required field.required
      json.field_options field.field_options
      json.description field.description
      json.include_other field.include_other
      json.include_blank field.include_blank
      json.integer_only field.integer_only
      json.min field.min
      json.max field.max
    end
  end

  # List Fields
  json.listFields list.list_fields

  unless list.services.blank?
    # Get the list services.
    service_ids = Service.where("id IN (#{list.services})").pluck(:label)
    json.active_services service_ids.join(", ")
    json.active_service_ids list.services
  else
    json.active_services ""
    json.active_service_ids ""
  end

  if list.list_fields.length > 0
    json.subCount @subscribers[list.id]
  else
    json.subCount 0
  end

  # Broadcasts.
  # broadcasts = Broadcast.find_by_sql("SELECT *, segments_count FROM broadcasts LEFT OUTER JOIN (SELECT COUNT(message_id) AS segments_count, message_id FROM segments GROUP BY message_id) AS s ON s.message_id = broadcasts.id WHERE list_id = (#{list.id});")

  # json.broadcasts broadcasts.each do |broadcast|
  json.broadcasts list.broadcasts.each do |broadcast|
    json.id broadcast.id
    json.title broadcast.subject
    json.body broadcast.content
    json.send_on broadcast.push_at
    json.sent_on broadcast.sent
    json.timezone broadcast.timezone
    json.gmtAdjustment broadcast.gmtAdjustment
    json.testType broadcast.test_type
    json.testNeighborId broadcast.test_neighbor_id
    json.isDraft broadcast.is_draft
    json.hasSegments broadcast.segments.length > 0

    # click_count = 0
    # opens = 0
    # broadcast.sent_mails.each do |sm|
    #   if sm.clicks.present?
    #     click_count += sm.parse_clicks.count
    #   end
    #   opens += 1 unless sm.opened.present?
    # end
    #
    # # Calculate click through rate.
    # unless click_count === 0 || opens === 0
    #   ctr = (click_count / opens ) * 100
    # else
    #   ctr = "0%"
    # end

    # json.opens opens
    # json.clicks click_count
    # json.ctr ctr
    # json.sends broadcast.sent_mails.length
  end

  # Followups.
  followups = list.followups
    json.followups followups.each do |followup|
    json.id followup.id
    json.title followup.subject
    json.body followup.content
    json.active followup.active
    json.scopeId followup.scope_id
    json.delayDays followup.delay_days
    json.triggerId followup.trigger_id
    json.type followup.type.try(:name)
    json.message_type followup.scope.try(:name)
    json.variables followup.trigger_variables.present? ? followup.parse_trigger_variables : nil
    json.messageParts followup.followup_message_parts.map do |followup_message|
      json.body followup_message.message
      json.interval followup_message.delay_days
      json.subject followup_message.subject
      json.id followup_message.id
    end
  end

end

#drafts
json.drafts @drafts.each do |broadcast|
  json.id broadcast.id
  json.title broadcast.subject
  json.body broadcast.content
  json.send_on broadcast.push_at
  json.sent_on broadcast.sent
  json.timezone broadcast.timezone
  json.gmtAdjustment broadcast.gmtAdjustment
  json.testType broadcast.test_type
  json.testNeighborId broadcast.test_neighbor_id
  json.isDraft broadcast.is_draft
end
