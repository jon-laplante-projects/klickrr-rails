json.id @broadcast.id
json.title @broadcast.subject
json.body @broadcast.content
json.send_on @broadcast.push_at
json.sent_on @broadcast.sent
json.timezone @broadcast.timezone
json.gmtAdjustment @broadcast.gmtAdjustment
json.testType @broadcast.test_type
json.testNeighborId @broadcast.test_neighbor_id
json.isDraft @broadcast.is_draft
json.hasSegments @broadcast.segments.length > 0
json.services @broadcast.services.try(:split, ',')
json.servicesLabels @broadcast.services_instances.pluck(:label).join(',') if @broadcast.services.present?