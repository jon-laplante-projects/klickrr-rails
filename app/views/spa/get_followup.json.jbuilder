json.id @followup.id
json.title @followup.subject
json.body @followup.content
json.active @followup.active
json.scopeId @followup.scope_id
json.delayDays @followup.delay_days
json.triggerId @followup.trigger_id
json.type @followup.type.try(:name)
json.message_type @followup.scope.try(:name)
json.messageId @followup.message_id
json.variables @followup.trigger_variables.present? ? @followup.parse_trigger_variables : nil
json.messageParts @followup.followup_message_parts.map do |followup_message|
  json.body followup_message.message
  json.interval followup_message.delay_days
  json.subject followup_message.subject
  json.id followup_message.id
end
json.currentInstance do
  json.id @followup.id	
  json.services @followup.services.try(:split, ',')
  json.servicesLabels @followup.services_instances.pluck(:label).join(',') if @followup.services.present?
end	