json.scopes @scopes.each do |scope|
  json.label scope.label
  json.name scope.name
  json.id scope.id

  idx = 0
  json.triggers scope.followup_triggers.each do |trigger|
    json.id trigger.id

    desc = "<input type=\"radio\" name=\"followup_trigger\" id=\"trigger_#{trigger.id}\" data-trigger-id=\"#{trigger.id}\" #{ 'checked ' if idx === 0 }/>&nbsp;#{trigger.descr}"
    desc = desc.gsub("{{x}}", "<input type=\"text\" name=\"trigger_var_#{trigger.id}\" class=\"trigger-value scope-select view\ x\" />")
    desc = desc.gsub("{{y}}", "<input type=\"text\" name=\"trigger_var_#{trigger.id}\" class=\"trigger-value scope-select view\ y\" />")
    json.desc desc

    idx += 1
  end
end