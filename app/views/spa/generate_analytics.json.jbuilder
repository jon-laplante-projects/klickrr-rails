json.analytics @analytics.each do |metric|
  json.clicks metric.clicks
  json.opened metric.opened
  json.dropped metric.dropped
  json.delivered metric.delivered
  json.bounced metric.bounced
end