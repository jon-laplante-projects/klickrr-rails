ruby "2.1.8"
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'

# Use SCSS for stylesheets
gem 'sass-rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
#gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
#gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  gem 'pry-rails'
  gem 'guard'
  gem 'guard-shell'
  gem 'guard-livereload'
  #gem 'did_you_mean'
  gem 'better_errors'
  gem 'byebug'
  gem 'active_record_query_trace'
  #gem 'debugger'
  gem 'quiet_assets'
  gem 'mailcatcher'
  gem "binding_of_caller" # Provides a REPL console in the browser.
end

# gem 'rails_12factor', group: :production
gem 'pg'
gem 'json'
# gem 'bootstrap-sass', '~> 3.3.6'
# gem 'font-awesome-rails'
gem 'devise' # Authorization.
gem 'cancancan' # Rights management/ACL.
gem 'nokogiri' # XML/HTML parsing.
#gem 'ckeditor_rails' # Rails-friendly inclusion of CKEditor into the asset pipeline.
gem 'letter_opener'
gem 'rack-cors', :require => 'rack/cors'
gem 'activeadmin', '~> 1.0.0.pre2'
gem 'httparty' # HTTP calls simplified.
gem 'rest-client'
#gem 'render_csv'
gem "schema_plus_views" # Facilitates creation of views via ActiveRecord migration.
# gem 'chartjs-ror' # Ruby on Rails interface for Chart.js.
gem 'chartkick' # Ruby interface for Google Charts.
gem 'rubysl-resolv' # For resolving DNS conflicts.
gem 'geoip' # IP lookup for geolocation data.
gem 'daemons'
# gem 'delayed_job_active_record'

# Gems for connection to transactional APIs.
gem 'sendgrid-ruby'
gem 'aws-ses'
gem 'mandrill-api', require: 'mandrill'
gem 'mailgun-ruby', require: 'mailgun'
gem 'mailjet'
#gem 'postmark'
gem 'postageapp'
gem 'socketlabs'
gem 'twilio-ruby'

#####################################
## Spam score checker.
#####################################
gem 'postmark_spamcheck'

#####################################
## SMTP mail delivery.
#####################################
gem 'mail'

# for cron job
gem 'whenever', :require => false

gem 'browser'
gem 'geocoder'
gem "http"
gem 'kaminari'
gem 'bootstrap-kaminari-views'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
gem 'slim-rails'
gem 'RubySpamAssassin', '~> 1.0', '>= 1.0.2'
gem 'gon', '~> 6.0', '>= 6.0.1'
gem 'valid_email'
gem 'aws-sdk', '~> 2'
gem 'formtastic'

#######################################
## This is running on Passenger!!!
##
## Stopgap for what appears to be
## a memory leak.
# gem 'unicorn-worker-killer'
#######################################
gem 'unicorn-worker-killer'
#######################################
